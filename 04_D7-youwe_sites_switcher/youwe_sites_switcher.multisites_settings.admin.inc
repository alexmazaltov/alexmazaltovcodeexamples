<?php
/**
 * Form with buttons to add one more and remove
 */
function youwe_sites_switcher_multisites_settings($form, &$form_state) {
  $sites_lang = variable_get('youwe_sites_switcher_sites_lang', range(0,4));

  $form['title'] = [
    '#type' => 'item',
    '#title' => t('Existing sites'),
  ];

  $form['field_container'] = [
    '#type' => 'container',
    '#tree' => TRUE,
    // Set up the wrapper so that AJAX will be able to replace the fieldset.
    '#prefix' => '<div id="js-ajax-elements-wrapper">',
    '#suffix' => '</div>',
  ];

  $form_state['field_deltas'] = isset($form_state['field_deltas']) ? $form_state['field_deltas'] : $sites_lang;

  $field_count = $form_state['field_deltas'];
  foreach ($field_count as $delta => $each_site) {
    $form['field_container'][$delta] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['container-inline'],
      ],
      '#tree' => TRUE,
    ];


    $form['field_container'][$delta]['enabled'] = [
      '#type' => 'checkbox',
      '#default_value' => isset($each_site['enabled'])?$each_site['enabled']:TRUE,
    ];

    $form['field_container'][$delta]['title'] = [
      '#type' => 'textfield',
      '#title' => t('Country Name'),
      '#default_value' => isset($each_site['title'])?$each_site['title']:$sites_lang['title'],
      '#size' => 10,
    ];

    $form['field_container'][$delta]['url'] = [
      '#type' => 'textfield',
      '#title' => t('Url'),
      '#default_value' => isset($each_site['url'])?$each_site['url']:$sites_lang['url'],
      //'#element_validate' => array('youwe_sites_switcher_url_validate'),
      '#size' => 20,
    ];

    $form['field_container'][$delta]['flag'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => [
        'us' => t('United States'),
        'au' => t('Australia'),
        'at' => t('Austria'),
        'fr' => t('France'),
        'de' => t('Germany'),
        'my' => t('Malaysia'),
        'nz' => t('New Zealand'),
        'sg' => t('Singapore'),
        'gb' => t('United Kingdom'),
        'nl' => t('Netherlands'),
        'be' => t('Belgium'),
        'ch' => t('ch'),
        'wales' => t('wales'),
        'im' => t('im'),
        'scott' => t('scott'),
        'mx' => t('mx'),
        'northanirld' => t('northanirld'),
        'se' => t('Swedish'),
        'no' => t('Norway'),
        'it' => t('Italy'),
        'fi' => t('Finlandia'),
        'dm' => t('Denmark'),
        'sp' => t('Spain'),
      ],
      '#default_value' => isset($each_site['flag'])?$each_site['flag']:($sites_lang['flag']?$sites_lang['flag']:'us'),
      '#title' => t('Flag'),
    ];

    $form['field_container'][$delta]['remove_site'] = [
      '#type' => 'submit',
      '#value' => t('-'),
      '#submit' => ['youwe_sites_switcher_multisites_settings_remove'],
      // See the examples in ajax_example.module for more details on the
      // properties of #ajax.
      '#ajax' => [
        'callback' => 'youwe_sites_switcher_multisites_settings_remove_callback',
        'wrapper' => 'js-ajax-elements-wrapper',
      ],
      '#weight' => -50,
      '#attributes' => [
        'class' => ['button-small'],
      ],
      '#name' => 'remove_name_' . $delta,
    ];
  }


  $form['field_container']['add_name'] = [
    '#type' => 'submit',
    '#value' => t('Add one more'),
    '#submit' => ['youwe_sites_switcher_multisites_settings_add_one'],
    // See the examples in ajax_example.module for more details on the
    // properties of #ajax.
    '#ajax' => [
      'callback' => 'youwe_sites_switcher_multisites_settings_add_one_callback',
      'wrapper' => 'js-ajax-elements-wrapper',
    ],
    '#weight' => 100,
  ];

  $form['default_site'] = [
    '#type' => 'select',
    '#required' => TRUE,
    '#options' => [
      'us' => t('United States'),
      'au' => t('Australia'),
      'at' => t('Austria'),
      'fr' => t('France'),
      'de' => t('Germany'),
      'my' => t('Malaysia'),
      'nz' => t('New Zealand'),
      'sg' => t('Singapore'),
      'gb' => t('United Kingdom'),
      'nl' => t('Netherlands'),
      'be' => t('Belgium'),
      'ch' => t('ch'),
      'wales' => t('wales'),
      'im' => t('im'),
      'scott' => t('scott'),
      'mx' => t('mx'),
      'northanirld' => t('northanirld'),
      'se' => t('Swedish'),
      'no' => t('Norway'),
      'it' => t('Italy'),
      'fi' => t('Finlandia'),
      'dm' => t('Denmark'),
      'sp' => t('Spain'),
    ],
    '#default_value' => variable_get('youwe_sites_switcher_sites_default_site', 'us'),
    '#title' => t('Current country'),
  ];

  $form['submit'] = [
    '#value' => t('Save'),
    '#type' => 'submit',
  ];

  return $form;
}


function youwe_sites_switcher_multisites_settings_remove($form, &$form_state) {
  $delta_remove = $form_state['triggering_element']['#parents'][1];
  $k = $delta_remove;//array_search($delta_remove, $form_state['field_deltas']);
  unset($form_state['field_deltas'][$k]);

  $form_state['rebuild'] = TRUE;
  drupal_get_messages();
}

function youwe_sites_switcher_multisites_settings_remove_callback($form, &$form_state) {
  return $form['field_container'];
}

function youwe_sites_switcher_multisites_settings_add_one($form, &$form_state) {
  $form_state['field_deltas'][] = ['enabled' => false,  'title' => '',  'url' => ''];

  $form_state['rebuild'] = TRUE;
  drupal_get_messages();
}

function youwe_sites_switcher_multisites_settings_add_one_callback($form, $form_state) {
  return $form['field_container'];
}

/**
 * Custom submit function for the team_page_settings form
 */
function youwe_sites_switcher_multisites_settings_submit($form, &$form_state) {
  $v = &$form_state['values'];
  variable_set('youwe_sites_switcher_sites_lang', $v['field_container']);
  variable_set('youwe_sites_switcher_sites_default_site', $v['default_site']);
}