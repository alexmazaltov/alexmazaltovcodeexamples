<?php
/**
 * @file Template for render block sites switcher in footer
 * Available variables:
 * - $sites_lang: All added sites
 * - $default_site: Default sites
 */
?>
<div class="language-selector row">
  <div class="col-md-offset-7 col-md-3" id="country-selector">
    <h3 class="widget-title second-title">
      <span><?php print t('Our Sites'); ?></span>
    </h3>
    <a href="javascript:void(0)" class="site selected">
      <span class="flag flag-<?php print $default_site;?>"></span>
      <?php foreach($sites_lang as $site){
        if($site['flag'] == $default_site){
          print $site['title'];
        }
      }?>
      <span class="dwn-arw"></span>
    </a>
    <div class="all-sites" style="display: none;">
      <ul>
        <?php foreach($sites_lang as $site):?>
          <?php if(isset($site['enabled']) && $site['enabled']  && ($site['flag'] != $default_site)):?>
            <li>
              <a href="http://<?php  print $site['url'];?>" class="site">
                <span class="flag flag-<?php print $site['flag'];?>"></span>
                <?php print $site['title'];?>
              </a>
            </li>
          <?php endif;?>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</div>