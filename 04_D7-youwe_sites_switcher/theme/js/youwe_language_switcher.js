/* ----------------------------------------------------------- */
/*  Sites selector
 /* ----------------------------------------------------------- */
(function($) {

  function siteSelect() {
    $(".language-selector .site.selected,.language-selector .all-sites").on("mouseover", function (event) {
      $(".language-selector .all-sites").show();
    });
    $(".language-selector .site.selected,.language-selector .all-sites").on("mouseout", function (event) {
      $(".language-selector .all-sites").hide();
    });
  }
  siteSelect();

})(jQuery);