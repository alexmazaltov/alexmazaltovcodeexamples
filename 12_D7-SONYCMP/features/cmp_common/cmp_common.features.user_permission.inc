<?php
/**
 * @file
 * cmp_common.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cmp_common_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access content'.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'representative' => 'representative',
      'task manager' => 'task manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'add media from remote sources'.
  $permissions['add media from remote sources'] = array(
    'name' => 'add media from remote sources',
    'roles' => array(
      'administrator' => 'administrator',
      'representative' => 'representative',
      'task manager' => 'task manager',
    ),
    'module' => 'media_internet',
  );

  // Exported permission: 'create files'.
  $permissions['create files'] = array(
    'name' => 'create files',
    'roles' => array(
      'administrator' => 'administrator',
      'representative' => 'representative',
      'task manager' => 'task manager',
    ),
    'module' => 'file_entity',
  );

  // Exported permission: 'crop and resize any media content'.
  $permissions['crop and resize any media content'] = array(
    'name' => 'crop and resize any media content',
    'roles' => array(
      'administrator' => 'administrator',
      'task manager' => 'task manager',
    ),
    'module' => 'cmp_common',
  );

  // Exported permission: 'download any audio files'.
  $permissions['download any audio files'] = array(
    'name' => 'download any audio files',
    'roles' => array(
      'administrator' => 'administrator',
      'representative' => 'representative',
      'task manager' => 'task manager',
    ),
    'module' => 'file_entity',
  );

  // Exported permission: 'download any document files'.
  $permissions['download any document files'] = array(
    'name' => 'download any document files',
    'roles' => array(
      'administrator' => 'administrator',
      'representative' => 'representative',
      'task manager' => 'task manager',
    ),
    'module' => 'file_entity',
  );

  // Exported permission: 'download any image files'.
  $permissions['download any image files'] = array(
    'name' => 'download any image files',
    'roles' => array(
      'administrator' => 'administrator',
      'representative' => 'representative',
      'task manager' => 'task manager',
    ),
    'module' => 'file_entity',
  );

  // Exported permission: 'download any video files'.
  $permissions['download any video files'] = array(
    'name' => 'download any video files',
    'roles' => array(
      'administrator' => 'administrator',
      'representative' => 'representative',
      'task manager' => 'task manager',
    ),
    'module' => 'file_entity',
  );

  return $permissions;
}
