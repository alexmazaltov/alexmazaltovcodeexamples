<?php
/**
 * @var $file file object
 * @var $widget_values_container jQuery selector for multiupload widget
 * @var $type file type: photo, video or file extension
 * @var $file_name original file name
 * @var $file_url url to file download
 * @var $preview url to preview image
 * @var $description attachement`s description
 *
 */
?>
<div
  class="b-attachment--restyle b-attachment-<?php print $type; ?>--restyle js--attachment"
  data-fid="<?php print $file->fid; ?>">
  <?php if (!empty($widget_values_container)): ?>
    <button class="btn-remove--restyle remove-file"
            data-valueswrapper="<?php print $widget_values_container; ?>">Remove
    </button>
  <?php endif; ?>
  <a
    class="b-attachment-content--restyle<?php if ($preview): ?> media-colorbox<?php endif; ?>"
    title="<?php print $file_name; ?>"
    <?php if ($type == "video") : ?>
      href="/media_colorbox/<?php print $file->fid; ?>/colorbox/und"
      data-mediaColorboxFixedWidth
      data-mediaColorboxFixedHeight
      data-mediaColorboxAudioPlaylist
    <?php else: ?>
      href="<?php print $file_url; ?>"
      target="_blank"
    <?php endif; ?>
  >
    <?php if ($preview): ?>
      <figure class="image--restyle">
        <?php if ($type == "video") : ?>
          <div class="file-video-oembed">
            <img alt="<?php print $file_name; ?>"
                 src="<?php print $preview; ?>"/>
          </div>
        <?php else : ?>
          <img alt="<?php print $file_name; ?>"
               src="<?php print $preview; ?>" id="file-image-preview-<?php print $file->fid;?>"/>
        <?php endif; ?>
      </figure>
    <?php endif; ?>
    <div class="b-attachment-description--restyle">
      <?php if (!is_array($description)): ?>

        <h3><?php print $type; ?></h3>
        <?php if ($description): ?>
          <p><?php print $description; ?></p>
        <?php endif; ?>
      <?php endif; ?>
    </div>
  </a>
  <?php if ($type == "photo" && $crop): ?>
    <div class="js--edit-image"
         data-image-url="<?php print check_plain($original_image_url); ?>"
         data-image-fid="<?php print $file->fid; ?>"
         data-image-crop-info="<?php print check_plain($crop_info); ?>"
    >edit
    </div>
  <?php endif; ?>
  <?php if (is_array($description)): ?>
  <div class="b-attachment-edit--restyle">
    <div
      class="form-item form-type-textfield media-title--restyle hide-counter--restyle">
      <?php print render($description); ?>
    </div>
  </div>
  <?php endif; ?>
</div>
