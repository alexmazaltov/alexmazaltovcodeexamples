<?php

/**
 * @file
 *  Provide debug functions.
 */

/***************************************************************************/

/**
 * Debug...
 */
function sho() {
  $args = func_get_args();
  print '<pre>';
  foreach ($args as $arg) {
    print_r($arg);
    print '<br/>-----------------------<br/>';
  }
  print '</pre>';
}

function shoc() {
  $args = func_get_args();
  print '<div style="width: 750px; height: 650px; overflow: auto; position: relative; background-color: #ffffcc; border: 2px solid #ffcc00;">';
  call_user_func_array('sho', $args);
  print '</div>';
}

function shodie() {
  $args = func_get_args();
  call_user_func_array('sho', $args);
  die();
}

function shocdie() {
  $args = func_get_args();
  call_user_func_array('shoc', $args);
  die();
}

function shokpr() {
  $input = func_get_args();
  if (merits_krumo($input)) {
    return krumo($input);
  }
  else {
    sho($input);
  }
}

function shokprdie() {
  $input = func_get_args();
  if (merits_krumo($input)) {
    krumo($input);
  }
  else {
    sho($input);
  }
  die;
}


/**
 * Devs functions
 */
function printr($val, $die = TRUE) {
  print_r($val);
  if ($die) {
    die();
  }
}
