/**
 * @file
 * JS behaviors for the cmp_common_attachments field widget.
 */
(function ($) {

    // Hide these in a ready to ensure that Drupal.ajax is set up first.
    $(function () {
        Drupal.ajax.prototype.commands.cmpAttachNewFile = function (ajax, response, status) {
            var added = response.data;
            var existing = $(response.selector).val();

            if (existing == '') {
                existing = '{}';
            }

            var all = $.extend({}, $.parseJSON(existing), $.parseJSON(added));
            $(response.selector).val(JSON.stringify(all)).trigger('change');

            var showNumberOfFiles = $(response.selector).prev().find('a').data('shownumberoffiles');
            // Create/update uploaded counter or not.
            if (showNumberOfFiles != 'undefined' && showNumberOfFiles != 0) {
                var filesCount = Object.keys(all).length;

                // Attach message if it the first file.
                if ($(response.selector).prev().find('.files-count').length == 0) {
                    $(response.selector).prev().prepend(
                        '<div class="files-count"></div>'
                    );
                }

                // Update files count.
                var countMessage = Drupal.formatPlural(filesCount, '<span class="number-of-files">1</span> File Uploaded', '<span class="number-of-files">@count</span> Files Uploaded');
                $(response.selector).prev().find('.files-count').html(countMessage);
            }

            // Row classes.
            var $row = $(response.selector).closest('.row');
            if ($row.hasClass('no-previews') && Object.keys(all).length > 0) {
                $row.toggleClass('no-previews has-previews');
            }
        };
    });

    // Remove link behavior.
    Drupal.behaviors.cmpRemoveAttachmentLink = {
        attach: function (context, settings) {
            if ($('.remove-file').length) {
                $('.remove-file').on('click', function (e) {
                    e.preventDefault();

                    var $this = $(this),
                        $wrapper = $this.parent(),
                        // Current file id.
                        fid = $wrapper.data('fid'),
                        // Current values wrapper.
                        $widgetValuesWrapper = $('#' + $this.data('valueswrapper')),
                        // Current attachments.
                        widgetValues = JSON.parse($widgetValuesWrapper.val());

                    // Remove current file.
                    delete widgetValues[fid];
                    $wrapper.fadeOut('fast', function () {
                        $(this).remove();
                    });

                    // Update field values.
                    $widgetValuesWrapper.val(JSON.stringify(widgetValues));
                    var filesCount = Object.keys(widgetValues).length;

                    var showNumberOfFiles = $widgetValuesWrapper.prev().find('a').data('shownumberoffiles');
                    // Remove/update uploaded counter or not.
                    if (showNumberOfFiles != 'undefined' && showNumberOfFiles != 0) {
                        // Update files count.
                        if (filesCount > 0) {
                            var countMessage = Drupal.formatPlural(filesCount, '<span class="number-of-files">1</span> File Uploaded', '<span class="number-of-files">@count</span> Files Uploaded');
                            $widgetValuesWrapper.prev().find('.files-count').html(countMessage);
                        }
                        else {
                            $widgetValuesWrapper.prev().find('.files-count').remove();
                        }
                    }

                    // Row classes.
                    var $row = $widgetValuesWrapper.closest('.row');
                    if ($row.hasClass('has-previews') && filesCount == 0) {
                        $row.toggleClass('no-previews has-previews');
                    }
                });
            }
        }
    };

    // Photos/Videos tabs in pop-up.
    Drupal.behaviors.cmpAttachmentsPopupTabs = {
        attach: function (context, settings) {
            $('.cmp-attachment-popup-tabs', context).once('cmp-attachment-popup-tabs', function () {
                var $wrapper = $(this);

                $('.tab-switcher a', $wrapper).click(function (e) {
                    e.preventDefault();
                    var $a = $(this);

                    $('.tab-switcher a', $wrapper).removeClass('active');
                    $a.addClass('active');

                    $('.tab', $wrapper).hide();
                    $('.tab.' + $a.data('tab'), $wrapper).show();
                });
            });
        }
    };

    // Fill in hidden field with values of existing attachments.
    Drupal.behaviors.cmpAttachmentsPopupExisting = {
        attach: function (context, settings) {
            $('form[id^="cmp-common-attachments-single-item-form"] input[name="existing"]', context).each(function () {
                var $destination = $(this);
                var $source = $('#' + $destination.data('source'));

                $destination.val($source.val());
            });
        }
    };


})(jQuery);
