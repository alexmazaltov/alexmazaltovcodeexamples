(function ($) {
    Drupal.behaviors.cmpAttachmentsEditor = {
        attach: function (context, settings) {
            $('.js--attachment .js--edit-image', context).on('click', function () {
                var $editButton = $(this);
                var imageSrc = $editButton.data('image-url');
                var imageFid = $editButton.data('image-fid');
                var imageCropData = $(this).data('image-crop-info');
                $.magnificPopup.open({
                    mainClass: 'mfp-popup-cropper',
                    items: [
                        {
                            type: 'inline',
                            src: '<div class="popup-image-editor">' +
                            '<div class="toolbar toolbar-top">' +
                            '<span class="rotate-left">left</span>' +
                            '<span class="rotate-right">right</span>' +
                            '</div>' +
                            '<div style="width: 100%; height: 400px; margin: auto">' +
                            '<img src="' + imageSrc + '" class="editor-area" style="width: 100%; height: 100%">' +
                            '</div>' +
                            '<div class="toolbar toolbar-bottom">' +
                            '<span class="reset">Reset</span>' +
                            '<span class="apply">Apply</span>' +
                            '</div>' +
                            '</div>'
                        }
                    ],
                    callbacks: {
                        open: function () {
                            var magnific = this;
                            var $popup = $('.popup-image-editor');
                            var image = $('.editor-area', $popup).get(0);
                            var cropper = new Cropper(image, {
                                // aspectRatio: 16 / 9,
                                viewMode: 2,
                                dragMode: 'move',
                                autoCropArea: 1,
                                toggleDragModeOnDblclick: false,
                                data: imageCropData
                            });
                            $('.rotate-left', $popup).on('click', function () {
                                cropper.rotate(-90);
                            });
                            $('.rotate-right', $popup).on('click', function () {
                                cropper.rotate(90);
                            });
                            $('.reset', $popup).on('click', function () {
                                cropper.reset();
                            });
                            $('.apply', $popup).on('click', function () {
                                var cropInfo = cropper.getData(true);
                                $.ajax(Drupal.settings.basePath + 'cmp/attachments/crop-and-rotate', {
                                    data: {
                                        fid: imageFid,
                                        cropInfo: cropInfo
                                    },
                                    success: function (data, textStatus, jqXHR) {
                                        if (data.status == 'error') {
                                            alert(data.message);
                                        } else {
                                            $('#file-image-preview-' + imageFid).attr('src', data.src);
                                            $('#file-image-preview-' + imageFid).attr('width', data.width);
                                            $('#file-image-preview-' + imageFid).attr('height', data.height);
                                            $('#file-image-preview-' + imageFid).css('width', data.width);
                                            $('#file-image-preview-' + imageFid).css('height', data.height);
                                            $('#file-image-preview-' + imageFid).parents('a').attr('href', data.big);
                                            $editButton.data('image-crop-info', data.cropInfo);
                                            magnific.close();
                                        }
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        alert('Error: ' + textStatus);
                                    }
                                });
                            });

                        }
                    }
                });

            });
        },
    };
})(jQuery);
