<?php
/**
 * @file
 * cmp_common.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cmp_common_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_type") {
    return array("version" => "1");
  }
  if ($module == "ife" && $api == "ife") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cmp_common_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_date_custom_date_formats().
 */
function cmp_common_fe_date_custom_date_formats() {
  $custom_date_formats = array();
  $custom_date_formats['n/d'] = 'n/d';
  $custom_date_formats['n/j/Y'] = 'n/j/Y';
  $custom_date_formats['n/j/y'] = 'n/j/y';
  return $custom_date_formats;
}

/**
 * Implements hook_date_format_types().
 */
function cmp_common_date_format_types() {
  $format_types = array();
  // Exported date format type: short_month_and_day_
  $format_types['short_month_and_day_'] = 'Short month and day';
  // Exported date format type: tiny
  $format_types['tiny'] = 'Tiny';
  // Exported date format type: tiny_full
  $format_types['tiny_full'] = 'Tiny full year';
  // Exported date format type: tiny_lower_case
  $format_types['tiny_lower_case'] = 'tiny_lower_case';
  return $format_types;
}

/**
 * Implements hook_image_default_styles().
 */
function cmp_common_image_default_styles() {
  $styles = array();

  // Exported image style: cmp_attachments_255x177.
  $styles['cmp_attachments_255x177'] = array(
    'label' => 'CMP attachments 255x177',
    'effects' => array(
      15 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 255,
          'height' => 177,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: cmp_attachments_80x80.
  $styles['cmp_attachments_80x80'] = array(
    'label' => 'CMP attachments 80x80',
    'effects' => array(
      19 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 80,
          'height' => 80,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: media_preview_241x241.
  $styles['media_preview_241x241'] = array(
    'label' => 'Media Preview 241x241',
    'effects' => array(
      13 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 241,
          'height' => 241,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: youtube_thumbnail_370x210.
  $styles['youtube_thumbnail_370x210'] = array(
    'label' => 'Youtube Thumbnail 370x210',
    'effects' => array(
      8 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 370,
          'height' => 210,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function cmp_common_node_info() {
  $items = array(
    'video' => array(
      'name' => t('Video'),
      'base' => 'node_content',
      'description' => t('Youtube video for homepage and dashboard'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
