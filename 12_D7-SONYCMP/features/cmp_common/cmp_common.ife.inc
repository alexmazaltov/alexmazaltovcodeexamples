<?php
/**
 * @file
 * cmp_common.ife.inc
 */

/**
 * Implements hook_ife_default_settings().
 */
function cmp_common_ife_default_settings() {
  $export = array();

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'ajax_login_pass_change_pass_form';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['ajax_login_pass_change_pass_form'] = $ife;

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_common_attachments_single_item_form';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_common_attachments_single_item_form'] = $ife;

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_common_attachments_single_item_form_files';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_common_attachments_single_item_form_files'] = $ife;

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_common_attachments_single_item_form_videos';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_common_attachments_single_item_form_videos'] = $ife;

  return $export;
}
