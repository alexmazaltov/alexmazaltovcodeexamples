<?php
/**
 * @file
 * cmp_dashboard.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function cmp_dashboard_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['ajax_login_pass-ajax_change_pass'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'ajax_change_pass',
    'module' => 'ajax_login_pass',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => -25,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -25,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['block-auth_logo'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'auth_logo',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(
      'authenticated user' => 2,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['block-auth_logo_footer'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'auth_logo_footer',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(
      'administrator' => 3,
      'authenticated user' => 2,
      'representative' => 5,
      'task manager' => 4,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => -42,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -42,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['block-cmp_rep_settings_dropdown'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'cmp_rep_settings_dropdown',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(
      'administrator' => 3,
      'representative' => 5,
      'task manager' => 4,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => -26,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -26,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['block-features'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'features',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => 'Features',
    'visibility' => 0,
  );

  $export['block-header_icons'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'header_icons',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(
      'administrator' => 3,
      'authenticated user' => 2,
      'representative' => 5,
      'task manager' => 4,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'header_right',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => -27,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -27,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['block-social_bottom'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'social_bottom',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(
      'authenticated user' => 2,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => -40,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -40,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['cmp_dashboard-rep_dashboard_statistics'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'rep_dashboard_statistics',
    'module' => 'cmp_dashboard',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['cmp_settings_misc-settings_change_password'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'settings_change_password',
    'module' => 'cmp_settings_misc',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'header_right',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['system-main-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => -12,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -12,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['system-user-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'user-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(
      'administrator' => 3,
      'authenticated user' => 2,
      'representative' => 5,
      'task manager' => 4,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => -38,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -38,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['views-leaderboard-leaderboard'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'leaderboard-leaderboard',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'dashboard',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
