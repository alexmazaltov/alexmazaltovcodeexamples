<?php
/**
 * @file
 * cmp_dashboard.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function cmp_dashboard_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'dashboard';
  $page->task = 'page';
  $page->admin_title = 'Dashboard';
  $page->admin_description = 'Dashboard page for managers and Reps';
  $page->path = 'dashboard';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
            2 => 5,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Dashboard',
    'name' => 'main-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'normal',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_dashboard__panel_context_be333bdc-96d6-4765-83a5-6b4b87d122a2';
  $handler->task = 'page';
  $handler->subtask = 'dashboard';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Dashboard for task manager',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 3,
              1 => 4,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'dashboard';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'top' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
      'bottom' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Dashboard';
  $display->uuid = '8f5622df-4156-46c1-9d71-705fcee275ce';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-4d491f63-8958-46c9-bde0-f94761dc6900';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4d491f63-8958-46c9-bde0-f94761dc6900';
    $display->content['new-4d491f63-8958-46c9-bde0-f94761dc6900'] = $pane;
    $display->panels['above_left'][0] = 'new-4d491f63-8958-46c9-bde0-f94761dc6900';
    $pane = new stdClass();
    $pane->pid = 'new-5e2633f4-d9cc-40ec-88be-1684b210915c';
    $pane->panel = 'below_left';
    $pane->type = 'block';
    $pane->subtype = 'block-6';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5e2633f4-d9cc-40ec-88be-1684b210915c';
    $display->content['new-5e2633f4-d9cc-40ec-88be-1684b210915c'] = $pane;
    $display->panels['below_left'][0] = 'new-5e2633f4-d9cc-40ec-88be-1684b210915c';
    $pane = new stdClass();
    $pane->pid = 'new-e8b931d7-88d3-4d0b-a87e-b7e33f19418b';
    $pane->panel = 'below_right';
    $pane->type = 'block';
    $pane->subtype = 'views-video-block';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e8b931d7-88d3-4d0b-a87e-b7e33f19418b';
    $display->content['new-e8b931d7-88d3-4d0b-a87e-b7e33f19418b'] = $pane;
    $display->panels['below_right'][0] = 'new-e8b931d7-88d3-4d0b-a87e-b7e33f19418b';
    $pane = new stdClass();
    $pane->pid = 'new-cb767f99-e877-4508-9923-242520d270e7';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'leaderboard';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'leaderboard',
      'override_title' => 1,
      'override_title_text' => 'Leaderboard',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'cb767f99-e877-4508-9923-242520d270e7';
    $display->content['new-cb767f99-e877-4508-9923-242520d270e7'] = $pane;
    $display->panels['middle'][0] = 'new-cb767f99-e877-4508-9923-242520d270e7';
    $pane = new stdClass();
    $pane->pid = 'new-201a8d8d-a75d-4a98-8693-aca3dc06b255';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'mapbox-mapbox';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Our Reps Coast to Coast',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '201a8d8d-a75d-4a98-8693-aca3dc06b255';
    $display->content['new-201a8d8d-a75d-4a98-8693-aca3dc06b255'] = $pane;
    $display->panels['middle'][1] = 'new-201a8d8d-a75d-4a98-8693-aca3dc06b255';
    $pane = new stdClass();
    $pane->pid = 'new-954573b7-f2db-455d-bae3-1848d4aa803c';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'block-20';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Features',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '954573b7-f2db-455d-bae3-1848d4aa803c';
    $display->content['new-954573b7-f2db-455d-bae3-1848d4aa803c'] = $pane;
    $display->panels['middle'][2] = 'new-954573b7-f2db-455d-bae3-1848d4aa803c';
    $pane = new stdClass();
    $pane->pid = 'new-d08ba427-0754-480a-99be-c98a5e92185d';
    $pane->panel = 'top_left';
    $pane->type = 'views';
    $pane->subtype = 'tasks';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'open_tasks',
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd08ba427-0754-480a-99be-c98a5e92185d';
    $display->content['new-d08ba427-0754-480a-99be-c98a5e92185d'] = $pane;
    $display->panels['top_left'][0] = 'new-d08ba427-0754-480a-99be-c98a5e92185d';
    $pane = new stdClass();
    $pane->pid = 'new-ed46f537-7937-45fb-ba6c-085b6ea92cf7';
    $pane->panel = 'top_right';
    $pane->type = 'views';
    $pane->subtype = 'tasks';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'overdue_tasks',
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ed46f537-7937-45fb-ba6c-085b6ea92cf7';
    $display->content['new-ed46f537-7937-45fb-ba6c-085b6ea92cf7'] = $pane;
    $display->panels['top_right'][0] = 'new-ed46f537-7937-45fb-ba6c-085b6ea92cf7';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_dashboard__panel_context_25b7e66e-53b3-49a4-9630-c20a5bf43e7e';
  $handler->task = 'page';
  $handler->subtask = 'dashboard';
  $handler->handler = 'panel_context';
  $handler->weight = 2;
  $handler->conf = array(
    'title' => 'Dashboard for representative',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 5,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'dashboard';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'top' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
      'bottom' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Dashboard';
  $display->uuid = '8f5622df-4156-46c1-9d71-705fcee275ce';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-4d491f63-8958-46c9-bde0-f94761dc6900';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4d491f63-8958-46c9-bde0-f94761dc6900';
    $display->content['new-4d491f63-8958-46c9-bde0-f94761dc6900'] = $pane;
    $display->panels['above_left'][0] = 'new-4d491f63-8958-46c9-bde0-f94761dc6900';
    $pane = new stdClass();
    $pane->pid = 'new-c851710c-e422-4da5-b8c3-9a81177324e7';
    $pane->panel = 'above_right';
    $pane->type = 'block';
    $pane->subtype = 'cmp_dashboard-rep_dashboard_statistics';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c851710c-e422-4da5-b8c3-9a81177324e7';
    $display->content['new-c851710c-e422-4da5-b8c3-9a81177324e7'] = $pane;
    $display->panels['above_right'][0] = 'new-c851710c-e422-4da5-b8c3-9a81177324e7';
    $pane = new stdClass();
    $pane->pid = 'new-5e2633f4-d9cc-40ec-88be-1684b210915c';
    $pane->panel = 'below_left';
    $pane->type = 'block';
    $pane->subtype = 'block-6';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5e2633f4-d9cc-40ec-88be-1684b210915c';
    $display->content['new-5e2633f4-d9cc-40ec-88be-1684b210915c'] = $pane;
    $display->panels['below_left'][0] = 'new-5e2633f4-d9cc-40ec-88be-1684b210915c';
    $pane = new stdClass();
    $pane->pid = 'new-e8b931d7-88d3-4d0b-a87e-b7e33f19418b';
    $pane->panel = 'below_right';
    $pane->type = 'block';
    $pane->subtype = 'views-video-block';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e8b931d7-88d3-4d0b-a87e-b7e33f19418b';
    $display->content['new-e8b931d7-88d3-4d0b-a87e-b7e33f19418b'] = $pane;
    $display->panels['below_right'][0] = 'new-e8b931d7-88d3-4d0b-a87e-b7e33f19418b';
    $pane = new stdClass();
    $pane->pid = 'new-fa48e789-684c-49cd-8013-1bd8f43f76ce';
    $pane->panel = 'bottom';
    $pane->type = 'views_panes';
    $pane->subtype = 'tasks-panel_reps_tasks_mobile';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'fa48e789-684c-49cd-8013-1bd8f43f76ce';
    $display->content['new-fa48e789-684c-49cd-8013-1bd8f43f76ce'] = $pane;
    $display->panels['bottom'][0] = 'new-fa48e789-684c-49cd-8013-1bd8f43f76ce';
    $pane = new stdClass();
    $pane->pid = 'new-d8b82997-b792-49a4-9dda-8a4988e09a0a';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'leaderboard';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'reps_leaderboard',
      'override_title' => 1,
      'override_title_text' => 'Leaderboard',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd8b82997-b792-49a4-9dda-8a4988e09a0a';
    $display->content['new-d8b82997-b792-49a4-9dda-8a4988e09a0a'] = $pane;
    $display->panels['middle'][0] = 'new-d8b82997-b792-49a4-9dda-8a4988e09a0a';
    $pane = new stdClass();
    $pane->pid = 'new-201a8d8d-a75d-4a98-8693-aca3dc06b255';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'mapbox-mapbox';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Our Reps Coast to Coast',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '201a8d8d-a75d-4a98-8693-aca3dc06b255';
    $display->content['new-201a8d8d-a75d-4a98-8693-aca3dc06b255'] = $pane;
    $display->panels['middle'][1] = 'new-201a8d8d-a75d-4a98-8693-aca3dc06b255';
    $pane = new stdClass();
    $pane->pid = 'new-954573b7-f2db-455d-bae3-1848d4aa803c';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'block-20';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Features',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '954573b7-f2db-455d-bae3-1848d4aa803c';
    $display->content['new-954573b7-f2db-455d-bae3-1848d4aa803c'] = $pane;
    $display->panels['middle'][2] = 'new-954573b7-f2db-455d-bae3-1848d4aa803c';
    $pane = new stdClass();
    $pane->pid = 'new-3833a6d5-0d57-4474-889d-f833099a0fbd';
    $pane->panel = 'top_left';
    $pane->type = 'views';
    $pane->subtype = 'tasks';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'open_reps_tasks',
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3833a6d5-0d57-4474-889d-f833099a0fbd';
    $display->content['new-3833a6d5-0d57-4474-889d-f833099a0fbd'] = $pane;
    $display->panels['top_left'][0] = 'new-3833a6d5-0d57-4474-889d-f833099a0fbd';
    $pane = new stdClass();
    $pane->pid = 'new-a3e74c77-0794-4397-84da-56052681023a';
    $pane->panel = 'top_right';
    $pane->type = 'views';
    $pane->subtype = 'tasks';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'overdue_reps_tasks',
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a3e74c77-0794-4397-84da-56052681023a';
    $display->content['new-a3e74c77-0794-4397-84da-56052681023a'] = $pane;
    $display->panels['top_right'][0] = 'new-a3e74c77-0794-4397-84da-56052681023a';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['dashboard'] = $page;

  return $pages;

}
