<?php
/**
 * @file
 * cmp_dashboard.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function cmp_dashboard_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Authenticated logo';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'auth_logo';
  $fe_block_boxes->body = '<p class="rtecenter er"><a href="/dashboard"><img alt="" src="/sites/all/themes/sonycmp/images/logo-black.png"/></a></p>
';

  $export['auth_logo'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Authenticated footer logo ';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'auth_logo_footer';
  $fe_block_boxes->body = '<p class="rtecenter"><a href="/dashboard"><img alt="" src="/sites/all/themes/sonycmp/images/logo-black.png" style="width: 300px; height: 43px;" /></a></p>
';

  $export['auth_logo_footer'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Settings dropdown menu for autenticated users';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'cmp_rep_settings_dropdown';
  $fe_block_boxes->body = '<div class="settings-popup"><ul>
	<li><a href="/settings">Edit Profile</a></li>
	<li><a href="/settings/password">Change Password</a></li>
</ul></div>
';

  $export['cmp_rep_settings_dropdown'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Features title';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'features';
  $fe_block_boxes->body = '<span></span>';

  $export['features'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Header icons for auth users';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'header_icons';
  $fe_block_boxes->body = '<ul class="controls-nav">
	<li class="hide-for-mobile"><a href="/settings"  id="ajax-settings-link" onclick="return false;"><img alt="Settings" src="/sites/all/themes/sonycmp/images/settings-icon.png" /></a></li>
	<li class="show-for-mobile"><a href="/settings/password"  id="settings-link"><img alt="Settings" src="/sites/all/themes/sonycmp/images/settings-icon.png" /></a></li>
	<li><a href="/dashboard/notifications" id="notifications-link" onclick="return false;"><img alt="" src="/sites/all/themes/sonycmp/images/alerts-icon.png" /><span class="warning badge"></span></a></li>
	<li><a href="/reps/logout"><img alt="" src="/sites/all/themes/sonycmp/images/logout-icon.png" /></a></li>
</ul>
';

  $export['header_icons'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Bottom social links';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'social_bottom';
  $fe_block_boxes->body = '<ul class="social-icons social-icons--bottom">
  <li><a href="https://twitter.com/SonyMusicU" target="_blank"><img alt="Twitter icon" src="/sites/all/themes/sonycmp/images/twitter-bottom-icon.png" /></a></li>
  <li><a href="https://instagram.com/sonymusicu/" target="_blank"><img alt="Instagram icon" src="/sites/all/themes/sonycmp/images/instagram-bottom-icon.png" /></a></li>
  <li><a href="https://www.youtube.com/channel/UCqd8NzWNwQtG2IlNbNyCuyA" target="_blank"><img alt="Youtube icon" src="/sites/all/themes/sonycmp/images/youtube-bottom-icon.png" /></a></li>
</ul>
';

  $export['social_bottom'] = $fe_block_boxes;

  return $export;
}
