<?php
/**
 * @file
 * cmp_dashboard.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cmp_dashboard_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'search content'.
  $permissions['search content'] = array(
    'name' => 'search content',
    'roles' => array(
      'administrator' => 'administrator',
      'representative' => 'representative',
      'task manager' => 'task manager',
    ),
    'module' => 'search',
  );

  return $permissions;
}
