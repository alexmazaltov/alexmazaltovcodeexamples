<?php
/**
 * @file
 * cmp_reportings.ife.inc
 */

/**
 * Implements hook_ife_default_settings().
 */
function cmp_reportings_ife_default_settings() {
  $export = array();

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'sonycmp_reports_add_new_report';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['sonycmp_reports_add_new_report'] = $ife;

  return $export;
}
