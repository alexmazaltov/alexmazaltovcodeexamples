<?php
/**
 * @file
 * cmp_reportings.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function cmp_reportings_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_reporting:reporting.
  $menu_links['main-menu_reporting:reporting'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'reporting',
    'router_path' => 'reporting',
    'link_title' => 'Reporting',
    'options' => array(
      'identifier' => 'main-menu_reporting:reporting',
      'attributes' => array(
        'id' => 'reporting-link',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: user-menu_reporting:reporting.
  $menu_links['user-menu_reporting:reporting'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'reporting',
    'router_path' => 'reporting',
    'link_title' => 'Reporting',
    'options' => array(
      'identifier' => 'user-menu_reporting:reporting',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Reporting');

  return $menu_links;
}
