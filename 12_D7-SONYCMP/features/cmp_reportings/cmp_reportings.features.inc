<?php
/**
 * @file
 * cmp_reportings.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cmp_reportings_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ife" && $api == "ife") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cmp_reportings_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function cmp_reportings_image_default_styles() {
  $styles = array();

  // Exported image style: cover_header_final_report.
  $styles['cover_header_final_report'] = array(
    'label' => 'Cover header final report',
    'effects' => array(
      20 => array(
        'name' => 'simplecrop',
        'data' => array(),
        'weight' => 1,
      ),
      21 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 2560,
          'height' => 720,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: final_report_effect_0.
  $styles['final_report_effect_0'] = array(
    'label' => 'Final report effect #0',
    'effects' => array(
      23 => array(
        'name' => 'simplecrop',
        'data' => array(),
        'weight' => 0,
      ),
      22 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1366,
          'height' => 434,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: final_report_effect_1.
  $styles['final_report_effect_1'] = array(
    'label' => 'Final report effect #1(light)',
    'effects' => array(
      26 => array(
        'name' => 'simplecrop',
        'data' => array(),
        'weight' => 0,
      ),
      24 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1366,
          'height' => 434,
        ),
        'weight' => 1,
      ),
      25 => array(
        'name' => 'sonycmp_reports_header_1',
        'data' => array(),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: final_report_effect_2.
  $styles['final_report_effect_2'] = array(
    'label' => 'Final report effect #2(dark)',
    'effects' => array(
      29 => array(
        'name' => 'simplecrop',
        'data' => array(),
        'weight' => 0,
      ),
      27 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1366,
          'height' => 434,
        ),
        'weight' => 1,
      ),
      28 => array(
        'name' => 'sonycmp_reports_header_2',
        'data' => array(),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: final_report_effect_3.
  $styles['final_report_effect_3'] = array(
    'label' => 'Final report effect #3(green)',
    'effects' => array(
      21 => array(
        'name' => 'simplecrop',
        'data' => array(),
        'weight' => 0,
      ),
      19 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1366,
          'height' => 434,
        ),
        'weight' => 1,
      ),
      20 => array(
        'name' => 'sonycmp_reports_header_3',
        'data' => array(),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: final_report_effect_4.
  $styles['final_report_effect_4'] = array(
    'label' => 'Final report effect #4(pink)',
    'effects' => array(
      32 => array(
        'name' => 'simplecrop',
        'data' => array(),
        'weight' => 0,
      ),
      30 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1366,
          'height' => 434,
        ),
        'weight' => 1,
      ),
      31 => array(
        'name' => 'sonycmp_reports_header_4',
        'data' => array(),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: final_report_popup_image.
  $styles['final_report_popup_image'] = array(
    'label' => 'Final report popup image',
    'effects' => array(
      37 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 752,
          'height' => 436,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: select_media_preview_280x184.
  $styles['select_media_preview_280x184'] = array(
    'label' => 'select_media_preview_280x184',
    'effects' => array(
      35 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 280,
          'height' => 184,
        ),
        'weight' => 1,
      ),
      36 => array(
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/jpeg',
          'quality' => 95,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function cmp_reportings_node_info() {
  $items = array(
    'report' => array(
      'name' => t('Final Report'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
