<?php
/**
 * @file
 * cmp_reportings.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function cmp_reportings_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__panel_context_72568045-3cd6-408f-95b5-4faf88b69c81';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'ST2 Final Report',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'page-report',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_access',
          'settings' => array(
            'type' => 'view',
          ),
          'context' => array(
            0 => 'logged-in-user',
            1 => 'argument_entity_id:node_1',
          ),
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'report' => 'report',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
        2 => array(
          'name' => 'theme',
          'settings' => array(
            'theme' => 'sonycmp_theme',
          ),
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'st2_final_report';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
    'middle' => array(
      'style' => 'default',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '89502991-aef4-4779-ae8c-5f74d30d4d1b';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-d103f251-24df-420c-9d69-c113a05a5aa7';
    $pane->panel = 'middle';
    $pane->type = 'node_content';
    $pane->subtype = 'node_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'links' => 0,
      'no_extras' => 0,
      'override_title' => 0,
      'override_title_text' => '',
      'identifier' => '',
      'link' => 0,
      'leave_node_title' => 1,
      'build_mode' => 'view_final_report',
      'context' => 'argument_entity_id:node_1',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd103f251-24df-420c-9d69-c113a05a5aa7';
    $display->content['new-d103f251-24df-420c-9d69-c113a05a5aa7'] = $pane;
    $display->panels['middle'][0] = 'new-d103f251-24df-420c-9d69-c113a05a5aa7';
  $display->hide_title = PANELS_TITLE_PANE;
  $display->title_pane = 'new-d103f251-24df-420c-9d69-c113a05a5aa7';
  $handler->conf['display'] = $display;
  $export['node_view__panel_context_72568045-3cd6-408f-95b5-4faf88b69c81'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function cmp_reportings_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'archived_reports';
  $page->task = 'page';
  $page->admin_title = 'Archived reports';
  $page->admin_description = '';
  $page->path = 'reporting/archived';
  $page->access = array();
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Archived',
    'name' => 'navigation',
    'weight' => '1',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_archived_reports__panel';
  $handler->task = 'page';
  $handler->subtask = 'archived_reports';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Archived reports',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'access' => array(
      'logic' => 'and',
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 3,
              1 => 4,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'reporting';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'middle' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Archived reports';
  $display->uuid = 'b29beb33-2b15-49a6-a276-fb0888bd3a81';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-264e6b75-684a-4052-a3e5-fdb8957fa7af';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '264e6b75-684a-4052-a3e5-fdb8957fa7af';
    $display->content['new-264e6b75-684a-4052-a3e5-fdb8957fa7af'] = $pane;
    $display->panels['above_left'][0] = 'new-264e6b75-684a-4052-a3e5-fdb8957fa7af';
    $pane = new stdClass();
    $pane->pid = 'new-14ea5148-26d3-42f9-b781-87eb95b6cea4';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'reportings-archived_reports_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '14ea5148-26d3-42f9-b781-87eb95b6cea4';
    $display->content['new-14ea5148-26d3-42f9-b781-87eb95b6cea4'] = $pane;
    $display->panels['middle'][0] = 'new-14ea5148-26d3-42f9-b781-87eb95b6cea4';
    $pane = new stdClass();
    $pane->pid = 'new-187ccfb3-af5b-4ec8-93bb-48b409276854';
    $pane->panel = 'top_left';
    $pane->type = 'page_tabs';
    $pane->subtype = 'page_tabs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'type' => 'both',
      'id' => 'tabs',
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '187ccfb3-af5b-4ec8-93bb-48b409276854';
    $display->content['new-187ccfb3-af5b-4ec8-93bb-48b409276854'] = $pane;
    $display->panels['top_left'][0] = 'new-187ccfb3-af5b-4ec8-93bb-48b409276854';
    $pane = new stdClass();
    $pane->pid = 'new-e65c7444-7b7b-4f00-b8e2-b28b922c08f2';
    $pane->panel = 'top_right';
    $pane->type = 'block';
    $pane->subtype = 'views-c9776be13ca8b617fd1f75b752962c4c';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'inherit_path' => 1,
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e65c7444-7b7b-4f00-b8e2-b28b922c08f2';
    $display->content['new-e65c7444-7b7b-4f00-b8e2-b28b922c08f2'] = $pane;
    $display->panels['top_right'][0] = 'new-e65c7444-7b7b-4f00-b8e2-b28b922c08f2';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-187ccfb3-af5b-4ec8-93bb-48b409276854';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['archived_reports'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'create_final_report';
  $page->task = 'page';
  $page->admin_title = 'Create Final Report';
  $page->admin_description = '';
  $page->path = 'reporting/%node/edit/!step';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'node_type',
        'settings' => array(
          'type' => array(
            'report' => 'report',
          ),
        ),
        'context' => 'argument_entity_id:node_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'action',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'node' => array(
      'id' => 1,
      'identifier' => 'Report ID',
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
    'step' => array(
      'id' => '',
      'identifier' => '',
      'argument' => '',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_create_final_report__panel_context_47e609cb-7ef3-4462-a977-90a83af87102';
  $handler->task = 'page';
  $handler->subtask = 'create_final_report';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'reportings_add';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'middle' => NULL,
      'bottom' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
      'top' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Create Final Report %node:title';
  $display->uuid = '4c8b067a-c408-4a1d-8956-3268a870becd';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $display->content['new-b78d2652-77fe-49fc-852f-ed71a4bbd880'] = $pane;
    $display->panels['above_left'][0] = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $pane = new stdClass();
    $pane->pid = 'new-51eb6578-f618-4427-97b8-11412eb9d4a1';
    $pane->panel = 'middle';
    $pane->type = 'create_new_report';
    $pane->subtype = 'create_new_report';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_entity_id:node_1',
      ),
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '51eb6578-f618-4427-97b8-11412eb9d4a1';
    $display->content['new-51eb6578-f618-4427-97b8-11412eb9d4a1'] = $pane;
    $display->panels['middle'][0] = 'new-51eb6578-f618-4427-97b8-11412eb9d4a1';
    $pane = new stdClass();
    $pane->pid = 'new-329d89c1-fd6a-4ad5-bee0-805a44c22690';
    $pane->panel = 'top';
    $pane->type = 'back_button';
    $pane->subtype = 'back_button';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_entity_id:node_1',
      ),
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '329d89c1-fd6a-4ad5-bee0-805a44c22690';
    $display->content['new-329d89c1-fd6a-4ad5-bee0-805a44c22690'] = $pane;
    $display->panels['top'][0] = 'new-329d89c1-fd6a-4ad5-bee0-805a44c22690';
    $pane = new stdClass();
    $pane->pid = 'new-4160f6c0-8dac-4d2b-a4d7-a691b3415d3b';
    $pane->panel = 'top_left';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
      'context' => array(
        0 => 'argument_entity_id:node_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4160f6c0-8dac-4d2b-a4d7-a691b3415d3b';
    $display->content['new-4160f6c0-8dac-4d2b-a4d7-a691b3415d3b'] = $pane;
    $display->panels['top_left'][0] = 'new-4160f6c0-8dac-4d2b-a4d7-a691b3415d3b';
    $pane = new stdClass();
    $pane->pid = 'new-833ce1ae-2096-40ac-baaf-0cefcc8ad880';
    $pane->panel = 'top_right';
    $pane->type = 'block';
    $pane->subtype = 'views-c01881d34f334ffcecbf0555603c9f09';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'path_visibility',
          'settings' => array(
            'visibility_setting' => '1',
            'paths' => 'reporting/*/task',
          ),
          'context' => 'empty',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'inherit_path' => 1,
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '833ce1ae-2096-40ac-baaf-0cefcc8ad880';
    $display->content['new-833ce1ae-2096-40ac-baaf-0cefcc8ad880'] = $pane;
    $display->panels['top_right'][0] = 'new-833ce1ae-2096-40ac-baaf-0cefcc8ad880';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['create_final_report'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'reporting';
  $page->task = 'page';
  $page->admin_title = 'Reporting';
  $page->admin_description = '';
  $page->path = 'reporting/listing';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'default tab',
    'title' => 'Reports',
    'name' => 'main-menu',
    'weight' => '-1',
    'parent' => array(
      'type' => 'normal',
      'title' => 'Reporting',
      'name' => 'main-menu',
      'weight' => '-45',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_reporting__panel_context_1ed270c3-0bbd-425a-ad97-b0e88906aa35';
  $handler->task = 'page';
  $handler->subtask = 'reporting';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'reporting';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Reporting';
  $display->uuid = '003073fc-4e56-4cb5-91b3-067f64cde79c';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-32102fbd-6918-4f3c-8596-feb0cd5cb863';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '32102fbd-6918-4f3c-8596-feb0cd5cb863';
    $display->content['new-32102fbd-6918-4f3c-8596-feb0cd5cb863'] = $pane;
    $display->panels['above_left'][0] = 'new-32102fbd-6918-4f3c-8596-feb0cd5cb863';
    $pane = new stdClass();
    $pane->pid = 'new-28b84d4d-4f5e-42f4-9ee0-afe2719b4778';
    $pane->panel = 'above_right';
    $pane->type = 'create_new_report_button';
    $pane->subtype = 'create_new_report_button';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '28b84d4d-4f5e-42f4-9ee0-afe2719b4778';
    $display->content['new-28b84d4d-4f5e-42f4-9ee0-afe2719b4778'] = $pane;
    $display->panels['above_right'][0] = 'new-28b84d4d-4f5e-42f4-9ee0-afe2719b4778';
    $pane = new stdClass();
    $pane->pid = 'new-91f2b4e6-df36-4054-a90d-176ff22110fe';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'reportings-reportings';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '91f2b4e6-df36-4054-a90d-176ff22110fe';
    $display->content['new-91f2b4e6-df36-4054-a90d-176ff22110fe'] = $pane;
    $display->panels['middle'][0] = 'new-91f2b4e6-df36-4054-a90d-176ff22110fe';
    $pane = new stdClass();
    $pane->pid = 'new-e1d7f2f6-e9d7-41d8-a8c0-541f89d2f4ea';
    $pane->panel = 'top_left';
    $pane->type = 'page_tabs';
    $pane->subtype = 'page_tabs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'type' => 'both',
      'id' => 'tabs',
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e1d7f2f6-e9d7-41d8-a8c0-541f89d2f4ea';
    $display->content['new-e1d7f2f6-e9d7-41d8-a8c0-541f89d2f4ea'] = $pane;
    $display->panels['top_left'][0] = 'new-e1d7f2f6-e9d7-41d8-a8c0-541f89d2f4ea';
    $pane = new stdClass();
    $pane->pid = 'new-2d639e47-86bc-4acf-abf8-c3dea80e5e31';
    $pane->panel = 'top_right';
    $pane->type = 'block';
    $pane->subtype = 'views--exp-reportings-reportings';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'inherit_path' => 1,
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2d639e47-86bc-4acf-abf8-c3dea80e5e31';
    $display->content['new-2d639e47-86bc-4acf-abf8-c3dea80e5e31'] = $pane;
    $display->panels['top_right'][0] = 'new-2d639e47-86bc-4acf-abf8-c3dea80e5e31';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-91f2b4e6-df36-4054-a90d-176ff22110fe';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['reporting'] = $page;

  return $pages;

}
