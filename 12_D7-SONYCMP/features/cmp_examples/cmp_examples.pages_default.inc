<?php
/**
 * @file
 * cmp_examples.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function cmp_examples_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'create_new_tutorial';
  $page->task = 'page';
  $page->admin_title = 'Create New Tutorial';
  $page->admin_description = '';
  $page->path = 'examples/new/add';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'action',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_create_new_tutorial__panel_context_a258d38e-334d-437a-8cdc-22a0f5503fd3';
  $handler->task = 'page';
  $handler->subtask = 'create_new_tutorial';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'tutorial_edit';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'middle' => NULL,
      'bottom' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Create New Example';
  $display->uuid = '4c8b067a-c408-4a1d-8956-3268a870becd';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $display->content['new-b78d2652-77fe-49fc-852f-ed71a4bbd880'] = $pane;
    $display->panels['above_left'][0] = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $pane = new stdClass();
    $pane->pid = 'new-e0b41ac1-f4ab-4451-9c5e-e343cf93b5ca';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'cmp_examples-edit_tutorial_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e0b41ac1-f4ab-4451-9c5e-e343cf93b5ca';
    $display->content['new-e0b41ac1-f4ab-4451-9c5e-e343cf93b5ca'] = $pane;
    $display->panels['middle'][0] = 'new-e0b41ac1-f4ab-4451-9c5e-e343cf93b5ca';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['create_new_tutorial'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'edit_tutorial';
  $page->task = 'page';
  $page->admin_title = 'Edit Tutorial';
  $page->admin_description = '';
  $page->path = 'examples/%node/edit';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'entity_bundle:node',
        'settings' => array(
          'type' => array(
            'tutorial' => 'tutorial',
          ),
        ),
        'context' => 'argument_entity_id:node_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'action',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'node' => array(
      'id' => 1,
      'identifier' => 'Tutorial ID',
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_edit_tutorial__panel_context_4a671c2e-9075-4ae4-b7ef-fb878c2f0fc1';
  $handler->task = 'page';
  $handler->subtask = 'edit_tutorial';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'tutorial_edit';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'middle' => NULL,
      'bottom' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Edit %node:field-tutorial-type';
  $display->uuid = '4c8b067a-c408-4a1d-8956-3268a870becd';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $display->content['new-b78d2652-77fe-49fc-852f-ed71a4bbd880'] = $pane;
    $display->panels['above_left'][0] = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $pane = new stdClass();
    $pane->pid = 'new-dbb89ec9-5ed9-4a36-9f97-893006e966ed';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'cmp_examples-edit_tutorial_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'dbb89ec9-5ed9-4a36-9f97-893006e966ed';
    $display->content['new-dbb89ec9-5ed9-4a36-9f97-893006e966ed'] = $pane;
    $display->panels['middle'][0] = 'new-dbb89ec9-5ed9-4a36-9f97-893006e966ed';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['edit_tutorial'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'example_node_view';
  $page->task = 'page';
  $page->admin_title = 'Example node view';
  $page->admin_description = 'Examples details';
  $page->path = 'examples/%node';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
            2 => 5,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'node_type',
        'settings' => array(
          'type' => array(
            'tutorial' => 'tutorial',
          ),
        ),
        'context' => 'argument_entity_id:node_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'action',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'node' => array(
      'id' => 1,
      'identifier' => 'Node: ID',
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_example_node_view__panel';
  $handler->task = 'page';
  $handler->subtask = 'example_node_view';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Example node view',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'tutorials';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'middle' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Example details';
  $display->uuid = '4d180d0d-70bc-441a-8d73-99c70b75c928';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-7dcc189c-7a97-4ec5-845a-24637d37e13c';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7dcc189c-7a97-4ec5-845a-24637d37e13c';
    $display->content['new-7dcc189c-7a97-4ec5-845a-24637d37e13c'] = $pane;
    $display->panels['above_left'][0] = 'new-7dcc189c-7a97-4ec5-845a-24637d37e13c';
    $pane = new stdClass();
    $pane->pid = 'new-84f1d2a0-5bc1-435d-b3df-650e254822d4';
    $pane->panel = 'middle';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '84f1d2a0-5bc1-435d-b3df-650e254822d4';
    $display->content['new-84f1d2a0-5bc1-435d-b3df-650e254822d4'] = $pane;
    $display->panels['middle'][0] = 'new-84f1d2a0-5bc1-435d-b3df-650e254822d4';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-7dcc189c-7a97-4ec5-845a-24637d37e13c';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['example_node_view'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'examples';
  $page->task = 'page';
  $page->admin_title = 'Examples';
  $page->admin_description = '';
  $page->path = 'examples/list';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
            2 => 5,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'default tab',
    'title' => 'Examples',
    'name' => 'main-menu',
    'weight' => '-1',
    'parent' => array(
      'type' => 'normal',
      'title' => 'Examples',
      'name' => 'main-menu',
      'weight' => '-48',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_examples__panel_context_3690ed32-072c-4222-85af-6e687c925b31';
  $handler->task = 'page';
  $handler->subtask = 'examples';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 3,
              1 => 4,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'tutorials';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Examples';
  $display->uuid = '003073fc-4e56-4cb5-91b3-067f64cde79c';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-32102fbd-6918-4f3c-8596-feb0cd5cb863';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '32102fbd-6918-4f3c-8596-feb0cd5cb863';
    $display->content['new-32102fbd-6918-4f3c-8596-feb0cd5cb863'] = $pane;
    $display->panels['above_left'][0] = 'new-32102fbd-6918-4f3c-8596-feb0cd5cb863';
    $pane = new stdClass();
    $pane->pid = 'new-14693e36-1844-4660-8537-2c5243f0b6d5';
    $pane->panel = 'above_right';
    $pane->type = 'block';
    $pane->subtype = 'cmp_examples-create_tutorial_button';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '14693e36-1844-4660-8537-2c5243f0b6d5';
    $display->content['new-14693e36-1844-4660-8537-2c5243f0b6d5'] = $pane;
    $display->panels['above_right'][0] = 'new-14693e36-1844-4660-8537-2c5243f0b6d5';
    $pane = new stdClass();
    $pane->pid = 'new-bab6c76a-be2e-4f2d-a6c2-0c5d065beb1b';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'tutorials';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '20',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'tutorials_list_manager',
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'bab6c76a-be2e-4f2d-a6c2-0c5d065beb1b';
    $display->content['new-bab6c76a-be2e-4f2d-a6c2-0c5d065beb1b'] = $pane;
    $display->panels['middle'][0] = 'new-bab6c76a-be2e-4f2d-a6c2-0c5d065beb1b';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_examples__examples-rep';
  $handler->task = 'page';
  $handler->subtask = 'examples';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Examples (rep)',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 5,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'name' => 'examples-rep',
  );
  $display = new panels_display();
  $display->layout = 'tutorials';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Examples';
  $display->uuid = '003073fc-4e56-4cb5-91b3-067f64cde79c';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-32102fbd-6918-4f3c-8596-feb0cd5cb863';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '32102fbd-6918-4f3c-8596-feb0cd5cb863';
    $display->content['new-32102fbd-6918-4f3c-8596-feb0cd5cb863'] = $pane;
    $display->panels['above_left'][0] = 'new-32102fbd-6918-4f3c-8596-feb0cd5cb863';
    $pane = new stdClass();
    $pane->pid = 'new-b27907a2-9a66-454a-8166-7a56a44ea7bc';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'tutorials';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '20',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'tutorials_list_rep',
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b27907a2-9a66-454a-8166-7a56a44ea7bc';
    $display->content['new-b27907a2-9a66-454a-8166-7a56a44ea7bc'] = $pane;
    $display->panels['middle'][0] = 'new-b27907a2-9a66-454a-8166-7a56a44ea7bc';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['examples'] = $page;

  return $pages;

}
