<?php
/**
 * @file
 * cmp_examples.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function cmp_examples_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-tutorial-field_attachements'.
  $field_instances['node-tutorial-field_attachements'] = array(
    'bundle' => 'tutorial',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'cmp_common',
        'settings' => array(
          'cmp_common_attachments_hide_extensions' => 0,
          'cmp_common_attachments_type' => 'files',
          'image_style' => 'cmp_attachments_255x177',
        ),
        'type' => 'cmp_common_attachments',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'tours_for_rep' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'view_final_report' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_attachements',
    'label' => 'Attachements',
    'required' => 0,
    'settings' => array(
      'description_field' => 1,
      'file_directory' => 'tutorials_attachements',
      'file_extensions' => 'png jpg jpeg gif doc pdf xls zip mp4',
      'max_filesize' => '10 MB',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'cmp_common',
      'settings' => array(
        'cmp_common_attachments_count' => 0,
        'cmp_common_attachments_hide_extensions' => 0,
        'cmp_common_attachments_popup_link_text' => 'Browse',
        'cmp_common_attachments_type' => 'files',
        'image_style' => 'cmp_attachments_255x177',
        'max_resolution' => '',
        'min_resolution' => '',
      ),
      'type' => 'cmp_common_attachments',
      'weight' => 38,
    ),
  );

  // Exported field_instance: 'node-tutorial-field_description'.
  $field_instances['node-tutorial-field_description'] = array(
    'bundle' => 'tutorial',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'tours_for_rep' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'view_final_report' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_description',
    'label' => 'Tutorial details',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'textarea_maxlength_js_widget',
      'settings' => array(
        'enable_recommended_maxlength' => 1,
        'format' => 'plain_text',
        'recommended_maxlength' => 1000,
        'rows' => 5,
      ),
      'type' => 'text_textarea_maxlength_js',
      'weight' => 33,
    ),
  );

  // Exported field_instance: 'node-tutorial-field_links'.
  $field_instances['node-tutorial-field_links'] = array(
    'bundle' => 'tutorial',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'tours_for_rep' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'view_final_report' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_links',
    'label' => 'Links',
    'required' => 0,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => '',
        'configurable_class' => 0,
        'configurable_title' => 0,
        'rel' => '',
        'target' => '_blank',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'rel_remove' => 'default',
      'title' => 'none',
      'title_label_use_field_label' => 1,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 36,
    ),
  );

  // Exported field_instance: 'node-tutorial-field_tutorial_type'.
  $field_instances['node-tutorial-field_tutorial_type'] = array(
    'bundle' => 'tutorial',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'tours_for_rep' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'view_final_report' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_tutorial_type',
    'label' => 'Type',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 34,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Attachements');
  t('Links');
  t('Tutorial details');
  t('Type');

  return $field_instances;
}
