<?php
/**
 * @file
 * cmp_examples.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function cmp_examples_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_:tasks/%.
  $menu_links['main-menu_:tasks/%'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'tasks/%',
    'router_path' => 'tasks/%',
    'link_title' => '',
    'options' => array(
      'identifier' => 'main-menu_:tasks/%',
    ),
    'module' => 'system',
    'hidden' => -1,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'main-menu_tasks:tasks',
  );


  return $menu_links;
}
