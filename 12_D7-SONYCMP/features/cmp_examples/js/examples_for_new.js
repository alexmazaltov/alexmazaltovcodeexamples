var tutorialType = Drupal.t('Example');
//Add event for Changing type for buttons and titles based on choosed type in field "Select type"
(function ($, Drupal) {
    Drupal.behaviors.newTutorialTypeTrigger = {
        attach: function (context, settings) {

            $('body').once(function(){
                $('#edit-tutorial-type').off('click').on('click',function (e) {
                    tutorialType = $('.form-item-tutorial-type input:checked+label').text();
                    $('.title-wrapper h3').text(Drupal.t('Create New ')+tutorialType);
                    $('#edit-label label').each(function(){
                        asterix = $(this).find('.form-required');
                        $(this).text(tutorialType + Drupal.t(' Details '));
                        $(this).append(asterix);
                    });
                    $('.form-item-title input').prop('placeholder', tutorialType + Drupal.t(' Name'));
                    $('.form-item-description textarea').prop('placeholder', Drupal.t('Describe what the ') + tutorialType + Drupal.t(' is for...'));
                    $('#edit-submit').text(Drupal.t('Publish ')+ tutorialType);

                })
            })

            $('#edit-tutorial-type').trigger('click');

        }
    };

})(jQuery, Drupal);