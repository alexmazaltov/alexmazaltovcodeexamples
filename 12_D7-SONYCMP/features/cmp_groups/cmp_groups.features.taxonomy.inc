<?php
/**
 * @file
 * cmp_groups.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function cmp_groups_taxonomy_default_vocabularies() {
  return array(
    'group' => array(
      'name' => 'Group',
      'machine_name' => 'group',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
