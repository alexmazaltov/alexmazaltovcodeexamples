<?php
/**
 * @file
 * cmp_groups.ife.inc
 */

/**
 * Implements hook_ife_default_settings().
 */
function cmp_groups_ife_default_settings() {
  $export = array();

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_groups_add_new_group';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_groups_add_new_group'] = $ife;

  return $export;
}
