<?php
/**
 * @file
 * Code for the Sony CMP Groups feature.
 */

define('GROUP_C_FORM_ID', 'group_new_add');
define('GROUP_U_FORM_ID', 'group_update');

include_once 'cmp_groups.features.inc';

function cmp_groups_menu() {

  $items['groups/%node/remove/%user'] = array(
    'title' => 'Remove from group',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cmp_groups_remove_from_group', 1, 3),
    'access callback' => 'cmp_core_user_is_manager',
    'modal' => TRUE,
  );
  return $items;
}

function cmp_groups_remove_from_group($form, &$form_state, $group, $rep) {
  $rep_name = $rep->field_first_name[LANGUAGE_NONE][0]['value'] . ' ' . $rep->field_last_name[LANGUAGE_NONE][0]['value'];
  $form_state['storage']['group_nid'] = $group->nid;
  $form_state['storage']['rep_uid'] = $rep->uid;
  $form_state['storage']['rep_name'] = $rep_name;
  $confirm_form = confirm_form($form,
    t('Are you sure you want to remove?'),
    'groups/' . $group->nid,
    t('Are you sure you want to remove <br /><strong>@rep_name</strong> from @group_title?<br />', array(
      '@rep_name' => $rep_name,
      '@group_title' => $group->title
    )),
    t('Yes'),
    t('Cancel')
  );
  $submit_button = $confirm_form['actions']['submit'];
  unset($confirm_form['actions']['submit']);
  $confirm_form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), '#',
      array(
        'attributes' => array(
          'class' => 'ctools-close-modal button'
        ),
        'external' => TRUE
      )
    )
  );
  $confirm_form['actions'][] = $submit_button;
  $confirm_form['actions']['#prefix'] = '<div class="confirm-actions-wrapper">';
  $confirm_form['actions']['#suffix'] = '</div>';
  return $confirm_form;
}

function cmp_groups_remove_from_group_submit($form, &$form_state) {
  $group = node_load($form_state['storage']['group_nid']);
  $rep = user_load($form_state['storage']['rep_uid']);
  $rep_name = $form_state['storage']['rep_name'];
  foreach ($group->field_reps[LANGUAGE_NONE] as $key => $referenced_user) {
    if ($referenced_user['target_id'] == $rep->uid) {
      unset($group->field_reps[LANGUAGE_NONE][$key]);
      break;
    }
  }
  node_save($group);
  drupal_set_message(t('Rep !rep_name was removed from group !group_title', array(
    '!rep_name' => $rep_name,
    '!group_title' => $group->title
  )));
  $commands[] = ctools_modal_command_dismiss();
  $commands[] = ctools_ajax_command_reload();
  print ajax_render($commands);
  exit;
}


/**
 * Implements hook_block_info()
 */
function cmp_groups_block_info() {
  $blocks['create_group_button'] = array(
    'info' => t('Create Group Button'),
  );
  $blocks['group_form'] = array(
    'info' => t('Group Form'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['groups_as_filters'] = array(
    'info' => t('Groups as filters for multistep form'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view()
 */
function cmp_groups_block_view($delta = '') {
  global $user;
  $block = array();
  switch ($delta) {
    case 'create_group_button':
      $block['subject'] = t('Create Group Button');
      $block['content'] = cmp_groups_get_group_button();
      break;
    case 'groups_as_filters':
      $block['subject'] = t('Groups');
      $block['content'] = cmp_groups_get_groups_as_filters();
      break;
    case 'group_form':
      $group = NULL;
      if (arg(1) && is_numeric(arg(1)) && arg(1) != 0) {
        $group = node_load(arg(1));
        if ($group->type != 'group') {
          drupal_goto('groups');
        }
        elseif ($user->uid > 1 && !$group->status) {
          drupal_goto('groups/' . $group->nid);
        }
      }
      $step = NULL;
      if (arg(3)) {
        $step = arg(3);
      }

      $block['subject'] = t('Create New Group');
      $block['content'] = cmp_groups_create_group_wizard($step, $group);
      break;
  }
  return $block;
}

function cmp_groups_get_group_button() {
  $output = '';
  if (arg(0) == 'groups') {
    if (arg(1) && is_numeric(arg(1)) && $group = node_load(arg(1))) {
      if ($group->type == 'group') {
        if ($group->status) {
          $output = l(t('Edit Group'), 'groups/' . $group->nid . '/edit', array('attributes' => array('class' => array('button'))));
        }
        else {
          $output = l(t('Edit Group'), 'groups/' . $group->nid, array(
            'attributes' => array(
              'onclick' => 'return false;',
              'class' => array('button', 'disabled')
            )
          ));
        }
      }
    }
    else {
      $output = l(t('Add New Group'), 'groups/new/add', array('attributes' => array('class' => array('button'))));
    }
  }
  return $output;
}

function cmp_groups_get_groups_as_filters() {
  $q = db_select('node', 'n');
  $q->fields('n', array('nid', 'title'));
  $q->condition('n.status', NODE_PUBLISHED)
    ->condition('n.type', 'group');
  $q->orderBy('n.created', 'DESC');
  $result = $q->execute()->fetchAllKeyed();
  $items = array();
  foreach ($result as $nid => $title) {
    $items[] = l($title, '#', array(
      'external' => TRUE,
      'attributes' => array('data-group-nid' => $nid)
    ));
  }

  $output = '<h2>' . t('Groups') . '</h2>';
  $output .= theme('item_list', array('items' => $items));

  return $output;
}

function cmp_groups_create_group_wizard($step = NULL, $group = NULL) {

  $form_info = array(
    //form id
    'id' => GROUP_C_FORM_ID,
    //pass the step we're on to the form, step1, step2, ..etc
    'path' => "groups/new/add/%step",
    //hide the breadcrumb / path trail for the forms
    'show trail' => FALSE,
    //show the back button
    'show back' => FALSE,
    //hide the cancel button
    'show cancel' => FALSE,
    //hide the update and return button
    'show return' => FALSE,
    //a callback function to run when the next button is pressed
    'next callback' => 'cmp_groups_create_group_wizard_next',
    //callback when finish button is pressed
    'finish callback' => 'cmp_groups_create_group_wizard_finish',
    'order' => array( // this controls order, as well as form labels
      'step1' => t('Add New Group'),
      'step2' => t('Assign Reps to Group'),
    ),
    'forms' => array( // here we map a step to a form id.
      'step1' => array(
        'form id' => 'cmp_groups_add_new_group',
      ),
      'step2' => array(
        'form id' => 'cmp_groups_assign_reps',
      ),
    ),
  );

  if (isset($group->nid) && $group->type == 'group') {
    $form_info['path'] = 'groups/' . $group->nid . '/edit/%step';
  }

  $object_id = isset($group->nid) ? $group->nid : 1; //1 - is object_id for new group

  if (empty($step)) {
    // We reset the form when $step is NULL because that means they have
    // for whatever reason started over.
    cmp_groups_cache_clear($object_id);
    $step = 'step1';
  }

  // This automatically gets defaults if there wasn't anything saved.
  $object = cmp_groups_cache_get($object_id);

  // live $form_state changes.
  $form_state = array(
    // Put our object and ID into the form state cache so we can easily find it.
    'object_id' => $object_id,
    'object' => &$object,
  );

  // Send this all off to our form. This is like drupal_get_form only wizardy.
  ctools_include('wizard');
  $form = ctools_wizard_multistep_form($form_info, $step, $form_state);

  $output = drupal_render($form);

  if ($output === FALSE || !empty($form_state['complete'])) {
    $message = cmp_groups_create_group_wizard_publish($object);
    drupal_set_message($message, 'group-was-saved');
    //Update cache for this group after updating
    cmp_groups_cache_clear($object_id);
    drupal_goto('groups');
  }
  else {
    return $output;
  }

}

/**
 * Clears the wizard cache.
 *
 * @param integer $id
 *   cache id.
 */
function cmp_groups_cache_clear($id) {
  ctools_include('object-cache');
  ctools_object_cache_clear(GROUP_C_FORM_ID, $id);
}

/**
 * Stores our little cache so that we can retain data from form to form.
 *
 * @param integer $id
 *   cache id.
 * @param object $object
 *   object with form values.
 */
function cmp_groups_cache_set($id, $object) {
  ctools_include('object-cache');
  ctools_object_cache_set(GROUP_C_FORM_ID, $id, $object);
}

/**
 * Gets the current object from the cache, or default.
 *
 * @param integer $id
 *   cache id.
 *
 * @return object
 *   cache with stored stuff.
 */
function cmp_groups_cache_get($id) {
  ctools_include('object-cache');
  $object = ctools_object_cache_get(GROUP_C_FORM_ID, $id);

  if (!$object) {
    $object_group = node_load($id);
    if (!$object_group) {
      //Create empty group object to process in multistep form
      global $user;
      $type = 'group';
      // Create a default empty object for group node.
      $object = (object) array(
        'uid' => $user->uid,
        'name' => (isset($user->name) ? $user->name : ''),
        'type' => $type,
        'language' => LANGUAGE_NONE,
        'is_new' => TRUE,
      );
    }
    else {
      $object = (object) array(
        'nid' => $object_group->nid,
        'uid' => $object_group->uid,
        'name' => $object_group->name,
        'title' => $object_group->title,
        'type' => $object_group->type,
        'language' => $object_group->language,
        'field_one_liner' => $object_group->field_one_liner[LANGUAGE_NONE][0]['value'],
        'field_reps' => isset($object_group->field_reps[LANGUAGE_NONE]) ? _cmp_common_get_target_ids_array($object_group->field_reps[LANGUAGE_NONE]) : array(),
        'is_new' => FALSE,
      );
    }

  }

  return $object;
}

/**
 * Handles the 'next' click on the add/edit pane form wizard.
 *
 * All we need to do is store the updated pane in the cache.
 */
function cmp_groups_create_group_wizard_next(&$form_state) {
  cmp_groups_cache_set($form_state['object_id'], $form_state['object']);
}

/**
 * Handles the 'finish' click on teh add/edit pane form wizard.
 *
 * All we need to do is set a flag so the return can handle adding
 * the pane.
 */
function cmp_groups_create_group_wizard_finish(&$form_state) {
  $form_state['complete'] = TRUE;
}

/**
 * Helper function that generate form-header for each step
 */
function cmp_groups_get_group_header(&$form, $form_state) {
  $form['#prefix'] = '<div class="back-link">' . l(t('< Back to All Groups'), 'groups') . '</div>';
  $form['group_header'] = array(
    '#type' => 'item',
    '#title' => t('Header'),
    '#title_display' => 'invisible',
    '#prefix' => '<div class="row">',
    '#suffix' => '</div><!-- .row header-->',
  );
  $form['group_header']['actions'] = array(
    '#type' => 'actions',
    '#prefix' => $form_state['step'] == 'step1' ? '<div class="column large-6 small-8 buttons-wrapper">' : '<div class="column large-6 small-7 buttons-wrapper">',
    '#suffix' => '</div></div><!-- .column.large-6 small-8 -->',
  );
  $form['group_header']['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'groups', array(
      'attributes' => array(
        'class' => array(
          'cancel-button',
          'button'
        )
      )
    )),
    '#weight' => -2000,
  );

  switch ($form_state['step']) {
    case 'step1':
      $form['group_header']['header_title'] = array(
        '#markup' => t('Create New Group'),
        '#prefix' => '<div class="column large-6 small-4"><h2>',
        '#suffix' => '</h2></div><!-- .column.large-6 small-4 -->',
      );

      $form['group_header']['actions']['assign_reps'] = array(
        '#type' => 'submit',
        '#value' => t('Assign Reps >'),
        '#next' => 'step2',
        '#wizard type' => 'next',
        '#weight' => -1000,
      );

      //For editing group we should add button to save group at first step
      if ($form_state['object_id'] !== 1) {
        $form['group_header']['header_title']['#markup'] = t('Edit Group');
        $form['group_header']['actions']['assign_reps']['#value'] = t('Edit Reps >');
        $form['group_header']['actions']['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Save and Close'),
          '#wizard type' => 'finish',
          '#weight' => -500,
          '#attributes' => array('class' => array('save-button')),
        );
      }
      break;

    case 'step2':
      $form['group_header']['header_group_details'] = array(
        '#type' => 'item',
        '#title' => t('Group details'),
        '#title_display' => 'invisible',
        '#prefix' => '<div class="column large-6 small-5">',
        '#suffix' => '</div><!-- .column.large-6 small-5 -->',
      );
      $form['group_header']['header_group_details']['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Group title'),
        '#title_display' => 'invisible',
        '#default_value' => $form_state['object']->title,
        '#attributes' => array('class' => array('not-clicked')),
      );
      $form['group_header']['header_group_details']['header_group_description'] = array(
        '#markup' => $form_state['object']->field_one_liner,
        '#prefix' => '<div class="group-description">',
        '#suffix' => '</div>',
      );
      $form['group_header']['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Publish Group'),
        '#wizard type' => 'finish',
        '#weight' => -1000,
      );

      //For editing group we should change value on the submit button
      if ($form_state['object_id'] !== 1) {
        $form['group_header']['actions']['submit']['#value'] = t('Save Changes');
      }
      break;
  }

  return $form;
}

/**
 * Helper function that generate form-trail for each step
 */
function cmp_groups_get_group_trail(&$form, $form_state) {
  $class = ($form_state['step'] == 'step1') ? ' progress-trail' : '';
  $form['group_trail'] = array(
    '#type' => 'item',
    '#title' => t('Group trail'),
    '#title_display' => 'invisible',
    '#prefix' => '<div class="row"><div class="group-trail' . $class . '">',
    '#suffix' => '</div></div><!-- .row trail-->',
  );
  $class = ($form_state['step'] == 'step1') ? 'active' : '';
  $form['group_trail']['step_1'] = array(
    "#markup" => t('Details'),
    '#prefix' => '<span class="step-1 ' . $class . '">',
    '#suffix' => '</span>',
  );
  $class = ($form_state['step'] == 'step2') ? 'active' : '';
  $form['group_trail']['step_2'] = array(
    "#markup" => t('Reps'),
    '#prefix' => '<span class="step-2 ' . $class . '">',
    '#suffix' => '</span>',
  );
  $form['group_trail']['step_3'] = array(
    "#markup" => t('Publish'),
    '#prefix' => '<span class="step-3">',
    '#suffix' => '</span>',
  );

  return $form;
}


function cmp_groups_add_new_group($form, &$form_state) {
  cmp_groups_get_group_header($form, $form_state);
  cmp_groups_get_group_trail($form, $form_state);

  $form['group_main'] = array(
    '#type' => 'item',
    '#title_display' => 'invisible',
    '#title' => t('Group Details'),
    '#prefix' => '<div class="row">',
    '#suffix' => '</div><!-- .row main-->',
  );
  $form['group_main']['left'] = array(
    '#type' => 'item',
    '#title' => t('left column 3 placeholder'),
    '#title_display' => 'invisible',
    '#prefix' => '<div class="column large-3 placeholder">',
    '#suffix' => '</div><!-- .column .left-->',
  );
  $form['group_main']['center'] = array(
    '#type' => 'item',
    '#title' => t('Group Details'),
    '#required' => TRUE,
    '#prefix' => '<div class="column large-6 placeholder">',
    '#suffix' => '</div><!-- .column .center-->',
  );
  $form['group_main']['right'] = array(
    '#type' => 'item',
    '#title' => t('Right column 3 placeholder'),
    '#title_display' => 'invisible',
    '#prefix' => '<div class="column large-3 placeholder">',
    '#suffix' => '</div><!-- .column .right-->',
  );

  $form['group_main']['center']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Group Name'),
    '#title_display' => 'invisible',
    '#attributes' => array('placeholder' => t('Group Name')),
    '#default_value' => isset($form_state['object']->title) ? $form_state['object']->title : '',
    '#required' => TRUE,
  );
  $form['group_main']['center']['field_one_liner'] = array(
    '#type' => 'textfield',
    '#title' => t('Group Description'),
    '#title_display' => 'invisible',
    '#attributes' => array('placeholder' => t('Add a short description of the Group')),
    '#default_value' => isset($form_state['object']->field_one_liner) ? $form_state['object']->field_one_liner : '',
    '#required' => TRUE,
  );
  //Unset default wizard prev-next buttons
  unset($form['buttons']);

  return $form;

}

function cmp_groups_add_new_group_submit($form, &$form_state) {
  $form_state['object']->title = $form_state['values']['title'];
  $form_state['object']->field_one_liner = $form_state['values']['field_one_liner'];
}

function cmp_groups_assign_reps($form, &$form_state) {
  cmp_groups_get_group_header($form, $form_state);
  cmp_groups_get_group_trail($form, $form_state);

  $form['group_main'] = array(
    '#type' => 'item',
    '#title' => t('The View'),
    '#title_display' => 'invisible',
    '#required' => TRUE,
    '#prefix' => '<div class="row">',
    '#suffix' => '</div><!-- .row main-->',
  );

  $field_reps = isset($form_state['object']->field_reps) ? $form_state['object']->field_reps : array();

  $form['group_main']['field_reps'] = array(
    '#type' => 'checkboxes',
    '#options' => $field_reps,
    '#default_value' => $field_reps,
  );
  $form['group_main']['new_assigned_uids'] = array(
    '#type' => 'hidden',
    '#title' => t('New assigned uids'),
    '#attributes' => array('style' => 'display:none;', 'id' => 'edit-new-assigned-uids')
  );

  $form['group_main']['field_reps_uid'] = array(
    '#type' => 'hidden',
    '#value' => ''
  );

  //Add suffix to that step (where is implemented all frontend logic according to design)
  $form['#suffix'] = theme('cmp_gmf_step2_suffix', array(
    'field_reps' => $field_reps,
    'assign_to' => t('Group')
  ));

  //Unset default wizard prev-next buttons
  unset($form['buttons']);

  return $form;
}


function cmp_groups_assign_reps_submit($form, &$form_state) {
  $form_state['object']->field_reps = array_merge(array_filter($form_state['values']['field_reps']), drupal_map_assoc(array_filter(explode(',', $form_state['values']['new_assigned_uids']))));
  $form_state['object']->title = $form_state['values']['title'];
}

function cmp_groups_create_group_wizard_publish($object) {

  $assigned_reps = array_filter($object->field_reps);

  if ($object->is_new) {
    $node = new stdClass();
    $node->type = 'group';
    $node->language = LANGUAGE_NONE;
    node_object_prepare($node);

    $node->status = 1;
    $node->promote = 0;
    $node->sticky = 0;
    $node->comment = 0;
    $node->uid = $object->uid;
  }
  else {
    $node = node_load($object->nid);
  }

  $node->title = $object->title;
  $node->field_one_liner[$node->language][0]['value'] = $object->field_one_liner;

  $assigned_reps_update = array();
  foreach ($assigned_reps as $key => $assign_this_one) {
    $assigned_reps_update[] = array('target_id' => $assign_this_one);
  }
  $node->field_reps[LANGUAGE_NONE] = $assigned_reps_update;

  node_save($node);
  if ($object->is_new) {
    $message = t('%group_name has been created!', array('%group_name' => $object->title));
  }
  else {
    $message = t('Changes have been saved!');
  }

  return $message;
}


function cmp_groups_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  if (in_array($root_path, array('groups', 'groups/archived'))) {
    foreach ($data['tabs'][0]['output'] as $i => $link) {
      switch ($link['#link']['path']) {
        case "groups/list":
          $count = cmp_groups_get_groups_count();
          break;
        case "groups/archived":
          $count = cmp_groups_get_groups_count(TRUE);
          break;
      }
      $data['tabs'][0]['output'][$i]['#link']['title'] .= ' (' . $count . ')';
    }
  }
}

function cmp_groups_get_groups_count($archived = FALSE) {
  global $user;
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'group', '=')
    ->propertyCondition('status', !$archived);
  $query->addMetaData('account', $user); // Run the query as current user.
  return $query->count()->execute();
}

function cmp_groups_form_views_exposed_form_alter(&$form, &$form_state) {
  if ($form['#id'] == 'views-exposed-form-groups-panel-groups') {
    $form['title']['#attributes']['placeholder'][] = t('Quickly find a group');
  }
  if ($form['#id'] == 'views-exposed-form-groups-panel-archived-groups') {
    $form['title']['#attributes']['placeholder'][] = t('Quickly find a group');
  }
  if ($form['#id'] == 'views-exposed-form-groups-panel-group-members') {
    $form['combine']['#attributes']['placeholder'][] = t('Quickly find a rep');
  }
}
