<?php
/**
 * @file
 * cmp_groups.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function cmp_groups_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'archived_groups';
  $page->task = 'page';
  $page->admin_title = 'Archived Groups';
  $page->admin_description = 'Groups list page';
  $page->path = 'groups/archived';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Archived',
    'name' => 'navigation',
    'weight' => '1',
    'parent' => array(
      'type' => 'normal',
      'title' => 'Tasks',
      'name' => 'main-menu',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_archived_groups__panel_context_1c5c460b-fb34-4915-9f02-609d88fb0c73';
  $handler->task = 'page';
  $handler->subtask = 'archived_groups';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'grouplist';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '003073fc-4e56-4cb5-91b3-067f64cde79c';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-32102fbd-6918-4f3c-8596-feb0cd5cb863';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '32102fbd-6918-4f3c-8596-feb0cd5cb863';
    $display->content['new-32102fbd-6918-4f3c-8596-feb0cd5cb863'] = $pane;
    $display->panels['above_left'][0] = 'new-32102fbd-6918-4f3c-8596-feb0cd5cb863';
    $pane = new stdClass();
    $pane->pid = 'new-ec16bd99-0543-4c22-81a5-4f9d654a4b7c';
    $pane->panel = 'above_right';
    $pane->type = 'block';
    $pane->subtype = 'cmp_groups-create_group_button';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ec16bd99-0543-4c22-81a5-4f9d654a4b7c';
    $display->content['new-ec16bd99-0543-4c22-81a5-4f9d654a4b7c'] = $pane;
    $display->panels['above_right'][0] = 'new-ec16bd99-0543-4c22-81a5-4f9d654a4b7c';
    $pane = new stdClass();
    $pane->pid = 'new-d8edf88c-525b-431b-ab20-0e1ae537215e';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'groups-panel_archived_groups';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd8edf88c-525b-431b-ab20-0e1ae537215e';
    $display->content['new-d8edf88c-525b-431b-ab20-0e1ae537215e'] = $pane;
    $display->panels['middle'][0] = 'new-d8edf88c-525b-431b-ab20-0e1ae537215e';
    $pane = new stdClass();
    $pane->pid = 'new-a0bd487a-ab5c-4a84-b093-776b4a742207';
    $pane->panel = 'top_left';
    $pane->type = 'page_tabs';
    $pane->subtype = 'page_tabs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'type' => 'both',
      'id' => 'tabs',
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a0bd487a-ab5c-4a84-b093-776b4a742207';
    $display->content['new-a0bd487a-ab5c-4a84-b093-776b4a742207'] = $pane;
    $display->panels['top_left'][0] = 'new-a0bd487a-ab5c-4a84-b093-776b4a742207';
    $pane = new stdClass();
    $pane->pid = 'new-69e35f9c-3550-47a7-888b-72228269439c';
    $pane->panel = 'top_right';
    $pane->type = 'block';
    $pane->subtype = 'views-6d761e415b21df66b22ee30c100bf664';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'inherit_path' => 1,
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'pane-views-exp-groups-panel-archived-groups',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '69e35f9c-3550-47a7-888b-72228269439c';
    $display->content['new-69e35f9c-3550-47a7-888b-72228269439c'] = $pane;
    $display->panels['top_right'][0] = 'new-69e35f9c-3550-47a7-888b-72228269439c';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['archived_groups'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'create_new_group';
  $page->task = 'page';
  $page->admin_title = 'Create New Group';
  $page->admin_description = '';
  $page->path = 'groups/new/add';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'action',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_create_new_group__panel_context_86be042a-6a71-4adb-b4dd-155ff1757375';
  $handler->task = 'page';
  $handler->subtask = 'create_new_group';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'group_edit';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'middle' => NULL,
      'bottom' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Create New Group';
  $display->uuid = '4c8b067a-c408-4a1d-8956-3268a870becd';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $display->content['new-b78d2652-77fe-49fc-852f-ed71a4bbd880'] = $pane;
    $display->panels['above_left'][0] = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $pane = new stdClass();
    $pane->pid = 'new-e6ba5da6-9e9a-4a86-a5f9-758d029718a9';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'cmp_groups-group_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e6ba5da6-9e9a-4a86-a5f9-758d029718a9';
    $display->content['new-e6ba5da6-9e9a-4a86-a5f9-758d029718a9'] = $pane;
    $display->panels['middle'][0] = 'new-e6ba5da6-9e9a-4a86-a5f9-758d029718a9';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['create_new_group'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'edit_group';
  $page->task = 'page';
  $page->admin_title = 'Edit Group';
  $page->admin_description = '';
  $page->path = 'groups/%node/edit';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'node_type',
        'settings' => array(
          'type' => array(
            'group' => 'group',
          ),
        ),
        'context' => 'argument_entity_id:node_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'action',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'node' => array(
      'id' => 1,
      'identifier' => 'Group ID',
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_edit_group__panel_context_0c61e7c5-5933-4c53-8f00-e2e53abe57fe';
  $handler->task = 'page';
  $handler->subtask = 'edit_group';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'group_edit';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'middle' => NULL,
      'bottom' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Edit Group';
  $display->uuid = '4c8b067a-c408-4a1d-8956-3268a870becd';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $display->content['new-b78d2652-77fe-49fc-852f-ed71a4bbd880'] = $pane;
    $display->panels['above_left'][0] = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
    $pane = new stdClass();
    $pane->pid = 'new-e6ba5da6-9e9a-4a86-a5f9-758d029718a9';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'cmp_groups-group_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e6ba5da6-9e9a-4a86-a5f9-758d029718a9';
    $display->content['new-e6ba5da6-9e9a-4a86-a5f9-758d029718a9'] = $pane;
    $display->panels['middle'][0] = 'new-e6ba5da6-9e9a-4a86-a5f9-758d029718a9';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['edit_group'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'group_detail';
  $page->task = 'page';
  $page->admin_title = 'Group detail';
  $page->admin_description = '';
  $page->path = 'groups/%node';
  $page->access = array(
    'logic' => 'and',
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'node_type',
        'settings' => array(
          'type' => array(
            'group' => 'group',
          ),
        ),
        'context' => 'argument_entity_id:node_1',
        'not' => FALSE,
      ),
    ),
  );
  $page->menu = array(
    'type' => 'action',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'node' => array(
      'id' => 1,
      'identifier' => 'Group ID',
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_group_detail__panel_context_4f25f82b-79a7-4a84-8f8b-d65d7b9c2077';
  $handler->task = 'page';
  $handler->subtask = 'group_detail';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'group_details';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'sidebar' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'middle' => NULL,
      'comments' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '78b64988-3b05-4197-be0f-002918c2b6f6';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-2060405d-cde1-4c8f-a22a-415b119218a5';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2060405d-cde1-4c8f-a22a-415b119218a5';
    $display->content['new-2060405d-cde1-4c8f-a22a-415b119218a5'] = $pane;
    $display->panels['above_left'][0] = 'new-2060405d-cde1-4c8f-a22a-415b119218a5';
    $pane = new stdClass();
    $pane->pid = 'new-1c9b8519-d7eb-4973-b123-17f840338d15';
    $pane->panel = 'above_right';
    $pane->type = 'block';
    $pane->subtype = 'cmp_groups-create_group_button';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1c9b8519-d7eb-4973-b123-17f840338d15';
    $display->content['new-1c9b8519-d7eb-4973-b123-17f840338d15'] = $pane;
    $display->panels['above_right'][0] = 'new-1c9b8519-d7eb-4973-b123-17f840338d15';
    $pane = new stdClass();
    $pane->pid = 'new-9984b42b-8bd0-40d1-8722-f0fef96078d5';
    $pane->panel = 'middle';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'teaser',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9984b42b-8bd0-40d1-8722-f0fef96078d5';
    $display->content['new-9984b42b-8bd0-40d1-8722-f0fef96078d5'] = $pane;
    $display->panels['middle'][0] = 'new-9984b42b-8bd0-40d1-8722-f0fef96078d5';
    $pane = new stdClass();
    $pane->pid = 'new-44a48f71-5d17-4098-b07b-b06aa48528ea';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'groups-panel_group_members';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '44a48f71-5d17-4098-b07b-b06aa48528ea';
    $display->content['new-44a48f71-5d17-4098-b07b-b06aa48528ea'] = $pane;
    $display->panels['middle'][1] = 'new-44a48f71-5d17-4098-b07b-b06aa48528ea';
    $pane = new stdClass();
    $pane->pid = 'new-c561d827-e37e-42fc-9b99-266d9efbf657';
    $pane->panel = 'sidebar';
    $pane->type = 'views_panes';
    $pane->subtype = 'groups-panel_groups_sidebar';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c561d827-e37e-42fc-9b99-266d9efbf657';
    $display->content['new-c561d827-e37e-42fc-9b99-266d9efbf657'] = $pane;
    $display->panels['sidebar'][0] = 'new-c561d827-e37e-42fc-9b99-266d9efbf657';
    $pane = new stdClass();
    $pane->pid = 'new-0629d21d-5893-4195-8198-de43fa55da0b';
    $pane->panel = 'top_right';
    $pane->type = 'block';
    $pane->subtype = 'views--exp-groups-panel_group_members';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'inherit_path' => 1,
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0629d21d-5893-4195-8198-de43fa55da0b';
    $display->content['new-0629d21d-5893-4195-8198-de43fa55da0b'] = $pane;
    $display->panels['top_right'][0] = 'new-0629d21d-5893-4195-8198-de43fa55da0b';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['group_detail'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'groups';
  $page->task = 'page';
  $page->admin_title = 'Groups';
  $page->admin_description = 'Groups list page';
  $page->path = 'groups/list';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'default tab',
    'title' => 'Groups',
    'name' => 'main-menu',
    'weight' => '-1',
    'parent' => array(
      'type' => 'normal',
      'title' => 'Groups',
      'name' => 'main-menu',
      'weight' => '-48',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_groups__panel_context_42b4996d-affb-4772-8328-bed5ac81208a';
  $handler->task = 'page';
  $handler->subtask = 'groups';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'grouplist';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'middle' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '87bea2ef-9288-43be-a6dc-1841976ea4d3';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-6f457f11-746c-4111-8aee-8117a6138f17';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '6f457f11-746c-4111-8aee-8117a6138f17';
    $display->content['new-6f457f11-746c-4111-8aee-8117a6138f17'] = $pane;
    $display->panels['above_left'][0] = 'new-6f457f11-746c-4111-8aee-8117a6138f17';
    $pane = new stdClass();
    $pane->pid = 'new-261e75d9-63bf-4162-9dc0-3b3bf3e35bc5';
    $pane->panel = 'above_right';
    $pane->type = 'block';
    $pane->subtype = 'cmp_groups-create_group_button';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '261e75d9-63bf-4162-9dc0-3b3bf3e35bc5';
    $display->content['new-261e75d9-63bf-4162-9dc0-3b3bf3e35bc5'] = $pane;
    $display->panels['above_right'][0] = 'new-261e75d9-63bf-4162-9dc0-3b3bf3e35bc5';
    $pane = new stdClass();
    $pane->pid = 'new-d51d11d5-da34-471f-83c5-a355a2478ff8';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'groups-panel_groups';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd51d11d5-da34-471f-83c5-a355a2478ff8';
    $display->content['new-d51d11d5-da34-471f-83c5-a355a2478ff8'] = $pane;
    $display->panels['middle'][0] = 'new-d51d11d5-da34-471f-83c5-a355a2478ff8';
    $pane = new stdClass();
    $pane->pid = 'new-c8303b8e-0886-4695-a36d-282fe064bed2';
    $pane->panel = 'top_left';
    $pane->type = 'page_tabs';
    $pane->subtype = 'page_tabs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'type' => 'both',
      'id' => 'tabs',
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c8303b8e-0886-4695-a36d-282fe064bed2';
    $display->content['new-c8303b8e-0886-4695-a36d-282fe064bed2'] = $pane;
    $display->panels['top_left'][0] = 'new-c8303b8e-0886-4695-a36d-282fe064bed2';
    $pane = new stdClass();
    $pane->pid = 'new-5762bd0e-4340-4cf3-a122-cb1af8445180';
    $pane->panel = 'top_right';
    $pane->type = 'block';
    $pane->subtype = 'views--exp-groups-panel_groups';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'inherit_path' => 1,
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5762bd0e-4340-4cf3-a122-cb1af8445180';
    $display->content['new-5762bd0e-4340-4cf3-a122-cb1af8445180'] = $pane;
    $display->panels['top_right'][0] = 'new-5762bd0e-4340-4cf3-a122-cb1af8445180';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-6f457f11-746c-4111-8aee-8117a6138f17';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['groups'] = $page;

  return $pages;

}
