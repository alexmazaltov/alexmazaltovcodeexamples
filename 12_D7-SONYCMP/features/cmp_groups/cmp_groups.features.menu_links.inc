<?php
/**
 * @file
 * cmp_groups.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function cmp_groups_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: user-menu_groups:groups.
  $menu_links['user-menu_groups:groups'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'groups',
    'router_path' => 'groups',
    'link_title' => 'Groups',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'user-menu_groups:groups',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Groups');

  return $menu_links;
}
