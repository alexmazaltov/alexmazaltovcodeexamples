<?php
/**
 * @file
 * cmp_messages.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function cmp_messages_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-messages-block_messages'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'messages-block_messages',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(
      'administrator' => 3,
      'representative' => 5,
      'task manager' => 4,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'header_messages',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => -45,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -45,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
