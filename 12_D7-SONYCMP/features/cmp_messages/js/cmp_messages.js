(function ($, Drupal) {
  Drupal.behaviors.cmp_messages = {
    attach: function (context) {
      var count = Drupal.settings.cmp_messages_count || 0;
      if (count > 0) {
        $('#notifications-link span.badge', context).text(count);
      }
    }
  };

  /**
   * User notifications checkboxes.
   */
  Drupal.behaviors.sonycmp_notifications = {
    attach: function (context) {
      var $wrap = $('.view.view-display-id-panel_messages');
      if ($wrap.length == 0) {
        return false;
      }
      var $table = $('table tbody', $wrap);

      $('.form-type-checkbox input.form-checkbox', $table).unbind();
      $('.form-type-checkbox').each(function () {
        if ($('span.custom', $(this)).length == 0) {
          // We should add checkboxes because checkboxes used from zurb_foundation is not working.
          $(this).append('<span class="custom checkbox"></span>');
        }
      });
      $('.form-type-checkbox input.form-checkbox', $table).on('change', function () {
        var $span = $(this).parent().find('span.checkbox');
        if ($(this).is(':checked')) {
          $span.addClass('checked');
        }
        else {
          $span.removeClass('checked');
        }
      });
      if ($('input.vbo-table-select-all', $wrap).is(':checked')) {
        $('input.vbo-table-select-all', $wrap).trigger('change');
      }
    }
  };
  /**
   * User notifications select all.
   */
  Drupal.behaviors.sonycmp_notifications_select_all = {
    attach: function (context) {
      var $wrap = $('.view.view-display-id-panel_messages', context);
      if ($wrap.length == 0) {
        return false;
      }
      var $table = $('table tbody', $wrap);
      var $th = $('.vbo-table-select-all', $wrap).parents('th').next();
      if ($th.find('.select-all-label').length == 0) {
        $th.append('<label class="select-all-label">' + Drupal.t('Select All') + '</label>');
      }
      // Select all notifications.
      $('input.vbo-table-select-all', $wrap).on('change', function () {
        var checkboxes = $('input.vbo-select.form-checkbox', $wrap);
        var spans = $('.form-type-checkbox span', $wrap);
        checkboxes.prop('checked', $(this).is(':checked'));
        checkboxes.trigger('change');

      });
      //when one row unselected unselect all checkbopx too
      $('.vbo-select', $wrap).on('change', function() {
        var $select_all_checkbox = $('input.vbo-table-select-all', $wrap);
        if (!$(this).is(':checked') && $select_all_checkbox.is(':checked')) {
          $select_all_checkbox.prop('checked', false);
          $('.vbo-table-this-page', $wrap).show();
          $('.vbo-table-all-pages', $wrap).hide();
          $('.select-all-rows', $wrap).val('0');
          $('.views-table-row-select-all', $wrap).hide();
          $select_all_checkbox.next('span.checkbox').removeClass('checked');
        }
      });
    }
  };
})(jQuery, Drupal);
