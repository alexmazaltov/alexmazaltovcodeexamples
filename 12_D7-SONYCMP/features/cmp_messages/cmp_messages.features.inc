<?php
/**
 * @file
 * cmp_messages.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cmp_messages_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cmp_messages_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_message_type().
 */
function cmp_messages_default_message_type() {
  $items = array();
  $items['report_was_submitted'] = entity_import('message_type', '{
    "name" : "report_was_submitted",
    "description" : "Report was submitted",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "\\u003Cdiv class=\\u0022user-picture columns large-1 small-1\\u0022\\u003E\\r\\n   \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cimg src=\\u0022@{message:field-student:avatar}\\u0022 width=\\u002236\\u0022 height=\\u002236\\u0022 \\/\\u003E\\u003C\\/a\\u003E\\r\\n\\u003C\\/div\\u003E\\r\\n\\u003Cdiv class=\\u0022columns large-11 small-11\\u0022\\u003E\\r\\n   \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cspan class=\\u0022user-name\\u0022\\u003E@{message:field-student:field_first_name}\\u0026nbsp;@{message:field-student:field_last_name}\\u003C\\/span\\u003E completed\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\r\\n\\u003C\\/div\\u003E\\r\\n",
          "format" : "full_html",
          "safe_value" : "\\u003Cdiv class=\\u0022user-picture columns large-1 small-1\\u0022\\u003E\\n   \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cimg src=\\u0022@{message:field-student:avatar}\\u0022 width=\\u002236\\u0022 height=\\u002236\\u0022 \\/\\u003E\\u003C\\/a\\u003E\\n\\u003C\\/div\\u003E\\n\\u003Cdiv class=\\u0022columns large-11 small-11\\u0022\\u003E\\n   \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cspan class=\\u0022user-name\\u0022\\u003E@{message:field-student:field_first_name}\\u0026nbsp;@{message:field-student:field_last_name}\\u003C\\/span\\u003E completed\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\n\\u003C\\/div\\u003E\\n"
        }
      ]
    }
  }');
  $items['task_became_completed'] = entity_import('message_type', '{
    "name" : "task_became_completed",
    "description" : "Task became completed",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "\\u003Cdiv class=\\u0022columns large-12 small-12\\u0022\\u003E\\r\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E completed \\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\r\\n\\u003C\\/div\\u003E",
          "format" : "full_html",
          "safe_value" : "\\u003Cdiv class=\\u0022columns large-12 small-12\\u0022\\u003E\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E completed \\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\n\\u003C\\/div\\u003E"
        }
      ]
    }
  }');
  $items['task_became_overdue'] = entity_import('message_type', '{
    "name" : "task_became_overdue",
    "description" : "Task became overdue",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "\\u003Cdiv class=\\u0022user-picture columns large-1 small-1\\u0022\\u003E\\r\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cimg src=\\u0022@{message:field-student:avatar}\\u0022 width=\\u002236\\u0022 height=\\u002236\\u0022 \\/\\u003E\\u003C\\/a\\u003E\\r\\n\\u003C\\/div\\u003E\\r\\n\\u003Cdiv class=\\u0022columns large-11 small-11\\u0022\\u003E\\r\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cspan class=\\u0022user-name\\u0022\\u003E@{message:field-student:field_first_name}\\u0026nbsp;@{message:field-student:field_last_name}\\u003C\\/span\\u003E overdue\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\r\\n\\u003C\\/div\\u003E",
          "format" : "full_html",
          "safe_value" : "\\u003Cdiv class=\\u0022user-picture columns large-1 small-1\\u0022\\u003E\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cimg src=\\u0022@{message:field-student:avatar}\\u0022 width=\\u002236\\u0022 height=\\u002236\\u0022 \\/\\u003E\\u003C\\/a\\u003E\\n\\u003C\\/div\\u003E\\n\\u003Cdiv class=\\u0022columns large-11 small-11\\u0022\\u003E\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cspan class=\\u0022user-name\\u0022\\u003E@{message:field-student:field_first_name}\\u0026nbsp;@{message:field-student:field_last_name}\\u003C\\/span\\u003E overdue\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\n\\u003C\\/div\\u003E"
        }
      ]
    }
  }');
  $items['task_due_in_48h'] = entity_import('message_type', '{
    "name" : "task_due_in_48h",
    "description" : "Task due in 48h",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "\\u003Cdiv class=\\u0022columns large-12 small-12\\u0022\\u003E\\r\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003ETask is due in 48h\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E \\u003Cspan class=\\u0022due-date\\u0022\\u003E(Due [message:field-task:field-due-date:tiny])\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\r\\n\\u003C\\/div\\u003E",
          "format" : "full_html",
          "safe_value" : "\\u003Cdiv class=\\u0022columns large-12 small-12\\u0022\\u003E\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003ETask is due in 48h\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E \\u003Cspan class=\\u0022due-date\\u0022\\u003E(Due [message:field-task:field-due-date:tiny])\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\n\\u003C\\/div\\u003E"
        }
      ]
    }
  }');
  $items['task_was_assigned'] = entity_import('message_type', '{
    "name" : "task_was_assigned",
    "description" : "Task was assigned",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "\\u003Cdiv class=\\u0022columns large-12 small-12\\u0022\\u003E\\r\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cspan class=\\u0022user-name\\u0022\\u003E@{message:field-manager:field_first_name}\\u0026nbsp;@{message:field-manager:field_last_name}\\u003C\\/span\\u003E assigned you to\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E \\u003Cspan class=\\u0022due-date\\u0022\\u003E(Due [message:field-task:field-due-date:tiny])\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\r\\n\\u003C\\/div\\u003E",
          "format" : "full_html",
          "safe_value" : "\\u003Cdiv class=\\u0022columns large-12 small-12\\u0022\\u003E\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cspan class=\\u0022user-name\\u0022\\u003E@{message:field-manager:field_first_name}\\u0026nbsp;@{message:field-manager:field_last_name}\\u003C\\/span\\u003E assigned you to\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E \\u003Cspan class=\\u0022due-date\\u0022\\u003E(Due [message:field-task:field-due-date:tiny])\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\n\\u003C\\/div\\u003E"
        }
      ]
    }
  }');
  $items['task_was_associated_with_manager'] = entity_import('message_type', '{
    "name" : "task_was_associated_with_manager",
    "description" : "Task was asso\\u0441iated with manager",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "\\u003Cdiv class=\\u0022columns large-12 small-12\\u0022\\u003E\\r\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cspan class=\\u0022user-name\\u0022\\u003E@{message:field-manager:field_first_name}\\u0026nbsp;@{message:field-manager:field_last_name}\\u003C\\/span\\u003E assigned you to\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E \\u003Cspan class=\\u0022due-date\\u0022\\u003E(Due [message:field-task:field-due-date:tiny])\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\r\\n\\u003C\\/div\\u003E",
          "format" : "full_html",
          "safe_value" : "\\u003Cdiv class=\\u0022columns large-12 small-12\\u0022\\u003E\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cspan class=\\u0022user-name\\u0022\\u003E@{message:field-manager:field_first_name}\\u0026nbsp;@{message:field-manager:field_last_name}\\u003C\\/span\\u003E assigned you to\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E \\u003Cspan class=\\u0022due-date\\u0022\\u003E(Due [message:field-task:field-due-date:tiny])\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\n\\u003C\\/div\\u003E"
        }
      ]
    }
  }');
  $items['task_was_commented'] = entity_import('message_type', '{
    "name" : "task_was_commented",
    "description" : "Task was commented",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "\\u003Cdiv class=\\u0022columns large-12 small-12\\u0022\\u003E\\r\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cspan class=\\u0022user-name\\u0022\\u003E@{message:field-comment:author:field_first_name}\\u0026nbsp;@{message:field-comment:author:field_last_name}\\u003C\\/span\\u003E commented on\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\r\\n\\u003C\\/div\\u003E\\r\\n",
          "format" : "full_html",
          "safe_value" : "\\u003Cdiv class=\\u0022columns large-12 small-12\\u0022\\u003E\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cspan class=\\u0022user-name\\u0022\\u003E@{message:field-comment:author:field_first_name}\\u0026nbsp;@{message:field-comment:author:field_last_name}\\u003C\\/span\\u003E commented on\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\n\\u003C\\/div\\u003E\\n"
        }
      ]
    }
  }');
  $items['task_was_unassigned'] = entity_import('message_type', '{
    "name" : "task_was_unassigned",
    "description" : "Task was unassigned",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "\\u003Cdiv class=\\u0022columns large-12 small-12\\u0022\\u003E\\r\\n  \\u003Cspan class=\\u0022user-name\\u0022\\u003E@{message:field-manager:field_first_name}\\u0026nbsp;@{message:field-manager:field_last_name}\\u003C\\/span\\u003E unassigned you from\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E \\u003Cspan class=\\u0022due-date\\u0022\\u003E(Due [message:field-task:field-due-date:tiny])\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\r\\n\\u003C\\/div\\u003E",
          "format" : "full_html",
          "safe_value" : "\\u003Cdiv class=\\u0022columns large-12 small-12\\u0022\\u003E\\n  \\u003Cspan class=\\u0022user-name\\u0022\\u003E@{message:field-manager:field_first_name}\\u0026nbsp;@{message:field-manager:field_last_name}\\u003C\\/span\\u003E unassigned you from\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E \\u003Cspan class=\\u0022due-date\\u0022\\u003E(Due [message:field-task:field-due-date:tiny])\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\n\\u003C\\/div\\u003E"
        }
      ]
    }
  }');
  $items['task_was_unassociated_with_manager'] = entity_import('message_type', '{
    "name" : "task_was_unassociated_with_manager",
    "description" : "Task was unassociated with manager",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "\\u003Cdiv class=\\u0022columns large-12 small-12\\u0022\\u003E\\r\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cspan class=\\u0022user-name\\u0022\\u003E@{message:field-manager:field_first_name}\\u0026nbsp;@{message:field-manager:field_last_name}\\u003C\\/span\\u003E unassigned you from\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E \\u003Cspan class=\\u0022due-date\\u0022\\u003E(Due [message:field-task:field-due-date:tiny])\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\r\\n\\u003C\\/div\\u003E",
          "format" : "full_html",
          "safe_value" : "\\u003Cdiv class=\\u0022columns large-12 small-12\\u0022\\u003E\\n  \\u003Ca href=\\u0022\\/dashboard\\/notifications\\/[message:mid]\\u0022\\u003E\\u003Cspan class=\\u0022user-name\\u0022\\u003E@{message:field-manager:field_first_name}\\u0026nbsp;@{message:field-manager:field_last_name}\\u003C\\/span\\u003E unassigned you from\\u003Cbr \\/\\u003E\\u003Cspan class=\\u0022task-name\\u0022\\u003E@{message:field-task:title}\\u003C\\/span\\u003E \\u003Cspan class=\\u0022due-date\\u0022\\u003E(Due [message:field-task:field-due-date:tiny])\\u003C\\/span\\u003E\\u003Cspan class=\\u0022date\\u0022\\u003E[message:timestamp:tiny]\\u003C\\/span\\u003E\\u003C\\/a\\u003E\\n\\u003C\\/div\\u003E"
        }
      ]
    }
  }');
  return $items;
}
