<?php
/**
 * @file
 * cmp_messages.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cmp_messages_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer message types'.
  $permissions['administer message types'] = array(
    'name' => 'administer message types',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message',
  );

  // Exported permission: 'bypass message access control'.
  $permissions['bypass message access control'] = array(
    'name' => 'bypass message access control',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'create a report_was_submitted message instance'.
  $permissions['create a report_was_submitted message instance'] = array(
    'name' => 'create a report_was_submitted message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'create a task_became_completed message instance'.
  $permissions['create a task_became_completed message instance'] = array(
    'name' => 'create a task_became_completed message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'create a task_became_overdue message instance'.
  $permissions['create a task_became_overdue message instance'] = array(
    'name' => 'create a task_became_overdue message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'create a task_due_in_48h message instance'.
  $permissions['create a task_due_in_48h message instance'] = array(
    'name' => 'create a task_due_in_48h message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'create a task_was_assigned message instance'.
  $permissions['create a task_was_assigned message instance'] = array(
    'name' => 'create a task_was_assigned message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'create a task_was_commented message instance'.
  $permissions['create a task_was_commented message instance'] = array(
    'name' => 'create a task_was_commented message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'create a task_was_unassigned message instance'.
  $permissions['create a task_was_unassigned message instance'] = array(
    'name' => 'create a task_was_unassigned message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'create any message instance'.
  $permissions['create any message instance'] = array(
    'name' => 'create any message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'create messages'.
  $permissions['create messages'] = array(
    'name' => 'create messages',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'message',
  );

  // Exported permission: 'delete a report_was_submitted message instance'.
  $permissions['delete a report_was_submitted message instance'] = array(
    'name' => 'delete a report_was_submitted message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'delete a task_became_completed message instance'.
  $permissions['delete a task_became_completed message instance'] = array(
    'name' => 'delete a task_became_completed message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'delete a task_became_overdue message instance'.
  $permissions['delete a task_became_overdue message instance'] = array(
    'name' => 'delete a task_became_overdue message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'delete a task_due_in_48h message instance'.
  $permissions['delete a task_due_in_48h message instance'] = array(
    'name' => 'delete a task_due_in_48h message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'delete a task_was_assigned message instance'.
  $permissions['delete a task_was_assigned message instance'] = array(
    'name' => 'delete a task_was_assigned message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'delete a task_was_commented message instance'.
  $permissions['delete a task_was_commented message instance'] = array(
    'name' => 'delete a task_was_commented message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'delete a task_was_unassigned message instance'.
  $permissions['delete a task_was_unassigned message instance'] = array(
    'name' => 'delete a task_was_unassigned message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'delete any message instance'.
  $permissions['delete any message instance'] = array(
    'name' => 'delete any message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'edit a report_was_submitted message instance'.
  $permissions['edit a report_was_submitted message instance'] = array(
    'name' => 'edit a report_was_submitted message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'edit a task_became_completed message instance'.
  $permissions['edit a task_became_completed message instance'] = array(
    'name' => 'edit a task_became_completed message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'edit a task_became_overdue message instance'.
  $permissions['edit a task_became_overdue message instance'] = array(
    'name' => 'edit a task_became_overdue message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'edit a task_due_in_48h message instance'.
  $permissions['edit a task_due_in_48h message instance'] = array(
    'name' => 'edit a task_due_in_48h message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'edit a task_was_assigned message instance'.
  $permissions['edit a task_was_assigned message instance'] = array(
    'name' => 'edit a task_was_assigned message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'edit a task_was_commented message instance'.
  $permissions['edit a task_was_commented message instance'] = array(
    'name' => 'edit a task_was_commented message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'edit a task_was_unassigned message instance'.
  $permissions['edit a task_was_unassigned message instance'] = array(
    'name' => 'edit a task_was_unassigned message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'edit any message instance'.
  $permissions['edit any message instance'] = array(
    'name' => 'edit any message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'update tokens'.
  $permissions['update tokens'] = array(
    'name' => 'update tokens',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'view a report_was_submitted message instance'.
  $permissions['view a report_was_submitted message instance'] = array(
    'name' => 'view a report_was_submitted message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'view a task_became_completed message instance'.
  $permissions['view a task_became_completed message instance'] = array(
    'name' => 'view a task_became_completed message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'view a task_became_overdue message instance'.
  $permissions['view a task_became_overdue message instance'] = array(
    'name' => 'view a task_became_overdue message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'view a task_due_in_48h message instance'.
  $permissions['view a task_due_in_48h message instance'] = array(
    'name' => 'view a task_due_in_48h message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'view a task_was_assigned message instance'.
  $permissions['view a task_was_assigned message instance'] = array(
    'name' => 'view a task_was_assigned message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'view a task_was_commented message instance'.
  $permissions['view a task_was_commented message instance'] = array(
    'name' => 'view a task_was_commented message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'view a task_was_unassigned message instance'.
  $permissions['view a task_was_unassigned message instance'] = array(
    'name' => 'view a task_was_unassigned message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  // Exported permission: 'view any message instance'.
  $permissions['view any message instance'] = array(
    'name' => 'view any message instance',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'message_ui',
  );

  return $permissions;
}
