<?php
/**
 * @file
 * cmp_messages.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function cmp_messages_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'notifications';
  $page->task = 'page';
  $page->admin_title = 'Notifications';
  $page->admin_description = 'Dashboard page for managers and Reps';
  $page->path = 'dashboard/notifications';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
            2 => 5,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Notifications',
    'name' => 'main-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'normal',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_notifications__panel_context_a7baa1d9-617c-4754-b1df-f73214639e03';
  $handler->task = 'page';
  $handler->subtask = 'notifications';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Notifications for manager',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 3,
              1 => 4,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'notifications';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'top' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
      'bottom' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Notifications';
  $display->uuid = '8f5622df-4156-46c1-9d71-705fcee275ce';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-4d491f63-8958-46c9-bde0-f94761dc6900';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4d491f63-8958-46c9-bde0-f94761dc6900';
    $display->content['new-4d491f63-8958-46c9-bde0-f94761dc6900'] = $pane;
    $display->panels['above_left'][0] = 'new-4d491f63-8958-46c9-bde0-f94761dc6900';
    $pane = new stdClass();
    $pane->pid = 'new-647c3851-51e3-41b6-8098-e8fc0d78045c';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'messages-panel_messages';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '647c3851-51e3-41b6-8098-e8fc0d78045c';
    $display->content['new-647c3851-51e3-41b6-8098-e8fc0d78045c'] = $pane;
    $display->panels['middle'][0] = 'new-647c3851-51e3-41b6-8098-e8fc0d78045c';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_notifications__panel_context_fc9d9d14-f1ee-400e-952e-4fd1c0768b48';
  $handler->task = 'page';
  $handler->subtask = 'notifications';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Notifications for rep',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 5,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'notifications';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'top' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
      'bottom' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Notifications';
  $display->uuid = '8f5622df-4156-46c1-9d71-705fcee275ce';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-4d491f63-8958-46c9-bde0-f94761dc6900';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4d491f63-8958-46c9-bde0-f94761dc6900';
    $display->content['new-4d491f63-8958-46c9-bde0-f94761dc6900'] = $pane;
    $display->panels['above_left'][0] = 'new-4d491f63-8958-46c9-bde0-f94761dc6900';
    $pane = new stdClass();
    $pane->pid = 'new-c851710c-e422-4da5-b8c3-9a81177324e7';
    $pane->panel = 'above_right';
    $pane->type = 'block';
    $pane->subtype = 'cmp_dashboard-rep_dashboard_statistics';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c851710c-e422-4da5-b8c3-9a81177324e7';
    $display->content['new-c851710c-e422-4da5-b8c3-9a81177324e7'] = $pane;
    $display->panels['above_right'][0] = 'new-c851710c-e422-4da5-b8c3-9a81177324e7';
    $pane = new stdClass();
    $pane->pid = 'new-19ea14d7-368e-4159-9260-287ef839ba82';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'messages-panel_messages';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '19ea14d7-368e-4159-9260-287ef839ba82';
    $display->content['new-19ea14d7-368e-4159-9260-287ef839ba82'] = $pane;
    $display->panels['middle'][0] = 'new-19ea14d7-368e-4159-9260-287ef839ba82';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['notifications'] = $page;

  return $pages;

}
