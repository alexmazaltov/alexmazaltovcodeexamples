<?php
/**
 * @file
 * cmp_core.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function cmp_core_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-black_footer_logo'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'black_footer_logo',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'access-denied
not-found',
    'roles' => array(
      'anonymous user' => 1,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => -41,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -41,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-black_logo'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'black_logo',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'access-denied
not-found',
    'roles' => array(
      'anonymous user' => 1,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-footer_social_guest'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'footer_social_guest',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'access-denied
not-found',
    'roles' => array(
      'anonymous user' => 1,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => -39,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -39,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
