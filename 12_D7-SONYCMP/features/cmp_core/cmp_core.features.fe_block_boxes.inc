<?php
/**
 * @file
 * cmp_core.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function cmp_core_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Black footer logo';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'black_footer_logo';
  $fe_block_boxes->body = '<p class="rtecenter"><a href="/"><img alt="" src="/sites/all/themes/sonycmp/images/logo-black.png" style="width: 300px; height: 43px;" /></a></p>';

  $export['black_footer_logo'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Black logo';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'black_logo';
  $fe_block_boxes->body = '<p class="rtecenter er"><a href="/"><img alt="" src="/sites/all/themes/sonycmp/images/logo-black.png"/></a></p>
';

  $export['black_logo'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Footer social links for guests';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'footer_social_guest';
  $fe_block_boxes->body = '<ul class="social-icons social-icons--bottom">
  <li><a href="https://twitter.com/SonyMusicU" target="_blank"><img alt="Twitter icon" src="/sites/all/themes/sonycmp/images/twitter-bottom-icon.png" /></a></li>
  <li><a href="https://instagram.com/sonymusicu/" target="_blank"><img alt="Instagram icon" src="/sites/all/themes/sonycmp/images/instagram-bottom-icon.png" /></a></li>
  <li><a href="https://www.youtube.com/channel/UCqd8NzWNwQtG2IlNbNyCuyA" target="_blank"><img alt="Youtube icon" src="/sites/all/themes/sonycmp/images/youtube-bottom-icon.png" /></a></li>
</ul>';

  $export['footer_social_guest'] = $fe_block_boxes;

  return $export;
}
