<?php

class cmp_generic_multistep_handler_field_custom_checkbox extends views_handler_field_custom {
  function render($values) {
    $uid = $values->uid;
    $output = '<input class="reps-reference form-checkbox hidden-field" type="checkbox" id="edit-assignthisrep-view-check-' . $uid . '" name="assign_this_rep_view[' . $uid . ']" value="' . $uid . '">
               <label class="element-invisible" for="edit-assignthisrep-view-check-' . $uid . '"><strong>' . $uid . '</strong></label>';
    return $output;
  }
}
