<?php
/**
 * @file
 * cmp_generic_multistep.features.inc
 */

/**
 * Implements hook_views_api().
 */
function cmp_generic_multistep_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
