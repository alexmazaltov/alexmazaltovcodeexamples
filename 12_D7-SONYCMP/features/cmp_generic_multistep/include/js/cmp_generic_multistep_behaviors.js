(function ($, Drupal) {

  Drupal.SelectAllReps = false;
  Drupal.behaviors.genericMultistrepFormStep2 = {
    attach: function (context, settings) {
      //Add extra classes to body if there are any assigned reps before
      if (!$.isEmptyObject(Drupal.settings.fieldRepsAssigned)) {
        $('body').addClass('edit-assigned-reps');
      }
      // @todo all this code should be rewritten.

      if (settings && settings.views && settings.views.ajaxViews) {
        $.each(settings.views.ajaxViews, function (i, ajax_settings) {

          if (ajax_settings.view_name == 'assign_reps_to') {

            var ajaxViewNameReplaced = ajax_settings.view_name.replace(/_/g, '-');

            //Rebuild table rows
            $('.view-' + ajaxViewNameReplaced).find('table tbody tr').each(function (i) {
              $(this).find('td:eq(2)').attr('colspan', '3');
              //Fix span for notexisting span after input checkbox
              var lastTd = $(this).find('td.views-field-assign-this-rep-checkbox');
              lastTd.find('span').detach();
              var label = lastTd.find('label');
              lastTd.append('<span class="custom checkbox"></span>');
              lastTd.append(label);
            });
            //Reset all filters on click by View All tr
            $('.view-' + ajaxViewNameReplaced + ' table thead tr').find('th:eq(2)').wrapInner('<a href="#" class="clear-all-filters"/>').off('click').on('click', function (e) {
              e.preventDefault();
              e.stopPropagation();

              $('input[name="combine"]').val('');
              $('input[name="nid"]').val('');
              $('a[data-group-nid]').removeClass('active');
              $('.bef-select-as-links input[type="hidden"][class="bef-new-value"]').val('All'); //[name="field_type_value"], [name="field_market_tid"]
              $('form[id^="views-exposed-form-' + ajaxViewNameReplaced + '"]').find('.views-submit-button *[type=submit]').click();
            });


            //Bind onclick event to make groups links change filter on group nid
            $('a[data-group-nid]').off('click').on('click', function (e) {
              e.preventDefault();
              e.stopPropagation();
              var el = $(this);
              $('input[name="nid"]').val(el.data('group-nid'));
              //Clear all filters for select as link
              $('.bef-select-as-links input[type="hidden"][class="bef-new-value"]').val('All');
              //clear combine filter on group change
              $('input[name="combine"]').val('');
              $('form[id^="views-exposed-form-' + ajaxViewNameReplaced + '"]').find('.views-submit-button *[type=submit]').click();
            });
            //set active class to group filter based on $('input[name="nid"]').val();
            $('a[data-group-nid="' + $('input[name="nid"]').val() + '"]').addClass('active');

            //Add event listener to unAssigned users in sidebar
            $('#choosed-reps .js-unassign-rep').off('click').on('click', function (e) {
              e.preventDefault();
              e.stopPropagation();
              var repChoosed = $(e.target).closest('.js-unassign-rep');
              $(document).trigger('unAssignThisRepDom', [repChoosed.data('uid')]);
            });

            //On each ajax dom rebuild do check for assigned reps (based on sidebar list)
            $('div[class^="choosed-uid-for-assign-"]').each(function (index) {
              var AssignedRepUid = $(this).data('uid');
              $('input[name="assign_this_rep_view[' + AssignedRepUid + ']"]').attr('checked', 'checked');
            });

            var checkboxes = $('input[name^="assign_this_rep_view"]');
            checkboxes.off('change').on('change', function (e) {
              e.preventDefault();
              e.stopPropagation();
              var checkbox = $(this);
              var assignThisUid = checkbox.val();

              if (checkbox.is(':checked')) {
                //Checkbox has been checked
                $(document).trigger('assignThisRepDom', [{
                  assignUid: assignThisUid,
                  appendInnerHtml: $(this).closest('tr').find('.cmp-groups-reps-column').html()
                }]);
                $(this).removeClass('unassigned');
              } else {
                $(this).addClass('unassigned');
                $(document).trigger('unAssignThisRepDom', [assignThisUid]);
              }
            });

            //Make select all checkbox be checked if all reps already assigned
            if (0 < $('[id^="edit-assignthisrep-view-check-"]').length && $('[id^="edit-assignthisrep-view-check-"]:checked').length == $('[id^="edit-assignthisrep-view-check-"]').length) {
              var selectAllHaveBeenChecked = ' checked="checked" ';
            }

            // @todo move this to template.
            var select_all_html = '' +
              '<div class="wrap-all-checkbox">' +
              '<input type="checkbox" id="select-all-reps" ' + selectAllHaveBeenChecked + ' value="selectedAll">' +
              '<span class="custom checkbox"></span>' +
              '<label class="element-invisible" for="select-all-reps">' + Drupal.t('Select All') + '</label>' +
              '</div>';


            $('.view-' + ajaxViewNameReplaced + ' table thead tr').find('th:eq(3)').html(select_all_html);

            $('#select-all-reps').off('change').on('change', function (e) {
              e.preventDefault();
              e.stopPropagation();
              var checkbox = $(this);
              if (checkbox.is(':checked')) {
                checkbox.addClass('active');
                Drupal.SelectAllReps = true;
                checkboxes.prop('checked', true);
              }
              else {
                Drupal.SelectAllReps = false;
                checkboxes.prop('checked', false);
              }
              checkboxes.trigger('change');
            });

            if (Drupal.SelectAllReps) {
              checkboxes.each(function () {
                if ($('div.choosed-uid-for-assign-' + $(this).val()).length == 0 && !$(this).hasClass('unassigned')) {
                  $(this).prop('checked', true);
                  $(this).trigger('change');
                }
              });
            }
          }
        });
      }
    }
  };

})(jQuery, Drupal);

jQuery(document).ready(function ($) {

  $('#edit-new-assigned-uids').off('unAssignThisRep').on('unAssignThisRep', function (e, unAssignThisUid) {
    var assignTheseUids = $.grep($(this).val().split(','), function (v) {
      return ((v != unAssignThisUid) && (v != ''));
    });
    if (assignTheseUids.length > 1) {
      $(this).val(assignTheseUids.join(','));
    }
    if (assignTheseUids.length == 1) {
      $(this).val(assignTheseUids.join(',') + ',');
    }
    if (assignTheseUids.length < 1) {
      $(this).val('');
    }
  });

  $('#edit-new-assigned-uids').off('assignThisRep').on('assignThisRep', function (e, assignThisUid) {
    if ($(this).val()) {
      var assignTheseNUids = $.grep($(this).val().split(','), function (v) {
        return ((v != assignThisUid) && (v != ''));
      });
      if (assignTheseNUids.length > 0) {
        $(this).val(assignTheseNUids.join(',') + ',' + assignThisUid + ',');
      }
    }
    else {
      $(this).val(assignThisUid + ',');
    }

  });

  $(document).off('unAssignThisRepDom').on('unAssignThisRepDom', function (e, unAssignThisUid) {
    var remFromTextField = !Drupal.settings.fieldRepsAssigned.hasOwnProperty(unAssignThisUid);
    if (remFromTextField) {
      //delete text uid from textfield
      $('#edit-new-assigned-uids').trigger('unAssignThisRep', [unAssignThisUid]);
    }
    else {
      //detach checkbox on form above that will delete checkbox item and it will not submit this uid
      $('#edit-field-reps .form-item-field-reps-' + unAssignThisUid).detach();
    }
    //Detach from right sidebar
    $('.choosed-uid-for-assign-' + unAssignThisUid).detach();
    //Decrease counter in sidebar
    $('.choosed-counter').text($('.choosed-counter').text() * 1 - 1);
    //if it was last assigned rep in right sidebar (do uncheck for select all checkbox)
    if ($('.choosed-counter').text() == 0) {
      $('#select-all-reps').attr('checked', false);
    }

    //after 250ms do count  all checkboxes (if all checkboxes is checked do check select all-checkbox)
    setTimeout(function () {
      if ($('input[name^="assign_this_rep_view"]:checked').length != $('input[name^="assign_this_rep_view"]').length) {
        $('#select-all-reps').prop('checked', false);
      }
    }, 250);

    $('input[name="assign_this_rep_view[' + unAssignThisUid + ']"]').attr('checked', false);

  });

  $(document).off('assignThisRepDom').on('assignThisRepDom', function (e, eventInfo) {
    var addToTextField = !Drupal.settings.fieldRepsAssigned.hasOwnProperty(eventInfo.assignUid);
    if (addToTextField) {
      //Add text uid to textfield
      $('#edit-new-assigned-uids').trigger('assignThisRep', [eventInfo.assignUid]);
    }
    else {
      //attach checkbox on form above that will add checkbox item and it will submit this uid
      $('#edit-field-reps').append(
        '<div class="form-item form-type-checkbox form-item-field-reps-' + eventInfo.assignUid + '">' +
        '<input type="checkbox" id="edit-field-reps-' + eventInfo.assignUid + '" name="field_reps[' + eventInfo.assignUid + ']" value="' + eventInfo.assignUid + '" checked="checked" class="form-checkbox hidden-field">' +
        '<span class="custom checkbox checked"></span>' +
        '<label class="option" for="edit-field-reps-' + eventInfo.assignUid + '">' + eventInfo.assignUid + '</label>' +
        '</div>');
    }
    if ($('div.choosed-uid-for-assign-' + eventInfo.assignUid).length == 0) {
      $('#choosed-reps').append(
        '<div class="choosed-uid-for-assign-' + eventInfo.assignUid + ' js-unassign-rep box" data-uid="' + eventInfo.assignUid + '">' +
        eventInfo.appendInnerHtml +
        '</div>'
      );
    }

    //Add event listener to this new div in sidebar
    $('#choosed-reps .choosed-uid-for-assign-' + eventInfo.assignUid).off('click').on('click', function (e) {
      e.preventDefault();
      e.stopPropagation();
      var repChoosed = $(e.target).closest('.js-unassign-rep');
      $(document).trigger('unAssignThisRepDom', [repChoosed.data('uid')]);
    });

    //Increase counter in sidebar
    $('.choosed-counter').text($('#choosed-reps .box').length);

    //after 250ms do count  all checkboxes (if all checkboxes is checked do check select all-checkbox)
    setTimeout(function () {
      if ($('input[name^="assign_this_rep_view"]:checked').length == $('input[name^="assign_this_rep_view"]').length) {
        $('#select-all-reps').prop('checked', true);
      }
    }, 250);
  });

  //Need to check it manually after wrong validation on step 2 empty assigned users is not allowed
  $('input[name^="field_reps"]').prop('checked', true);

});
