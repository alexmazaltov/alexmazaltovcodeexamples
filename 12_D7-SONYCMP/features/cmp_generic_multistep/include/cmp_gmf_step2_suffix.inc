<?php

function theme_cmp_gmf_step2_suffix($vars) {
  $output_suffix = theme('cmp_gmf_step2_suffix_assign_reps_to_', array(
      'assigned_uids' => $vars['field_reps'],
      'assign_to' => $vars['assign_to']
    ));

  $output_suffix .= theme('cmp_gmf_step2_suffix_chossed_reps_to_assign_to_', array('choosed_uids' => $vars['field_reps']));

  $module_path = drupal_get_path('module', 'cmp_generic_multistep');
  drupal_add_js(array('fieldRepsAssigned' => $vars['field_reps']), 'setting');
  drupal_add_js($module_path . '/include/js/cmp_generic_multistep_behaviors.js');
  return $output_suffix;
}

function theme_cmp_gmf_step2_suffix_assign_reps_to_($vars) {
  $output = '<div class="column large-9 step-2-main-right-wrapper">';
  $view = views_get_view('assign_reps_to');
  $view->set_display('groups_reps_reference');
  $view->override_path = 'views/ajax';
  $view->init_handlers(); //initialize display handlers
  $view->use_ajax = TRUE;
  $rendered_view = $view->preview();

  $output .= '<h2>' . t('Select Reps for this !assign_to:', array('!assign_to' => $vars['assign_to'])) . '</h2>' .
    '<div>' . $rendered_view . '</div>';

  $view->destroy();

  return $output . '</div><!-- .column.large-9 -->';
}

function theme_cmp_gmf_step2_suffix_chossed_reps_to_assign_to_($vars) {
  $output = '<h3>' . t('Selected Reps') . ' <span class="choosed-counter">' . count($vars['choosed_uids']) . '</span>' . '</h3>';
  $output .= '<div id="choosed-reps" class="checked-reps">';
  foreach ($vars['choosed_uids'] as $key => $uid) {
    $rep_node = user_load($uid);
    if ($rep_node) {
      if (!empty($rep_node->picture->uri)) {
        $user_image = theme(
          'image_style',
          array(
            'style_name' => 'small_avatar_36x36',
            'path' => $rep_node->picture->uri,
            'attributes' => array(
              'class' => 'avatar'
            )
          )
        );
      }
      else {
        $user_image = '<img src="/sites/all/themes/sonycmp/images/user-image-36x36.png">';
      }
      $user_profile = '<div class="choosed-uid-for-assign-' . $uid . ' js-unassign-rep box" data-uid="' . $uid . '">';
      $user_profile .= '<div class="assign-this-rep row collapse">';
      $user_profile .= '<div class="small-3 large-3 columns">' . $user_image . '</div>';
      $rep_array = _cmp_common_load_rep($rep_node);
      $first_last_name = $rep_array['first_name'] . ' ' . $rep_array['last_name'];
      $user_profile .= '<div class="small-9 large-9 columns">' .
        '<p class="rep-name"><strong>' . $first_last_name . '</strong></p>' .
        '<p class="rep-descr"><span>' . $rep_array['college'] . '</span>|<span>' . $rep_array['market'] . '</span>|<span>' . $rep_array['type'] . '</span></p>' .
        '</div>';
      $user_profile .= '</div>';
      $user_profile .= '</div>';
      $output .= $user_profile;
    }
  }
  $output .= '</div>';

  $output = '<div class="column large-3 step-2-main-left-wrapper">' . $output . '</div><!-- .column.large-3 -->';

  return $output;
}