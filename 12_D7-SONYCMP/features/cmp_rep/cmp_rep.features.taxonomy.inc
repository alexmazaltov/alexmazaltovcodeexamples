<?php
/**
 * @file
 * cmp_rep.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function cmp_rep_taxonomy_default_vocabularies() {
  return array(
    'college' => array(
      'name' => 'College',
      'machine_name' => 'college',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'market' => array(
      'name' => 'Market',
      'machine_name' => 'market',
      'description' => 'Market taxonomy',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
