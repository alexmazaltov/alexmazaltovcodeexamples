<?php
/**
 * @file
 * cmp_rep.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function cmp_rep_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: user-menu_reps:reps.
  $menu_links['user-menu_reps:reps'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'reps',
    'router_path' => 'reps',
    'link_title' => 'Reps',
    'options' => array(
      'identifier' => 'user-menu_reps:reps',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Reps');

  return $menu_links;
}
