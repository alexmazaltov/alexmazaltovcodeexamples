<?php

/**
 * @file
 * Plugin to handle the 'page' content type which allows the standard page
 * template variables to be embedded into a panel.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Sony CMP Tabs'),
  'single' => TRUE,
  'icon' => 'icon_page.png',
  'description' => t('Add the tabs (local tasks) as content. Displays a tab even if it is the only one one page. Provides hook to alter tabs.'),
  'category' => t('Page elements'),
  'render last' => TRUE,
  'defaults' => array(
    'type' => 'both',
    'id' => 'cmp_tabs',
  ),
);

/**
 * Output function for the 'page_tabs' content type.
 *
 * Outputs the tabs (local tasks) of the current page.
 */
function cmp_rep_cmp_page_tabs_content_type_render($subtype, $conf, $panel_args) {
  $block = new stdClass();
  $primary = menu_local_tasks(0);
  $secondary = menu_local_tasks(1);
  $menus = array(
    '#theme' => 'menu_local_tasks',
    '#primary' => ($primary['tabs']['count'] > 0
      ? $primary['tabs']['output'] : ''),
    '#secondary' => ($secondary['tabs']['count'] > 0
      ? $secondary['tabs']['output'] : ''),
  );

  if (empty($menus['#secondary']) && empty($menus['#primary'])) {
    return;
  }

  switch ($conf['type']) {
    case 'primary':
      unset($menus['#secondary']);
      break;
    case 'secondary':
      unset($menus['#primary']);
      break;
  }
  if ($conf['id']) {
    $menus['#theme_wrappers'][] = 'container';
    $menus['#attributes']['id'] = $conf['id'];
  }

  $block->content = $menus;

  return $block;
}

function cmp_rep_cmp_page_tabs_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['type'] = array(
    '#title' => t('Tabs type'),
    '#type' => 'select',
    '#options' => array(
      'both' => t('Primary and secondary'),
      'primary' => t('Primary'),
      'secondary' => t('Secondary'),
    ),
    '#default_value' => $conf['type'],
  );

  $form['id'] = array(
    '#title' => t('CSS id to use'),
    '#type' => 'textfield',
    '#default_value' => $conf['id'],
  );
  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function cmp_rep_cmp_page_tabs_content_type_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}
