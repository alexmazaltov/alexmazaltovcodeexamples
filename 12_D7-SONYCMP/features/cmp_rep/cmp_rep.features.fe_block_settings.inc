<?php
/**
 * @file
 * cmp_rep.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function cmp_rep_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['cmp_dashboard-hello_username'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'hello_username',
    'module' => 'cmp_dashboard',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(
      'representative' => 5,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'header_right',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => -32,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -32,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['cmp_rep-create_rep_button'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'create_rep_button',
    'module' => 'cmp_rep',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['cmp_rep-rep_form'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'rep_form',
    'module' => 'cmp_rep',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['cmp_rep-rep_statistics'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'rep_statistics',
    'module' => 'cmp_rep',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => -24,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -24,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views--exp-reps-pane_archived_reps'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '-exp-reps-pane_archived_reps',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views--exp-reps-pane_reps'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '-exp-reps-pane_reps',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
