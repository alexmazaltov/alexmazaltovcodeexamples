<?php

/**
 * @file
 * Definition of cmp_rep_handler_tasks_overdue.
 */
class cmp_rep_handler_field_tasks_ontime extends views_handler_field_numeric {
  function query() {
    $alias = 'tasks_ontime';
    if (!isset($this->query->relationships[$alias])) {
      $this->ensure_my_table();
      if (!isset($this->query->table_queue['node'])) {
        $node_join = new views_join();
        $node_join->definition = array(
          'left_table' => 'users',
          'left_field' => 'uid',
          'table' => 'node',
          'field' => 'uid',
          'type' => 'LEFT',
          'extra' => "node.type IN ('bi_weekly_report', 'misc_report', 'tour_report') AND (node.status = 1 AND node.sticky = 1 AND node.promote = 1)",
          'extra_type' => 'AND',
        );;
        $node_join->construct();
        $node_join->adjusted = TRUE;
        $this->query->add_relationship('node', $node_join, 'users', $this->relationship);
      }

      $join = new views_join();
      $join->definition = array(
        'left_table' => 'node',
        'left_field' => 'nid',
        'table' => 'field_data_field_status',
        'field' => 'entity_id',
        'type' => 'LEFT',
        'extra' => $alias . ".deleted = '0' AND " . $alias . ".entity_type = 'node' AND (" . $alias . ".field_status_value = " . CMP_REPORTS_REPORT_STATUS_ON_TIME . ")",
        'extra_type' => 'AND',
      );
      $join->construct();
      $join->adjusted = TRUE;
      $this->query->add_relationship($alias, $join, 'node', 'node');
      $this->field_alias = $this->query->add_field('', $alias . ".entity_id", $alias, array('count' => TRUE));
      $this->add_additional_fields();
    }
  }
}