<?php
/**
 * @file
 * cmp_rep.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function cmp_rep_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  // Exported role: representative.
  $roles['representative'] = array(
    'name' => 'representative',
    'weight' => 4,
  );

  // Exported role: task manager.
  $roles['task manager'] = array(
    'name' => 'task manager',
    'weight' => 3,
  );

  return $roles;
}
