<?php
/**
 * @file
 * cmp_rep.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cmp_rep_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'add reps'.
  $permissions['add reps'] = array(
    'name' => 'add reps',
    'roles' => array(
      'administrator' => 'administrator',
      'task manager' => 'task manager',
    ),
    'module' => 'cmp_rep',
  );

  // Exported permission: 'delete reps'.
  $permissions['delete reps'] = array(
    'name' => 'delete reps',
    'roles' => array(
      'administrator' => 'administrator',
      'task manager' => 'task manager',
    ),
    'module' => 'cmp_rep',
  );

  // Exported permission: 'edit reps'.
  $permissions['edit reps'] = array(
    'name' => 'edit reps',
    'roles' => array(
      'administrator' => 'administrator',
      'task manager' => 'task manager',
    ),
    'module' => 'cmp_rep',
  );

  return $permissions;
}
