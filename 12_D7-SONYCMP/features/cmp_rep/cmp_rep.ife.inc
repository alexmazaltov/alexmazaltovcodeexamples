<?php
/**
 * @file
 * cmp_rep.ife.inc
 */

/**
 * Implements hook_ife_default_settings().
 */
function cmp_rep_ife_default_settings() {
  $export = array();

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_rep_rep_form';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_rep_rep_form'] = $ife;

  return $export;
}
