<?php
/**
 * @file
 * cmp_rep.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cmp_rep_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ife" && $api == "ife") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cmp_rep_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function cmp_rep_image_default_styles() {
  $styles = array();

  // Exported image style: big_avatar_142x142.
  $styles['big_avatar_142x142'] = array(
    'label' => 'Big avatar 142x142',
    'effects' => array(
      9 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 142,
          'height' => 142,
        ),
        'weight' => -10,
      ),
      10 => array(
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/png',
          'quality' => 100,
        ),
        'weight' => -9,
      ),
      11 => array(
        'name' => 'imagecache_alpha',
        'data' => array(
          'opacity' => 1,
          'RGB' => array(
            'HEX' => '#ffffff',
          ),
          'flatten' => TRUE,
        ),
        'weight' => -8,
      ),
      12 => array(
        'name' => 'canvasactions_roundedcorners',
        'data' => array(
          'radius' => 71,
          'independent_corners_set' => array(
            'independent_corners' => 0,
            'radii' => array(
              'tl' => 0,
              'tr' => 0,
              'bl' => 0,
              'br' => 0,
            ),
          ),
        ),
        'weight' => -7,
      ),
    ),
  );

  // Exported image style: mapbox_marker.
  $styles['mapbox_marker'] = array(
    'label' => 'Mapbox marker',
    'effects' => array(
      14 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 50,
          'height' => 50,
        ),
        'weight' => -10,
      ),
      15 => array(
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/png',
          'quality' => 100,
        ),
        'weight' => -9,
      ),
      16 => array(
        'name' => 'imagecache_alpha',
        'data' => array(
          'opacity' => 1,
          'RGB' => array(
            'HEX' => '#ffffff',
          ),
          'flatten' => TRUE,
        ),
        'weight' => -8,
      ),
      17 => array(
        'name' => 'canvasactions_roundedcorners',
        'data' => array(
          'radius' => 25,
          'independent_corners_set' => array(
            'independent_corners' => 0,
            'radii' => array(
              'tl' => 0,
              'tr' => 0,
              'bl' => 0,
              'br' => 0,
            ),
          ),
        ),
        'weight' => -7,
      ),
    ),
  );

  // Exported image style: medium_avatar_72x72.
  $styles['medium_avatar_72x72'] = array(
    'label' => 'Medium avatar 72x72',
    'effects' => array(
      13 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 72,
          'height' => 72,
        ),
        'weight' => -10,
      ),
      14 => array(
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/png',
          'quality' => 100,
        ),
        'weight' => -9,
      ),
      15 => array(
        'name' => 'imagecache_alpha',
        'data' => array(
          'opacity' => 1,
          'RGB' => array(
            'HEX' => '#ffffff',
          ),
          'flatten' => TRUE,
        ),
        'weight' => -8,
      ),
      16 => array(
        'name' => 'canvasactions_roundedcorners',
        'data' => array(
          'radius' => 36,
          'independent_corners_set' => array(
            'independent_corners' => 0,
            'radii' => array(
              'tl' => 0,
              'tr' => 0,
              'bl' => 0,
              'br' => 0,
            ),
          ),
        ),
        'weight' => -7,
      ),
    ),
  );

  // Exported image style: medium_avatar_80x80.
  $styles['medium_avatar_80x80'] = array(
    'label' => 'Medium avatar (80x80)',
    'effects' => array(
      14 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 80,
          'height' => 80,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: report_owner_avatar_98x98.
  $styles['report_owner_avatar_98x98'] = array(
    'label' => 'Report owner avatar 98x98',
    'effects' => array(
      14 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 98,
          'height' => 98,
        ),
        'weight' => -10,
      ),
      15 => array(
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/png',
          'quality' => 100,
        ),
        'weight' => -9,
      ),
      16 => array(
        'name' => 'imagecache_alpha',
        'data' => array(
          'opacity' => 1,
          'RGB' => array(
            'HEX' => '#ffffff',
          ),
          'flatten' => TRUE,
        ),
        'weight' => -8,
      ),
      17 => array(
        'name' => 'canvasactions_roundedcorners',
        'data' => array(
          'radius' => 49,
          'independent_corners_set' => array(
            'independent_corners' => 0,
            'radii' => array(
              'tl' => 0,
              'tr' => 0,
              'bl' => 0,
              'br' => 0,
            ),
          ),
        ),
        'weight' => -7,
      ),
    ),
  );

  // Exported image style: small_avatar_36x36.
  $styles['small_avatar_36x36'] = array(
    'label' => 'Small avatar 36x36',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 36,
          'height' => 36,
        ),
        'weight' => -10,
      ),
      4 => array(
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/png',
          'quality' => 100,
        ),
        'weight' => -9,
      ),
      7 => array(
        'name' => 'imagecache_alpha',
        'data' => array(
          'opacity' => 1,
          'RGB' => array(
            'HEX' => '#ffffff',
          ),
          'flatten' => TRUE,
        ),
        'weight' => -8,
      ),
      5 => array(
        'name' => 'canvasactions_roundedcorners',
        'data' => array(
          'radius' => 18,
          'independent_corners_set' => array(
            'independent_corners' => 0,
            'radii' => array(
              'tl' => 0,
              'tr' => 0,
              'bl' => 0,
              'br' => 0,
            ),
          ),
        ),
        'weight' => -7,
      ),
    ),
  );

  return $styles;
}
