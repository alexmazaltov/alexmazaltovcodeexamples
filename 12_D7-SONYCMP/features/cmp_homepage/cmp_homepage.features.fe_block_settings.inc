<?php
/**
 * @file
 * cmp_homepage.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function cmp_homepage_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['ajax_login_pass-ajax_pass'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'ajax_pass',
    'module' => 'ajax_login_pass',
    'node_types' => array(),
    'pages' => 'homepage
access-denied
not-found',
    'roles' => array(
      'anonymous user' => 1,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => -43,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -43,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-a_day_in_the_life'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'a_day_in_the_life',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => -22,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -22,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-become_a_rep'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'become_a_rep',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'homepage
not-found
access-denied',
    'roles' => array(
      'anonymous user' => 1,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'header_right',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => -31,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -31,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-footer_section'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'footer_section',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => -16,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -16,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-happening_now'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'happening_now',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => -15,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -15,
      ),
    ),
    'title' => 'Happening Now',
    'visibility' => 1,
  );

  $export['block-logout_link'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'logout_link',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(
      'administrator' => 3,
      'authenticated user' => 2,
      'representative' => 5,
      'task manager' => 4,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'header_right',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => -29,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -29,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-more_about'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'more_about',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => -8,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -8,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-social_network_icons'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'social_network_icons',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(
      'anonymous user' => 1,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'header_left',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => -13,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -13,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['block-spotify_playlist'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'spotify_playlist',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => -2,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -2,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['mapbox-mapbox'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'mapbox',
    'module' => 'mapbox',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => -9,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -9,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['system-main'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => -44,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -44,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['user-login'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'login',
    'module' => 'user',
    'node_types' => array(),
    'pages' => 'homepage
access-denied
not-found',
    'roles' => array(
      'anonymous user' => 1,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => -44,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => -44,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['views-spotlight-spotlight'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'spotlight-spotlight',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 2,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 2,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['views-video-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'video-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 4,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 4,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
