<?php
/**
 * @file
 * cmp_homepage.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function cmp_homepage_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'homepage';
  $page->task = 'page';
  $page->admin_title = 'Homepage';
  $page->admin_description = '';
  $page->path = 'homepage';
  $page->access = array();
  $page->menu = array(
    'type' => 'none',
    'title' => 'Home',
    'name' => 'main-menu',
    'weight' => '10',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_homepage__st2-default';
  $handler->task = 'page';
  $handler->subtask = 'homepage';
  $handler->handler = 'panel_context';
  $handler->weight = -30;
  $handler->conf = array(
    'title' => 'ST2 Default',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'st2-default',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'theme',
          'settings' => array(
            'theme' => 'sonycmp_theme',
          ),
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'st2_homepage';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left_above' => NULL,
      'right_above' => NULL,
      'middle' => NULL,
      'left_below' => NULL,
      'right_below' => NULL,
      'bottom' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
    ),
    'style' => 'naked',
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '8f485b1d-e488-4740-a25f-f97ab9e3d735';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-ecf62fec-dca5-4026-b0a3-ddeec4543133';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'mapbox-mapbox';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'section-map',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ecf62fec-dca5-4026-b0a3-ddeec4543133';
    $display->content['new-ecf62fec-dca5-4026-b0a3-ddeec4543133'] = $pane;
    $display->panels['middle'][0] = 'new-ecf62fec-dca5-4026-b0a3-ddeec4543133';
    $pane = new stdClass();
    $pane->pid = 'new-47408684-78ba-4a4d-a316-0e2508ac8ea2';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'block-26';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array(
      'method' => 'simple',
      'settings' => array(
        'lifetime' => '3600',
        'granularity' => 'none',
      ),
    );
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'section-rep',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '47408684-78ba-4a4d-a316-0e2508ac8ea2';
    $display->content['new-47408684-78ba-4a4d-a316-0e2508ac8ea2'] = $pane;
    $display->panels['middle'][1] = 'new-47408684-78ba-4a4d-a316-0e2508ac8ea2';
    $pane = new stdClass();
    $pane->pid = 'new-4a1be2d6-4f8f-42b9-8428-625d080f6276';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'spotlight';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '3',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'st2_spotlight',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'section-spotlight',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '4a1be2d6-4f8f-42b9-8428-625d080f6276';
    $display->content['new-4a1be2d6-4f8f-42b9-8428-625d080f6276'] = $pane;
    $display->panels['middle'][2] = 'new-4a1be2d6-4f8f-42b9-8428-625d080f6276';
    $pane = new stdClass();
    $pane->pid = 'new-e7bd5760-b72c-4073-8a1e-9a339f3aa156';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'video-st2_grid_4x4';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'section-now',
    );
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'e7bd5760-b72c-4073-8a1e-9a339f3aa156';
    $display->content['new-e7bd5760-b72c-4073-8a1e-9a339f3aa156'] = $pane;
    $display->panels['middle'][3] = 'new-e7bd5760-b72c-4073-8a1e-9a339f3aa156';
    $pane = new stdClass();
    $pane->pid = 'new-6a7cb090-8e3e-4500-bc3e-6101ddb062ca';
    $pane->panel = 'middle';
    $pane->type = 'bean_panels';
    $pane->subtype = 'bean_panels';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'default',
      'bean_delta' => 'sony-music-collage',
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'section-collage',
    );
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '6a7cb090-8e3e-4500-bc3e-6101ddb062ca';
    $display->content['new-6a7cb090-8e3e-4500-bc3e-6101ddb062ca'] = $pane;
    $display->panels['middle'][4] = 'new-6a7cb090-8e3e-4500-bc3e-6101ddb062ca';
    $pane = new stdClass();
    $pane->pid = 'new-ee55c094-3d49-4287-8667-0afb684fe330';
    $pane->panel = 'middle';
    $pane->type = 'node';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'nid' => '9',
      'links' => 0,
      'leave_node_title' => 0,
      'identifier' => '',
      'build_mode' => 'full',
      'link_node_title' => 0,
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'section-subscribe',
    );
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'ee55c094-3d49-4287-8667-0afb684fe330';
    $display->content['new-ee55c094-3d49-4287-8667-0afb684fe330'] = $pane;
    $display->panels['middle'][5] = 'new-ee55c094-3d49-4287-8667-0afb684fe330';
    $pane = new stdClass();
    $pane->pid = 'new-dcabcdfc-edc9-4785-91a6-3210c0927b24';
    $pane->panel = 'middle';
    $pane->type = 'collage_users_avatars';
    $pane->subtype = 'collage_users_avatars';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'section-user_avatar',
    );
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = 'dcabcdfc-edc9-4785-91a6-3210c0927b24';
    $display->content['new-dcabcdfc-edc9-4785-91a6-3210c0927b24'] = $pane;
    $display->panels['middle'][6] = 'new-dcabcdfc-edc9-4785-91a6-3210c0927b24';
    $pane = new stdClass();
    $pane->pid = 'new-a6aae202-207d-4bdd-901c-244012c9c4a2';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'ajax_login_pass-ajax_login_pass';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 1,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 7;
    $pane->locks = array();
    $pane->uuid = 'a6aae202-207d-4bdd-901c-244012c9c4a2';
    $display->content['new-a6aae202-207d-4bdd-901c-244012c9c4a2'] = $pane;
    $display->panels['middle'][7] = 'new-a6aae202-207d-4bdd-901c-244012c9c4a2';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_homepage__panel_context_ca740b41-4549-4f07-b8d8-bcb2665f04cb';
  $handler->task = 'page';
  $handler->subtask = 'homepage';
  $handler->handler = 'panel_context';
  $handler->weight = -29;
  $handler->conf = array(
    'title' => 'ST1 Default',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'homepage';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left_above' => NULL,
      'right_above' => NULL,
      'middle' => NULL,
      'left_below' => NULL,
      'right_below' => NULL,
      'bottom' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '8f485b1d-e488-4740-a25f-f97ab9e3d735';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-5501cdb0-1e0b-4898-9676-459e90fb0a0e';
    $pane->panel = 'above_left';
    $pane->type = 'block';
    $pane->subtype = 'block-6';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5501cdb0-1e0b-4898-9676-459e90fb0a0e';
    $display->content['new-5501cdb0-1e0b-4898-9676-459e90fb0a0e'] = $pane;
    $display->panels['above_left'][0] = 'new-5501cdb0-1e0b-4898-9676-459e90fb0a0e';
    $pane = new stdClass();
    $pane->pid = 'new-85da9d68-e4cc-4400-896f-6af2bdb33fb1';
    $pane->panel = 'above_right';
    $pane->type = 'block';
    $pane->subtype = 'views-video-block';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '85da9d68-e4cc-4400-896f-6af2bdb33fb1';
    $display->content['new-85da9d68-e4cc-4400-896f-6af2bdb33fb1'] = $pane;
    $display->panels['above_right'][0] = 'new-85da9d68-e4cc-4400-896f-6af2bdb33fb1';
    $pane = new stdClass();
    $pane->pid = 'new-3f1729b7-5fbb-4f46-b42e-e7f5809ca6fd';
    $pane->panel = 'below_left';
    $pane->type = 'block';
    $pane->subtype = 'block-13';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => 'More About Sony Music',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3f1729b7-5fbb-4f46-b42e-e7f5809ca6fd';
    $display->content['new-3f1729b7-5fbb-4f46-b42e-e7f5809ca6fd'] = $pane;
    $display->panels['below_left'][0] = 'new-3f1729b7-5fbb-4f46-b42e-e7f5809ca6fd';
    $pane = new stdClass();
    $pane->pid = 'new-ee55c094-3d49-4287-8667-0afb684fe330';
    $pane->panel = 'below_right';
    $pane->type = 'node';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'nid' => '9',
      'links' => 0,
      'leave_node_title' => 0,
      'identifier' => '',
      'build_mode' => 'full',
      'link_node_title' => 0,
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ee55c094-3d49-4287-8667-0afb684fe330';
    $display->content['new-ee55c094-3d49-4287-8667-0afb684fe330'] = $pane;
    $display->panels['below_right'][0] = 'new-ee55c094-3d49-4287-8667-0afb684fe330';
    $pane = new stdClass();
    $pane->pid = 'new-dcabcdfc-edc9-4785-91a6-3210c0927b24';
    $pane->panel = 'bottom';
    $pane->type = 'collage_users_avatars';
    $pane->subtype = 'collage_users_avatars';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'dcabcdfc-edc9-4785-91a6-3210c0927b24';
    $display->content['new-dcabcdfc-edc9-4785-91a6-3210c0927b24'] = $pane;
    $display->panels['bottom'][0] = 'new-dcabcdfc-edc9-4785-91a6-3210c0927b24';
    $pane = new stdClass();
    $pane->pid = 'new-ae01b607-7f9d-414b-8fcd-61155d89b666';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'block-9';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ae01b607-7f9d-414b-8fcd-61155d89b666';
    $display->content['new-ae01b607-7f9d-414b-8fcd-61155d89b666'] = $pane;
    $display->panels['middle'][0] = 'new-ae01b607-7f9d-414b-8fcd-61155d89b666';
    $pane = new stdClass();
    $pane->pid = 'new-6a7cb090-8e3e-4500-bc3e-6101ddb062ca';
    $pane->panel = 'middle';
    $pane->type = 'bean_panels';
    $pane->subtype = 'bean_panels';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'default',
      'bean_delta' => 'sony-music-collage',
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '6a7cb090-8e3e-4500-bc3e-6101ddb062ca';
    $display->content['new-6a7cb090-8e3e-4500-bc3e-6101ddb062ca'] = $pane;
    $display->panels['middle'][1] = 'new-6a7cb090-8e3e-4500-bc3e-6101ddb062ca';
    $pane = new stdClass();
    $pane->pid = 'new-ecf62fec-dca5-4026-b0a3-ddeec4543133';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'mapbox-mapbox';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ecf62fec-dca5-4026-b0a3-ddeec4543133';
    $display->content['new-ecf62fec-dca5-4026-b0a3-ddeec4543133'] = $pane;
    $display->panels['top'][0] = 'new-ecf62fec-dca5-4026-b0a3-ddeec4543133';
    $pane = new stdClass();
    $pane->pid = 'new-b497cd18-edd3-41d3-b54b-4370cf9f949b';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'block-12';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => 'A Day in the Life of a Rep',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'b497cd18-edd3-41d3-b54b-4370cf9f949b';
    $display->content['new-b497cd18-edd3-41d3-b54b-4370cf9f949b'] = $pane;
    $display->panels['top'][1] = 'new-b497cd18-edd3-41d3-b54b-4370cf9f949b';
    $pane = new stdClass();
    $pane->pid = 'new-556d02f2-a404-4236-a07d-7afe9b42be46';
    $pane->panel = 'top';
    $pane->type = 'views';
    $pane->subtype = 'spotlight';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '3',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'spotlight',
      'override_title' => 1,
      'override_title_text' => 'Spotlight',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '556d02f2-a404-4236-a07d-7afe9b42be46';
    $display->content['new-556d02f2-a404-4236-a07d-7afe9b42be46'] = $pane;
    $display->panels['top'][2] = 'new-556d02f2-a404-4236-a07d-7afe9b42be46';
    $pane = new stdClass();
    $pane->pid = 'new-bf47f1b6-e3b0-49fc-a228-f682ae70dae1';
    $pane->panel = 'top';
    $pane->type = 'block';
    $pane->subtype = 'block-15';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Happening Now',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'bf47f1b6-e3b0-49fc-a228-f682ae70dae1';
    $display->content['new-bf47f1b6-e3b0-49fc-a228-f682ae70dae1'] = $pane;
    $display->panels['top'][3] = 'new-bf47f1b6-e3b0-49fc-a228-f682ae70dae1';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['homepage'] = $page;

  return $pages;

}
