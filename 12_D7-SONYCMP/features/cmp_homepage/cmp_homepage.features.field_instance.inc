<?php
/**
 * @file
 * cmp_homepage.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function cmp_homepage_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'bean-collage-field_collage_body'.
  $field_instances['bean-collage-field_collage_body'] = array(
    'bundle' => 'collage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'footer_collage' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bean',
    'field_name' => 'field_collage_body',
    'label' => 'Collage body',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'textarea_maxlength_js_widget',
      'settings' => array(
        'enable_recommended_maxlength' => 0,
        'format' => 'full_html',
        'recommended_maxlength' => 100,
        'rows' => 5,
      ),
      'type' => 'text_textarea_maxlength_js',
      'weight' => 12,
    ),
  );

  // Exported field_instance: 'bean-collage-field_collage_image'.
  $field_instances['bean-collage-field_collage_image'] = array(
    'bundle' => 'collage',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'collageformatter',
        'settings' => array(
          'advanced' => array(
            'original_image_reference' => 'symlink',
          ),
          'border_color' => '#000000',
          'border_size' => 0,
          'collage_border_color' => '#ffffff',
          'collage_border_size' => 0,
          'collage_height' => '',
          'collage_number' => 1,
          'collage_orientation' => 0,
          'collage_width' => 1920,
          'gap_color' => '#ffffff',
          'gap_size' => 0,
          'generate_image_derivatives' => 0,
          'image_link' => '',
          'image_link_class' => '',
          'image_link_image_style' => '',
          'image_link_modal' => '',
          'image_link_rel' => '',
          'images_per_collage' => '',
          'images_to_skip' => 0,
          'prevent_upscale' => 0,
        ),
        'type' => 'collageformatter',
        'weight' => 1,
      ),
      'footer_collage' => array(
        'label' => 'hidden',
        'module' => 'collageformatter',
        'settings' => array(
          'advanced' => array(
            'original_image_reference' => 'symlink',
          ),
          'border_color' => '#000000',
          'border_size' => 0,
          'collage_border_color' => '#ffffff',
          'collage_border_size' => 0,
          'collage_height' => 215,
          'collage_number' => 1,
          'collage_orientation' => 0,
          'collage_width' => '',
          'gap_color' => '#ffffff',
          'gap_size' => 0,
          'generate_image_derivatives' => 0,
          'image_link' => 'file',
          'image_link_class' => '',
          'image_link_image_style' => '',
          'image_link_modal' => '',
          'image_link_rel' => '',
          'images_per_collage' => '',
          'images_to_skip' => 0,
          'prevent_upscale' => 0,
        ),
        'type' => 'collageformatter',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bean',
    'field_name' => 'field_collage_image',
    'label' => 'Collage Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'dragndrop_upload_image',
      'settings' => array(
        'allow_replace' => 0,
        'droppable_area_text' => 'Drop files here to upload',
        'media_browser' => 0,
        'multiupload' => 0,
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
        'standard_upload' => 1,
        'upload_button_text' => 'Upload',
        'upload_event' => 'manual',
      ),
      'type' => 'dragndrop_upload_image',
      'weight' => 11,
    ),
  );

  // Exported field_instance: 'node-spotlight-body'.
  $field_instances['node-spotlight-body'] = array(
    'bundle' => 'spotlight',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
      'tours_for_rep' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Description',
    'required' => 0,
    'settings' => array(
      'display_summary' => 1,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-spotlight-field_imagespotlight'.
  $field_instances['node-spotlight-field_imagespotlight'] = array(
    'bundle' => 'spotlight',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'tours_for_rep' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_imagespotlight',
    'label' => 'Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-spotlight-field_student'.
  $field_instances['node-spotlight-field_student'] = array(
    'bundle' => 'spotlight',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'tours_for_rep' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_student',
    'label' => 'Student',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 6,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Collage Image');
  t('Collage body');
  t('Description');
  t('Image');
  t('Student');

  return $field_instances;
}
