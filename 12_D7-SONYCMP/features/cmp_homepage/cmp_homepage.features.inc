<?php
/**
 * @file
 * cmp_homepage.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cmp_homepage_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "bean_admin_ui" && $api == "bean") {
    return array("version" => "5");
  }
  if ($module == "ife" && $api == "ife") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cmp_homepage_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function cmp_homepage_image_default_styles() {
  $styles = array();

  // Exported image style: collage__big_avatar.
  $styles['collage__big_avatar'] = array(
    'label' => 'Collage: big avatar',
    'effects' => array(
      19 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 212,
          'height' => 212,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: collage__small_avatar.
  $styles['collage__small_avatar'] = array(
    'label' => 'Collage: small avatar',
    'effects' => array(
      20 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 106,
          'height' => 106,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: spotlight_275x275.
  $styles['spotlight_275x275'] = array(
    'label' => 'Spotlight 275x275',
    'effects' => array(
      13 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 275,
          'height' => 275,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function cmp_homepage_node_info() {
  $items = array(
    'spotlight' => array(
      'name' => t('Spotlight'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
