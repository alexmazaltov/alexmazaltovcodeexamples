<?php
/**
 * @file
 * cmp_homepage.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function cmp_homepage_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'A Day in the Life';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'a_day_in_the_life';
  $fe_block_boxes->body = '<h2 class="pane-title hide-for-mobile">A Day in the Life of a Rep</h2>
<h2 class = "pane-title devided-title show-for-mobile">
<div>A Day in the Life</div>
<div>of a Rep</div>
</h2>
<div class="description">Close to the action and even closer to the artists</div>
<div><a class="button btn btn--orange" href="http://sonymusic.com/careers/" target="_blank">Apply to be a College Rep</a></div>
<ul class="icons-list">
  <li class="icon-list_item icon-list_item--campus">Campus Events</li>
  <li class="icon-list_item icon-list_item--marketing">Lifestyle Marketing</li>
  <li class="icon-list_item icon-list_item--parties">Listening Parties</li>
  <li class="icon-list_item icon-list_item--artist">Artist Development</li>
  <li class="icon-list_item icon-list_item--social">Social Media Promotions</li>
  <li class="icon-list_item icon-list_item--grassroots">Grassroots Marketing</li>
  <li class="icon-list_item icon-list_item--live">Live Shows</li>
</ul>';

  $export['a_day_in_the_life'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Become a rep link & User login link';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'become_a_rep';
  $fe_block_boxes->body = '<ul>
	<li><a href="http://sonymusic.com/careers/" target="_blank">Become a Rep</a></li>
	<li><a href="/user/login" onclick="return false;" id="ajax-login-link">Login</a></li>
</ul>
';

  $export['become_a_rep'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Footer section';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'footer_section';
  $fe_block_boxes->body = '<div class="section_content-wrapper section_content-wrapper--bordered section_content-wrapper--small">
<div class="section_content section_content--highlighted">Sony Music College Marketing<br />
<span class="lighter">HAS CAPTURED THE NEW<br/> AND THE NEXT SINCE THE EARLY ‘60s</span></div>
</div>
';

  $export['footer_section'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Happening Now block';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'happening_now';
  $fe_block_boxes->body = '<div class="description">Scroll through some current department highlights and be sure
to follow our College 101 Playlist on Spotify</div>
';

  $export['happening_now'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Logout link';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'logout_link';
  $fe_block_boxes->body = '<p class="rteright"><a href="/settings">Profile</a> &nbsp; <a href="/reps/logout">Logout</a></p>
';

  $export['logout_link'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'More About Sony Music';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'more_about';
  $fe_block_boxes->body = '<div><a class="button more btn btn--orange" href="https://www.sonymusic.com/" target="_blank">More About Sony Music</a></div>
';

  $export['more_about'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Social Network Icons';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'social_network_icons';
  $fe_block_boxes->body = '<p>
  <a href="https://twitter.com/SonyMusicU" target="_blank">
  <img class="icn-mobile" alt="Twitter icon" src="/sites/all/themes/sonycmp/images/tw-grey.png" style="height:22px; width:26px" />
  <img class="icn-desktop" alt="Twitter icon" src="/sites/all/themes/sonycmp/images/tw.png" style="height:25px; width:29px" /></a>&nbsp;
  <a href="https://instagram.com/sonymusicu/" target="_blank">
  <img class="icn-mobile" alt="Instagram icon" src="/sites/all/themes/sonycmp/images/ins-grey.png" style="height:23px; width:22px" />
  <img class="icn-desktop" alt="Instagram icon" src="/sites/all/themes/sonycmp/images/ins.png" style="height:25px; width:29px" /></a>&nbsp;
  <a href="https://www.youtube.com/channel/UCqd8NzWNwQtG2IlNbNyCuyA" target="_blank">
  <img class="icn-mobile" alt="Youtube icon" src="/sites/all/themes/sonycmp/images/yt-grey.png" style="height:25px; width:21px" />
  <img class="icn-desktop" alt="Youtube icon" src="/sites/all/themes/sonycmp/images/yt.png" style="height:26px; width:29px" /></a></p>';

  $export['social_network_icons'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Spotify Playlist';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'spotify_playlist';
  $fe_block_boxes->body = '<p><iframe allowtransparency="true" frameborder="0" height="100%" src="https://embed.spotify.com/?uri=spotify:user:myplay.com:playlist:0VkhN48djiq7VMzTc2nuZ9" width="100%"></iframe></p>
';

  $export['spotify_playlist'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'ST2: A Day in the Life';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'st2_a_day_in_the_life';
  $fe_block_boxes->body = '          <div class="wrapper-rep">
            <div class="title">
              <h2>A Day in the Life of a Rep</h2>

              <div class="description">
                <p>Close to the action and even closer to the artists</p>
              </div>
            </div>
            <div class="wrapper-btn">
              <a  class="btn btn--red btn--lg" href="http://sonymusic.com/careers/" target="_blank">Apply to be a College Rep</a>
            </div>
            <div class="wrapper-info">
              <ul class="list-info">
                <li class="item-info--graduation_cap"><span class="text">Campus<br/>Events</span></li>
                <li class="item-info--signal"><span class="text">Lifestyle<br/>Marketing</span></li>
                <li class="item-info--record_player"><span class="text">Listening<br/>Parties</span></li>
                <li class="item-info--mixer"><span class="text">Artist<br/>Development</span></li>
                <li class="item-info--twitter_front"><span class="text">Social Media<br/>Promotions</span></li>
                <li class="item-info--lightbulb"><span class="text">Grassroots<br/>Marketing</span></li>
                <li class="item-info--rock_on"><span class="text">Live<br/>Shows</span></li>
              </ul>
            </div>
          </div>
';

  $export['st2_a_day_in_the_life'] = $fe_block_boxes;

  return $export;
}
