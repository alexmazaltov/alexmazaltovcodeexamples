<?php
/**
 * @file
 * cmp_homepage.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cmp_homepage_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create any collage bean'.
  $permissions['create any collage bean'] = array(
    'name' => 'create any collage bean',
    'roles' => array(),
    'module' => 'bean',
  );

  // Exported permission: 'create spotlight content'.
  $permissions['create spotlight content'] = array(
    'name' => 'create spotlight content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any collage bean'.
  $permissions['delete any collage bean'] = array(
    'name' => 'delete any collage bean',
    'roles' => array(),
    'module' => 'bean',
  );

  // Exported permission: 'delete any spotlight content'.
  $permissions['delete any spotlight content'] = array(
    'name' => 'delete any spotlight content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own spotlight content'.
  $permissions['delete own spotlight content'] = array(
    'name' => 'delete own spotlight content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any collage bean'.
  $permissions['edit any collage bean'] = array(
    'name' => 'edit any collage bean',
    'roles' => array(),
    'module' => 'bean',
  );

  // Exported permission: 'edit any spotlight content'.
  $permissions['edit any spotlight content'] = array(
    'name' => 'edit any spotlight content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own spotlight content'.
  $permissions['edit own spotlight content'] = array(
    'name' => 'edit own spotlight content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view any collage bean'.
  $permissions['view any collage bean'] = array(
    'name' => 'view any collage bean',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'bean',
  );

  return $permissions;
}
