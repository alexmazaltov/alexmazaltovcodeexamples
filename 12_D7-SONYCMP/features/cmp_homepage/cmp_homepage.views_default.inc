<?php
/**
 * @file
 * cmp_homepage.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cmp_homepage_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'collage_users_with_avatars';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'users';
  $view->human_name = 'Collage: users with avatars';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access user profiles';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: User: Uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'users';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  /* Field: User: Picture */
  $handler->display->display_options['fields']['picture']['id'] = 'picture';
  $handler->display->display_options['fields']['picture']['table'] = 'users';
  $handler->display->display_options['fields']['picture']['field'] = 'picture';
  $handler->display->display_options['fields']['picture']['link_photo_to_profile'] = FALSE;
  /* Filter criterion: User: Active */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'users';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: User: Picture */
  $handler->display->display_options['filters']['picture']['id'] = 'picture';
  $handler->display->display_options['filters']['picture']['table'] = 'users';
  $handler->display->display_options['filters']['picture']['field'] = 'picture';
  $handler->display->display_options['filters']['picture']['value'] = '1';
  /* Filter criterion: User: Roles */
  $handler->display->display_options['filters']['rid']['id'] = 'rid';
  $handler->display->display_options['filters']['rid']['table'] = 'users_roles';
  $handler->display->display_options['filters']['rid']['field'] = 'rid';
  $handler->display->display_options['filters']['rid']['value'] = array(
    5 => '5',
  );
  $export['collage_users_with_avatars'] = $view;

  $view = new view();
  $view->name = 'spotlight';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Spotlight';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Spotlight';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'time';
  $handler->display->display_options['cache']['results_lifespan'] = '3600';
  $handler->display->display_options['cache']['results_lifespan_custom'] = '0';
  $handler->display->display_options['cache']['output_lifespan'] = '3600';
  $handler->display->display_options['cache']['output_lifespan_custom'] = '0';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Header: Global: Unfiltered text */
  $handler->display->display_options['header']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['header']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['content'] = '<div class="description">Reps on the Rise</div>';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_student_target_id']['id'] = 'field_student_target_id';
  $handler->display->display_options['relationships']['field_student_target_id']['table'] = 'field_data_field_student';
  $handler->display->display_options['relationships']['field_student_target_id']['field'] = 'field_student_target_id';
  $handler->display->display_options['relationships']['field_student_target_id']['label'] = 'student';
  $handler->display->display_options['relationships']['field_student_target_id']['required'] = TRUE;
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_college_target_id']['id'] = 'field_college_target_id';
  $handler->display->display_options['relationships']['field_college_target_id']['table'] = 'field_data_field_college';
  $handler->display->display_options['relationships']['field_college_target_id']['field'] = 'field_college_target_id';
  $handler->display->display_options['relationships']['field_college_target_id']['relationship'] = 'field_student_target_id';
  $handler->display->display_options['relationships']['field_college_target_id']['label'] = 'college';
  $handler->display->display_options['relationships']['field_college_target_id']['required'] = TRUE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_imagespotlight']['id'] = 'field_imagespotlight';
  $handler->display->display_options['fields']['field_imagespotlight']['table'] = 'field_data_field_imagespotlight';
  $handler->display->display_options['fields']['field_imagespotlight']['field'] = 'field_imagespotlight';
  $handler->display->display_options['fields']['field_imagespotlight']['label'] = '';
  $handler->display->display_options['fields']['field_imagespotlight']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_imagespotlight']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_imagespotlight']['settings'] = array(
    'image_style' => 'spotlight_275x275',
    'image_link' => '',
  );
  /* Field: User: Picture */
  $handler->display->display_options['fields']['picture']['id'] = 'picture';
  $handler->display->display_options['fields']['picture']['table'] = 'users';
  $handler->display->display_options['fields']['picture']['field'] = 'picture';
  $handler->display->display_options['fields']['picture']['relationship'] = 'field_student_target_id';
  $handler->display->display_options['fields']['picture']['label'] = '';
  $handler->display->display_options['fields']['picture']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['picture']['empty'] = '<img src="/sites/all/themes/sonycmp/images/user-image-default.png" width="80" height="80" />';
  $handler->display->display_options['fields']['picture']['link_photo_to_profile'] = FALSE;
  $handler->display->display_options['fields']['picture']['image_style'] = 'medium_avatar_80x80';
  /* Field: User: First Name */
  $handler->display->display_options['fields']['field_first_name']['id'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['table'] = 'field_data_field_first_name';
  $handler->display->display_options['fields']['field_first_name']['field'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['relationship'] = 'field_student_target_id';
  $handler->display->display_options['fields']['field_first_name']['label'] = '';
  $handler->display->display_options['fields']['field_first_name']['element_label_colon'] = FALSE;
  /* Field: User: Market */
  $handler->display->display_options['fields']['field_market']['id'] = 'field_market';
  $handler->display->display_options['fields']['field_market']['table'] = 'field_data_field_market';
  $handler->display->display_options['fields']['field_market']['field'] = 'field_market';
  $handler->display->display_options['fields']['field_market']['relationship'] = 'field_student_target_id';
  $handler->display->display_options['fields']['field_market']['label'] = '';
  $handler->display->display_options['fields']['field_market']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_market']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Taxonomy term: Short name */
  $handler->display->display_options['fields']['field_short_name']['id'] = 'field_short_name';
  $handler->display->display_options['fields']['field_short_name']['table'] = 'field_data_field_short_name';
  $handler->display->display_options['fields']['field_short_name']['field'] = 'field_short_name';
  $handler->display->display_options['fields']['field_short_name']['relationship'] = 'field_college_target_id';
  $handler->display->display_options['fields']['field_short_name']['label'] = '';
  $handler->display->display_options['fields']['field_short_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_short_name']['type'] = 'text_plain';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Description */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'spotlight' => 'spotlight',
  );

  /* Display: Spotlight Block */
  $handler = $view->new_display('block', 'Spotlight Block', 'spotlight');
  $handler->display->display_options['block_description'] = 'Spotlight';

  /* Display: ST2: Spotlight Block */
  $handler = $view->new_display('block', 'ST2: Spotlight Block', 'st2_spotlight');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_imagespotlight' => 'field_imagespotlight',
    'picture' => 'picture',
    'field_first_name' => 'field_first_name',
    'field_market' => 'field_market',
    'field_short_name' => 'field_short_name',
    'title' => 'title',
    'body' => 'body',
  );
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['block_description'] = 'Spotlight';
  $export['spotlight'] = $view;

  $view = new view();
  $view->name = 'video';
  $view->description = '';
  $view->tag = 'homepage';
  $view->base_table = 'node';
  $view->human_name = 'Video';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Happening Now';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'time';
  $handler->display->display_options['cache']['results_lifespan'] = '3600';
  $handler->display->display_options['cache']['results_lifespan_custom'] = '0';
  $handler->display->display_options['cache']['output_lifespan'] = '3600';
  $handler->display->display_options['cache']['output_lifespan_custom'] = '0';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '4';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Youtube video */
  $handler->display->display_options['fields']['field_youtube_video']['id'] = 'field_youtube_video';
  $handler->display->display_options['fields']['field_youtube_video']['table'] = 'field_data_field_youtube_video';
  $handler->display->display_options['fields']['field_youtube_video']['field'] = 'field_youtube_video';
  $handler->display->display_options['fields']['field_youtube_video']['label'] = '';
  $handler->display->display_options['fields']['field_youtube_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_youtube_video']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_youtube_video']['type'] = 'media_colorbox';
  $handler->display->display_options['fields']['field_youtube_video']['settings'] = array(
    'file_view_mode' => 'frontpage_preview',
    'colorbox_view_mode' => 'colorbox',
    'fixed_width' => '',
    'fixed_height' => '',
    'colorbox_gallery' => 'none',
    'colorbox_gallery_custom' => '',
    'colorbox_caption' => 'none',
    'audio_playlist' => 0,
  );
  /* Sort criterion: Content: Weight (field_weight) */
  $handler->display->display_options['sorts']['field_weight_value']['id'] = 'field_weight_value';
  $handler->display->display_options['sorts']['field_weight_value']['table'] = 'field_data_field_weight';
  $handler->display->display_options['sorts']['field_weight_value']['field'] = 'field_weight_value';
  /* Sort criterion: Content: Nid */
  $handler->display->display_options['sorts']['nid']['id'] = 'nid';
  $handler->display->display_options['sorts']['nid']['table'] = 'node';
  $handler->display->display_options['sorts']['nid']['field'] = 'nid';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'video' => 'video',
  );

  /* Display: ST1: Video grid 4x4 */
  $handler = $view->new_display('block', 'ST1: Video grid 4x4', 'block');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Youtube video */
  $handler->display->display_options['fields']['field_youtube_video']['id'] = 'field_youtube_video';
  $handler->display->display_options['fields']['field_youtube_video']['table'] = 'field_data_field_youtube_video';
  $handler->display->display_options['fields']['field_youtube_video']['field'] = 'field_youtube_video';
  $handler->display->display_options['fields']['field_youtube_video']['label'] = '';
  $handler->display->display_options['fields']['field_youtube_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_youtube_video']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_youtube_video']['type'] = 'media_colorbox';
  $handler->display->display_options['fields']['field_youtube_video']['settings'] = array(
    'file_view_mode' => 'frontpage_preview',
    'colorbox_view_mode' => 'colorbox',
    'fixed_width' => '',
    'fixed_height' => '',
    'colorbox_gallery' => 'none',
    'colorbox_gallery_custom' => '',
    'colorbox_caption' => 'none',
    'audio_playlist' => 0,
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Short description */
  $handler->display->display_options['fields']['field_short_description']['id'] = 'field_short_description';
  $handler->display->display_options['fields']['field_short_description']['table'] = 'field_data_field_short_description';
  $handler->display->display_options['fields']['field_short_description']['field'] = 'field_short_description';
  $handler->display->display_options['fields']['field_short_description']['label'] = '';
  $handler->display->display_options['fields']['field_short_description']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_short_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_short_description']['element_default_classes'] = FALSE;
  $handler->display->display_options['block_description'] = 'Video grid 4x4';

  /* Display: ST2: Video grid 4x4 */
  $handler = $view->new_display('panel_pane', 'ST2: Video grid 4x4', 'st2_grid_4x4');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'block-video';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = 'Spotify hint';
  $handler->display->display_options['header']['area']['content'] = 'Scroll through some current department highlights and be sure to follow our College 101 Playlist on Spotify';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $export['video'] = $view;

  return $export;
}
