<?php
/**
 * @file
 * cmp_homepage.ife.inc
 */

/**
 * Implements hook_ife_default_settings().
 */
function cmp_homepage_ife_default_settings() {
  $export = array();

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'user_login_block';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['user_login_block'] = $ife;

  return $export;
}
