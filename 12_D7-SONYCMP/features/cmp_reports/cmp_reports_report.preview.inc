<?php

/**
 * Callback function for rendering a full_mode_view of reports content type
 *  and returns markup with ajax command for replacing content
 *  in existing div#wrapper-report-sidebar on the page tasks/%nid
 * Function is called from  onClick event that was rendered in cmp_reports_view_report_info_and_button()
 *   where link of this button hase hardcoded class for 'use-ajax = true'
 *
 * @param $nid
 *  Node ID of the report-node for which the 'full'-mode view are requested
 */
function cmp_reports_get_report_full($nid) {
  $report_node = node_load($nid);
  if (!$report_node || !in_array($report_node->type, array(
      SONY_CMP_CT_BI_WEEKLY_REPORT,
      SONY_CMP_CT_TOUR_REPORT,
      SONY_CMP_CT_MISC_REPORT,
    ))
  ) {
    ctools_include('ajax');
    $commands[] = ctools_ajax_command_redirect('not-found');
    return array('#type' => 'ajax', '#commands' => $commands);
  }
  else {
    $report_view = node_view($report_node, 'full');
    $path = drupal_get_path('theme', 'sonycmp');
    drupal_add_js($path . '/js/reports_full_sidebar.js');
    $markup = drupal_render($report_view);
    $commands[] = ajax_command_html('#wrapper-report-sidebar', $markup);
    $commands[] = ajax_command_invoke('#wrapper-report-sidebar', 'addClass', array('visible'));
    $commands[] = array('command' => 'htabs_custom_width');
    return array('#type' => 'ajax', '#commands' => $commands);
  }
}
