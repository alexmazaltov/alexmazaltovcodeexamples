<?php

function cmp_reports_edit_report_form_misc_report_details($form, &$form_state, $report, $task, $current_step) {
  return sonycmp_reports_edit_form_form($form, $form_state, $report, $task, $current_step);

}

function cmp_reports_edit_report_form_misc_report_details_validate($form, &$form_state) {
  //Convert date field
  if (!empty($form_state['values']['field_event_date']) && !is_array($form_state['values']['field_event_date'])) {
    $date = strtotime($form_state['values']['field_event_date']);
    if ($date) {
      form_set_value($form['report_main']['left']['field_event_date'], $date, $form_state);
    }
  }
  sonycmp_reports_edit_form_validate($form, $form_state);
}

