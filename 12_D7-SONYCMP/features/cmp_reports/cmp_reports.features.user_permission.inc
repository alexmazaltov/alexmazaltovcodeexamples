<?php
/**
 * @file
 * cmp_reports.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cmp_reports_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create bi_weekly_report content'.
  $permissions['create bi_weekly_report content'] = array(
    'name' => 'create bi_weekly_report content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create misc_report content'.
  $permissions['create misc_report content'] = array(
    'name' => 'create misc_report content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create tour_report content'.
  $permissions['create tour_report content'] = array(
    'name' => 'create tour_report content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bi_weekly_report content'.
  $permissions['delete any bi_weekly_report content'] = array(
    'name' => 'delete any bi_weekly_report content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any misc_report content'.
  $permissions['delete any misc_report content'] = array(
    'name' => 'delete any misc_report content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any tour_report content'.
  $permissions['delete any tour_report content'] = array(
    'name' => 'delete any tour_report content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bi_weekly_report content'.
  $permissions['delete own bi_weekly_report content'] = array(
    'name' => 'delete own bi_weekly_report content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own misc_report content'.
  $permissions['delete own misc_report content'] = array(
    'name' => 'delete own misc_report content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own tour_report content'.
  $permissions['delete own tour_report content'] = array(
    'name' => 'delete own tour_report content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bi_weekly_report content'.
  $permissions['edit any bi_weekly_report content'] = array(
    'name' => 'edit any bi_weekly_report content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any misc_report content'.
  $permissions['edit any misc_report content'] = array(
    'name' => 'edit any misc_report content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any tour_report content'.
  $permissions['edit any tour_report content'] = array(
    'name' => 'edit any tour_report content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bi_weekly_report content'.
  $permissions['edit own bi_weekly_report content'] = array(
    'name' => 'edit own bi_weekly_report content',
    'roles' => array(
      'administrator' => 'administrator',
      'representative' => 'representative',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own misc_report content'.
  $permissions['edit own misc_report content'] = array(
    'name' => 'edit own misc_report content',
    'roles' => array(
      'administrator' => 'administrator',
      'representative' => 'representative',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own tour_report content'.
  $permissions['edit own tour_report content'] = array(
    'name' => 'edit own tour_report content',
    'roles' => array(
      'administrator' => 'administrator',
      'representative' => 'representative',
    ),
    'module' => 'node',
  );

  return $permissions;
}
