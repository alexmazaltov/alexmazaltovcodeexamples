<?php
/**
 * @file
 * cmp_reports.ife.inc
 */

/**
 * Implements hook_ife_default_settings().
 */
function cmp_reports_ife_default_settings() {
  $export = array();

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_reports_edit_report_form_bi_weekly_report_market';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_reports_edit_report_form_bi_weekly_report_market'] = $ife;

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_reports_edit_report_form_bi_weekly_report_marketing';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_reports_edit_report_form_bi_weekly_report_marketing'] = $ife;

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_reports_edit_report_form_misc_report_details';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_reports_edit_report_form_misc_report_details'] = $ife;

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_reports_edit_report_form_misc_report_media';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_reports_edit_report_form_misc_report_media'] = $ife;

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_reports_edit_report_form_tour_report_details';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_reports_edit_report_form_tour_report_details'] = $ife;

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_reports_edit_report_form_tour_report_market';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_reports_edit_report_form_tour_report_market'] = $ife;

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_reports_edit_report_form_tour_report_marketing';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_reports_edit_report_form_tour_report_marketing'] = $ife;

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_reports_edit_report_form_tour_report_media';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_reports_edit_report_form_tour_report_media'] = $ife;

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_reports_edit_report_form_tour_report_venue';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_reports_edit_report_form_tour_report_venue'] = $ife;

  return $export;
}
