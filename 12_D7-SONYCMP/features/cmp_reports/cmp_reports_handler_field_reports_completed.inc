<?php

/**
 * @file
 * Definition of cmp_reports_handler_reports_completed.
 */
class cmp_reports_handler_field_reports_completed extends views_handler_field_numeric {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = array('table' => 'node', 'field' => 'nid');
  }
  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }
  function render($values) {
    $task_nid = $values->{$this->aliases['nid']};
    return cmp_reports_get_reports_count_by_task($task_nid, CMP_REPORTS_REPORT_STATUS_ON_TIME);
  }
}