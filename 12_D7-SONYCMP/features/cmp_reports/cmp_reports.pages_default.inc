<?php
/**
 * @file
 * cmp_reports.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function cmp_reports_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'edit_report';
  $page->task = 'page';
  $page->admin_title = 'Edit Report';
  $page->admin_description = '';
  $page->path = 'reports/%node/edit/!step';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 5,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'node_type',
        'settings' => array(
          'type' => array(
            'bi_weekly_report' => 'bi_weekly_report',
            'misc_report' => 'misc_report',
            'tour_report' => 'tour_report',
          ),
        ),
        'context' => 'argument_entity_id:node_1',
        'not' => FALSE,
      ),
      2 => array(
        'name' => 'node_access',
        'settings' => array(
          'type' => 'update',
        ),
        'context' => array(
          0 => 'logged-in-user',
          1 => 'argument_entity_id:node_1',
        ),
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'action',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'node' => array(
      'id' => 1,
      'identifier' => 'Task ID',
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
    'step' => array(
      'id' => '',
      'identifier' => '',
      'argument' => '',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_edit_report__panel_context_1203f35d-fc15-4dc7-b95c-c24d9ee63e9a';
  $handler->task = 'page';
  $handler->subtask = 'edit_report';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'report_edit';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'middle' => NULL,
      'bottom' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Edit Report %node:title';
  $display->uuid = '4c8b067a-c408-4a1d-8956-3268a870becd';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_edit_report__panel_context_1203f35d-fc15-4dc7-b95c-c24d9ee63e9a';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $pane->panel = 'above_left';
  $pane->type = 'block';
  $pane->subtype = 'system-main-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $display->content['new-b78d2652-77fe-49fc-852f-ed71a4bbd880'] = $pane;
  $display->panels['above_left'][0] = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $pane = new stdClass();
  $pane->pid = 'new-b9000028-0e2d-4669-84d3-b3e1331ea271';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'cmp_reports-edit_report_form';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b9000028-0e2d-4669-84d3-b3e1331ea271';
  $display->content['new-b9000028-0e2d-4669-84d3-b3e1331ea271'] = $pane;
  $display->panels['middle'][0] = 'new-b9000028-0e2d-4669-84d3-b3e1331ea271';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['edit_report'] = $page;

  return $pages;

}
