<?php
/**
 * @file
 * cmp_reports.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function cmp_reports_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views--exp-reports-panel_reports'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '-exp-reports-panel_reports',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
