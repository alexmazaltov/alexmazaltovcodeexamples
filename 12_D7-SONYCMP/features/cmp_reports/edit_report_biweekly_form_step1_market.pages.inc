<?php

function cmp_reports_edit_report_form_bi_weekly_report_market($form, &$form_state, $report, $task, $current_step) {
  return sonycmp_reports_edit_form_form($form, $form_state, $report, $task, $current_step);
}

function cmp_reports_edit_report_form_bi_weekly_report_market_validate($form, &$form_state) {
  sonycmp_reports_edit_form_validate($form, $form_state);
}
