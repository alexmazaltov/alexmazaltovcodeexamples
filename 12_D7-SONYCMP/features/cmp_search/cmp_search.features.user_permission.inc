<?php
/**
 * @file
 * cmp_search.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cmp_search_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'use search_api_autocomplete for search_api_views_search'.
  $permissions['use search_api_autocomplete for search_api_views_search'] = array(
    'name' => 'use search_api_autocomplete for search_api_views_search',
    'roles' => array(
      'administrator' => 'administrator',
      'task manager' => 'task manager',
    ),
    'module' => 'search_api_autocomplete',
  );

  return $permissions;
}
