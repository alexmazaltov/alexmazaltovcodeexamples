<?php
/**
 * @file
 * cmp_search.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function cmp_search_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views--exp-search-panel_search'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '-exp-search-panel_search',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'homepage',
    'roles' => array(
      'administrator' => 3,
      'task manager' => 4,
    ),
    'themes' => array(
      'sonycmp' => array(
        'region' => 'header_left',
        'status' => 1,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
