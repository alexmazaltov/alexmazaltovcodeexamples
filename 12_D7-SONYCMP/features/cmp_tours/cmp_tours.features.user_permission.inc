<?php
/**
 * @file
 * cmp_tours.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cmp_tours_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access user profiles'.
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      'administrator' => 'administrator',
      'representative' => 'representative',
      'task manager' => 'task manager',
    ),
    'module' => 'user',
  );

  return $permissions;
}
