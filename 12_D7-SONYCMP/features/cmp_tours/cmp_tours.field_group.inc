<?php
/**
 * @file
 * cmp_tours.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function cmp_tours_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_digital_tools|node|tours|form';
  $field_group->group_name = 'group_digital_tools';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'tours';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Digital Tools',
    'weight' => '5',
    'children' => array(
      0 => 'field_digital_tools_link',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Digital Tools',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-digital-tools field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_digital_tools|node|tours|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_physical_tools|node|tours|form';
  $field_group->group_name = 'group_physical_tools';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'tours';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Physical Tools',
    'weight' => '3',
    'children' => array(
      0 => 'field_physical_tools_description',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Physical Tools',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-physical-tools field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_physical_tools|node|tours|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_preshow_contact|node|tours|form';
  $field_group->group_name = 'group_preshow_contact';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'tours';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Pre-show contact',
    'weight' => '2',
    'children' => array(
      0 => 'field_field_tours_ps_email',
      1 => 'field_field_tours_ps_phone',
      2 => 'field_field_tours_ps_role',
      3 => 'field_tours_ps_contact_name',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Pre-show contact',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-preshow-contact field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_preshow_contact|node|tours|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tour_details|node|tours|form';
  $field_group->group_name = 'group_tour_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'tours';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Tour Details',
    'weight' => '1',
    'children' => array(
      0 => 'field_college_department_manager',
      1 => 'field_label_name',
      2 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Tour Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-tour-details field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_tour_details|node|tours|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Digital Tools');
  t('Physical Tools');
  t('Pre-show contact');
  t('Tour Details');

  return $field_groups;
}
