<?php
/**
 * @file
 * cmp_tours.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cmp_tours_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'tours';
  $view->description = 'Views with rendered entity of tour node in full mode view';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Tours';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Tours';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'time';
  $handler->display->display_options['cache']['results_lifespan'] = '3600';
  $handler->display->display_options['cache']['results_lifespan_custom'] = '0';
  $handler->display->display_options['cache']['output_lifespan'] = '3600';
  $handler->display->display_options['cache']['output_lifespan_custom'] = '0';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
  $handler->display->display_options['exposed_form']['options']['autosubmit_hide'] = FALSE;
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'combine' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'secondary_label' => NULL,
    'collapsible_label' => NULL,
    'combine_rewrite' => NULL,
    'reset_label' => NULL,
    'bef_filter_description' => NULL,
    'any_label' => NULL,
    'filter_rewrite_values' => NULL,
  );
  $handler->display->display_options['exposed_form']['options']['input_required'] = 0;
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'full_html';
  $handler->display->display_options['pager']['type'] = 'load_more';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['waypoint']['infinite'] = 1;
  $handler->display->display_options['pager']['options']['effects']['speed'] = 'slow';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['uses_fields'] = TRUE;
  $handler->display->display_options['row_plugin'] = 'entity';
  /* Relationship: Content: table_collection (field_table_collection) */
  $handler->display->display_options['relationships']['field_table_collection_value']['id'] = 'field_table_collection_value';
  $handler->display->display_options['relationships']['field_table_collection_value']['table'] = 'field_data_field_table_collection';
  $handler->display->display_options['relationships']['field_table_collection_value']['field'] = 'field_table_collection_value';
  $handler->display->display_options['relationships']['field_table_collection_value']['required'] = TRUE;
  $handler->display->display_options['relationships']['field_table_collection_value']['delta'] = '-1';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_tc_rep_name_target_id']['id'] = 'field_tc_rep_name_target_id';
  $handler->display->display_options['relationships']['field_tc_rep_name_target_id']['table'] = 'field_data_field_tc_rep_name';
  $handler->display->display_options['relationships']['field_tc_rep_name_target_id']['field'] = 'field_tc_rep_name_target_id';
  $handler->display->display_options['relationships']['field_tc_rep_name_target_id']['relationship'] = 'field_table_collection_value';
  $handler->display->display_options['relationships']['field_tc_rep_name_target_id']['required'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: User: First Name */
  $handler->display->display_options['fields']['field_first_name']['id'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['table'] = 'field_data_field_first_name';
  $handler->display->display_options['fields']['field_first_name']['field'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['relationship'] = 'field_tc_rep_name_target_id';
  $handler->display->display_options['fields']['field_first_name']['exclude'] = TRUE;
  /* Field: User: Last Name */
  $handler->display->display_options['fields']['field_last_name']['id'] = 'field_last_name';
  $handler->display->display_options['fields']['field_last_name']['table'] = 'field_data_field_last_name';
  $handler->display->display_options['fields']['field_last_name']['field'] = 'field_last_name';
  $handler->display->display_options['fields']['field_last_name']['relationship'] = 'field_tc_rep_name_target_id';
  $handler->display->display_options['fields']['field_last_name']['exclude'] = TRUE;
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'tours' => 'tours',
  );
  /* Filter criterion: Global: Combine fields filter */
  $handler->display->display_options['filters']['combine']['id'] = 'combine';
  $handler->display->display_options['filters']['combine']['table'] = 'views';
  $handler->display->display_options['filters']['combine']['field'] = 'combine';
  $handler->display->display_options['filters']['combine']['operator'] = 'contains';
  $handler->display->display_options['filters']['combine']['exposed'] = TRUE;
  $handler->display->display_options['filters']['combine']['expose']['operator_id'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['operator'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['identifier'] = 'combine';
  $handler->display->display_options['filters']['combine']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['combine']['expose']['autocomplete_filter'] = 1;
  $handler->display->display_options['filters']['combine']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['combine']['expose']['autocomplete_min_chars'] = '0';
  $handler->display->display_options['filters']['combine']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['combine']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['combine']['expose']['autocomplete_dependent'] = 0;
  $handler->display->display_options['filters']['combine']['fields'] = array(
    'field_last_name' => 'field_last_name',
  );

  /* Display: Tours manager */
  $handler = $view->new_display('block', 'Tours manager', 'tours_manger');
  $handler->display->display_options['display_description'] = 'Rendered full tours node for manager';

  /* Display: Tours rep */
  $handler = $view->new_display('block', 'Tours rep', 'tours_rep');
  $handler->display->display_options['display_description'] = 'Rendered full tours node for representaive without edit delete column';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['uses_fields'] = TRUE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'tours_for_rep';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $export['tours'] = $view;

  $view = new view();
  $view->name = 'tours_field_collection_view';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'field_collection_item';
  $view->human_name = 'Tours field collection view';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'host_entity_id' => 'host_entity_id',
    'uid' => 'uid',
    'field_tc_date' => 'field_tc_date',
    'field_tc_city_state' => 'field_tc_city_state',
    'field_tc_venue' => 'field_tc_venue',
    'field_first_name' => 'field_first_name',
    'field_last_name' => 'field_last_name',
    'field_tc_mt_sp_only_' => 'field_tc_mt_sp_only_',
    'field_tc_plus_one_' => 'field_tc_plus_one_',
    'nothing_1' => 'nothing_1',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'host_entity_id' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'uid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_tc_date' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_tc_city_state' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_tc_venue' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_first_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_last_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_tc_mt_sp_only_' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_tc_plus_one_' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing_1' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_tc_rep_name_target_id']['id'] = 'field_tc_rep_name_target_id';
  $handler->display->display_options['relationships']['field_tc_rep_name_target_id']['table'] = 'field_data_field_tc_rep_name';
  $handler->display->display_options['relationships']['field_tc_rep_name_target_id']['field'] = 'field_tc_rep_name_target_id';
  $handler->display->display_options['relationships']['field_tc_rep_name_target_id']['required'] = TRUE;
  /* Field: Field collection item: Host Entity ID */
  $handler->display->display_options['fields']['host_entity_id']['id'] = 'host_entity_id';
  $handler->display->display_options['fields']['host_entity_id']['table'] = 'field_collection_item';
  $handler->display->display_options['fields']['host_entity_id']['field'] = 'host_entity_id';
  $handler->display->display_options['fields']['host_entity_id']['label'] = '';
  $handler->display->display_options['fields']['host_entity_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['host_entity_id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['host_entity_id']['hide_alter_empty'] = FALSE;
  /* Field: User: Uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'users';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['relationship'] = 'field_tc_rep_name_target_id';
  $handler->display->display_options['fields']['uid']['exclude'] = TRUE;
  /* Field: Field collection item: Date */
  $handler->display->display_options['fields']['field_tc_date']['id'] = 'field_tc_date';
  $handler->display->display_options['fields']['field_tc_date']['table'] = 'field_data_field_tc_date';
  $handler->display->display_options['fields']['field_tc_date']['field'] = 'field_tc_date';
  $handler->display->display_options['fields']['field_tc_date']['settings'] = array(
    'format_type' => 'tiny',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Field collection item: City, State */
  $handler->display->display_options['fields']['field_tc_city_state']['id'] = 'field_tc_city_state';
  $handler->display->display_options['fields']['field_tc_city_state']['table'] = 'field_data_field_tc_city_state';
  $handler->display->display_options['fields']['field_tc_city_state']['field'] = 'field_tc_city_state';
  /* Field: Field collection item: Venue */
  $handler->display->display_options['fields']['field_tc_venue']['id'] = 'field_tc_venue';
  $handler->display->display_options['fields']['field_tc_venue']['table'] = 'field_data_field_tc_venue';
  $handler->display->display_options['fields']['field_tc_venue']['field'] = 'field_tc_venue';
  /* Field: User: First Name */
  $handler->display->display_options['fields']['field_first_name']['id'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['table'] = 'field_data_field_first_name';
  $handler->display->display_options['fields']['field_first_name']['field'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['relationship'] = 'field_tc_rep_name_target_id';
  $handler->display->display_options['fields']['field_first_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_first_name']['type'] = 'text_plain';
  /* Field: User: Last Name */
  $handler->display->display_options['fields']['field_last_name']['id'] = 'field_last_name';
  $handler->display->display_options['fields']['field_last_name']['table'] = 'field_data_field_last_name';
  $handler->display->display_options['fields']['field_last_name']['field'] = 'field_last_name';
  $handler->display->display_options['fields']['field_last_name']['relationship'] = 'field_tc_rep_name_target_id';
  $handler->display->display_options['fields']['field_last_name']['label'] = 'Rep Name';
  $handler->display->display_options['fields']['field_last_name']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_last_name']['alter']['text'] = '[field_first_name] [field_last_name]';
  $handler->display->display_options['fields']['field_last_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_last_name']['alter']['path'] = '/reps/[uid]';
  $handler->display->display_options['fields']['field_last_name']['type'] = 'text_plain';
  /* Field: Field collection item: Plus One? */
  $handler->display->display_options['fields']['field_tc_plus_one_']['id'] = 'field_tc_plus_one_';
  $handler->display->display_options['fields']['field_tc_plus_one_']['table'] = 'field_data_field_tc_plus_one_';
  $handler->display->display_options['fields']['field_tc_plus_one_']['field'] = 'field_tc_plus_one_';
  $handler->display->display_options['fields']['field_tc_plus_one_']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_tc_plus_one_']['alter']['text'] = '<span class="plus-one checked-[field_tc_plus_one_-value]">--</span>';
  /* Field: Field collection item: Physical Setup? */
  $handler->display->display_options['fields']['field_tc_physical_setup_']['id'] = 'field_tc_physical_setup_';
  $handler->display->display_options['fields']['field_tc_physical_setup_']['table'] = 'field_data_field_tc_physical_setup_';
  $handler->display->display_options['fields']['field_tc_physical_setup_']['field'] = 'field_tc_physical_setup_';
  $handler->display->display_options['fields']['field_tc_physical_setup_']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_tc_physical_setup_']['alter']['text'] = '<span class="physical-setup checked-[field_tc_physical_setup_-value]">--</span>';
  /* Field: Field collection item: Online Setup? */
  $handler->display->display_options['fields']['field_tc_online_setup']['id'] = 'field_tc_online_setup';
  $handler->display->display_options['fields']['field_tc_online_setup']['table'] = 'field_data_field_tc_online_setup';
  $handler->display->display_options['fields']['field_tc_online_setup']['field'] = 'field_tc_online_setup';
  $handler->display->display_options['fields']['field_tc_online_setup']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_tc_online_setup']['alter']['text'] = '<span class="online-setup checked-[field_tc_online_setup-value]">--</span>';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['label'] = 'empty';
  /* Sort criterion: Field collection item: Date (field_tc_date) */
  $handler->display->display_options['sorts']['field_tc_date_value']['id'] = 'field_tc_date_value';
  $handler->display->display_options['sorts']['field_tc_date_value']['table'] = 'field_data_field_tc_date';
  $handler->display->display_options['sorts']['field_tc_date_value']['field'] = 'field_tc_date_value';
  /* Contextual filter: Field collection item: Field collection item ID */
  $handler->display->display_options['arguments']['item_id']['id'] = 'item_id';
  $handler->display->display_options['arguments']['item_id']['table'] = 'field_collection_item';
  $handler->display->display_options['arguments']['item_id']['field'] = 'item_id';
  $handler->display->display_options['arguments']['item_id']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['item_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['item_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['item_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['item_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['item_id']['break_phrase'] = TRUE;
  /* Filter criterion: Global: Combine fields filter */
  $handler->display->display_options['filters']['combine']['id'] = 'combine';
  $handler->display->display_options['filters']['combine']['table'] = 'views';
  $handler->display->display_options['filters']['combine']['field'] = 'combine';
  $handler->display->display_options['filters']['combine']['operator'] = 'contains';
  $handler->display->display_options['filters']['combine']['exposed'] = TRUE;
  $handler->display->display_options['filters']['combine']['expose']['operator_id'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['operator'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['identifier'] = 'combine';
  $handler->display->display_options['filters']['combine']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['combine']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['combine']['expose']['autocomplete_min_chars'] = '0';
  $handler->display->display_options['filters']['combine']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['combine']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['combine']['expose']['autocomplete_dependent'] = 0;
  $handler->display->display_options['filters']['combine']['fields'] = array(
    'field_last_name' => 'field_last_name',
  );

  /* Display: tours fc rep */
  $handler = $view->new_display('block', 'tours fc rep', 'tours_fc_rep');
  $handler->display->display_options['display_description'] = 'tours filed collection for rep';
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Field collection item: Host Entity ID */
  $handler->display->display_options['fields']['host_entity_id']['id'] = 'host_entity_id';
  $handler->display->display_options['fields']['host_entity_id']['table'] = 'field_collection_item';
  $handler->display->display_options['fields']['host_entity_id']['field'] = 'host_entity_id';
  $handler->display->display_options['fields']['host_entity_id']['label'] = '';
  $handler->display->display_options['fields']['host_entity_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['host_entity_id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['host_entity_id']['hide_alter_empty'] = FALSE;
  /* Field: User: Uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'users';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['relationship'] = 'field_tc_rep_name_target_id';
  $handler->display->display_options['fields']['uid']['exclude'] = TRUE;
  /* Field: Field collection item: Date */
  $handler->display->display_options['fields']['field_tc_date']['id'] = 'field_tc_date';
  $handler->display->display_options['fields']['field_tc_date']['table'] = 'field_data_field_tc_date';
  $handler->display->display_options['fields']['field_tc_date']['field'] = 'field_tc_date';
  $handler->display->display_options['fields']['field_tc_date']['settings'] = array(
    'format_type' => 'tiny',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Field collection item: City, State */
  $handler->display->display_options['fields']['field_tc_city_state']['id'] = 'field_tc_city_state';
  $handler->display->display_options['fields']['field_tc_city_state']['table'] = 'field_data_field_tc_city_state';
  $handler->display->display_options['fields']['field_tc_city_state']['field'] = 'field_tc_city_state';
  /* Field: Field collection item: Venue */
  $handler->display->display_options['fields']['field_tc_venue']['id'] = 'field_tc_venue';
  $handler->display->display_options['fields']['field_tc_venue']['table'] = 'field_data_field_tc_venue';
  $handler->display->display_options['fields']['field_tc_venue']['field'] = 'field_tc_venue';
  /* Field: User: First Name */
  $handler->display->display_options['fields']['field_first_name']['id'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['table'] = 'field_data_field_first_name';
  $handler->display->display_options['fields']['field_first_name']['field'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['relationship'] = 'field_tc_rep_name_target_id';
  $handler->display->display_options['fields']['field_first_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_first_name']['type'] = 'text_plain';
  /* Field: User: Last Name */
  $handler->display->display_options['fields']['field_last_name']['id'] = 'field_last_name';
  $handler->display->display_options['fields']['field_last_name']['table'] = 'field_data_field_last_name';
  $handler->display->display_options['fields']['field_last_name']['field'] = 'field_last_name';
  $handler->display->display_options['fields']['field_last_name']['relationship'] = 'field_tc_rep_name_target_id';
  $handler->display->display_options['fields']['field_last_name']['label'] = 'Rep Name';
  $handler->display->display_options['fields']['field_last_name']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_last_name']['alter']['text'] = '[field_first_name] [field_last_name]';
  $handler->display->display_options['fields']['field_last_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_last_name']['alter']['path'] = '/reps/[uid]';
  $handler->display->display_options['fields']['field_last_name']['type'] = 'text_plain';
  /* Field: Field collection item: Plus One? */
  $handler->display->display_options['fields']['field_tc_plus_one_']['id'] = 'field_tc_plus_one_';
  $handler->display->display_options['fields']['field_tc_plus_one_']['table'] = 'field_data_field_tc_plus_one_';
  $handler->display->display_options['fields']['field_tc_plus_one_']['field'] = 'field_tc_plus_one_';
  $handler->display->display_options['fields']['field_tc_plus_one_']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_tc_plus_one_']['alter']['text'] = '<span class="plus-one checked-[field_tc_plus_one_-value]">--</span>';
  /* Field: Field collection item: Physical Setup? */
  $handler->display->display_options['fields']['field_tc_physical_setup_']['id'] = 'field_tc_physical_setup_';
  $handler->display->display_options['fields']['field_tc_physical_setup_']['table'] = 'field_data_field_tc_physical_setup_';
  $handler->display->display_options['fields']['field_tc_physical_setup_']['field'] = 'field_tc_physical_setup_';
  $handler->display->display_options['fields']['field_tc_physical_setup_']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_tc_physical_setup_']['alter']['text'] = '<span class="physical-setup checked-[field_tc_physical_setup_-value]">--</span>';
  /* Field: Field collection item: Online Setup? */
  $handler->display->display_options['fields']['field_tc_online_setup']['id'] = 'field_tc_online_setup';
  $handler->display->display_options['fields']['field_tc_online_setup']['table'] = 'field_data_field_tc_online_setup';
  $handler->display->display_options['fields']['field_tc_online_setup']['field'] = 'field_tc_online_setup';
  $handler->display->display_options['fields']['field_tc_online_setup']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_tc_online_setup']['alter']['text'] = '<span class="online-setup checked-[field_tc_online_setup-value]">--</span>';
  $export['tours_field_collection_view'] = $view;

  return $export;
}
