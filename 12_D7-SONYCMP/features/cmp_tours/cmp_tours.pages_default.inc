<?php
/**
 * @file
 * cmp_tours.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function cmp_tours_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'create_new_tour';
  $page->task = 'page';
  $page->admin_title = 'Create New Tour';
  $page->admin_description = '';
  $page->path = 'tours/new/add';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'action',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_create_new_tour__panel_context_ecbe80a9-b719-4144-8bd5-6c0740322f59';
  $handler->task = 'page';
  $handler->subtask = 'create_new_tour';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'tutorial_edit';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'middle' => NULL,
      'bottom' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Create New Tour';
  $display->uuid = '4c8b067a-c408-4a1d-8956-3268a870becd';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_create_new_tour__panel_context_ecbe80a9-b719-4144-8bd5-6c0740322f59';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $pane->panel = 'above_left';
  $pane->type = 'block';
  $pane->subtype = 'system-main-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $display->content['new-b78d2652-77fe-49fc-852f-ed71a4bbd880'] = $pane;
  $display->panels['above_left'][0] = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $pane = new stdClass();
  $pane->pid = 'new-103d5237-64bf-42e5-92ea-ed0378604dc8';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'cmp_tours-edit_tours_form';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '103d5237-64bf-42e5-92ea-ed0378604dc8';
  $display->content['new-103d5237-64bf-42e5-92ea-ed0378604dc8'] = $pane;
  $display->panels['middle'][0] = 'new-103d5237-64bf-42e5-92ea-ed0378604dc8';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['create_new_tour'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'edit_tour';
  $page->task = 'page';
  $page->admin_title = 'Edit Tour';
  $page->admin_description = '';
  $page->path = 'tours/%node/edit';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'entity_bundle:node',
        'settings' => array(
          'type' => array(
            'tours' => 'tours',
          ),
        ),
        'context' => 'argument_entity_id:node_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'action',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'node' => array(
      'id' => 1,
      'identifier' => 'tours ID',
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_edit_tour__panel_context_503a655f-f330-431e-b688-9cfec0280d25';
  $handler->task = 'page';
  $handler->subtask = 'edit_tour';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'tutorial_edit';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'middle' => NULL,
      'bottom' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Edit Tour';
  $display->uuid = '4c8b067a-c408-4a1d-8956-3268a870becd';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_edit_tour__panel_context_503a655f-f330-431e-b688-9cfec0280d25';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $pane->panel = 'above_left';
  $pane->type = 'block';
  $pane->subtype = 'system-main-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $display->content['new-b78d2652-77fe-49fc-852f-ed71a4bbd880'] = $pane;
  $display->panels['above_left'][0] = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $pane = new stdClass();
  $pane->pid = 'new-d081e1ad-a530-4652-87d8-0d11e2565acf';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'cmp_tours-edit_tours_form';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd081e1ad-a530-4652-87d8-0d11e2565acf';
  $display->content['new-d081e1ad-a530-4652-87d8-0d11e2565acf'] = $pane;
  $display->panels['middle'][0] = 'new-d081e1ad-a530-4652-87d8-0d11e2565acf';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-b78d2652-77fe-49fc-852f-ed71a4bbd880';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['edit_tour'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'tours';
  $page->task = 'page';
  $page->admin_title = 'Tours';
  $page->admin_description = '';
  $page->path = 'tours/list';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
            2 => 5,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'default tab',
    'title' => 'Tours',
    'name' => 'main-menu',
    'weight' => '-1',
    'parent' => array(
      'type' => 'normal',
      'title' => 'Tours',
      'name' => 'main-menu',
      'weight' => '-48',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_tours__panel_context_f63fadff-a7db-45f7-a935-ba06fa7b6918';
  $handler->task = 'page';
  $handler->subtask = 'tours';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Tutorials manager',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 3,
              1 => 4,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'tutorials';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Tours';
  $display->uuid = '003073fc-4e56-4cb5-91b3-067f64cde79c';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_tours__panel_context_f63fadff-a7db-45f7-a935-ba06fa7b6918';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-32102fbd-6918-4f3c-8596-feb0cd5cb863';
  $pane->panel = 'above_left';
  $pane->type = 'block';
  $pane->subtype = 'system-main-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '32102fbd-6918-4f3c-8596-feb0cd5cb863';
  $display->content['new-32102fbd-6918-4f3c-8596-feb0cd5cb863'] = $pane;
  $display->panels['above_left'][0] = 'new-32102fbd-6918-4f3c-8596-feb0cd5cb863';
  $pane = new stdClass();
  $pane->pid = 'new-d7631fec-4553-4a5f-8c23-044254780c24';
  $pane->panel = 'above_right';
  $pane->type = 'block';
  $pane->subtype = 'cmp_tours-create_tours_button';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd7631fec-4553-4a5f-8c23-044254780c24';
  $display->content['new-d7631fec-4553-4a5f-8c23-044254780c24'] = $pane;
  $display->panels['above_right'][0] = 'new-d7631fec-4553-4a5f-8c23-044254780c24';
  $pane = new stdClass();
  $pane->pid = 'new-2fd253f3-1d3a-44e1-8691-93ff5b9e9c89';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'tours';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '3',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'tours_manger',
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '2fd253f3-1d3a-44e1-8691-93ff5b9e9c89';
  $display->content['new-2fd253f3-1d3a-44e1-8691-93ff5b9e9c89'] = $pane;
  $display->panels['middle'][0] = 'new-2fd253f3-1d3a-44e1-8691-93ff5b9e9c89';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_tours__panel_context_637c4850-467e-4c69-9966-0dd46cb9a1ab';
  $handler->task = 'page';
  $handler->subtask = 'tours';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Tutorials (rep)',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 5,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'tutorials';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'above_left' => NULL,
      'above_right' => NULL,
      'top_left' => NULL,
      'top_right' => NULL,
      'below_left' => NULL,
      'below_right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Tours';
  $display->uuid = '003073fc-4e56-4cb5-91b3-067f64cde79c';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_tours__panel_context_637c4850-467e-4c69-9966-0dd46cb9a1ab';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-32102fbd-6918-4f3c-8596-feb0cd5cb863';
  $pane->panel = 'above_left';
  $pane->type = 'block';
  $pane->subtype = 'system-main-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '32102fbd-6918-4f3c-8596-feb0cd5cb863';
  $display->content['new-32102fbd-6918-4f3c-8596-feb0cd5cb863'] = $pane;
  $display->panels['above_left'][0] = 'new-32102fbd-6918-4f3c-8596-feb0cd5cb863';
  $pane = new stdClass();
  $pane->pid = 'new-37a5aac4-91cf-49d1-930e-5ae483798645';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'tours';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '3',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'tours_rep',
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '37a5aac4-91cf-49d1-930e-5ae483798645';
  $display->content['new-37a5aac4-91cf-49d1-930e-5ae483798645'] = $pane;
  $display->panels['middle'][0] = 'new-37a5aac4-91cf-49d1-930e-5ae483798645';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['tours'] = $page;

  return $pages;

}
