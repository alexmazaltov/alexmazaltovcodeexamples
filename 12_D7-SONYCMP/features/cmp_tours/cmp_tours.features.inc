<?php
/**
 * @file
 * cmp_tours.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cmp_tours_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "ife" && $api == "ife") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cmp_tours_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function cmp_tours_node_info() {
  $items = array(
    'tours' => array(
      'name' => t('Tours'),
      'base' => 'node_content',
      'description' => t('Content type for Tours page'),
      'has_title' => '1',
      'title_label' => t('Artist Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
