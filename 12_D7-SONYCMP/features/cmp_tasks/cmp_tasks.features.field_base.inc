<?php
/**
 * @file
 * cmp_tasks.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function cmp_tasks_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_associated_manager'.
  $field_bases['field_associated_manager'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_associated_manager',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'views',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'view' => array(
          'args' => array(),
          'display_name' => 'entityreference_1',
          'view_name' => 'active_managers',
        ),
      ),
      'target_type' => 'user',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_bw_allowed_content'.
  $field_bases['field_bw_allowed_content'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_bw_allowed_content',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'market' => 'Market Visibility',
        '-market_store' => 'Independent Record Store',
        '-market_campus' => 'On Campus',
        '-market_shops' => 'Off Campus',
        'marketing' => 'Online Marketing',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_bw_required_content'.
  $field_bases['field_bw_required_content'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_bw_required_content',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'market' => 'Market Visibility',
        '-market_store' => 'Independent Record Store',
        '-market_campus' => 'On Campus',
        '-market_shops' => 'Off Campus',
        'marketing' => 'Online Marketing',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_misc_allowed_content'.
  $field_bases['field_misc_allowed_content'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_misc_allowed_content',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'media' => 'Media',
        'marketing' => 'Online Marketing',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_misc_required_content'.
  $field_bases['field_misc_required_content'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_misc_required_content',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'media' => 'Media',
        'marketing' => 'Online Marketing',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_task_status'.
  $field_bases['field_task_status'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_task_status',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'Open',
        1 => 'Completed',
        2 => 'Overdue',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_integer',
  );

  // Exported field_base: 'field_tour_allowed_content'.
  $field_bases['field_tour_allowed_content'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_tour_allowed_content',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'media' => 'Performance Media',
        '-media_photos' => 'Performance Photos',
        '-media_video' => 'Performance Videos',
        'venue' => 'Venue Promo',
        '-venue_photos' => 'Promotion Efforts',
        '-venue_testimonials' => 'Fan Testimonials',
        'market' => 'Market Visibility',
        '-market_shops' => 'Off Campus',
        '-market_campus' => 'On Campus',
        '-market_store' => 'Independent Record Store',
        'marketing' => 'Online Marketing',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_tour_required_content'.
  $field_bases['field_tour_required_content'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_tour_required_content',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'media' => 'Performance Media',
        '-media_photos' => 'Performance Photos',
        '-media_video' => 'Performance Videos',
        'venue' => 'Venue Promo',
        '-venue_photos' => 'Promotion Efforts',
        '-venue_testimonials' => 'Fan Testimonials',
        'market' => 'Market Visibility',
        '-market_shops' => 'Off Campus',
        '-market_campus' => 'On Campus',
        '-market_store' => 'Independent Record Store',
        'marketing' => 'Online Marketing',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
