<?php
/**
 * @file
 * cmp_tasks.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function cmp_tasks_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: user-menu_tasks:tasks.
  $menu_links['user-menu_tasks:tasks'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'tasks',
    'router_path' => 'tasks',
    'link_title' => 'Tasks',
    'options' => array(
      'identifier' => 'user-menu_tasks:tasks',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Tasks');

  return $menu_links;
}
