<?php
/**
 * @file
 * cmp_tasks.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function cmp_tasks_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['cmp_tasks-create_task_button'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'create_task_button',
    'module' => 'cmp_tasks',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['cmp_tasks-task_statistics'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'task_statistics',
    'module' => 'cmp_tasks',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['sonycmp_tasks-create_self_misc_task_button'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'create_self_misc_task_button',
    'module' => 'sonycmp_tasks',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views--exp-tasks-block_archived_tasks'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '-exp-tasks-block_archived_tasks',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views--exp-tasks-block_tasks'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '-exp-tasks-block_tasks',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views--exp-tasks-panel_reps_tasks'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '-exp-tasks-panel_reps_tasks',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-00910c7818389e93f401ceff09aa66bc'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '00910c7818389e93f401ceff09aa66bc',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-tasks-block_archived_tasks'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'tasks-block_archived_tasks',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-tasks-block_tasks'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'tasks-block_tasks',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-tasks-open_reps_tasks'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'tasks-open_reps_tasks',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-tasks-open_tasks'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'tasks-open_tasks',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'user/*',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 3,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 3,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['views-tasks-overdue_reps_tasks'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'tasks-overdue_reps_tasks',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-tasks-overdue_tasks'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'tasks-overdue_tasks',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sonycmp' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp',
        'weight' => 0,
      ),
      'sonycmp_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sonycmp_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
