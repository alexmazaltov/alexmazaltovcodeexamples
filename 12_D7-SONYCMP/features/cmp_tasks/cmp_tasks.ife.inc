<?php
/**
 * @file
 * cmp_tasks.ife.inc
 */

/**
 * Implements hook_ife_default_settings().
 */
function cmp_tasks_ife_default_settings() {
  $export = array();

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_tasks_add_new_task';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_tasks_add_new_task'] = $ife;

  $ife = new stdClass();
  $ife->api_version = 1;
  $ife->form_id = 'cmp_tasks_edit_task_form_details';
  $ife->field_types = '';
  $ife->status = 1;
  $ife->display = 2;
  $export['cmp_tasks_edit_task_form_details'] = $ife;

  return $export;
}
