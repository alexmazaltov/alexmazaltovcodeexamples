<?php
/**
 * @file
 * cmp_tasks.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cmp_tasks_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ife" && $api == "ife") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cmp_tasks_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function cmp_tasks_node_info() {
  $items = array(
    'bi_weekly_task' => array(
      'name' => t('Priority'),
      'base' => 'node_content',
      'description' => t('Priority task created by managers, assigned to representatives'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'misc_task' => array(
      'name' => t('Misc'),
      'base' => 'node_content',
      'description' => t('Misc task created by managers, assigned to representatives'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'tour_task' => array(
      'name' => t('Tour'),
      'base' => 'node_content',
      'description' => t('Tour task created by managers, assigned to representatives'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
