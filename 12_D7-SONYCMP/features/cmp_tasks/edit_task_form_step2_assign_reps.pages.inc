<?php

function cmp_tasks_edit_task_form_assign_reps($form, &$form_state, $task = NULL) {

  $form = array();
  $form_state['storage']['#task_node'] = $task;

  cmp_tasks_edit_add_task_header($form, $task);

  $form['task_header']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('!action Attachments >', array('!action' => (($task->sticky) ? t('Edit') : t('Add')))),
    '#weight' => -1000,
    '#submit' => array('cmp_tasks_edit_task_form_assign_reps_next'),
    '#validate' => array('cmp_tasks_edit_task_form_assign_reps_validate'),
    '#attributes' => array('class' => array('button', 'submit-button')),
  );
  if ($form_state['storage']['#task_node']->sticky) {
    $form['task_header']['actions']['save_and_close'] = array(
      '#type' => 'submit',
      '#value' => t('Save and Close'),
      '#weight' => -500,
      '#submit' => array('cmp_tasks_edit_task_form_assign_reps_save'),
      '#validate' => array('cmp_tasks_edit_task_form_assign_reps_validate'),
      '#attributes' => array('class' => array('button', 'save-button')),
    );
  }

  cmp_tasks_edit_add_task_trail($form, $task, 'assign-reps');

  $form['task_main'] = array(
    '#type' => 'item',
    '#title' => t('The View'),
    '#title_display' => 'invisible',
    '#required' => TRUE,
    '#prefix' => '<div class="row">',
    '#suffix' => '</div><!-- .row main-->',
  );

  $field_reps = isset($task->field_assigned_to[LANGUAGE_NONE]) ? _cmp_common_get_target_ids_array($task->field_assigned_to[LANGUAGE_NONE]) : array();

  $form['task_main']['field_reps'] = array(
    '#type' => 'checkboxes',
    '#options' => $field_reps,
    '#default_value' => $field_reps,
  );
  $form['task_main']['new_assigned_uids'] = array(
    '#type' => 'hidden',
    '#title' => t('New assigned uids'),
    '#attributes' => array('id' => 'edit-new-assigned-uids'),
  );

  $form['#suffix'] = theme('cmp_gmf_step2_suffix', array(
    'field_reps' => $field_reps,
    'assign_to' => t('Task'),
  ));

  return $form;
}

function cmp_tasks_edit_task_form_assign_reps_validate($form, &$form_state) {
  //Validation of assigned reps field empty assigned users is not allowed
  $vals = $form_state['values'];
  $assigned_reps = array_merge(array_filter($vals['field_reps']), drupal_map_assoc(array_filter(explode(',', $vals['new_assigned_uids']))));
  if (empty($assigned_reps)) {
    form_set_error('field_reps', t("You should assign reps to this task!"));
  }
  else {
    $task = $form_state['storage']['#task_node'];
    if (!$task->status) {
      $old_reps = array();
      foreach ($task->field_assigned_to[LANGUAGE_NONE] as $value) {
        $old_reps[] = $value['target_id'];
      }
      sort($old_reps);
      sort($assigned_reps);
      if (array_diff($old_reps, $assigned_reps)) {
        form_set_error('field_reps', t("You cannot change reps assigned for archived task"));
      }
    }
  }
}

function cmp_tasks_edit_task_form_assign_reps_next($form, &$form_state) {
  $task = cmp_tasks_edit_task_form_assign_reps_save_task($form_state['storage']['#task_node'], $form_state['values']);
  $form_state['storage']['#task_node'] = $task;
  $form_state['redirect'] = 'tasks/' . $task->nid . '/edit/attachments';
}

function cmp_tasks_edit_task_form_assign_reps_save($form, &$form_state) {
  $task = cmp_tasks_edit_task_form_assign_reps_save_task($form_state['storage']['#task_node'], $form_state['values']);
  $form_state['storage']['#task_node'] = $task;
  $form_state['redirect'] = 'tasks/' . $task->nid;
  if ($task->sticky) {
    drupal_set_message(t('Changes have been saved!'), 'task-was-saved');
  }
}

function cmp_tasks_edit_task_form_assign_reps_save_task($task, $vals) {
  $task->title = $vals['title'];
  if (cmp_tasks_task_can_be_deleted($task->nid)) {
    $assigned_reps = array_merge(array_filter($vals['field_reps']), drupal_map_assoc(array_filter(explode(',', $vals['new_assigned_uids']))));
    foreach ($assigned_reps as $key => $assigne_this_one) {
      $assigned_reps_update[] = array('target_id' => $assigne_this_one);
    }
    $task->field_assigned_to[LANGUAGE_NONE] = $assigned_reps_update;
    $task->update_reports = TRUE;
    node_save($task);
  }
  return $task;
}
