<?php

function cmp_tasks_edit_task_form_attachments($form, &$form_state, $task = NULL) {
  $form = array();
  $form_state['storage']['#task_node'] = $task;

  cmp_tasks_edit_add_task_header($form, $task);

  if (!$task->sticky) {
    $form['task_header']['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Publish Task >'),
      '#weight' => -1000,
      '#submit' => array('cmp_tasks_edit_task_form_attachments_next'),
      '#validate' => array('cmp_tasks_edit_task_form_attachments_validate'),
      '#attributes' => array('class' => array('button', 'submit-button')),
    );
  }
  else {
    $form['task_header']['actions']['save_and_close'] = array(
      '#type' => 'submit',
      '#value' => t('Save Changes'),
      '#weight' => -500,
      '#submit' => array('cmp_tasks_edit_task_form_attachments_next'),
      '#validate' => array('cmp_tasks_edit_task_form_attachments_validate'),
      '#attributes' => array('class' => array('button', 'save-button')),
    );
  }

  cmp_tasks_edit_add_task_trail($form, $task, 'attachments');

  $form['task_main'] = array(
    '#type' => 'item',
    '#title' => t('The View'),
    '#title_display' => 'invisible',
    '#required' => TRUE,
    '#prefix' => '<div class="row">',
    '#suffix' => '</div><!-- .row main-->',
  );
  $form['task_main']['left'] = array(
    '#type' => 'item',
    '#title' => t('Add Attachments'),
    '#prefix' => '<div class="column large-6 small-4">',
    '#suffix' => '</div><!-- .column.large-6.small-4-->',
  );
  $form['task_main']['right'] = array(
    '#type' => 'item',
    '#title' => t('Right column'),
    '#title_display' => 'invisible',
    '#prefix' => '<div class="column large-6 small-8">',
    '#suffix' => '</div><!-- .column.large-6.small-8-->',
  );

  // Attachments.
  field_attach_form('node', $task, $form['task_main']['left'], $form_state, LANGUAGE_NONE, array('field_name' => 'field_attachements'));
  unset($form['task_main']['left']['field_attachements'][LANGUAGE_NONE]['title']);

  // Move attachments preview container from widget to separate row.
  $form['#after_build'][] = 'cmp_tasks_move_attachment_previews';

  // Links.
  $form['task_main']['right']['links'] = array(
    '#title' => t('Add Links'),
    '#type' => 'item',
    '#tree' => TRUE,
    '#prefix' => '<div id="links-wrapper">',
    '#suffix' => '</div>',
    '#id' => 'edit-links',
  );

  $links_values = !empty($task->field_links[LANGUAGE_NONE]) ? $task->field_links[LANGUAGE_NONE] : array();

  if (empty($form_state['links_count'])) {
    $form_state['links_count'] = count($links_values) > 3 ? count($links_values) : 3;
  }

  for ($i = 0; $i < $form_state['links_count']; $i++) {
    $form['task_main']['right']['links'][$i]['url'] = array(
      '#title' => t('Link value !count', array('!count' => $i + 1)),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => isset($links_values[$i]) ? $links_values[$i]['url'] : '',
      '#attributes' => array(
        'placeholder' => t('Paste link here'),
        'class' => array('link-input'),
      ),
      '#prefix' => '<div class="cmp-task-link-wrapper">',
    );
    $form['task_main']['right']['links'][$i]['title'] = array(
      '#title' => t('Title'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => isset($links_values[$i]) ? $links_values[$i]['title'] : '',
      '#attributes' => array(
        'placeholder' => t('Link description'),
        'class' => array('title-input'),
      ),
      '#suffix' => '</div>',
    );
  }
  if ($form_state['links_count'] > 3) {
    $form['task_main']['right']['links']['remove'] = array(
      '#type' => 'submit',
      '#value' => t('Remove last'),
      '#submit' => array('cmp_tasks_links_remove_one'),
      '#attributes' => array(
        'class' => array(
          'button',
          'small',
          'remove-item',
        ),
      ),
      '#ajax' => array(
        'callback' => 'cmp_tasks_links_add_more_callback',
        'wrapper' => 'links-wrapper',
      ),
      '#limit_validation_errors' => array(),
    );
  }
  $form['task_main']['right']['links']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add more'),
    '#attributes' => array('class' => array('button', 'small', 'add-item')),
    '#submit' => array('cmp_tasks_links_add_one'),
    '#ajax' => array(
      'callback' => 'cmp_tasks_links_add_more_callback',
      'wrapper' => 'links-wrapper',
    ),
    '#limit_validation_errors' => array(),
  );

  return $form;
}

/**
 * After build callback for attachments element in task form.
 *
 * This callback moves file previews to separate form container.
 *
 * @see cmp_tasks_edit_task_form_attachments()
 */
function cmp_tasks_move_attachment_previews($element, $form_state) {
  if (!empty($element['task_main']['left']['field_attachements'][LANGUAGE_NONE]['preview'])) {
    $element['attachments_preview'] = $element['task_main']['left']['field_attachements'][LANGUAGE_NONE]['preview'];
    $element['attachments_preview']['#prefix'] = '<div class="row">';
    $element['attachments_preview']['#suffix'] = '</div><!-- .row main-->';

    unset($element['task_main']['left']['field_attachements'][LANGUAGE_NONE]['preview']);
  }

  return $element;
}

function cmp_tasks_edit_task_form_attachments_validate($form, &$form_state) {
  $values = $form_state['values'];

  if (in_array($form_state['clicked_button']['#value'], array(
    t('Add more'),
    t('Remove last'),
  ))) {
    $errors = form_get_errors();
    if ($errors) {
      // Clear errors.
      form_clear_error();
      // Clear error messages.
      drupal_get_messages('error');
    }
  }

  // Because node forms for wimps, we should use custom "crutch" validation.
  $links = $values['links'];
  unset($links['add']);
  unset($links['remove']);
  foreach ($links as $id => $link) {
    $validate = link_validate_url($link['url']);
    if (!empty($link['url']) && !$validate || ($validate == LINK_INTERNAL && !drupal_valid_path($link['url']))) {
      form_error($form['task_main']['right']['links'][$id]['url'], t('Link URL is not valid'));
      ife_errors('set', $form['task_main']['right']['links'][$id]['url']['#id'], t('Link URL is not valid'));
    }
  }

  drupal_get_messages();
}

function cmp_tasks_edit_task_form_attachments_next($form, &$form_state) {
  $is_new = !$form_state['storage']['#task_node']->sticky;
  $task = cmp_tasks_edit_task_form_attachments_save_task($form_state['storage']['#task_node'], $form_state['values']);
  if ($is_new) {
    $message = t('<span class="high-five">!high_five</span> <span class="task-title">%task_name</span> has been published!', array(
      '!high_five' => t('High five!'),
      '%task_name' => $task->title,
    ));
  }
  else {
    $message = t('Changes have been saved!');
  }
  drupal_set_message($message, 'task-was-saved');
  $form_state['redirect'] = 'tasks/' . $task->nid;
}

function cmp_tasks_edit_task_form_attachments_save_task($task, $vals) {
  if (!empty($vals['field_attachements'][LANGUAGE_NONE])) {
    $task->field_attachements[LANGUAGE_NONE] = array();

    foreach ($vals['field_attachements'][LANGUAGE_NONE] as $value) {
      if (!empty($value['fid'])) {
        $task->field_attachements[LANGUAGE_NONE][] = $value;
      }
    }
  }

  if (!empty($vals['links'])) {
    $task->field_links[LANGUAGE_NONE] = array();
    foreach ($vals['links'] as $key => $value) {
      if (is_array($value) && $value['url']) {
        $task->field_links[LANGUAGE_NONE][$key]['url'] = $value['url'];
        $task->field_links[LANGUAGE_NONE][$key]['title'] = $value['title'];
      }
    }
  }

  $task->title = $vals['title'];
  if (!$task->sticky) {
    $task->sticky = 1;
  }
  node_save($task);
  return $task;
}
