<?php

function cmp_tasks_edit_task_form_details($form, &$form_state, $task = NULL) {
  global $user;
  $form = array();
  $form_state['storage']['#task_node'] = $task;
  $can_change = cmp_tasks_task_can_be_changed($task->nid);
  cmp_tasks_edit_add_task_header($form, $task);


  $form['task_header']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => $form_state['storage']['#task_node']->sticky ? t('Edit Reps >') : t('Assign Reps >'),
    '#weight' => -1000,
    '#submit' => array('cmp_tasks_edit_task_form_details_next'),
    '#attributes' => array('class' => array('button', 'submit-button')),
  );


  if ($form_state['storage']['#task_node']->sticky) {
    $form['task_header']['actions']['save_and_close'] = array(
      '#type' => 'submit',
      '#value' => t('Save and Close'),
      '#weight' => -500,
      '#submit' => array('cmp_tasks_edit_task_form_details_save'),
      '#attributes' => array('class' => array('button', 'save-button')),
    );
  }

  cmp_tasks_edit_add_task_trail($form, $task, 'details');

  $form['task_main'] = array(
    '#type' => 'item',
    '#title' => t('The View'),
    '#title_display' => 'invisible',
    '#required' => TRUE,
    '#prefix' => '<div class="row">',
    '#suffix' => '</div><!-- .row main-->',
  );
  $form['task_main']['left'] = array(
    '#type' => 'item',
    '#title' => t('Left column'),
    '#title_display' => 'invisible',
    '#prefix' => '<div class="column large-6">',
    '#suffix' => '</div><!-- .column.large-6-->',
  );
  $form['task_main']['right'] = array(
    '#type' => 'item',
    '#title' => t('Right column'),
    '#title_display' => 'invisible',
    '#prefix' => '<div class="column large-6">',
    '#suffix' => '</div><!-- .column.large-6-->',
  );

  $description_value = isset($task->field_description[LANGUAGE_NONE][0]['value']) ? $task->field_description[LANGUAGE_NONE][0]['value'] : '';
  $description_attributes = array(
    'class' => array(
      'text-full',
      'textarea-maxlength-js',
    ),
  );
  $description_instance = field_info_instance('node', 'field_description', $task->type);
  $description_field = field_info_field('field_description');
  if (!empty($description_instance['widget']['settings']['enable_recommended_maxlength'])) {
    $description_attributes['maxlength_js'] = $description_instance['widget']['settings']['recommended_maxlength'];
  }
  else {
    $description_attributes['maxlength_js'] = $description_field['settings']['maxlength'];
  }
  if (!empty($description_field['settings']['max_length'])) {
    $description_attributes['maxlength'] = $description_field['settings']['max_length'];
  }
  $description_attributes['placeholder'] = t('Describe what the task is about here.');

  $form['task_main']['left']['description'] = array(
    '#title' => t('What is this Task for?'),
    '#type' => 'text_format',
    '#format' => 'filtered_html',
    '#default_value' => $description_value,
    '#rows' => $description_instance['widget']['settings']['rows'],
    '#attributes' => $description_attributes,
    '#required' => TRUE,
  );
//  $form['description']['#attached']['js'][] = drupal_get_path('module', 'textarea_maxlength_js_widget') . '/js/textarea_maxlength_js_widget.js';
//  $form['description']['#attached']['js'][] = array(
//    'data' => array(
//      'textarea_maxlength_js_options' => array(
//        'counterText' => '',
//        'counterDivider' => '/',
//        'enforce' => TRUE,
//        'warning' => $form['task_main']['left']['description']['#attributes']['maxlength_js'] - 10,
//      ),
//    ),
//    'type' => 'setting',
//  );

  $associated_manager = isset($task->field_associated_manager[LANGUAGE_NONE][0]) ? $task->field_associated_manager[LANGUAGE_NONE][0]['target_id'] : 0;

  $form['task_main']['left']['associated_manager'] = array(
    '#title' => t('Associated manager'),
    '#type' => 'select',
    '#default_value' => $associated_manager,
    '#options' => sonycmp_tasks_get_allowed_associated_managers(FALSE),
    '#required' => TRUE,
  );

  if (!empty($form_state['input']['associated_manager']) && $form_state['input']['associated_manager'] == 'All') {
    $form_state['input']['amanager'] = '';
  }
// Disabled by CMP-1182
//  $form['task_main']['left']['checklist'] = array(
//    '#title' => t('Need to add more detail? <small>Create a Task Checklist below:</small>'),
//    '#type' => 'item',
//    '#tree' => TRUE,
//    '#prefix' => '<div id="checklist-wrapper">',
//    '#suffix' => '</div>',
//    '#id' => 'edit-checklist',
//  );
//
//  $checklist_values = !empty($task->field_call_to_action[LANGUAGE_NONE]) ? $task->field_call_to_action[LANGUAGE_NONE] : array();
//  $checklist_attributes = array(
//    'class' => array(
//      'text-full',
//      'textarea-maxlength-js',
//    ),
//  );
//  $checklist_instance = field_info_instance('node', 'field_call_to_action', $task->type);
//  $checklist_field = field_info_field('field_call_to_action');
//  if (!empty($checklist_instance['widget']['settings']['enable_recommended_maxlength'])) {
//    $checklist_attributes['maxlength_js'] = $checklist_instance['widget']['settings']['recommended_maxlength'];
//  }
//  else {
//    $checklist_attributes['maxlength_js'] = $checklist_field['settings']['maxlength'];
//  }
//  if (!empty($field['settings']['max_length'])) {
//    $checklist_attributes['maxlength'] = $checklist_field['settings']['max_length'];
//  }
//  $checklist_attributes['placeholder'] = t('Add an individual task here.');
//
//  if (empty($form_state['checklist_count'])) {
//    $form_state['checklist_count'] = count($checklist_values) > 3 ? count($checklist_values) : 3;
//  }
//
//  for ($i = 0; $i < $form_state['checklist_count']; $i++) {
//    $form['task_main']['left']['checklist'][$i] = array(
//      '#title' => t('Cheklist value !count', array('!count' => $i + 1)),
//      '#title_display' => 'invisible',
//      '#type' => 'textarea',
//      '#default_value' => isset($checklist_values[$i]) ? $checklist_values[$i]['value'] : '',
//      '#rows' => $checklist_instance['widget']['settings']['rows'],
//      '#attributes' => $checklist_attributes,
//    );
//    $form['task_main']['left']['checklist'][$i]['#attached']['js'][] = drupal_get_path('module', 'textarea_maxlength_js_widget') . '/js/textarea_maxlength_js_widget.js';
//    $form['task_main']['left']['checklist'][$i]['#attached']['js'][] = array(
//      'data' => array(
//        'textarea_maxlength_js_options' => array(
//          'counterText' => '',
//          'counterDivider' => '/',
//          'enforce' => TRUE,
//          'warning' => $checklist_attributes['maxlength_js'] - 10,
//        ),
//      ),
//      'type' => 'setting',
//    );
//  }
//  if ($form_state['checklist_count'] > 3) {
//    $form['task_main']['left']['checklist']['remove'] = array(
//      '#type' => 'submit',
//      '#value' => t('Remove last'),
//      '#submit' => array('cmp_tasks_checklist_remove_one'),
//      '#attributes' => array(
//        'class' => array(
//          'button',
//          'small',
//          'remove-item',
//        ),
//      ),
//      '#ajax' => array(
//        'callback' => 'cmp_tasks_checklist_add_more_callback',
//        'wrapper' => 'checklist-wrapper',
//      ),
//      '#limit_validation_errors' => array(),
//    );
//  }
//  $form['task_main']['left']['checklist']['add'] = array(
//    '#type' => 'submit',
//    '#value' => t('Add more'),
//    '#attributes' => array('class' => array('button', 'small', 'add-item')),
//    '#submit' => array('cmp_tasks_checklist_add_one'),
//    '#ajax' => array(
//      'callback' => 'cmp_tasks_checklist_add_more_callback',
//      'wrapper' => 'checklist-wrapper',
//    ),
//    '#limit_validation_errors' => array(),
//  );


  $section_field_name = 'field_' . _sonycmp_tasks_get_section_fields_prefix($task->type) . '_allowed_content';
  $section_instance = field_info_instance('node', $section_field_name, $task->type);
  $section_field = field_info_field($section_field_name);
  $allowed_values = list_allowed_values($section_field, $section_instance);
  $section_values = array();
  if (!empty($task->{$section_field_name}[LANGUAGE_NONE])) {
    foreach ($task->{$section_field_name}[LANGUAGE_NONE] as $key => $value) {
      $section_values[$key] = $value['value'];
    }
  }
  else {
    foreach ($section_instance['default_value'] as $key => $value) {
      $section_values[$key] = $value['value'];
    }
  }

  $required_section_field_name = 'field_' . _sonycmp_tasks_get_section_fields_prefix($task->type) . '_required_content';
  $required_section_instance = field_info_instance('node', $required_section_field_name, $task->type);
  $required_section_field = field_info_field($required_section_field_name);
  $required_allowed_values = list_allowed_values($required_section_field, $required_section_instance);
  $required_section_values = array();
  if (!empty($task->{$required_section_field_name}[LANGUAGE_NONE])) {
    foreach ($task->{$required_section_field_name}[LANGUAGE_NONE] as $key => $value) {
      $required_section_values[$key] = $value['value'];
    }
  }
  else {
    foreach ($required_section_instance['default_value'] as $key => $value) {
      $required_section_values[$key] = $value['value'];
    }
  }


  $form['task_main']['right']['sections'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select Task Sections:'),
    '#options' => $allowed_values,
    '#default_value' => $section_values,
    '#required' => TRUE,
    '#multiple' => TRUE,
    '#disabled' => !$can_change,
    '#process' => array(
      'form_process_checkboxes',
      'cmp_tasks_process_checkboxes',
    ),
    '#id' => 'edit-sections',
  );
  $form['task_main']['right']['required_sections'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select Required Sections:'),
    '#options' => $required_allowed_values,
    '#default_value' => $required_section_values,
    '#required' => FALSE,
    '#multiple' => TRUE,
    '#disabled' => !$can_change,
    '#process' => array(
      'form_process_checkboxes',
      'cmp_tasks_process_checkboxes',
    ),
    '#id' => 'edit-required-sections',
  );

  $old_due_date = isset($task->field_due_date[LANGUAGE_NONE][0]['value']) ?  $task->field_due_date[LANGUAGE_NONE][0]['value'] : NULL;
  $due_date =  $old_due_date ? format_date($old_due_date, 'custom', 'Y-m-d H:i:s') : format_date(strtotime('tomorrow'), 'custom', 'Y-m-d H:i:s');
  //for misc task due_date is optional
  if ($task->type == SONY_CMP_CT_MISC_TASK) {
    $form['task_main']['right']['empty_due_date'] = array(
      '#type' => 'checkbox',
      '#title' => 'Due date is Empty',
      '#default_value' => $old_due_date || !$task->sticky ? FALSE : TRUE
    );
  }

  $form['task_main']['right']['due_date'] = array(
    '#title' => t('When is it due?'),
    '#attributes' => array('placeholder' => t('Due Date')),
    '#type' => 'datepicker',
    '#timepicker' => 'default',
    '#date_type' => DATE_UNIX,
    '#date_format' => 'm/d/Y',
    '#date_text_parts' => array(),
    '#date_year_range' => '-0:+1',
    '#date_label_position' => 'none',
    '#default_value' => $due_date,
    '#required' => $task->type != SONY_CMP_CT_MISC_TASK,
    '#id' => 'edit-due-date',
    //only future for new dates
    '#datepicker_options' => $old_due_date ? array() : array('minDate' => "+0")
  );

  $form['#attached']['js'][] = drupal_get_path('module', 'cmp_tasks') . '/cmp_tasks.js';

  return $form;
}

function cmp_tasks_checklist_add_one($form, &$form_state) {
  $form_state['checklist_count']++;
  $form_state['rebuild'] = TRUE;
}

function cmp_tasks_checklist_remove_one($form, &$form_state) {
  if ($form_state['checklist_count'] > 3) {
    $form_state['checklist_count']--;
  }
  $form_state['rebuild'] = TRUE;
}

function cmp_tasks_checklist_add_more_callback($form, $form_state) {
  $js = drupal_add_js();
  $found = FALSE;
  foreach ($js['settings']['data'] as $i => $data) {
    if (isset($data['datePopup'])) {
      if (!$found) {
        $found = TRUE;
      }
      else {
        unset($js['settings']['data'][$i]);
      }
    }
  }
  return $form['task_main']['left']['checklist'];
}

function cmp_tasks_edit_task_form_details_validate($form, &$form_state) {
  if (in_array($form_state['clicked_button']['#value'], array(
    t('Add more'),
    t('Remove last'),
  ))) {
    $errors = form_get_errors();
    if ($errors) {
      // Clear errors.
      form_clear_error();
      // Clear error messages.
      drupal_get_messages('error');
    }
    return;
  }

  $task = $form_state['storage']['#task_node'];
//  //validate description field
//  $description_instance = field_info_instance('node', 'field_description', $task->type);
//  $description_maxlength = $description_instance['widget']['settings']['recommended_maxlength'];
//  $length = drupal_strlen($form_state['values']['description']);
//  if ($length > $description_maxlength) {
//    form_set_error('description', t("You've exceeded the maximum number of characters (!count)", array('!count' => $description_maxlength)));
//  }

  //validate checklist field
  $checklist_instance = field_info_instance('node', 'field_call_to_action', $task->type);
  $checklist_maxlength = $checklist_instance['widget']['settings']['recommended_maxlength'];
  $form_state['values']['checklist'] = array_filter($form_state['values']['checklist']);
  foreach ($form_state['values']['checklist'] as $field => $value) {
    if (is_numeric($field)) {
      $length = drupal_strlen($form_state['values']['checklist'][$field]);
      if ($length > $checklist_maxlength) {
        form_set_error('checklist[' . $field, t("You've exceeded the maximum number of characters (!count)", array('!count' => $checklist_maxlength)));
      }
    }
  }

  if (!empty($form_state['values']['empty_due_date'])) {
    $form_state['values']['due_date'] = NULL;
  }
  else {//Convert date field
    $due_date = strtotime($form_state['values']['due_date']);
    if ($due_date) {
      form_set_value($form['task_main']['right']['due_date'], $due_date, $form_state);
      if ($due_date < strtotime('today')) {
        form_set_error('due_date', t("Due date should be in future"));
      }
    }
  }
}

function cmp_tasks_edit_task_form_details_next($form, &$form_state) {
  $task = cmp_tasks_edit_task_form_details_save_task($form_state['storage']['#task_node'], $form_state['values']);
  $form_state['storage']['#task_node'] = $task;
  $form_state['redirect'] = 'tasks/' . $task->nid . '/edit/assign_reps';
}


function cmp_tasks_edit_task_form_details_save($form, &$form_state) {
  $task = cmp_tasks_edit_task_form_details_save_task($form_state['storage']['#task_node'], $form_state['values']);
  $form_state['storage']['#task_node'] = $task;
  $form_state['redirect'] = 'tasks/' . $task->nid;
  if ($task->sticky) {
    drupal_set_message(t('Changes have been saved!'), 'task-was-saved');
  }
}

function cmp_tasks_edit_task_form_details_save_task($task, $vals) {
  $types = array(
    'bi_weekly_task' => 'bw',
    'tour_task' => 'tour',
    'misc_task' => 'misc',
  );
  $task->title = $vals['title'];
  $task->field_description[LANGUAGE_NONE][0] = $vals['description'];
  if (!empty($vals['checklist'])) {
    $task->field_call_to_action[LANGUAGE_NONE] = array();
    foreach ($vals['checklist'] as $key => $value) {
      if (is_numeric($key)) {
        $task->field_call_to_action[LANGUAGE_NONE][$key]['value'] = $value;
      }
    }
  }
  $task->field_associated_manager[LANGUAGE_NONE][0]['target_id'] = $vals['associated_manager'];
  if (cmp_tasks_task_can_be_changed($task->nid)) {
    if (!empty($vals['sections'])) {
      $field_name = 'field_' . $types[$task->type] . '_allowed_content';
      $task->{$field_name}[LANGUAGE_NONE] = array();
      $vals['sections'] = array_filter($vals['sections']);
      foreach ($vals['sections'] as $key => $value) {
        $task->{$field_name}[LANGUAGE_NONE][]['value'] = $value;
      }
    }
    if (!empty($vals['required_sections'])) {
      $field_name = 'field_' . $types[$task->type] . '_required_content';
      $task->{$field_name}[LANGUAGE_NONE] = array();
      $vals['required_sections'] = array_filter($vals['required_sections']);
      foreach ($vals['required_sections'] as $key => $value) {
        $task->{$field_name}[LANGUAGE_NONE][]['value'] = $value;
      }
    }
  }
  $task->field_due_date[LANGUAGE_NONE][0]['value'] = $vals['due_date'];
  node_save($task);
  return $task;
}

