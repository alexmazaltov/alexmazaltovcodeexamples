<?php
/**
 * @file
 * cmp_tasks.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cmp_tasks_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'archive task'.
  $permissions['archive task'] = array(
    'name' => 'archive task',
    'roles' => array(
      'administrator' => 'administrator',
      'task manager' => 'task manager',
    ),
    'module' => 'sonycmp_tasks',
  );

  // Exported permission: 'create bi_weekly_task content'.
  $permissions['create bi_weekly_task content'] = array(
    'name' => 'create bi_weekly_task content',
    'roles' => array(
      'administrator' => 'administrator',
      'task manager' => 'task manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create misc_task content'.
  $permissions['create misc_task content'] = array(
    'name' => 'create misc_task content',
    'roles' => array(
      'administrator' => 'administrator',
      'task manager' => 'task manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create self misc task'.
  $permissions['create self misc task'] = array(
    'name' => 'create self misc task',
    'roles' => array(
      'representative' => 'representative',
    ),
    'module' => 'sonycmp_tasks',
  );

  // Exported permission: 'create tour_task content'.
  $permissions['create tour_task content'] = array(
    'name' => 'create tour_task content',
    'roles' => array(
      'administrator' => 'administrator',
      'task manager' => 'task manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bi_weekly_task content'.
  $permissions['delete any bi_weekly_task content'] = array(
    'name' => 'delete any bi_weekly_task content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any misc_task content'.
  $permissions['delete any misc_task content'] = array(
    'name' => 'delete any misc_task content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any tour_task content'.
  $permissions['delete any tour_task content'] = array(
    'name' => 'delete any tour_task content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bi_weekly_task content'.
  $permissions['delete own bi_weekly_task content'] = array(
    'name' => 'delete own bi_weekly_task content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own misc_task content'.
  $permissions['delete own misc_task content'] = array(
    'name' => 'delete own misc_task content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own tour_task content'.
  $permissions['delete own tour_task content'] = array(
    'name' => 'delete own tour_task content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bi_weekly_task content'.
  $permissions['edit any bi_weekly_task content'] = array(
    'name' => 'edit any bi_weekly_task content',
    'roles' => array(
      'administrator' => 'administrator',
      'task manager' => 'task manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any misc_task content'.
  $permissions['edit any misc_task content'] = array(
    'name' => 'edit any misc_task content',
    'roles' => array(
      'administrator' => 'administrator',
      'task manager' => 'task manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any tour_task content'.
  $permissions['edit any tour_task content'] = array(
    'name' => 'edit any tour_task content',
    'roles' => array(
      'administrator' => 'administrator',
      'task manager' => 'task manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bi_weekly_task content'.
  $permissions['edit own bi_weekly_task content'] = array(
    'name' => 'edit own bi_weekly_task content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own misc_task content'.
  $permissions['edit own misc_task content'] = array(
    'name' => 'edit own misc_task content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own tour_task content'.
  $permissions['edit own tour_task content'] = array(
    'name' => 'edit own tour_task content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'rebuild tasks and reports statuses'.
  $permissions['rebuild tasks and reports statuses'] = array(
    'name' => 'rebuild tasks and reports statuses',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'sonycmp_common',
  );

  return $permissions;
}
