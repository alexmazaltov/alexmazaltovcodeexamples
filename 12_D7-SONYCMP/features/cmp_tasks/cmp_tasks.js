(function($){
    Drupal.behaviors.cmp_tasks = {
        attach: function (context, settings) {
            //enable/disable required sections elements when section selected/deselected
            $('#edit-sections  input.form-checkbox', context).on('change', function() {
                var $this = $(this);
                var $requiredControl = $('#edit-required-sections input[name="' + 'required_' + $this.attr('name') + '"]', context);
                $requiredControl.attr('disabled', !$this.is(':checked'));
            });
        }
    }
})(jQuery);