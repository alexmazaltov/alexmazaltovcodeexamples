(
    function($) {
        Drupal.behaviors.sonycmp_theme_migrations = {
            attach: function (context, settings) {
                if (context == document && settings.ajaxPageState.theme == 'sonycmp_theme') {
                    var originalBeforeSerialize = Drupal.ajax.prototype.beforeSerialize;
                    Drupal.ajax.prototype.beforeSerialize = function(element_settings, options) {
                        originalBeforeSerialize.call(this, element_settings, options);
                        options.data['sct'] = 1;

                    }
                }
            }
        }
    }
)(jQuery);