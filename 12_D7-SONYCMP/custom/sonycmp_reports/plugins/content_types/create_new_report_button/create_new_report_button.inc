<?php

/**
 * @file
 * Button for creating a new report.
 */

$plugin = array(
  'title' => t('Sonycmp Create new report Button'),
  'description' => t('Adds a pane with a button.'),
  'content_types' => 'sonycmp_common_create_new_report_button',
  'single' => TRUE,
  'render callback' => 'sonycmp_common_create_new_report_button_render',
  'edit form' => 'sonycmp_common_create_new_report_button_edit_form',
  'category' => t('SonyCMP'),
);

/**
 * Implements render callback for this content type.
 */
function sonycmp_common_create_new_report_button_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->content = l(t('Create New Report'), 'reporting/new/add', array(
    'attributes' => array(
      'class' => array('button'),
    ),
  ));

  return $block;
}

/**
 * Implements the edit settings form for this content type.
 */
function sonycmp_common_create_new_report_button_edit_form($form, &$form_state) {
  return $form;
}
