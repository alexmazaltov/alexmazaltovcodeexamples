<?php

/**
 * @file Step 1 of creating a new report.
 */

/**
 * Step Content
 *
 * @param $node
 *   Report node,
 * @return array|mixed
 */
function sonycmp_reports_report_get_content_step_task($node) {
  $form = drupal_get_form('sonycmp_reports_report_form_task', $node);
  $form['#theme'] = array('sonycmp_reports_report_form');
  return $form;
}

/**
 * Build a form for step 1 of creating a new report.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 * @param \stdClass|null $report
 *   Node object.
 *
 * @return array
 *   Form array.
 */
function sonycmp_reports_report_form_task($form, &$form_state, $report = NULL) {
  $form = array();
  $form_state['storage']['#report'] = $report;
  $form['report_header']['details'] = array(
    '#type' => 'item',
    '#title' => t('Report details'),
    '#title_display' => 'invisible',
    '#prefix' => '<div class="column large-6 medium-6 small-5">',
    '#suffix' => '</div>',
  );
  $field = field_info_field('field_report_type');
  $types = list_allowed_values($field);
  $report_type = key($types) . '_task';

  $default_type = key($types);
  $edit_mode = FALSE;
  if (!empty($report->field_report_type[LANGUAGE_NONE][0]['value'])) {
    $report_type = $report->field_report_type[LANGUAGE_NONE][0]['value'] . '_task';
    $default_type = $report->field_report_type[LANGUAGE_NONE][0]['value'];
  }
  if (!empty($report->field_content)) {
    $edit_mode = TRUE;
  }

  $tabs_html = '';
  foreach ($types as $key => $type) {
    $ct_type = $key . '_task';
    if ($edit_mode && $ct_type != $report_type) {
      continue;
    }
    $tasks_count = cmp_tasks_get_tasks_count(NULL, FALSE, $ct_type);
    $tabs_html .= '<li' . ($ct_type === $report_type ? ' class="active"' : '') . '>';
    $tabs_html .= '<a href="javascript:void(0);" data-filter="' . $ct_type . '" data-type="' . $key . '" ';
    $tabs_html .= 'class="small button secondary' . ($ct_type === $report_type ? ' active' : '') . '">';
    $tabs_html .= $types[$key] . ' (' . $tasks_count . ')';
    $tabs_html .= '</a></li>';
  }
  $form['report_header'] = array(
    '#type' => 'item',
    '#title' => '',
    '#prefix' => '<div class="row">',
    '#suffix' => '</div>',
  );
  $form['report_header']['details'] = array(
    '#type' => 'item',
    '#title' => t('Report details'),
    '#title_display' => 'invisible',
    '#prefix' => '<div class="column large-6 medium-6 small-5">',
    '#suffix' => '</div>',
  );
  $form['report_header']['details']['tab'] = array(
    '#type' => 'markup',
    '#markup' => '<ul class="button-group">' . $tabs_html . '</ul>',
  );
  $form['report_header']['actions'] = array(
    '#type' => 'actions',
    '#prefix' => '<div class="column large-5 medium-6 small-7 buttons-wrapper">',
    '#suffix' => '</div>',
  );
  $form['report_header']['actions']['cancel'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Cancel'), 'reporting/' . $report->nid . '/cancel', array(
      'attributes' => array(
        'class' => array(
          'button',
          'cancel-button',
          'ctools-modal-create-report-modal-dialog-style',
          'ctools-modal-confirmation-modal-dialog-style-' . sonycmp_common_get_length_of_one_line_for_cp($report->title),
        ),
      ),
    )),
    '#weight' => -2000,
  );
  $form['report_header']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Next, Add Content >'),
    '#weight' => -1000,
    '#attributes' => array(
      'class' => array(
        'button',
        'submit-button',
      ),
    ),
    '#submit' => array('sonycmp_reports_report_form_task_submit'),
    '#validate' => array('sonycmp_reports_report_form_task_validate'),
  );

  $task_nid = 'all';
  if (isset($report->field_task[LANGUAGE_NONE][0]['target_id'])) {
    $task_nid = $report->field_task[LANGUAGE_NONE][0]['target_id'];
  }
  $form['task'] = array(
    '#type' => 'hidden',
    '#default_value' => $task_nid,
  );
  $form['type'] = array(
    '#type' => 'hidden',
    '#default_value' => $default_type,
  );
  $form['#attributes']['class'][] = 'create-final-report-task';
  $form['#attached']['js'][] = drupal_get_path('module', 'sonycmp_reports') . '/js/reporting_select_task.js';
  $form['#attached']['js'][] = array(
    'data' => array(
      'selectTaskJson' => array(
        'taskNid' => $task_nid,
      ),
    ),
    'type' => 'setting',
  );
  $task_nid = $edit_mode ? $task_nid : 'all';
  $form['#suffix'] = sonycmp_reports_get_select_task_view($report_type, $task_nid);

  return $form;
}

/**
 * Validate form.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 */
function sonycmp_reports_report_form_task_validate(&$form, &$form_state) {
  $v = $form_state['values'];
  $task_nid = $v['task'];
  if (empty($task_nid)) {
    form_set_error('task', t('Please select the task'));
  }
  else {
    $task_node = node_load($task_nid);
    if (!sonycmp_tasks_node_is_task($task_node)) {
      form_set_error('task', t('Wrong task selected'));
    }
  }
}

/**
 * Save data to node.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 */
function sonycmp_reports_report_form_task_submit(&$form, &$form_state) {
  $values = $form_state['values'];
  $report = $form_state['storage']['#report'];
  $report->field_task[LANGUAGE_NONE][0]['target_id'] = $values['task'];
  $report->field_report_type[LANGUAGE_NONE][0]['value'] = $values['type'];
  node_save($report);
  $form_state['redirect'] = 'reporting/' . $report->nid . '/edit/content';
}

/**
 * Get result of execution views for selecting list of tasks.
 *
 * @param string $task_type
 *   Content type machine name.
 * @param string $task_nid
 *   Node id or "all".
 *
 * @return string
 *   HTML output.
 */
function sonycmp_reports_get_select_task_view($task_type, $task_nid) {
  $view = views_get_view('tasks');
  $view->set_display('panel_tasks_for_report');
  $view->set_arguments(array($task_nid));
  $view->exposed_input = array('type' => $task_type);
  $view->init_handlers();
  $view->use_ajax = TRUE;
  $output = $view->preview();
  $view->destroy();

  return $output;
}
