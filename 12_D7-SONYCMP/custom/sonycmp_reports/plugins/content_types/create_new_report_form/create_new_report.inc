<?php

/**
 * @file
 * Create a new report form plugin definition.
 */

$plugin = array(
  'title' => t('Sonycmp Create a New Report form'),
  'description' => t('Adds a pane with final reports node form.'),
  'content_types' => 'sonycmp_reports_create_new_report',
  'single' => TRUE,
  'render callback' => 'sonycmp_reports_create_new_report_render',
  'edit form' => 'sonycmp_reports_create_new_report_edit_form',
  'hook theme' => 'sonycmp_reports_create_new_report_theme',
  'category' => t('SonyCMP'),
  'required context' => array(
    0 => new ctools_context_optional(t('Node'), 'node'),
  ),
);

/**
 * Implements render callback for this content type.
 */
function sonycmp_reports_create_new_report_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->content = '';
  $node = $context[0]->data;
  
  $supported_steps = array('task', 'content', 'finalize');
  $args = arg();
  $step = !empty($args[3]) ? $args[3] : 'task';
  if ($node->type != SONY_CMP_CT_REPORT || $args[0] != 'reporting' | !in_array($step, $supported_steps)) {
    drupal_goto('reporting');
  }
  ctools_include('content_types/create_new_report_form/report_form_step1', 'sonycmp_reports', 'plugins');
  ctools_include('content_types/create_new_report_form/report_form_step2', 'sonycmp_reports', 'plugins');
  ctools_include('content_types/create_new_report_form/report_form_step3', 'sonycmp_reports', 'plugins');

  $callback = 'sonycmp_reports_report_get_content_step_' . $step;
  if (function_exists($callback)) {
    $block->content = $callback($node);
  }

  return $block;
}

/**
 * Implements the edit settings form for this content type.
 */
function sonycmp_reports_create_new_report_edit_form($form, &$form_state) {
  return $form;
}

/**
 * Delegated implementation of hook_theme().
 */
function sonycmp_reports_create_new_report_theme(&$theme, $plugin) {
  $theme['sonycmp_reports_report_form'] = array(
    'render element' => 'form',
    'template' => 'create-new-report-form',
    'path' => $plugin['path'],
  );
}
