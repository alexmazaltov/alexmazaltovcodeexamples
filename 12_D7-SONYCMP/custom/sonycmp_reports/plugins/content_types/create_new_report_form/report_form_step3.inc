<?php

/**
 * @file
 *
 * Step 3 of creating a new final report.
 */

/**
 * Helper for get content of step.
 *
 * @param $report
 *   Report node
 *
 * @return array|mixed
 * @see sonycmp_reports_form_alter().
 */
function sonycmp_reports_report_get_content_step_finalize($report) {
  module_load_include('inc', 'node', 'node.pages');

  // Save special flag in node, because form will be rebuild.
  $report->finalize_report = TRUE;
  $form = drupal_get_form(SONY_CMP_CT_REPORT . '_node_form', $report);

  if ($form['title']['#value'] == 'New final report') {
    $form['title']['#default_value'] = '';
    $form['title']['#value'] = '';
  }

  return $form;
}
