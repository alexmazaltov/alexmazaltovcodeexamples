<?php

/**
 * @file Step 2 of creating a new report.
 */

/**
 * Step content.
 *
 * @param $node
 *   Report node.
 *
 * @return array|mixed
 */
function sonycmp_reports_report_get_content_step_content($node) {
  $form = drupal_get_form('sonycmp_reports_report_form_content', $node);
  $form['#theme'] = array('sonycmp_reports_report_form');
  return $form;
}

/**
 * Build a form for step 2 of creating a new report.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 * @param \stdClass $report
 *   Node object.
 *
 * @return array
 *   Form array.
 */
function sonycmp_reports_report_form_content($form, &$form_state, $report) {
  $form = array();
  $form_state['storage']['#report'] = $report;

  $field = field_info_field('field_report_type');
  $types = list_allowed_values($field);

  $report_type = $report->field_report_type[LANGUAGE_NONE][0]['value'];
  $task_title = $types[$report_type] . ' ' . t('Report');

  $form['details'] = array(
    '#type' => 'markup',
    '#markup' => '<div class="column large-6 medium-6 small-5"><span class="task-title-suffix">' . $task_title . '</span></div>',
  );

  $form['actions_wrap']['actions'] = array(
    '#type' => 'actions',
    '#prefix' => '<div class="column large-6 medium-6 small-7 buttons-wrapper">',
    '#suffix' => '</div>',
  );
  $form['actions_wrap']['actions']['cancel'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Cancel'), 'reporting/' . $report->nid . '/cancel', array(
      'attributes' => array(
        'class' => array(
          'button',
          'cancel-button',
          'ctools-modal-create-report-modal-dialog-style',
          'ctools-modal-confirmation-modal-dialog-style-' . sonycmp_common_get_length_of_one_line_for_cp($report->title),
        ),
      ),
    )),
    '#weight' => -2,
  );
  $form['actions_wrap']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Finalize Report'),
    '#weight' => -1,
    '#attributes' => array('class' => array('button', 'submit-button')),
    '#submit' => array('sonycmp_reports_report_form_content_submit'),
    '#validate' => array('sonycmp_reports_report_form_content_validate'),
  );

  $content_json_string = '[]';
  //If json content was initialized set it as value in text area.
  if (!empty($report->field_content[LANGUAGE_NONE][0]['value'])) {
    $content_json_string = $report->field_content[LANGUAGE_NONE][0]['value'];
    if (!sonycmp_common_is_json($content_json_string)) {
      $content_json_string = '[]';
    }
  }
  $form['content'] = array(
    '#type' => 'hidden',
    '#default_value' => $content_json_string,
  );
  $form['global_select_all'] = array(
    '#type' => 'hidden',
    '#default_value' => 0,
  );

  $content = sonycmp_reports_get_report_statistics($report);

  $task = $report->field_task[LANGUAGE_NONE][0]['target_id'];
  $content .= sonycmp_reports_get_select_task_content_view($task);
  $form['#suffix'] = $content;

  $form['#attributes']['class'][] = 'create-final-report-content';
  $form['#attributes']['class'][] = 'create-final-report-content-restyle';
  $form['#attributes']['class'][] = 'clearfix';
  $form['#attached']['js'][] = drupal_get_path('module', 'sonycmp_reports') . '/js/reporting_select_media.js';
  $form['#attached']['js'][] = array(
    'data' => array(
      'selectedFieldsScope' => drupal_json_decode($content_json_string),
    ),
    'type' => 'setting',
  );
  $form['#attached']['js'][] = drupal_get_path('theme', 'sonycmp') . '/js/lib/autogrow.js';

  //add cropper
  $form['#attached']['js'][] = drupal_get_path('theme', 'sonycmp_theme') . '/js/lib/jquery.magnific-popup.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'sonycmp_reports') . '/js/sonycmp_reports_cropper.js';
  $form['#attached']['js'][] = 'sites/all/libraries/cropperjs/dist/cropper.js';
  $form['#attached']['css'][] = 'sites/all/libraries/cropperjs/dist/cropper.css';

  return $form;
}

/**
 * Validate form.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 */
function sonycmp_reports_report_form_content_validate(&$form, &$form_state) {
  $content_json_string = $form_state['values']['content'];
  if (!sonycmp_common_is_json($content_json_string)) {
    form_set_error('content', 'Wrong json data');
  }
}

/**
 * Save data to node.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 */
function sonycmp_reports_report_form_content_submit(&$form, &$form_state) {
  $report = $form_state['storage']['#report'];
  $page_content = json_decode($form_state['values']['content'], TRUE);
  if ($form_state['values']['global_select_all']) {
    $task = $form_state['storage']['#report']->field_task[LANGUAGE_NONE][0]['target_id'];
    $query = db_select('node', 'n');
    $query->addField('n', 'nid');
    $query->leftJoin('field_data_field_task', 'fdft', 'n.nid = fdft.entity_id AND (fdft.entity_type = \'node\' AND fdft.deleted = 0)');
    $query->condition('fdft.field_task_target_id', $task, '=');
    $query->condition('n.status', NODE_PUBLISHED, '=');
    $query->condition('n.sticky', NODE_STICKY, '=');
    $query->condition('n.type', array(
      SONY_CMP_CT_BI_WEEKLY_REPORT,
      SONY_CMP_CT_MISC_REPORT,
      SONY_CMP_CT_TOUR_REPORT,
    ), 'IN');
    $query->orderBy('n.created');
    $result = $query->execute()->fetchAll();

    $selected_on_page = array();
    foreach ($page_content as $item) {
      $selected_on_page[$item['nid']][] = $item;
    }

    $content = array();
    foreach ($result as $n) {
      if (array_key_exists($n->nid, $selected_on_page)) {
        foreach ($selected_on_page[$n->nid] as $on_page_item) {
          $content[] = (object) $on_page_item;
        }
        continue;
      }
      $node = node_load($n->nid);
      // Media, text and links.
      $map = sonycmp_reports_fields_to_process_map($node->type);
      foreach ($map as $map_level2) {
        foreach ($map_level2 as $key => $item) {
          $media_field = $item['media']['field'];
          $text_field = $item['text']['field'];
          if (isset($item['#links'])) {
            // Links only links, no text.
            if (!empty($node->{$media_field}[LANGUAGE_NONE])) {
              foreach ($node->{$media_field}[LANGUAGE_NONE] as $delta => $f) {
                $content[] = (object) array(
                  'nid' => (int) $node->nid,
                  'name' => $media_field,
                  'delta' => $delta,
                  'column' => 'url',
                );
              }
            }
          }
          else {
            if ($media_field && !empty($node->{$media_field}[LANGUAGE_NONE])) {
              // Media value.
              foreach ($node->{$media_field}[LANGUAGE_NONE] as $delta => $f) {
                $content[] = (object) array(
                  'nid' => (int) $node->nid,
                  'name' => $media_field,
                  'delta' => $delta,
                  'column' => 'fid',
                  'value' => $f->description
                );
              }
            }
            if ($text_field && !empty($node->{$text_field}[LANGUAGE_NONE][0])) {
              // Text value.
              $content[] = (object) array(
                'nid' => (int) $node->nid,
                'name' => $text_field,
                'delta' => 0,
                'column' => 'value',
              );
            }
          }
        }
      }

      $details_map = sonycmp_reports_details_fields_to_process_map($node->type);
      foreach ($details_map as $field) {
        $value = $node->{$field}[LANGUAGE_NONE][0]['value'];
        foreach ($page_content as $item) {
          if ($item['name'] == $field && $item['nid'] == $node->nid && $item['deilta'] == 0) {
            $value = $item['value'];
          }
        }
        switch ($field) {
          case 'field_event_name':
            if (!empty($node->field_event_type[LANGUAGE_NONE][0]['value'])) {
              $value .= ' | ' . $node->field_event_type[LANGUAGE_NONE][0]['value'];
            }
            break;

          case 'field_event_place':
            if (!empty($node->field_event_date[LANGUAGE_NONE][0]['value'])) {
              $value .= ' | ' . format_date($node->field_event_date[LANGUAGE_NONE][0]['value'], 'tiny');
            }
            break;
        }

        $content[] = (object) array(
          'nid' => (int) $node->nid,
          'name' => $field,
          'delta' => 0,
          'column' => 'value',
          'value' => $value,
        );
      }
    }
    $content_json_string = json_encode($content);
  }
  else {
    $content_json_string = $form_state['values']['content'];
  }
  $report->field_content[LANGUAGE_NONE][0]['value'] = $content_json_string;
  node_save($report);
  $form_state['redirect'] = 'reporting/' . $report->nid . '/edit/finalize';
}

/**
 * Get result of execution views for selecting list of tasks.
 *
 * @param int $task
 *   Task id.
 *
 * @return string
 *   HTML output.
 */
function sonycmp_reports_get_select_task_content_view($task) {
  $view = views_get_view('select_media_view');
  $view->set_display('block_select_media');
  $view->set_arguments(array($task));
  $view->init_handlers();
  $view->use_ajax = TRUE;
  $view_output = $view->preview();
  $view->destroy();
  $output = '<div class="edit-final-report-media-content js--edit-final-report-media-content edit-final-report-media-content--restyle">';
  $output .= sonycmp_reports_select_task_content_global_tabs();
  $output .= $view_output;
  $output .= '</div>';

  return $output;
}

/**
 * Add global tabs.
 *
 * @return string
 */
function sonycmp_reports_select_task_content_global_tabs() {
  $output = '
    <div class="global-tabs--restyle js--global-tabs">
      <ul class="button-group--restyle button-type-tab--restyle js--button-group">
        <li class="select-all--restyle">
          <a class="button--restyle js--button-select-all" href="javascript:void(0);">' . t('Select All Reps') . '</a>
        </li>
        <li class="clear-all--restyle">
          <a class="button--restyle js--button-clear-all" href="javascript:void(0);">' . t('Clear') . '</a>
        </li>
      </ul>
    </div>';
  return $output;
}
