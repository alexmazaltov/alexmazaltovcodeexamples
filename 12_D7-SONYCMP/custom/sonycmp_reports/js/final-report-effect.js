(function ($) {
    Drupal.behaviors.sonycmpFinalReportEffects = {
        selectedEffect: 0,
        attach: function (context, settings) {
            var $simplecropDataWidget = $('.simplecrop-widget-data', context);
            var $editButton = $('button[name="field_cover_image_und_0_display_reload"][value="Edit crop"]', $simplecropDataWidget);
            //do we have crop widget here and doesn have Filter button?
            if ($editButton.length && !$simplecropDataWidget.find('button.edit-effect').length) {
                //image control
                var $coverImageFormItem = $simplecropDataWidget.parents('.field-type-image.field-name-field-cover-image');
                //simplecrop preview image
                var $croppedImagePreview = $coverImageFormItem.find('.cropped-image');
                //form self
                var $form = $coverImageFormItem.parents('form');
                var $form_build_id = $form.find('input[name="form_build_id"]');
                var $fid = $form.find('input[name="field_cover_image[und][0][fid]"]');

                //our form effect field
                var $effectFormItem = $('#edit-field-cover-image-effect');
                var $effectFormSelect = $('select', $effectFormItem);

                //lets add our preview item
                $croppedImagePreview.before('<div class="preview-with-effect"><img/></div>');
                var $previewWithEffect = $coverImageFormItem.find('.preview-with-effect');
                var $previewImageWithEffect = $previewWithEffect.find('img');


                Drupal.behaviors.sonycmpFinalReportEffects.selectedEffect = $effectFormSelect.val();

                //hide default image preview
                $croppedImagePreview.hide();
                var composePreviewUrl = function() {
                  return  '/reporting/preview-header?'
                    + 'form_build_id=' + $form_build_id.val()
                    + '&fid=' + $fid.val()
                    + '&effect=' + Drupal.behaviors.sonycmpFinalReportEffects.selectedEffect
                    + '&rnd=' + Math.random()
                      ;
                };
                $previewImageWithEffect.attr('src', composePreviewUrl());
                $previewWithEffect.show();

                //lets add effects toolbox
                $simplecropDataWidget.before('<div class="effects-widget"></div>');
                var $effectsWidget = $simplecropDataWidget.prev('.effects-widget');
                $effectsWidget.hide();
                $effectsWidget.append('<button value="Cancel" name="effect-cancel" type="submit" class="form-cancel form-submit">Cancel</button>');
                for (i=0; i<5; i++) {
                    $effectsWidget.append('<span class="effect effect-' + i + '" data-effect="' + i + '"></span>');
                }
                var $effectButtons = $effectsWidget.find('.effect');
                $effectButtons.on('change', function() {
                    $effectButtons.removeClass('active');
                    $effectButtons.filter('*[data-effect="' + Drupal.behaviors.sonycmpFinalReportEffects.selectedEffect +'"]').addClass('active');
                    $previewImageWithEffect.attr('src', composePreviewUrl());
                });
                $effectButtons.on('click', function() {
                    var $this = $(this);
                    Drupal.behaviors.sonycmpFinalReportEffects.selectedEffect = $this.data('effect');
                    $this.trigger('change');
                });
                $effectsWidget.append('<button value="Apply" name="effect-apply" type="submit" class="form-apply form-submit">Apply Filter</button>');
                var $effectsButtonCancel = $effectsWidget.find('button[name="effect-cancel"]');
                var $effectsButtonApply = $effectsWidget.find('button[name="effect-apply"]');

                //add edit/switch toolbox button
                $editButton.before('<button value="Filter" class="edit-effect" name="edit-effect" type="submit" class="form-submit">Filter</button>');
                var $filterButton = $simplecropDataWidget.find('button[name="edit-effect"]');
                $filterButton.on('click', function () {
                    var $this = $(this);
                    $($effectButtons.get(0)).trigger('change');
                    $simplecropDataWidget.hide();
                    $effectsWidget.show();
                    return false;
                });

                $effectsButtonCancel.on('click', function() {
                    $effectsWidget.hide();
                    Drupal.behaviors.sonycmpFinalReportEffects.selectedEffect = $effectFormSelect.val();
                    $previewImageWithEffect.attr('src', composePreviewUrl());
                    $simplecropDataWidget.show();
                    return false;
                });

                $effectsButtonApply.on('click', function() {
                    $effectFormSelect.val(Drupal.behaviors.sonycmpFinalReportEffects.selectedEffect);
                    $effectsWidget.hide();
                    $simplecropDataWidget.show();
                    return false;
                });
            }
        }
    }
})(jQuery);
