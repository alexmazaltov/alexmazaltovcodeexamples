Drupal.behaviors.sonycmp_reports_page_confirmation_upload = {
    forms: {},
    attach: function (context, settings) {
        $ = jQuery;
        if (context == document) {
            $(window).on('beforeunload', function (evt) {
                var self = Drupal.behaviors.sonycmp_reports_page_confirmation_upload;
                if (self.formHasChanges() && !self.formWasSubmitted() && !self.modalDialogIsVisible()) {
                    return 'Unsaved changes on current step won`t be saved.';
                }
            });
            $("form", context).each(function () {
                var self = Drupal.behaviors.sonycmp_reports_page_confirmation_upload;
                var $form = $(this);
                var formId = $form.attr('id');
                var formInfo = {
                    form: $form,
                    original: $form.serialize(),
                    submitted: false
                };
                self.forms[formId] = formInfo;
                var submitButtons = $('.button.cancel-button, .button.save-button, .button.submit-button', $form)
                submitButtons.on('click mousedown change', function () {
                    formInfo.submitted = true;
                });
                $form.on('submit', function() {
                    formInfo.submitted = true;
                });
            });
        }
    },
    formHasChanges: function () {   
        var formId;
        for (formId in this.forms) {
            var formInfo = this.forms[formId];
            if (formInfo.form.serialize() != formInfo.original) {
                return true;
            }
        }
        return false;
    },
    formWasSubmitted: function () {
        var formId;
        for (formId in this.forms) {
            var formInfo = this.forms[formId];
            if (formInfo.submitted) {
                return true;
            }
        }
        return false;
    },
    modalDialogIsVisible: function() {
        return $('#modalContent').length ? true : false;
    }

}