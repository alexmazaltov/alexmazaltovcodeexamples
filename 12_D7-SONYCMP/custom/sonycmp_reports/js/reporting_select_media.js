/**
 * @file
 *  Select media content.
 */
(function ($) {
    /**
     * Custom plugin to remove array item with knowing index.
     * @param index
     */
    $.fn.removeThisElement = function (index) {
        var newArray = [];
        for (var i = 0; i < this.length; i++) {
            if (i == index) {
                continue;
            }
            newArray.push(this[i]);
        }
        Drupal.settings.selectedFieldsScope = newArray;
    };

    /**
     * Custom plugin to process div with custom attributes ot get them in json object.
     */
    $.fn.processDivToGetJSON = function () {
        return {
            nid: $(this).data('nid'),
            name: $(this).data('name'),
            delta: $(this).data('delta'),
            column: $(this).data('column')
        };
    };

    /**
     * This plugin need to compare each object in selected fields scope with new fields that is tried to be added.
     * @param compareTo
     * @returns {boolean}
     */
    $.fn.equalsFieldJsonObj = function (compareTo) {
        return (this[0].nid == compareTo.nid && this[0].name == compareTo.name &&
        this[0].delta == compareTo.delta && this[0].column == compareTo.column);
    };
})(jQuery);

(function ($, Drupal) {

    Drupal.globalClearAll = false;
    /**
     * Add span with stylized checkbox.
     */
    Drupal.behaviors.sonyCmpReportSelectMediaCheckboxes = {
        attach: function (context, settings) {
            if (context == "#cboxLoadedContent") {
                return;
            }
            //Disable all text input for each ajax execution
            $('input.text', context).prop('disabled', true);
            var checkboxes = $('input[name="choose-this-item"]', context);
            var checkboxesDuplicate = $('input[name="choose-this-item-dublicate"]', context);
            var $submit = $('.create-final-report-content button.form-submit');
            var $global_select_all = $('.js--global-tabs .js--button-select-all');
            var $global_clear = $('.js--global-tabs .js--button-clear-all');

            $(checkboxes).each(function () {
                if (!$(this).next().hasClass('checkbox')) {
                    $(this).after('<span class="custom checkbox"></span>');
                }
            });

            // Handle checkboxes.
            checkboxes.off('change').on('change', function (e, duplicate) {
                duplicate = typeof duplicate == 'undefined' ? false : duplicate;
                e.preventDefault();
                e.stopPropagation();
                var $checkbox = $(this);
                var $rep_select_all = $('.js--rep-details .js--button-select-all', $checkbox.parents('article'));
                var $rep_clear_all = $('.js--rep-details .js--button-clear-all', $checkbox.parents('article'));
                var $checkboxDuplicate = $('#' + $checkbox.prop('id') + '-dublicate');

                var isTextCheckbox = ($checkbox.data('text-checkbox-id') != 'undefined' ) ? $checkbox.data('text-checkbox-id') : false;
                if (isTextCheckbox) {
                    var $textCheckbox = $('#' + $checkbox.data('text-checkbox-id'));
                }
                var divToProcess = $checkbox.closest('.process-this-div');
                var $divToAddOverlay = $checkbox.closest('.column.media');
                var drupalField = $(divToProcess).processDivToGetJSON();

                if ($checkbox.is(':checked')) {
                    $(this).addClass('checked');
                    if ($(this).hasClass('media')) {
                        $divToAddOverlay.addClass('checked');
                    }
                    // Process checkboxes for details of report.
                    if ($checkbox.hasClass('details')) {
                        var $textWrap = $checkbox.parents('.process-this-div').find('.value-data-text');
                        if ($('textarea', $textWrap).length == 0) {
                            var text = $textWrap.text().trim();
                            var height = $textWrap.height();
                            var width = $textWrap.width();
                            if (height < 19) { //minimum 1 line
                                height = 19;
                            }
                            height += 20; //border and padding
                            drupalField.value = text;
                            $textWrap.data('origin', text);
                            var input = $('<textarea class="form-text"/>');
                            var $input = $(input);
                            $input.val(text);
                            // $input.height(height);
                            // $input.width(width);

                            $textWrap.empty();
                            $textWrap.append(input);
                            $input = $('textarea', $textWrap);
                            $input.autogrow({
                                onInitialize: false,
                                fixMinHeight: false,
                                animate: false
                            });
                            //$input.trigger('keyup');
                            $input.on('change', function () {
                                var drupalField = $(divToProcess).processDivToGetJSON();
                                Drupal.sonyCmpReportRemoveContent(drupalField);
                                drupalField.value = $(this).val().trim();
                                Drupal.sonyCmpReportAddContent(drupalField);
                            });
                        }
                    }
                    // See if in array of added fields already exist this field and delete it from json array.
                    Drupal.sonyCmpReportAddContent(drupalField);
                    // Enable button to finalize report.

                    if (isTextCheckbox && $checkbox.parents('.column-content').find('.js--media-item-text').length == 0) {
                        $textCheckbox.prop('disabled', false);
                    }
                    if (!duplicate) {
                        $checkboxDuplicate.prop('checked', true);
                        $checkboxDuplicate.trigger('change', true);
                    }
                } else {
                    $checkbox.removeClass('checked');
                    if ($checkbox.hasClass('media')) {
                        $divToAddOverlay.removeClass('checked');
                    }
                    // Process checkboxes for details of report.
                    if ($checkbox.hasClass('details')) {
                        var $textWrap = $checkbox.parents('.process-this-div').find('.value-data-text');
                        $('textarea.form-text', $textWrap).off('change');
                        $textWrap.html($textWrap.data('origin'));
                    }
                    Drupal.sonyCmpReportRemoveContent(drupalField);

                    if (!duplicate) {
                        $checkboxDuplicate.prop('checked', false);
                        $checkboxDuplicate.trigger('change', true);
                    }

                    if (isTextCheckbox) {
                        var idsRelatedMediaCheckboxes = $textCheckbox.data('media-checkbox-ids');
                        var $textCheckboxDrupalField = $($textCheckbox.closest('.process-this-div')).processDivToGetJSON();
                        var relatedMediaChecked = $('input[id^="' + idsRelatedMediaCheckboxes + '"]:checked').length;
                        $('input[id^="' + idsRelatedMediaCheckboxes + '"]:checked').each(function () {
                            if ($(this).parents('.column-content').find('.js--media-item-text').length) {
                                relatedMediaChecked--;
                            }
                        });
                        if (relatedMediaChecked) {
                            // There are one or more checked related media.
                            $textCheckbox.prop("disabled", false);
                        }
                        else {
                            Drupal.sonyCmpReportRemoveContent($textCheckboxDrupalField);
                            if ($textCheckbox.hasClass('checked')) {
                                $textCheckbox.prop('checked', false);
                                $textCheckbox.trigger('change', true);
                            }
                            $textCheckbox.prop("disabled", true);
                        }
                    }
                }
                $submit.trigger('checkAvailability');
                $global_select_all.trigger('change');
                $global_clear.trigger('change');
                $rep_select_all.trigger('change');
                $rep_clear_all.trigger('change');
            });

            // Handle duplicate checkboxes.
            checkboxesDuplicate.off('change').on('change', function (e, duplicate) {
                duplicate = typeof duplicate == 'undefined' ? false : duplicate;
                e.preventDefault();
                e.stopPropagation();
                var $checkbox = $(this);

                var $thisCheckboxLabel = $('label[for="' + $(this).prop('id') + '"]');

                if ($checkbox.is(':checked')) {
                    // Duplicate Checkbox has been checked.
                    $checkbox.addClass('checked');
                    $thisCheckboxLabel.html(Drupal.t('Remove from Report'));
                    $thisCheckboxLabel.addClass('added');
                } else {
                    $checkbox.removeClass('checked');
                    $thisCheckboxLabel.html(Drupal.t('Add to Report'));
                    $thisCheckboxLabel.removeClass('added');
                }
                if (!duplicate) {
                    var $checkboxOrigin = $('#' + $checkbox.data('original-checkbox-id'));
                    $checkboxOrigin.prop('checked', $checkbox.is(':checked'));
                    $checkboxOrigin.trigger('change', true);
                }
                $submit.trigger('checkAvailability');
            });
        }
    };

    /**
     * Remove field from selected list.
     * @param drupalField
     *   Selected Drupal field.
     */
    Drupal.sonyCmpReportRemoveContent = function (drupalField) {
        var $content = $('.create-final-report-content input[name="content"]');
        // See if in array of added fields already exist this field and delete it from json array.
        $(Drupal.settings.selectedFieldsScope).each(function (index) {
            if ($(this).equalsFieldJsonObj(drupalField)) {
                // We found equal json object that was already added before should be detached from Drupal.settings.selectedFieldsScope.
                $(Drupal.settings.selectedFieldsScope).removeThisElement(index);
                $content.trigger('rewriteContent', [{selectedFieldsScope: Drupal.settings.selectedFieldsScope}]);
            }
        });
    };

    /**
     * Add field to selected list.
     * @param drupalField
     *   Selected Drupal field.
     */
    Drupal.sonyCmpReportAddContent = function (drupalField) {
        var $content = $('.create-final-report-content input[name="content"]');
        // See if in array of added fields does not exist this field and add it to json array.
        var add = true;
        $(Drupal.settings.selectedFieldsScope).each(function () {
            if ($(this).equalsFieldJsonObj(drupalField)) {
                add = false;
            }
        });
        if (add) {
            Drupal.settings.selectedFieldsScope.push(drupalField);
            $content.trigger('rewriteContent', [{selectedFieldsScope: Drupal.settings.selectedFieldsScope}]);
        }
    };

    /**
     * Logic of writing information about selected content.
     */
    Drupal.behaviors.sonyCmpReportWriteContentLogic = {
        attach: function (context, settings) {
            if (context == "#cboxLoadedContent") {
                return;
            }
            var $content = $('.create-final-report-content input[name="content"]', context);
            if ($content.length == 0) {
                return false;
            }
            // Write to hidden field information about selected content.
            $content.on('rewriteContent', function (e, eventInfo) {
                $(this).val(JSON.stringify(eventInfo.selectedFieldsScope));
            });
        }
    };

    /**
     * Handle click event for select all buttons.
     * @param $this Button.
     * @param context context for searching checkboxes.
     * @param status Checked status.
     */
    Drupal.sonyCmpSelectClearAllEvent = function ($this, context, status, only_unprocessed) {
        var checkboxes = $('input[name^="choose-this-item"]', context);
        if (only_unprocessed) {
            checkboxes = $('input[name^="choose-this-item"]:not(.processed)', context);
        }
        if (checkboxes.length == 0) {
            $this.removeClass('active');
            return false;
        }
        var $wrap = $('.js--edit-final-report-media-content');
        var $submit = $('.create-final-report-content button.form-submit');
        var $global_select_all = $('.js--global-tabs .js--button-select-all', $wrap);
        var $global_clear = $('.js--global-tabs .js--button-clear-all', $wrap);
        if (status) {
            $this.addClass('active');
        } else {
            $this.addClass('active');
        }
        checkboxes.prop('checked', status);
        checkboxes.trigger('change', true);
        $submit.trigger('checkAvailability');
        $('.js--button-clear-all', $this.parents('.js--button-group')).trigger('change');
        $global_select_all.trigger('change');
        $global_clear.trigger('change');
    };

    /**
     * Handle change event for select all buttons.
     * @param $this Button.
     * @param context Context for searching checkboxes.
     */
    Drupal.sonyCmpSelectAllChange = function ($this, context) {
        var checkboxes = $('input[name^="choose-this-item"]', context);
        var selectAllCheckbox = $('.create-final-report-content input[name="global_select_all"]');
        if (checkboxes.length == 0) {
            selectAllCheckbox.val(0);
            $this.removeClass('active');
            return false;
        }
        if (checkboxes.filter(':checked').length == checkboxes.length) {
            $this.addClass('active');
            return true;
        }
        else {
            $this.removeClass('active');
            selectAllCheckbox.val(0);
            return false;
        }
    };

    /**
     * Handle change event for clear all buttons.
     * @param $this Button.
     * @param context Context for searching checkboxes.
     */
    Drupal.sonyCmpClearAllChange = function ($this, context) {
        var checkboxes = $('input[name^="choose-this-item"]', context);
        if (checkboxes.length == 0) {
            $this.removeClass('active');
            return false;
        }
        if (checkboxes.filter(':checked').length > 0) {
            $this.removeClass('active');
            return true;
        }
        else {
            $this.addClass('active');
            return false;
        }
    };

    /**
     * Logic of work with buttons Clear, Select all, Finalize report.
     */
    Drupal.behaviors.sonyCmpReportSelectMediaButtons = {
        attach: function (context, settings) {
            if (context == "#cboxLoadedContent") {
                return;
            }
            var $wrap = $('.js--edit-final-report-media-content', context);
            if ($wrap.length == 0) {
                return false;
            }

            var $global_select_all = $('.js--global-tabs .js--button-select-all', $wrap);
            var $global_clear = $('.js--global-tabs .js--button-clear-all', $wrap);
            var $submit = $('.create-final-report-content button.form-submit');

            // Disable button to Finalize report while one of fields have't been selected.
            $submit.off('checkAvailability').on('checkAvailability', function () {
                if (!Drupal.settings.selectedFieldsScope.length) {
                    $(this).prop('disabled', true);
                }
                else {
                    $(this).prop('disabled', false);
                }
            });
            $submit.trigger('checkAvailability');

            // Handle global select all.
            $global_select_all.off('click touchend').on('click touchend', function () {
                var $content = $('.view-content', $('.js--edit-final-report-media-content'));
                Drupal.sonyCmpSelectClearAllEvent($(this), $content, true, false);
                $('.create-final-report-content input[name="global_select_all"]').val(1);
                Drupal.globalClearAll = false;
            });

            // Handle global clear all.
            $global_clear.off('click touchend').on('click touchend', function () {
                Drupal.settings.selectedFieldsScope = [];
                var $content = $('.view-content', $('.js--edit-final-report-media-content'));
                $('.create-final-report-content input[name="content"]').trigger('rewriteContent', [{selectedFieldsScope: []}]);
                Drupal.sonyCmpSelectClearAllEvent($(this), $content, false, false);
                $(this).addClass('clicked');
                $('.create-final-report-content input[name="global_select_all"]').val(0);
                Drupal.globalClearAll = true;
            });

            // Change event for global select.
            $global_select_all.off('change').on('change', function () {
                var $content = $('.view-content', $('.js--edit-final-report-media-content'));
                Drupal.sonyCmpSelectAllChange($(this), $content);
            });

            // Change event for global clear.
            $global_clear.off('change').on('change', function () {
                if (!$(this).hasClass('clicked')) {
                    return;
                }
                var $content = $('.view-content', $('.js--edit-final-report-media-content'));
                Drupal.sonyCmpClearAllChange($(this), $content);
            });
            $('input[name^="choose-this-item"]', $wrap).addClass('processed');
        }
    };

    /**
     * Handle all dynamic buttons, loaded by ajax.
     */
    Drupal.behaviors.sonyCmpReportSelectMediaButtonsDynamic = {
        attach: function (context, settings) {
            if (context == "#cboxLoadedContent") {
                return;
            }
            var $wrap = $('.js--edit-final-report-media-content');
            var $global_select_all = $('.js--global-tabs .js--button-select-all', $wrap);
            var $global_clear = $('.js--global-tabs .js--button-clear-all', $wrap);
            var $rep_select_all = $('.js--rep-details .js--button-select-all', context);
            var $rep_clear_all = $('.js--rep-details .js--button-clear-all', context);

            if ($('.create-final-report-content input[name="global_select_all"]').val() == 1) {
                Drupal.sonyCmpSelectClearAllEvent($global_select_all, context, true, true);
            }
            else {
                $global_select_all.trigger('change');
            }
            if ($global_clear.hasClass('active') || Drupal.globalClearAll) {
                Drupal.sonyCmpSelectClearAllEvent($global_select_all, context, false, true);
            }
            else {
                $global_clear.trigger('change');
            }

            // Select checkboxes for media items and in popup as checked on ajax event.
            $(Drupal.settings.selectedFieldsScope).each(function () {
                var id = '#checkbox-nid-' + this.nid + '-' + this.name + '-delta-' + this.delta;
                var $this = $(id);
                if ($this.length) {
                    $this.prop('checked', true);
                    $this.prop('disabled', false);
                    if ($this.hasClass('details')) {
                        $this.parents('.process-this-div').find('.value-data-text').html(this.value);
                    }
                    $this.trigger('change', true);
                }

                var $thisDuplicate = $(id + '-dublicate');
                if ($thisDuplicate.length) {
                    $thisDuplicate.prop('checked', true);
                    $thisDuplicate.prop('disabled', false);
                    $thisDuplicate.trigger('change', true);
                }
            });
            // Enable only those text input for which media was checked.
            $('input.media:checked', context).each(function () {
                var relatedTextCheckboxExist = ($(this).data('text-checkbox-id') != 'undefined' ) ? $(this).data('text-checkbox-id') : false;
                if (relatedTextCheckboxExist) {
                    var $textCheckboxForThisMedias = $('#' + $(this).data('text-checkbox-id'));
                    $textCheckboxForThisMedias.prop('disabled', false);
                }
            });

            // Handle select all for rep.
            $rep_select_all.off('click touchend').on('click touchend', function () {
                Drupal.sonyCmpSelectClearAllEvent($(this), $(this).parents('article'), true, false);
            });
            // Handle clear all for rep.
            $rep_clear_all.off('click touchend').on('click touchend', function () {
                Drupal.sonyCmpSelectClearAllEvent($(this), $(this).parents('article'), false, false);
            });

            // Change event for rep select all.
            $rep_select_all.off('change').on('change', function () {
                Drupal.sonyCmpSelectAllChange($(this), $(this).parents('article'));
            });

            // Change event for rep clear all.
            $rep_clear_all.off('change').on('change', function (e, initial) {
                initial = typeof initial == 'undefined' ? false : initial;
                if (initial && !$global_clear.hasClass('active') && !Drupal.globalClearAll) {
                    return;
                }
                Drupal.sonyCmpClearAllChange($(this), $(this).parents('article'));
            });
            $rep_select_all.trigger('change');
            $rep_clear_all.trigger('change', true);
            $('input[name^="choose-this-item"]:not(.processed)', $wrap).addClass('processed');
        }
    };

})(jQuery, Drupal);
