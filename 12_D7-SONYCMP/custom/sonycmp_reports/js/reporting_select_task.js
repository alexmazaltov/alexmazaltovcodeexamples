(function ($, Drupal) {
  Drupal.behaviors.cmpReportingsFormStep1SelectTask = {
    attach: function (context, settings) {
      var $form = $('#sonycmp-reports-report-form-task');
      if ($form.length === 0) {
        return false;
      }
      if (settings && settings.views && settings.views.ajaxViews) {
        $.each(settings.views.ajaxViews, function (i, ajax_settings) {
          var radios = $('input[name="select_this_task_view"]');
          radios.off('change').on('change', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var radio = e.target;
            var assignThisNid = $(radio).val();

            if (radio.checked) {
              //Change value in the text field
              $('input[name="task"]', $form).val(assignThisNid);
              $('.form-submit', $form).trigger('checkAvailability');
            }
          });
        });
      }
    }
  };

  /**
   * Switcher for task types on the page of creating a new report.
   */
  Drupal.behaviors.cmpReportingsFormStep1SelectType = {
    attach: function (context, settings) {
      var $form = $('#sonycmp-reports-report-form-task');
      if ($form.length === 0) {
        return false;
      }
      $('.form-submit', $form).on('checkAvailability', function () {
        if ($('.view-tasks input.form-radio:checked').length == 0) {
          $(this).attr('disabled', true);
        }
        else {
          $(this).attr('disabled', false);
        }
      });
      var $exposed_form = $('#views-exposed-form-tasks-panel-tasks-for-report');
      $('.button-group a', $form).on('click touchend', function () {
        if ($(this).hasClass('active')) {
          return true;
        }
        $('input[name="task"]', $form).val('all');
        $('.button-group a', $form).each(function () {
          $(this).removeClass('active');
          $(this).parent().removeClass('active');
        });

        $(this).addClass('active');
        $(this).parent().addClass('active');

        $('.views-widget-filter-type select', $exposed_form).val($(this).data('filter'));
        $('.views-widget-filter-field_status_value select', $exposed_form).val('All');
        $('input[name="field_status_value"]').val('All');
        $('input[name="type"]', $form).val($(this).data('type'));
        $('.form-submit', $exposed_form).trigger('click');
      });
      $('.view-tasks').on('DOMSubtreeModified', function () {
        $('.form-submit', $form).trigger('checkAvailability');
      });
    }
  };
})(jQuery, Drupal);

jQuery(document).ready(function ($) {
  var $form = $('#sonycmp-reports-report-form-task');
  if ($form.length === 0) {
    return false;
  }
  $('.button-group a.active', $form).trigger('click');
  //Check already selected task for this report
  if (Drupal.settings.selectTaskJson.taskNid !== null) {
    $('#edit-selectthistask-view-radio-' + Drupal.settings.selectTaskJson.taskNid).attr('checked', 'checked');
    $('#edit-submit', $form).attr('disabled', false);
  }
  $('.form-submit', $form).trigger('checkAvailability');
});
