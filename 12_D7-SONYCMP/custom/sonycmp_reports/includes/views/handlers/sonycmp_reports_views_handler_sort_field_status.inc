<?php

class sonycmp_reports_views_handler_sort_field_status extends views_handler_sort {
  function query() {
    $this->ensure_my_table();
    // Add the field.
    $field =
    $formula = 'CASE ' . $this->real_field
      . ' WHEN ' . CMP_REPORTS_REPORT_STATUS_OPEN . ' THEN 0'
      . ' WHEN ' . CMP_REPORTS_REPORT_STATUS_MISSING . ' THEN 1'
      . ' WHEN ' . CMP_REPORTS_REPORT_STATUS_LATE . ' THEN 2'
      . ' WHEN ' . CMP_REPORTS_REPORT_STATUS_ON_TIME . ' THEN 3'
      . ' ELSE 1000'
      . ' END';
    $alias = $this->table_alias . '_' . $this->real_field . '_custom_order';
    $this->query->add_orderby(NULL, $formula, $this->options['order'], $alias);
  }
}