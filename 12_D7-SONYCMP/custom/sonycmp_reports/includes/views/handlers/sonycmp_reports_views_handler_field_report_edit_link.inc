<?php

/**
 * @file
 * Views field handler for reports edit link.
 */

class sonycmp_reports_views_handler_field_report_edit_link extends views_handler_field_node {

  function render($values) {
    return array(
      '#theme' => 'link',
      '#text' => t('Edit'),
      '#path' => 'reporting/' . $values->nid . '/edit/content',
      '#options' => array(
        'attributes' => array('class' => array('icon-edit')),
        'html' => FALSE,
      ),
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }
}
