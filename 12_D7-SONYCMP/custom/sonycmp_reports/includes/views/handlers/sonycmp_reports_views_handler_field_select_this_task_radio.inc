<?php

/**
 * @file
 * Definition of sonycmp_reports_handler_field_custom_radio.
 */
class sonycmp_reports_views_handler_field_select_this_task_radio extends views_handler_field_custom {
  /**
   * Render.
   *
   * @param $values
   *
   * @return string
   */
  function render($values) {
    $nid = $values->nid;
    $output = '<input class="task-reference form-radio hidden-field" type="radio" id="edit-selectthistask-view-radio-' . $nid . '" name="select_this_task_view" value="' . $nid . '">
               <span class="custom radio"></span>
               <label class="element-invisible" for="edit-selectthistask-view-radio-' . $nid . '">' . $nid . '</label>';
    return $output;
  }

}
