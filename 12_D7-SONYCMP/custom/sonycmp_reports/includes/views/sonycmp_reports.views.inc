<?php

/**
 * @file
 * Provide views data and handlers.
 */

/**
 * Implements hook_views_data().
 */
function sonycmp_reports_views_data() {
  $data = array();
  $data['node']['report_edit_link'] = array(
    'field' => array(
      'title' => t('Sony CMP report edit link'),
      'help' => t('Provide a report edit link.'),
      'handler' => 'sonycmp_reports_views_handler_field_report_edit_link',
    ),
  );
  $data['node']['select_this_task_radio'] = array(
    'title' => t('Select this task'),
    'help' => t('Custom radio button in row based on node nid'),
    'field' => array(
      'handler' => 'sonycmp_reports_views_handler_field_select_this_task_radio',
      'click sortable' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}


/**
 * Implements hook_views_data_alter().
 */
function sonycmp_reports_views_data_alter(&$data) {
  $data['field_data_field_status']['field_status_value']['sort']['handler'] = 'sonycmp_reports_views_handler_sort_field_status';
}
