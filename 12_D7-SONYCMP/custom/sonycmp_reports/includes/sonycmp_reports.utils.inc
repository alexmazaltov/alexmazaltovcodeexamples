<?php
/**
 * @file
 * Additional reports functions.
 */

/**
 * Get report statistic block ready to output HTML.
 *
 * @param $report
 *   Report node.
 *
 * @return string
 * @throws \Exception
 */
function sonycmp_reports_get_report_statistics($report) {
  $task = node_load($report->field_task[LANGUAGE_NONE][0]['target_id']);
  $nids = cmp_reports_get_all_started_reports_by_task($task);
  $reports = entity_load('node', $nids);
  $reps = count($reports);
  $due_date = format_date($task->field_due_date[LANGUAGE_NONE][0]['value'], 'tiny');
  $media_stats = sonycmp_reports_get_media_statistics($reports);
  $vars = array(
    'reps' => $reps,
    'photos' => $media_stats['image'],
    'videos' => $media_stats['video'],
    'screenshots' => sonycmp_reports_get_screenshots_count($reports),
    'links' => sonycmp_reports_get_links_count($reports),
    'due_date' => $due_date,
  );
  return theme('report_statistics', $vars);
}

/**
 * Helper for get count of active reports.
 *
 * @return mixed
 */
function sonycmp_reports_get_reports_count($archived = FALSE) {
  $query = db_select('node', 'n');
  $query->condition('n.status', !$archived);
  $query->condition('n.sticky', NODE_STICKY);
  $query->condition('n.type', SONY_CMP_CT_REPORT);
  $query->addExpression('COUNT (*)');
  return $query->execute()->fetchField();
}

/**
 * @param $alias
 * @param $nid
 *
 * @return mixed
 */
function sonycmp_reports_find_alias($alias, $nid) {
  $query = db_select('url_alias')
    ->condition('alias', $alias)
    ->condition('language', LANGUAGE_NONE)
    ->condition('source', 'node/' . $nid, '<>');
  $query->addExpression('1');
  $query->range(0, 1);

  return $query->execute()->fetchField();
}

/**
 * @param \stdClass $node
 *
 * @return string
 */
function sonycmp_reports_get_alias($node) {
  $title = preg_replace('/[^a-z0-9]+/', '-', strtolower($node->title));
  $alias = substr('reporting/' . $title, 0, 250);
  $i = 0;
  while (sonycmp_reports_find_alias($alias, $node->nid)) {
    $alias += '_' . $i;
    $i++;
  }

  return $alias;
}

/**
 * @param array $reports
 *
 * @return int
 */
function sonycmp_reports_get_links_count(array $reports) {
  $count = 0;
  foreach ($reports as $report) {
    if (isset($report->field_marketing_links) && !empty($report->field_marketing_links[LANGUAGE_NONE])) {
      $count += count($report->field_marketing_links[LANGUAGE_NONE]);
    }
  }

  return $count;
}

/**
 * Get number of screenshots .
 *
 * @param array $reports
 *   List of reports.
 *
 * @return int
 */
function sonycmp_reports_get_screenshots_count(array $reports) {
  $count = 0;
  foreach ($reports as $report) {
    if (isset($report->field_marketing_media) && !empty($report->field_marketing_media[LANGUAGE_NONE])) {
      $count += count($report->field_marketing_media[LANGUAGE_NONE]);
    }
  }

  return $count;
}


/**
 * Get statistics about attached media
 * @param $reports
 */
function sonycmp_reports_get_media_statistics($reports) {
  if (!is_array($reports)) {
    $reports = array($reports);
  }
  $result = array(
    'image' => 0,
    'video' => 0
  );
  foreach ($reports as $report) {
    $task = node_load($report->field_task[LANGUAGE_NONE][0]['target_id']);
    $steps_info = sonycmp_reports_structure_meta_info($report->type);
    $allowed_content = sonycmp_tasks_get_allowed_content($task);
    foreach ($steps_info as $step_info) {

      foreach ($step_info['sections'] as $section_id => $section_info) {
        if (
        !(in_array($section_id, $allowed_content) || in_array('-' . $section_id, $allowed_content))
        ) {
          //section is not allowed
          continue;
        }
        foreach ($section_info['fields'] as $field_name => $field_info) {
          $field = field_info_field($field_name);
          if ($field['type'] == 'file' && !empty($report->{$field_name}[LANGUAGE_NONE])) {
            foreach ($report->{$field_name}[LANGUAGE_NONE] as $file) {
              $result[$file['type']]++;
            }
          }
        }
      }
    }
  }
  return $result;
}


/**
 * Get all attached media from report
 * @param $report
 */
function sonycmp_reports_get_media($report) {
  $result = array();
  $task = node_load($report->field_task[LANGUAGE_NONE][0]['target_id']);
  $steps_info = sonycmp_reports_structure_meta_info($report->type);
  $allowed_content = sonycmp_tasks_get_allowed_content($task);
  foreach ($steps_info as $step_id => $step_info) {
    foreach ($step_info['sections'] as $section_id => $section_info) {
      if (
      !(in_array($section_id, $allowed_content) || in_array('-' . $section_id, $allowed_content))
      ) {
        //section is not allowed
        continue;
      }
      foreach ($section_info['fields'] as $field_name => $field_info) {
        $field = field_info_field($field_name);
        if ($field['type'] == 'file' && !empty($report->{$field_name}[LANGUAGE_NONE])) {
          foreach ($report->{$field_name}[LANGUAGE_NONE] as $file) {
            if (!array_key_exists($step_id, $result)) {
              $result[$step_id] = array(
                'title' => $step_info['title'],
                'sections' => array()
              );
            }
            if (!array_key_exists($section_id, $result[$step_id]['sections'])) {
              $result[$step_id]['sections'][$section_id] = array(
                'title' => $section_info['title'],
                'medias' => array()
              );
            }
            $result[$step_id]['sections'][$section_id]['medias'][] = $file;
          }
        }
      }
    }
  }
  return $result;
}


