<?php

/**
 * Implements hook_image_effect_info().
 */
function sonycmp_reports_image_effect_info() {
  $effects['sonycmp_reports_header_1'] = array(
    'label' => t('Report Header effect #1(light)'),
    'help' => t('Effect #1.'),
    'effect callback' => 'sonycmp_reports_header_effect_1_callback',
    'dimensions callback' => 'sonycmp_reports_header_dimensions',

  );
  $effects['sonycmp_reports_header_2'] = array(
    'label' => t('Report Header effect #2(dark)'),
    'help' => t('Effect #1.'),
    'effect callback' => 'sonycmp_reports_header_effect_2_callback',
    'dimensions callback' => 'sonycmp_reports_header_dimensions',

  );
  $effects['sonycmp_reports_header_3'] = array(
    'label' => t('Report Header effect #3(green)'),
    'help' => t('Effect #3.'),
    'effect callback' => 'sonycmp_reports_header_effect_3_callback',
    'dimensions callback' => 'sonycmp_reports_header_dimensions',

  );
  $effects['sonycmp_reports_header_4'] = array(
    'label' => t('Report Header effect #4(pink)'),
    'help' => t('Effect #1.'),
    'effect callback' => 'sonycmp_reports_header_effect_4_callback',
    'dimensions callback' => 'sonycmp_reports_header_dimensions',

  );
  return $effects;
}

function sonycmp_reports_header_effect_1_callback(&$image) {
  $module_path = drupal_get_path('module', 'sonycmp_reports');
  sonycmp_reports_header_effect_callback($image, $module_path . '/images/noise.png', $module_path . '/images/gradient1.png');
}

function sonycmp_reports_header_effect_2_callback(&$image) {
  $module_path = drupal_get_path('module', 'sonycmp_reports');
  sonycmp_reports_header_effect_callback($image, $module_path . '/images/noise.png', $module_path . '/images/gradient2.png');
}

function sonycmp_reports_header_effect_3_callback(&$image) {
  $module_path = drupal_get_path('module', 'sonycmp_reports');
  sonycmp_reports_header_effect_callback($image, $module_path . '/images/noise.png', $module_path . '/images/gradient3.png');
}

function sonycmp_reports_header_effect_4_callback(&$image) {
  $module_path = drupal_get_path('module', 'sonycmp_reports');
  sonycmp_reports_header_effect_callback($image, $module_path . '/images/noise.png', $module_path . '/images/gradient4.png');
}


function sonycmp_reports_header_effect_callback(&$image, $layer1, $layer2) {

  $tmp_input =drupal_realpath(drupal_tempnam('temporary://', 'sonycmp_reports_header_effect_callback_input'));
  $tmp_output = drupal_realpath(drupal_tempnam('temporary://', 'sonycmp_reports_header_effect_callback_output'));

  if (!image_save($image, $tmp_input)) {
    watchdog(WATCHDOG_ERROR, "Cannot save temporary image @filename", array("@filename" => $tmp_input));
    return FALSE;
  }
  $convert = variable_get('SONYCMP_REPORTS_CONVERT_PATH', 'convert');
  $exec_string = $convert . ' \\( ' . escapeshellarg($tmp_input) . ' ' . escapeshellarg($layer1) . ' -compose  overlay -define compose:args=40 -composite \\) ' . escapeshellarg($layer2) . ' -compose screen -composite ' . $tmp_output . ' 2>&1';
  $out = shell_exec($exec_string);
  if ($out) {
    watchdog(WATCHDOG_ERROR, "Cannot apply effect to image: @error", array('@error' => $out));
    return FALSE;
  }
  $new_image = image_load($tmp_output);
  $image->info = $new_image->info;
  $image->resource = $new_image->resource;
  return TRUE;
}

/**
 * Image dimensions callback; Resize.
 *
 * @param $dimensions
 *   Dimensions to be modified - an array with components width and height, in
 *   pixels.
 * @param $data
 *   An array of attributes to use when performing the resize effect with the
 *   following items:
 *   - "width": An integer representing the desired width in pixels.
 *   - "height": An integer representing the desired height in pixels.
 */
function sonycmp_reports_header_dimensions(&$dimensions, $data) {
  $dimensions['width'] = 1366;
  $dimensions['height'] = 434;
}