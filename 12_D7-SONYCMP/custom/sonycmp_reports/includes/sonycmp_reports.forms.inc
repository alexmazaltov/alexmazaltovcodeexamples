<?php

/**
 * Function will return info about report editing form: steps, sections, fields
 * @param $type
 * @return array
 */
function sonycmp_reports_structure_meta_info($type) {
  switch ($type) {
    case SONY_CMP_CT_MISC_REPORT:
      return array(
        'details' => array(
          'number' => 1,
          'title' => t('Details'),
          'sections' => array(
            'details' => array(
              'title' => t('Details'),
              'fields' => array(
                'field_description' => array(
                  'title' => t('Summary'),
                  'placeholder' => t('Describe the event and other notable details of the event...'),
                  'required' => TRUE,
                ),
                'field_event_name' => array(
                  'properties' => array(
                    '#prefix' => '<h3>If this task is for an event, please complete the inputs below:</h3>'
                  ),
                  'title' => t('Event Name?'),
                  'placeholder' => t('Add the Event Name')
                ),
                'field_event_type' => array(
                  'title' => t('Event Type?'),
                  'placeholder' => t('e.g. Listening Party, Album Release, On Campus Activation'),
                ),
                'field_event_place' => array(
                  'title' => t('Where was the event?'),
                  'placeholder' => t('City, State | Venue Name'),
                ),
                'field_event_date' => array(
                  'title' => t('When was the event?'),
                  'placeholder' => t('Date (e.g. MM/DD/YY)'),
                )
              )
            )
          )
        ),
        'media' => array(
          'number' => 2,
          'title' => t('Media'),
          'sections' => array(
            'media' => array(
              'title' => t('Media'),
              'short_title' => t('Marketing Materials'),
              'controlled' => TRUE,
              'fields' => array(
                'field_media_photos_media' => array(
                  'title' => t('Marketing Materials Media'),
                )
              )
            )
          )
        ),
        'marketing' => array(
          'number' => 3,
          'title' => t('Online Marketing'),
          'sections' => array(
            'marketing' => array(
              'title' => t('Online Marketing'),
              'controlled' => TRUE,
              'fields' => array(
                'field_marketing_media' => array(
                  'title' => t('Online Marketing Screenshots')
                )
              )
            )
          )
        ),
      );
    case SONY_CMP_CT_BI_WEEKLY_REPORT:
      return array(
        'market' => array(
          'number' => 1,
          'title' => t('Market Visibility'),
          'sections' => array(
            'market_store' => array(
              'title' => t('Independent Record Store'),
              'controlled' => TRUE,
              'fields' => array(
                'field_market_store_media' => array(
                  'title' => t('Independent Record Store Media')
                )
              )
            ),
            'market_shops' => array(
              'title' => t('Off Campus'),
              'controlled' => TRUE,
              'fields' => array(
                'field_market_shops_media' => array(
                  'title' => t('Off Campus Media')
                )
              )
            ),
            'market_campus' => array(
              'title' => t('On Campus'),
              'controlled' => TRUE,
              'fields' => array(
                'field_market_campus_media' => array(
                  'title' => t('On Campus Media')
                )
              )
            ),
          )
        ),
        'marketing' => array(
          'number' => 2,
          'title' => t('Online Marketing'),
          'sections' => array(
            'marketing' => array(
              'title' => t('Online Marketing'),
              'controlled' => TRUE,
              'fields' => array(
                'field_marketing_media' => array(
                  'title' => t('Online Marketing Screenshots')
                )
              )
            ),
            'links' => array(
              'title' => t('Links'),
              'fields' => array(
                'field_marketing_links' => array(
                  'title' => t('Add Links'),
                  'placeholder' => t('Paste url here'),
                ),
              )
            )
          ),
        )
      );
    case SONY_CMP_CT_TOUR_REPORT:
      return array(
        'details' => array(
          'number' => 1,
          'title' => t('Details'),
          'sections' => array(
            'details' => array(
              'title' => t('Details'),
              'fields' => array(
                'field_event_place' => array(
                  'title' => t('Where was the show?'),
                  'placeholder' => t('City, State | Venue Name'),
                  'required' => TRUE,

                ),
                'field_event_date' => array(
                  'title' => t('When was the show?'),
                  'placeholder' => t('Date (e.g. MM/DD/YY)'),
                  'required' => TRUE,

                ),
                'field_event_demographic' => array(
                  'title' => t('What was the Show Demographic?'),
                  'placeholder' => t('Describe the demographic of the show...'),
                  'required' => TRUE,

                ),
                'field_description' => array(
                  'title' => t('Performance Recap'),
                  'placeholder' => t('Describe how the show went, how the crowd reacted and other notable details of the show...'),
                  'required' => TRUE,

                )
              )
            )
          )
        ),
        'media' => array(
          'number' => 2,
          'title' => t('Performance Media'),
          'sections' => array(
            'media_photos' => array(
              'title' => t('Performance Photos'),
              'controlled' => TRUE,
              'fields' => array(
                'field_media_photos_media' => array(
                  'title' => t('Performance Photos Media')
                )
              )
            ),
            'media_video' => array(
              'title' => t('Performance Videos'),
              'controlled' => TRUE,
              'fields' => array(
                'field_media_video_media' => array(
                  'title' => t('Performance Videos Media'),
                )
              )
            )
          )
        ),
        'venue' => array(
          'number' => 3,
          'title' => t('Venue Promo'),
          'sections' => array(
            'venue_photos' => array(
              'title' => t('Promotion Efforts'),
              'controlled' => TRUE,
              'fields' => array(
                'field_venue_photos_media' => array(
                  'title' => t('Promotion Efforts Media'),
                )
              )
            ),
            'venue_testimonials' => array(
              'title' => t('Fan Testimonials'),
              'controlled' => TRUE,
              'fields' => array(
                'field_venue_testimonials_media' => array(
                  'title' => t('Fan Testimonials Media'),
                )
              )
            )
          )
        ),
        'market' => array(
          'number' => 4,
          'title' => t('Market Visibility'),
          'sections' => array(
            'market_store' => array(
              'title' => t('Independent Record Store'),
              'controlled' => TRUE,
              'fields' => array(
                'field_market_store_media' => array(
                  'title' => t('Independent Record Store Media')
                )
              )
            ),
            'market_shops' => array(
              'title' => t('Off Campus'),
              'controlled' => TRUE,
              'fields' => array(
                'field_market_shops_media' => array(
                  'title' => t('Off Campus Media')
                )
              )
            ),
            'market_campus' => array(
              'title' => t('On Campus'),
              'controlled' => TRUE,
              'fields' => array(
                'field_market_campus_media' => array(
                  'title' => t('On Campus Media')
                )
              )
            )
          )
        ),
        'marketing' => array(
          'number' => 5,
          'title' => t('Online Marketing'),
          'sections' => array(
            'marketing' => array(
              'title' => t('Online Marketing'),
              'short_title' => t('Screenshots'),
              'controlled' => TRUE,
              'fields' => array(
                'field_marketing_media' => array(
                  'title' => t('Online Marketing Screenshots'),
                )
              )
            ),
            'links' => array(
              'title' => t('Links'),
              'fields' => array(
                'field_marketing_links' => array(
                  'title' => t('Add Links'),
                  'placeholder' => t('Paste url here'),
                ),
              )
            )
          ),
        )
      );
  }
  die;
}

/**
 *
 * Universal form for report editing
 * @param $form
 * @param $form_state
 * @param $report
 * @param $task
 * @param $current_step
 */
function sonycmp_reports_edit_form_form($form, &$form_state, $report, $task, $current_step) {
  $steps = cmp_reports_get_steps($task);
  cmp_reports_edit_report_form_get_header($form, $form_state, $task, $report, $current_step, $steps);
  cmp_reports_edit_report_form_get_trails($form, $steps, $current_step);
  sonycmp_reports_edit_form_add_fields($form, $form_state, $report, $task, $current_step);
  $form_state['storage']['#step'] = $current_step;
  return $form;
}

/**
 * Default implementation for saving report form step
 * @param $form
 * @param $form_state
 * @return mixed
 */
function sonycmp_reports_edit_form_save_report($form, &$form_state) {
  $step = $form_state['storage']['#current_step'];
  $task = $form_state['storage']['#task'];
  $report = $form_state['storage']['#report'];
  sonycmp_reports_edit_form_load_report_fields_from_form_state($report, $task, $step, $form_state);
  node_save($report);
  return $report;
}

/**
 * Add to for all allowed sections with fields for given step
 *
 * @param $form
 * @param $form_state
 * @param $report
 * @param $task
 * @param $step
 */
function sonycmp_reports_edit_form_add_fields(&$form, &$form_state, $report, $task, $step) {
  $steps_info = sonycmp_reports_structure_meta_info($report->type);
  $allowed_content = sonycmp_tasks_get_allowed_content($task);
  $required_content = sonycmp_tasks_get_required_content($task);

  $form_state['storage']['#required_media_fields'] = array();


  foreach ($steps_info[$step]['sections'] as $section_id => $section_info) {
    if (
      !empty($section_info['controlled'])
      && !(in_array($section_id, $allowed_content) || in_array('-' . $section_id, $allowed_content))
    ) {
      //section is not allowed
      continue;
    }

    if (
      !empty($section_info['controlled'])
      && (in_array($section_id, $required_content) || in_array('-' . $section_id, $required_content))
    ) {
      //section is required
      $section_required = TRUE;
    }
    else {
      $section_required = FALSE;
    }

    $form[$section_id] = array();
    cmp_reports_edit_report_form_get_main($form[$section_id]);
    foreach ($section_info['fields'] as $field_name => $settings) {
      $value = isset($report->{$field_name}[LANGUAGE_NONE][0]['value']) ? $report->{$field_name}[LANGUAGE_NONE][0]['value'] : '';
      $instance = field_info_instance('node', $field_name, $report->type);
      $field = field_info_field($field_name);
      $form_item = array(
        '#attributes' => array(),
        '#title' => $settings['title'],
        '#required' => !empty($settings['required']),
        '#attached' => array(
          'css' .
          'js'
        )
      );
      if (!empty($settings['placeholder'])) {
        $form_item['#attributes']['placeholder'] = $settings['placeholder'];
      }
      switch ($field['type']) {
        case 'text_long':
          $form_item += array(
            '#type' => 'textarea',
            '#rows' => $instance['widget']['settings']['rows'],
            '#default_value' => $value,
          );
          $form_item['#attributes']['class'] = array(
            'text-full',
            'textarea-maxlength-js'
          );
          if (!empty($instance['widget']['settings']['enable_recommended_maxlength'])) {
            $form_item['#attributes']['maxlength_js'] = $instance['widget']['settings']['recommended_maxlength'];
          }
          else {
            $form_item['#attributes']['maxlength_js'] = $field['settings']['maxlength'];
          }
          if (!empty($field['settings']['max_length'])) {
            $form_item['#attributes']['maxlength'] = $field['settings']['max_length'];
          }
          $form_item['#attributes']['#attached']['js'][] = drupal_get_path('module', 'textarea_maxlength_js_widget') . '/js/textarea_maxlength_js_widget.js';
          $form_item['#attributes']['#attached']['js'][] = array(
            'data' => array(
              'textarea_maxlength_js_options' => array(
                'counterText' => '',
                'counterDivider' => '/',
                'enforce' => TRUE,
                'warning' => $form_item['#attributes']['maxlength_js'] - 10,
              )
            ),
            'type' => 'setting'
          );
          break;
        case 'datestamp':
          $form_item += array(
            '#type' => 'date_popup',
            '#timepicker' => 'default',
            '#date_type' => DATE_UNIX,
            '#date_format' => 'm/d/Y',
            '#date_text_parts' => array(),
            '#date_year_range' => '-1:+0',
            '#date_label_position' => 'none',
            '#default_value' => $value ? format_date($value, 'custom', 'Y-m-d H:i:s') : '',
            '#datepicker_options' => array(
              'maxDate' => "+0",
            ),
          );
          break;

        case 'link_field':
          $links_values = isset($report->{$field_name}[LANGUAGE_NONE]) ? $report->{$field_name}[LANGUAGE_NONE] : array();
          if (empty($form_state['links_count'])) {
            $form_state['links_count'] = count($links_values) > 3 ? count($links_values) : 3;
          }
          $form_item = array(
            '#title' => $settings['title'],
            '#type' => 'item',
            '#tree' => TRUE,
            '#prefix' => '<div id="links-wrapper">',
            '#suffix' => '</div>',
            '#id' => 'edit-links',
          );
          for ($i = 0; $i < $form_state['links_count']; $i++) {
            $form_item[$i]['url'] = array(
              '#title' => t('Link value !count', array('!count' => $i + 1)),
              '#title_display' => 'invisible',
              '#type' => 'textfield',
              '#default_value' => isset($links_values[$i]) ? $links_values[$i]['url'] : '',
              '#attributes' => array(
                'placeholder' => $settings['placeholder'],
                'class' => array('link-input'),
              ),
              '#prefix' => '<div class="cmp-task-link-wrapper">',
            );
            $form_item[$i]['title'] = array(
              '#title' => t('Link value !count', array('!count' => $i + 1)),
              '#title_display' => 'invisible',
              '#type' => 'textfield',
              '#default_value' => isset($links_values[$i]) ? $links_values[$i]['title'] : '',
              '#attributes' => array(
                'placeholder' => t('Link description'),
                'class' => array('title-input'),
              ),
              '#suffix' => '</div>',
            );
          }
          if ($form_state['links_count'] > 3) {
            $form_item['remove'] = array(
              '#type' => 'submit',
              '#value' => t('Remove last'),
              '#submit' => array('cmp_reports_links_remove_one'),
              '#attributes' => array(
                'class' => array(
                  'button',
                  'small',
                  'remove-item'
                )
              ),
              '#ajax' => array(
                'callback' => 'cmp_reports_links_add_more_callback',
                'wrapper' => 'links-wrapper',
              ),
              '#limit_validation_errors' => array(),
            );
          }
          $form_item['add'] = array(
            '#type' => 'submit',
            '#value' => t('Add more'),
            '#attributes' => array(
              'class' => array(
                'button',
                'small',
                'add-item'
              )
            ),
            '#submit' => array('cmp_reports_links_add_one'),
            '#ajax' => array(
              'callback' => 'cmp_reports_links_add_more_callback',
              'wrapper' => 'links-wrapper',
            ),
            '#limit_validation_errors' => array(),
          );
          break;

        case 'file': //our media fields
          //hack for second column size
          if ($section_required) {
            $form_state['storage']['#required_media_fields'][] = $field_name;
          }
          $form_parent = &$form[$section_id]['report_main']['left'];
          field_attach_form('node', $report, $form_parent, $form_state, LANGUAGE_NONE, array('field_name' => $field_name));
          // Remove unneeded array elements.
          unset($form_parent['#fieldgroups']);
          unset($form_parent[$field_name][LANGUAGE_NONE]['add']['#description']);

          $form[$section_id]['#media_field_name'] = $field_name;
          $form[$section_id]['#after_build'][] = 'cmp_reports_move_attachment_previews';
          $form[$section_id]['report_main']['left']['#prefix'] = '<div class="column large-5 small-7">';
          $form[$section_id]['report_main']['right']['#prefix'] = '<div class="column large-7 small-5">';

          continue 2;

      }
      $form[$section_id]['report_main']['left'][$field_name] = $form_item;
    }
  }
}

/**
 * Return key-value aray of allowed(non-empty) steps
 * @param $task
 * @return array
 */
function sonycmp_reports_get_allowed_steps($task) {
  $result = array();
  $report_type = sonycmp_tasks_and_reports_map($task->type);
  $steps_info = sonycmp_reports_structure_meta_info($report_type);
  $allowed_content = sonycmp_tasks_get_allowed_content($task);
  foreach ($steps_info as $step_id => $step) {
    foreach ($step['sections'] as $section_id => $section_info) {
      if (
        !empty($section_info['controlled'])
        && !(in_array($section_id, $allowed_content) || in_array('-' . $section_id, $allowed_content))
      ) {
        //section is not allowed
        continue;
      }
      $result[$step_id] = $step;
      break;//we need only non-empty steps
    }
  }
  return $result;
}

/**
 * Load to report form submitted values for current step
 * @param $report
 * @param $task
 * @param $step
 * @param $form_state
 */
function sonycmp_reports_edit_form_load_report_fields_from_form_state(&$report, $task, $step, $form_state) {
  $steps_info = sonycmp_reports_structure_meta_info($report->type);
  $allowed_content = sonycmp_tasks_get_allowed_content($task);
  $values = $form_state['values'];
  foreach ($steps_info[$step]['sections'] as $section_id => $section_info) {
    if (
      !empty($section_info['controlled'])
      && !(in_array($section_id, $allowed_content) || in_array('-' . $section_id, $allowed_content))
    ) {
      //section is not allowed
      continue;
    }
    foreach ($section_info['fields'] as $field_name => $field_info) {
      $instance = field_info_instance('node', $field_name, $report->type);
      $field = field_info_field($field_name);
      switch ($field['type']) {
        case 'link_field':
          if (isset($values[$field_name])) {
            $report->{$field_name} = array(LANGUAGE_NONE => array());
            foreach ($values[$field_name] as $delta => $row) {
              if (is_numeric($delta) && !empty($row['url'])) {
                $report->{$field_name}[LANGUAGE_NONE][$delta] = array(
                  'url' => $row['url'],
                  'title' => $row['title']
                );
              }
            }
          }
        case 'file':
          if (isset($values[$field_name][LANGUAGE_NONE])) {
            $report->{$field_name} = array(LANGUAGE_NONE => array());
            foreach ($values[$field_name][LANGUAGE_NONE] as $value) {
              if (is_array($value) && !empty($value['fid'])) {
                $description_field = 'description_' . $field_name . '_' . $value['fid'];
                if (isset($_POST[$description_field])) {
                  $value['description'] = $_POST[$description_field];
                }
                $report->{$field_name}[LANGUAGE_NONE][] = $value;
              }
            }
          }
          break;

        default:
          if (isset($values[$field_name])) {
            if (is_array($values[$field_name])) {
              $report->{$field_name} = array(LANGUAGE_NONE => array());
              foreach ($values[$field_name] as $delta => $value) {
                $report->{$field_name}[LANGUAGE_NONE][$delta] = array('value' => $value);
              }
            }
            else {
              $report->{$field_name} = array(LANGUAGE_NONE => array(array('value' => $values[$field_name])));

            }
          }

      }
    }
  }
}

/**
 * Validate if report ready for submission
 * @param $report
 * @param $task
 * @return array
 */
function sonycmp_reports_validate_report($report, $task) {
  $errors = array();
  $steps_info = sonycmp_reports_structure_meta_info($report->type);
  $allowed_content = sonycmp_tasks_get_allowed_content($task);
  $required_content = sonycmp_tasks_get_required_content($task);

  foreach ($steps_info as $step_id => $step) {
    foreach ($step['sections'] as $section_id => $section_info) {
      if (
        !empty($section_info['controlled'])
        && !(in_array($section_id, $allowed_content) || in_array('-' . $section_id, $allowed_content))
      ) {
        //section is not allowed
        continue;
      }

      if (
        !empty($section_info['controlled'])
        && (in_array($section_id, $required_content) || in_array('-' . $section_id, $required_content))
      ) {
        //section is required
        $section_required = TRUE;
      }
      else {
        $section_required = FALSE;
      }

      foreach ($section_info['fields'] as $field_name => $field_info) {
        $instance = field_info_instance('node', $field_name, $report->type);
        $field = field_info_field($field_name);
        switch ($field['type']) {
          case 'text_long':
            if (!empty($field_info['required']) && empty($report->{$field_name}[LANGUAGE_NONE])) {
              $errors[] = array(
                'message' => t('%name is required', array('%name' => $field_info['title'])),
                'step' => $step_id,
                'section' => $section_id,
                'field' => $field_name
              );
              break;
            }
            elseif (!empty($report->{$field_name}[LANGUAGE_NONE])) {
              $max_length = NULL;
              if (!empty($instance['widget']['settings']['enable_recommended_maxlength'])) {
                $max_length = $instance['widget']['settings']['recommended_maxlength'];
              }
              elseif (!empty($field['settings']['maxlength'])) {
                $max_length = $field['settings']['maxlength'];
              }
              foreach ($report->{$field_name}[LANGUAGE_NONE] as $row) {
                if (strlen($row['value'])) {
                  if ($max_length && strlen($row['value']) > $max_length) {
                    $errors[] = array(
                      'message' => t("You've exceeded the maximum number of characters (!count)", array('!count' => $max_length)),
                      'step' => $step_id,
                      'section' => $section_id,
                      'field' => $field_name
                    );
                    break;
                  }
                }
                else {
                  if (!empty($field_info['required'])) {
                    $errors[] = array(
                      'message' => t('%name is required', array('%name' => $field_info['title'])),
                      'step' => $step_id,
                      'section' => $section_id,
                      'field' => $field_name
                    );
                    break;
                  }
                }
              }
            }
            break;
          case 'file':
            if ($section_required) {
              if (empty($report->{$field_name}[LANGUAGE_NONE])) {
                $errors[] = array(
                  'message' => t('%name is required', array('%name' => $field_info['title'])),
                  'step' => $step_id,
                  'section' => $section_id,
                  'field' => $field_name
                );
                break;
              }

            }
            foreach ($report->{$field_name}[LANGUAGE_NONE] as $delta => $item) {
              if (!strlen($item['description'])) {
                $errors[] = array(
                  'message' => t('All titles must be filled.'),
                  'step' => $step_id,
                  'section' => $section_id,
                  'field' => $field_name
                );
                break 2;
              }
            }
            break;
          default:
            if ($section_required) {
              if (empty($report->{$field_name}[LANGUAGE_NONE])) {
                $errors[] = array(
                  'message' => t('%name is required', array('%name' => $field_info['title'])),
                  'step' => $step_id,
                  'section' => $section_id,
                  'field' => $field_name
                );
                break;
              }
            }
        }
      }
    }
  }

  return $errors;
}

/**
 * Validate if current step in report form is valid
 * @param $form
 * @param $form_state
 */
function sonycmp_reports_edit_form_validate($form, &$form_state) {
  $report = $form_state['storage']['#report'];
  $task = $form_state['storage']['#task'];
  $step = $form_state['storage']['#step'];

  sonycmp_reports_edit_form_load_report_fields_from_form_state($report, $task, $step, $form_state);
  $errors = sonycmp_reports_validate_report($report, $task);
  foreach ($errors as $error) {
    if ($error['step'] == $step) {
      form_set_error($error['field'], $error['message']);
    }
  }
  if (form_get_errors()) {
    $form_state['complete form']['redirect']['#value'] = '';
    form_set_value($form['redirect'], '', $form_state);
  }
}

/**
 * Validate if report ready for submission and set red non valid fields in current step
 * For ajax call
 * @param $form
 * @param $form_state
 */
function sonycmp_reports_edit_form_validate_report_ajax($form, &$form_state) {
  $report = $form_state['storage']['#report'];
  $task = $form_state['storage']['#task'];
  $step = $form_state['storage']['#step'];

  sonycmp_reports_edit_form_load_report_fields_from_form_state($report, $task, $step, $form_state);
  $errors = sonycmp_reports_validate_report($report, $task);

  if ($errors) {
    $steps_info = sonycmp_reports_structure_meta_info($report->type);
    $wrong_steps = array();
    foreach ($errors as $error) {
      if (!array_keys($error['step'], $wrong_steps)) {
        $wrong_steps[$error['step']] = $steps_info[$error['step']]['title'];
      }
      if ($error['step'] == $step) {
        form_set_error($error['field'], $error['message']);
      }
    }
    reset($wrong_steps);
    $step = key($wrong_steps);

    $message = t('You cannot submit this task for review until all required sections are completed.');

    $content['message'] = array(
      'type' => 'markup',
      '#markup' => '<p class="custom-style--restyle">' . $message . '</p>',
    );
    $content['description'] = array(
      'type' => 'markup',
      '#markup' => '<p class="custom-style--restyle">' . t('The following sections are incomplete: @sections', array('@sections' => implode(', ', $wrong_steps))) . '</p>'
    );
    $content['continue_link'] = array(
      '#theme' => 'link',
      '#text' => t('Continue'),
      '#path' => 'reports/' . $report->nid . '/edit/' . $step,
      '#options' => array(
        'attributes' => array(
          'class' => array(
            'button',
            'validate-continue-btn',
          ),
        ),
        'html' => FALSE,
      ),
    );

    ctools_include('modal');
    ctools_modal_add_js();
    drupal_get_messages();
    $commands[] = ctools_modal_command_display('title', $content);

    print ajax_render($commands);
    drupal_exit();

  }
}