<?php
/**
 * @file
 * Page callbacks for reports.
 */

/**
 * Create a new empty report.
 */
function sonycmp_reports_add_new_report() {
  $node = (object) array(
    'type' => SONY_CMP_CT_REPORT,
    'language' => LANGUAGE_NONE,
    'status' => NODE_NOT_PUBLISHED,
    'title' => 'New final report',
  );
  node_object_prepare($node);
  node_save($node);
  drupal_goto('reporting/' . $node->nid . '/edit/task');
}

/**
 * Popup for canceling report.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 * @param \stdClass $report
 *   Node object.
 *
 * @return string
 *   HTML output.
 */
function sonycmp_reports_cancel_report($form, &$form_state, \stdClass $report) {
  $form_state['storage']['report'] = $report;

  $form_description = t('Are you sure you want </br> to cancel creating this Report?');
  if ($report->sticky && $report->status) {
    $form_description = t('Are you sure you want to <br>cancel editing <strong>' . $report->title . '</strong>?<br> Unsaved changes will not be saved.');
  }

  $confirm_form = confirm_form($form, t('Are you sure you want to cancel?'), 'reporting', $form_description, t('Yes'), t('No'));
  $submit_button = $confirm_form['actions']['submit'];
  unset($confirm_form['actions']['submit']);
  $confirm_form['actions']['cancel'] = array(
    '#markup' => l(t('No'), '#', array(
      'attributes' => array(
        'class' => 'ctools-close-modal close-modal-restyle button',
      ),
      'external' => TRUE,
    )),
  );
  $confirm_form['actions'][] = $submit_button;
  $confirm_form['actions']['#prefix'] = '<div class="confirm-actions-wrapper">';
  $confirm_form['actions']['#suffix'] = '</div>';

  return $confirm_form;
}

/**
 * Submit handler for form of removing report.
 */
function sonycmp_reports_cancel_report_submit($form, &$form_state) {
  $report = $form_state['storage']['report'];

  if ($report && $report->type == SONY_CMP_CT_REPORT && !$report->sticky && !$report->status) {
    node_delete($report->nid);
  }

  $commands[] = ctools_ajax_command_redirect('reporting');
  print ajax_render($commands);
  exit;
}


//function sonycmp_reports_preview_iframe($report) {
//  if (!in_array($report->type, sonycmp_reports_content_types())) {
//    return drupal_not_found();
//  }
//  $report_view = node_view($report, 'full');
////  $path = drupal_get_path('theme', 'sonycmp');
////  drupal_add_js($path . '/js/reports_full_sidebar.js');
//  //$markup = drupal_render_page($report_view);
//  //print $markup;
//  //drupal_exit();
//  drupal_add_js(
//    'Drupal.settings.admin_menu.suppress = true;',
//    array('type' => 'inline', 'scope' => 'footer')
//  );
//  return $report_view;
//}

function sonycmp_reports_sidebar($report) {
  if (!in_array($report->type, sonycmp_reports_content_types())) {
    ctools_include('ajax');
    $commands[] = ctools_ajax_command_redirect('not-found');
    return array('#type' => 'ajax', '#commands' => $commands);
  }
  else {
    $report_view = node_view($report, 'st2_full');
    $markup = drupal_render($report_view);
    $commands[] = ajax_command_html('.report-sidebar-content', $markup);
    $output['#commands'][] = array('command' => 'sidebar_refresh');
    $output['#commands'][] = array('command' => 'sidebar_scroll_to', 'data' => '.report-sidebar-content');

    return array('#type' => 'ajax', '#commands' => $commands);
  }

}