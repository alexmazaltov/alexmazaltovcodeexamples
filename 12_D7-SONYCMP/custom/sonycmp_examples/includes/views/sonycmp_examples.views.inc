<?php

/**
 * @file
 *
 * Provide views data and handlers.
 */

/**
 * Implements hook_views_data().
 */
function sonycmp_examples_views_data() {
  $data['node']['example_edit_link'] = array(
    'field' => array(
      'title' => t('Sony CMP example edit link'),
      'help' => t('Provide example edit link.'),
      'handler' => 'sonycmp_examples_views_handler_field_example_edit_link',
    )
  );

  return $data;
}
