<?php

/**
 * @file
 *
 * Views field handler for example edit link.
 */

class sonycmp_examples_views_handler_field_example_edit_link extends views_handler_field_node {

  function render($values) {
    return array(
      '#theme' => 'link',
      '#text' => t('Edit'),
      '#path' => 'examples/' . $values->nid . '/edit',
      '#options' => array(
        'attributes' => array('class' => array('icon-edit')),
        'html' => FALSE,
      ),
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }
}
