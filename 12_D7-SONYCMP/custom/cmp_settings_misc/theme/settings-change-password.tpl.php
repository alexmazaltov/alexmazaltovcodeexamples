<?php
/**
 * @file Template for rendering list with links for settings pages
 *  - Settings
 *  - Password change
 * Available variables:
 * - $uid
 */
?>

<div class="settings-popup">
  <ul>
    <li><?php print l(t('Edit Profile'), "settings/$uid/edit"); ?></li>
    <li><?php print l(t('Change Password'), "settings/$uid/edit/password"); ?></li>
  </ul>
</div>
