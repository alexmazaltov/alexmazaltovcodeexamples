<?php

/**
 * Implements hook_form_alter().
 */
function sonycmp_tasks_form_alter(&$form, &$form_state, $form_id) {
  $forms = array(
    'comment_node_bi_weekly_task_form',
    'comment_node_misc_task_form',
    'comment_node_tour_task_form',
  );
  if (in_array($form_id, $forms)) {
    $submit_callback = sonycmp_theme_migrate_enabled() ? 'sonycmp_tasks_ajax_comment_submit_js' : 'cmp_tasks_ajax_comments_submit_js';
    $form['actions']['submit']['#ajax']['callback'] = $submit_callback;
    // Quick fix of undefined function call.
    // @see https://www.drupal.org/node/2359837
    $form_state['ctools comment alter'] = FALSE;
    $form['comment_body'][LANGUAGE_NONE][0]['value']['#attributes']['required'] = TRUE;
  }
}

/**
 * Comment form custom callback
 * @param $form
 * @param $form_state
 * @return array
 */
function sonycmp_tasks_ajax_comment_submit_js($form, &$form_state) {
  $output = ajax_comments_submit_js($form, $form_state);

  if (!form_get_errors()) {
    $form['#node']->comment_count++;
    $node = node_load($form['#node']->nid);
    $type = str_replace('_', '-', $node->type);
    $comment_count = $node->comment_count + 1;

    $count_comments = array(_sonycmp_comments_number_text($comment_count));
    $output['#commands'][] = ajax_command_invoke('.comments-sidebar h2.title', 'html', $count_comments);
    $output['#commands'][] = ajax_command_invoke('.main-content h2.node-title .comments-switcher', 'html', $count_comments);
    $output['#commands'][] = ajax_command_invoke('article.node-' . $type . ' .field-name-field-description .comments-switcher', 'html', array(_sonycmp_comments_number_text($comment_count)));
    $output['#commands'][] = ajax_command_invoke('.comments-sidebar .pane-content', 'scrollTop', array(4294967295));
    $output['#commands'][] = ajax_command_invoke('.pane-node-comments .pane-content', 'perfectScrollbar', array('update'));

  }
  else {
    // Do not show error messages, so field will be just highlighted.
    drupal_get_messages();
  }

  return $output;
}