<?php


/**
 * Return global tasks statistics groupped by types and statuses
 * @param bool $archived
 */
function sonycmp_tasks_get_statistics($archived = FALSE) {

  $cid = __FUNCTION__ . ':' . (is_null($archived) ? 'NULL' : $archived ? 'TRUE' : 'FALSE');
  $cached = cache_get($cid, SONY_CMP_CACHE_BIN);
  if ($cached) {
    return $cached->data;
  }
  $query = db_select('node', 'task');
//tasks only
  $query->condition('task.type', sonycmp_tasks_content_types(), 'IN');
  if (!is_null($archived)) {
    $query->condition('task.status', $archived ? NODE_NOT_PUBLISHED : NODE_PUBLISHED, '=');
  }
//which are "published"
  $query->condition('task.sticky', NODE_STICKY, '=');
//also fetch status
  $query->join('field_data_field_task_status', 'field_status', 'field_status.entity_id = task.nid');
//group by status and report type
  $query->groupBy('field_status.field_task_status_value')
    ->groupBy('task.type');
//add required fields
  $query->addField('task', 'type', 'type');
  $query->addField('field_status', 'field_task_status_value', 'status');
  $query->addExpression('COUNT(*)', 'cnt');

  $stats = $query->execute()->fetchAll();

  $stats_by_type = array();
  $stats_by_status = array(
    CMP_TASKS_TASK_STATUS_OPEN => 0,
    CMP_TASKS_TASK_STATUS_COMPLETED => 0,
    CMP_TASKS_TASK_STATUS_OVERDUE => 0
  );
  foreach (sonycmp_tasks_content_types() as $type) {
    $stats_by_type[$type] = 0;
  }

  foreach ($stats as $stat) {
    $stats_by_type[$stat->type] += $stat->cnt;
    $stats_by_status[$stat->status] += $stat->cnt;
  }
  $result = array(
    'statuses' => $stats_by_status,
    'types' => $stats_by_type,
    'raw' => $stats
  );
  cache_set($cid, $result, SONY_CMP_CACHE_BIN, CACHE_TEMPORARY);
  return $result;
}


/**
 * Return global tasks statistics groupped by types and statuses
 * @param stdClass $task
 * @param bool $archived
 */
function sonycmp_tasks_get_statistics_for_task($task, $archived = FALSE) {

  $cid = __FUNCTION__ . ':' . $task->nid . ':' . (is_null($archived) ? 'NULL' : $archived ? 'TRUE' : 'FALSE');
  $cached = cache_get($cid, SONY_CMP_CACHE_BIN);
  if (FALSE && $cached) {
    return $cached->data;
  }
  $query = db_select('node', 'report');
  //tasks only
  $query->condition('report.type', sonycmp_reports_content_types(), 'IN');
  if (!is_null($archived)) {
    $query->condition('report.status', $archived ? NODE_NOT_PUBLISHED : NODE_PUBLISHED, '=');
  }
  //also fetch task field
  $query->join('field_data_field_task', 'field_task', 'field_task.entity_id = report.nid');
  $query->condition('field_task.field_task_target_id', $task->nid, '=');

  //also fetch status
  $query->join('field_data_field_status', 'field_status', 'field_status.entity_id = report.nid');
  //group by status and report type
  $query->groupBy('field_status.field_status_value')
    ->groupBy('report.type');
  //add required fields
  $query->addField('field_status', 'field_status_value', 'status');
  $query->addExpression('COUNT(*)', 'cnt');

  $result = array(
    CMP_REPORTS_REPORT_STATUS_OPEN => 0,
    CMP_REPORTS_REPORT_STATUS_ON_TIME => 0,
    CMP_REPORTS_REPORT_STATUS_LATE => 0,
    CMP_REPORTS_REPORT_STATUS_MISSING => 0
  );
  $stats = $query->execute()->fetchAllKeyed();

  foreach ($stats as $status => $count) {
    $result[$status] = (int) $count;
  }

  cache_set($cid, $result, SONY_CMP_CACHE_BIN, CACHE_TEMPORARY);
  return $result;
}