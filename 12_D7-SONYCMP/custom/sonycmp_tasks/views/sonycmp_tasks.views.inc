<?php

/**
 * Implements hook_views_data().
 */
function sonycmp_tasks_views_data() {
  $data = array();
  $data['node']['task_reports_open'] = array(
    'title' => t('Task: Open reports count'),
    'help' => t('Task`s Open reports counter.'),
    'field' => array(
      'handler' => 'sonycmp_tasks_handler_field_reports_open',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['node']['task_reports_ontime'] = array(
    'title' => t('Task: On-time reports count'),
    'help' => t('Task`s on time counter.'),
    'field' => array(
      'handler' => 'sonycmp_tasks_handler_field_reports_ontime',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['node']['task_reports_late'] = array(
    'title' => t('Task: Late reports count'),
    'help' => t('Task`s late counter.'),
    'field' => array(
      'handler' => 'sonycmp_tasks_handler_field_reports_late',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['node']['task_reports_missing'] = array(
    'title' => t('Task: Missing reports count'),
    'help' => t('Task`s missing counter.'),
    'field' => array(
      'handler' => 'sonycmp_tasks_handler_field_reports_missing',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['node']['task_reports_completed'] = array(
    'title' => t('Task: Completed reports count'),
    'help' => t('Task`s completed counter. On-time + Late reports.'),
    'field' => array(
      'handler' => 'sonycmp_tasks_handler_field_reports_completed',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  return $data;
}