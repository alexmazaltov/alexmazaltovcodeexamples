<?php

abstract class sonycmp_tasks_handler_field_reports_field_status_filter_count extends views_handler_field  {
  protected $status_value = -1;
  public $field_alias = 'status_name';
  protected static $field_task_alias = FALSE;

  function query() {
    $this->ensure_my_table();

    if (!self::$field_task_alias) {

      $field_task_join = new views_join();

      $field_task_join->definition = array(
        'left_table' => $this->table,
        'left_field' => 'nid',
        'table' => 'field_data_field_task',
        'field' => 'field_task_target_id',
        'type' => 'LEFT',
        'extra' => array(
          array('field' => 'entity_type', 'value' => 'node'),
          array(
            'field' => 'bundle',
            'value' => sonycmp_reports_content_types()
          ),
          array('field' => 'deleted', 'formula' => ' 0 ')
        ),
        'extra_type' => 'AND',
      );

      $field_task_join->construct();
      $field_task_join->adjusted = TRUE;
      self::$field_task_alias = $this->query->add_relationship('referenced_as_task', $field_task_join, $this->table_alias, $this->relationship);
    }

    $field_status_join = new views_join();
    if ($this->status_value === 0) {
      $extra_key = 'formula';
      $extra_value = ' 0 '; //not empty
    } else {
      $extra_key = 'value';
      $extra_value = $this->status_value;
    }
    $field_status_join->definition = array(
      'left_table' => self::$field_task_alias,
      'left_field' => 'entity_id',
      'table' => 'field_data_field_status',
      'field' => 'entity_id',
      'type' => 'LEFT',
      'extra' => array(
        array('field' => 'deleted', 'formula' => ' 0 '),
        array('field' => 'entity_type', 'value' => 'node'),
        array('field' => 'field_status_value', $extra_key => $extra_value),
      ),
    );
    $field_status_join->construct();
    $field_status_join->adjusted = TRUE;
    $field_status_alias = $this->query->add_relationship('field_status_' . $this->field_alias, $field_status_join, self::$field_task_alias, self::$field_task_alias);
    $this->field_alias = $this->query->add_field($field_status_alias, "entity_id", $this->field_alias, array('count' => TRUE));
    $this->add_additional_fields();
  }
}