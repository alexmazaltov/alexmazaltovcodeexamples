<?php

class sonycmp_tasks_handler_field_reports_completed extends sonycmp_tasks_handler_field_reports_field_status_filter_count {
  protected $status_value = array(CMP_REPORTS_REPORT_STATUS_ON_TIME, CMP_REPORTS_REPORT_STATUS_LATE);
  public $field_alias = 'completed';
}