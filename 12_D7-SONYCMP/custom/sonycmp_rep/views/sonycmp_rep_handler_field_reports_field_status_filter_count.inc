<?php

abstract class sonycmp_rep_handler_field_reports_field_status_filter_count extends views_handler_field_numeric {
  protected $status_value = -1;
  protected static $node_alias = FALSE;
  public $field_alias = 'status_name';


  function query() {
    $this->ensure_my_table();

    if (!self::$node_alias) {

      $node_join = new views_join();

      $node_join->definition = array(
        'left_table' => $this->table_alias,
        'left_field' => 'uid',
        'table' => 'node',
        'field' => 'uid',
        'type' => 'LEFT',
        'extra' => array(
          array(
            'field' => 'type',
            'value' => sonycmp_reports_content_types()
          )
        ),
        'extra_type' => 'AND',
      );

      $node_join->construct();
      $node_join->adjusted = TRUE;
      self::$node_alias = $this->query->add_relationship('node_users_reports', $node_join, $this->table_alias, $this->relationship);
    }
    $field_status_join = new views_join();
    if ($this->status_value === 0) {
      $extra_key = 'formula';
      $extra_value = ' 0 '; //not empty
    } else {
      $extra_key = 'value';
      $extra_value = $this->status_value;
    }
    $field_status_join->definition = array(
      'left_table' => self::$node_alias,
      'left_field' => 'nid',
      'table' => 'field_data_field_status',
      'field' => 'entity_id',
      'type' => 'LEFT',
      'extra' => array(
        array('field' => 'deleted', 'formula' => ' 0 '),
        array('field' => 'entity_type', 'value' => 'node'),
        array('field' => 'field_status_value', $extra_key => $extra_value),
      ),
    );
    $field_status_join->construct();
    $field_status_join->adjusted = TRUE;
    $field_status_alias = $this->query->add_relationship('field_status_' . $this->field_alias, $field_status_join, self::$node_alias, self::$node_alias);
    $this->field_alias = $this->query->add_field($field_status_alias, "entity_id", $this->field_alias, array('count' => TRUE));
    $this->add_additional_fields();
  }
}