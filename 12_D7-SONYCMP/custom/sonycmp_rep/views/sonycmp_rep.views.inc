<?php

/**
 * Implements hook_views_data().
 */
function sonycmp_rep_views_data() {
  $data = array();
  $data['users']['reports_open'] = array(
    'title' => t('Reports Open'),
    'help' => t('User`s Open reports counter.'),
    'field' => array(
      'handler' => 'sonycmp_rep_handler_field_reports_open',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['users']['reports_ontime'] = array(
    'title' => t('Reports On Time'),
    'help' => t('User`s on time counter.'),
    'field' => array(
      'handler' => 'sonycmp_rep_handler_field_reports_ontime',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['users']['reports_late'] = array(
    'title' => t('Reports Late'),
    'help' => t('User`s late counter.'),
    'field' => array(
      'handler' => 'sonycmp_rep_handler_field_reports_late',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['users']['reports_missing'] = array(
    'title' => t('Reports Missing'),
    'help' => t('User`s missing counter.'),
    'field' => array(
      'handler' => 'sonycmp_rep_handler_field_reports_missing',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['users']['reports_completed'] = array(
    'title' => t('Reports Completed'),
    'help' => t('User`s completed counter.'),
    'field' => array(
      'handler' => 'sonycmp_rep_handler_field_reports_completed',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}
