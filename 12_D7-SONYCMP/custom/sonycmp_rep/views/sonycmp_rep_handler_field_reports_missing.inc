<?php
class sonycmp_rep_handler_field_reports_missing extends sonycmp_rep_handler_field_reports_field_status_filter_count {
  protected $status_value = CMP_REPORTS_REPORT_STATUS_MISSING;
  public $field_alias = 'missing';
}