<?php
class sonycmp_rep_handler_field_reports_open extends sonycmp_rep_handler_field_reports_field_status_filter_count {
  protected $status_value = CMP_REPORTS_REPORT_STATUS_OPEN;
  public $field_alias = 'open';
}