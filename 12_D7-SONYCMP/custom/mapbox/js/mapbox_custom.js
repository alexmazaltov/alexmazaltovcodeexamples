(function ($, Drupal) {

    Drupal.behaviors.mapboxCustom = {
        attach: function (context, settings) {
            if ($("#map").hasClass("leaflet-container")) return;
            L.mapbox.accessToken = Drupal.settings.mapbox_access_token;
            var map = L.mapbox.map('map', 'mapbox.streets')
                .setView([38.957813, -95.247372], 4);
            map.scrollWheelZoom.disable();
            var points = Drupal.settings.mapbox_points;
            var geoJson = points.map(function (item) {
                return {
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": [item.lon, item.lat]
                    },
                    "properties": {
                        "title": item.title,
                        "description": item.content,
                        "divIcon": {
                            "html": item.marker || "&nbsp;",
                            "iconSize": [50, 50],
                            "iconAnchor": [26, 60],
                            "popupAnchor": [0, -65],
                            "className": "dot"
                        }
                    }
                };
            });

            var myLayer = L.mapbox.featureLayer().addTo(map);

            // Set a custom icon on each marker based on feature properties.
            myLayer.on('layeradd', function (e) {
                var marker = e.layer,
                    feature = marker.feature;
                marker.setIcon(L.divIcon(feature.properties.divIcon));
                marker.bindPopup(feature.properties.description);
            });

            // Add features to the map.
            myLayer.setGeoJSON(geoJson);
        }
    };

})(jQuery, Drupal);
