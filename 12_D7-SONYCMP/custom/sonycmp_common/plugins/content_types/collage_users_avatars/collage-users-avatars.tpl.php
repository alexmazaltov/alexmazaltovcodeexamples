<div class="<?php print $classes; ?>">
  <?php if ($avatars) : ?>
    <div class="collage-users-avatar-row--restyle">
      <?php foreach (array_slice($avatars_chunks, 0, count($avatars_chunks) / 2) as $chunk): ?>
        <div class="collage-users-avatar-pattern--restyle">
          <div class="avatar-big--restyle">
            <?php print theme('image_style', array(
                'style_name' => SONY_CMP_USERS_COLLAGE_BIG_AVATAR,
                'path' => $chunk[2]->uri,
                'width' => $chunk[2]->metadata['width'],
                'height' => $chunk[2]->metadata['height'],
              )
            ); ?>
          </div>
          <div class="avatar-small--restyle">
            <?php print theme('image_style', array(
                'style_name' => SONY_CMP_USERS_COLLAGE_SMALL_AVATAR,
                'path' => $chunk[0]->uri,
                'width' => $chunk[0]->metadata['width'],
                'height' => $chunk[0]->metadata['height'],
              )
            ); ?>
            <?php print theme('image_style', array(
                'style_name' => SONY_CMP_USERS_COLLAGE_SMALL_AVATAR,
                'path' => $chunk[1]->uri,
                'width' => $chunk[1]->metadata['width'],
                'height' => $chunk[1]->metadata['height'],
              )
            ); ?>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
    <div class="collage-users-avatar-row--restyle">
      <?php foreach (array_slice($avatars_chunks, count($avatars_chunks) / 2) as $chunk): ?>
        <div class="collage-users-avatar-pattern--restyle">
          <div class="avatar-small--restyle">
            <?php print theme('image_style', array(
                'style_name' => SONY_CMP_USERS_COLLAGE_SMALL_AVATAR,
                'path' => $chunk[0]->uri,
                'width' => $chunk[0]->metadata['width'],
                'height' => $chunk[0]->metadata['height'],
              )
            ); ?>
            <?php print theme('image_style', array(
                'style_name' => SONY_CMP_USERS_COLLAGE_SMALL_AVATAR,
                'path' => $chunk[1]->uri,
                'width' => $chunk[1]->metadata['width'],
                'height' => $chunk[1]->metadata['height'],
              )
            ); ?>
          </div>
          <div class="avatar-big--restyle">
            <?php print theme('image_style', array(
                'style_name' => SONY_CMP_USERS_COLLAGE_BIG_AVATAR,
                'path' => $chunk[2]->uri,
                'width' => $chunk[2]->metadata['width'],
                'height' => $chunk[2]->metadata['height'],
              )
            ); ?>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>
