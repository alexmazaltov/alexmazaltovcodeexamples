<?php

define('SONY_CMP_COLLAGE_USERS_AVATARS_NUMBER', 48);

/**
 * @file
 * Collage with reps avatars
 */

$plugin = array(
  'title' => t('Collage with user`s avatars'),
  'description' => t('Will show collage from user`s avatars.'),
  'content_types' => 'sonycmp_common_collage_users_avatars',
  'single' => TRUE,
  'render callback' => 'sonycmp_common_collage_users_avatars_render',
  'edit form' => 'sonycmp_common_collage_users_avatars_edit_form',
  'category' => t('SonyCMP'),
  'hook theme' => 'sonycmp_common_collage_users_avatars_theme',
);

/**
 * Implements render callback for this content type.
 */
function sonycmp_common_collage_users_avatars_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->content = theme('sonycmp_collage_users_avatars', array(
    'title' => '',
    'avatars' => sonycmp_collage_get_random_users_avatars(SONY_CMP_COLLAGE_USERS_AVATARS_NUMBER)
  ));
  return $block;
}

/**
 * Implements the edit settings form for this content type.
 */
function sonycmp_common_collage_users_avatars_edit_form($form, &$form_state) {
  return $form;
}

function sonycmp_common_collage_users_avatars_preprocess(&$variables) {
  $avatars = $variables['avatars'];
  if (!$avatars) {
    $variables['class'] .= ' collage-users-avatar-empty--restyle';
  }
  else {
    $prepared = $avatars;
    while (count($prepared) < SONY_CMP_COLLAGE_USERS_AVATARS_NUMBER) {
      shuffle($avatars);
      $prepared = array_merge($prepared, $avatars);
    }
    $prepared = array_slice($prepared, 0, SONY_CMP_COLLAGE_USERS_AVATARS_NUMBER);
    //lets create chunks
    $chunks = array_chunk($prepared, 3);
    //lets order images inside chunks by sizes
    foreach ($chunks as & $chunk) {
      usort($chunk, '_sonycmp_common_collage_users_avatars_sort_chunk');
    }
    $variables['avatars_chunks'] = $chunks;

  }
}

function _sonycmp_common_collage_users_avatars_sort_chunk($a, $b) {
  return $a->width > $b->width ? 1 : -1;
}

/**
 * Delegated implementation of hook_theme().
 */
function sonycmp_common_collage_users_avatars_theme(&$theme, $plugin) {
  $theme['sonycmp_collage_users_avatars'] = array(
    'variables' => array(
      'avatars' => NULL,
      'classes_array' => array('collage-users-avatar--restyle'),
      'attributes_array' => array(),
      'title_attributes_array' => array(),
      'content_attributes_array' => array()
    ),
    'template' => 'collage-users-avatars',
    'path' => $plugin['path'],
    'preprocess functions' => array(
      'sonycmp_common_collage_users_avatars_preprocess'
    )
  );
}
