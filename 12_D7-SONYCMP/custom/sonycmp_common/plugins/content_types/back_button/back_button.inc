<?php

/**
 * @file
 * Back button for different pages.
 */

$plugin = array(
  'title' => t('Sonycmp Back Button'),
  'description' => t('Adds a pane with a back button.'),
  'content_types' => 'sonycmp_common_back_button',
  'single' => TRUE,
  'render callback' => 'sonycmp_common_back_button_render',
  'edit form' => 'sonycmp_common_back_button_edit_form',
  'category' => t('SonyCMP'),
  'hook theme' => 'sonycmp_common_back_button_theme',
  'required context' => array(
    0 => new ctools_context_optional(t('Node'), 'node'),
  ),
);

/**
 * Implements render callback for this content type.
 */
function sonycmp_common_back_button_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->content = '';
  $args = arg();
  $url = '';
  $title = '';

  switch ($args[0]) {
    case 'reporting':
      if ($args[2] == 'edit') {
        switch ($args[3]) {
          case 'task':
            $title = t('Back to Reporting Home');
            $url = 'reporting';
            break;

          case 'content':
            if (!empty($context[0]->data) && $context[0]->data->status != NODE_PUBLISHED) {
              $title = t('Back to Select the Task for Report');
              $url = 'reporting/' . $context[0]->data->nid . '/edit/task';
            }
            break;

          case 'finalize':
            if (!empty($context[0]->data)) {
              $title = t('Back to Select Content');
              $url = 'reporting/' . $context[0]->data->nid . '/edit/content';
            }
            break;
        }
      }
      break;
  }

  if (!empty($url) && !empty($title)) {
    $title = '&lt; ' . $title;
    $link = l($title, $url, array('html' => TRUE));
    $block->content = theme('sonycmp_page_back_button', array('link' => $link));
  }

  return $block;
}

/**
 * Implements the edit settings form for this content type.
 */
function sonycmp_common_back_button_edit_form($form, &$form_state) {
  return $form;
}

/**
 * Delegated implementation of hook_theme().
 */
function sonycmp_common_back_button_theme(&$theme, $plugin) {
  $theme['sonycmp_page_back_button'] = array(
    'variables' => array(
      'link' => NULL,
    ),
    'template' => 'back-button',
    'path' => $plugin['path'],
  );
}
