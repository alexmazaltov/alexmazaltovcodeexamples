<?php
/**
 * @file
 * Template for a back button on the page.
 */
?>
<?php if (!empty($link)): ?>
  <div class="back-link"> <?php print $link; ?></div>
<?php endif; ?>
