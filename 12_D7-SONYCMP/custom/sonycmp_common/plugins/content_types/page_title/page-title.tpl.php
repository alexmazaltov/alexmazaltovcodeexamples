<?php
/**
 * @file
 * Template for a page title on the page.
 */
?>
<?php if (!empty($title)): ?>
  <div class="page-title">
    <h2><?php print $title; ?></h2>
  </div>
<?php endif; ?>
