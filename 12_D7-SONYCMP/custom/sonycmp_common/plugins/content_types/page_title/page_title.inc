<?php

/**
 * @file
 * Back button for different pages.
 */

$plugin = array(
  'title' => t('Sonycmp Page Title'),
  'description' => t('Adds a pane page title.'),
  'content_types' => 'sonycmp_common_page_title',
  'single' => TRUE,
  'render callback' => 'sonycmp_common_page_title_render',
  'edit form' => 'sonycmp_common_page_title_edit_form',
  'category' => t('SonyCMP'),
  'hook theme' => 'sonycmp_common_page_title_theme',
  'required context' => array(
    0 => new ctools_context_optional(t('Node'), 'node'),
  )
);

/**
 * Implements render callback for this content type.
 */
function sonycmp_common_page_title_render($subtype, $conf, $panel_args, $context) {
  $title = '';
  $args = arg();
  switch ($args[0]) {
    case 'reporting':
      $report = !empty($context[0]->data) ? $context[0]->data : NULL;
      if ($args[2] == 'edit') {
        switch ($args[3]) {
          case 'task':
            $title = t('Select the Task for your Report:');
            break;
          
          case 'content':
          case 'finalize':
            $task = '';
            if ($report) {
              $wrapper = entity_metadata_wrapper('node', $report);
              $task = $wrapper->field_task->title->value();
            }
            $title = t('Creating report for: "@task"', array('@task' => $task));
            break;
        }
      }
      break;
  }
  $block = new stdClass();
  $block->content = theme('sonycmp_page_title', array(
    'title' => $title,
  ));

  return $block;
}

/**
 * Implements the edit settings form for this content type.
 */
function sonycmp_common_page_title_edit_form($form, &$form_state) {
  return $form;
}

/**
 * Delegated implementation of hook_theme().
 */
function sonycmp_common_page_title_theme(&$theme, $plugin) {
  $theme['sonycmp_page_title'] = array(
    'variables' => array(
      'title' => NULL,
      'link' => NULL,
    ),
    'template' => 'page-title',
    'path' => $plugin['path'],
  );
}
