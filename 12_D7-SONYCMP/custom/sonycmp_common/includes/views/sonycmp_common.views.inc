<?php

/**
 * @file
 * Provide views data and handlers.
 */

/**
 * Implements hook_views_data().
 */
function sonycmp_common_views_data() {
  $data['node']['node_archive_link'] = array(
    'field' => array(
      'title' => t('Sony CMP node archive link'),
      'help' => t('Provide a node archive link.'),
      'handler' => 'sonycmp_common_views_handler_field_node_archive_link',
    )
  );

  $data['node']['node_delete_link'] = array(
    'field' => array(
      'title' => t('Sony CMP node delete link'),
      'help' => t('Provide a node delete link.'),
      'handler' => 'sonycmp_common_views_handler_field_node_delete_link',
    )
  );


  $data['users']['first_and_last_name'] = array(
    'title' => t('First + Last name'),
    'help' => t('Provide field which contain names separated by space'),
    'field' => array(
      'handler' => 'sonycmp_common_views_handler_field_first_and_last_name',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'sonycmp_common_views_handler_field_first_and_last_name_sort',
    ),
  );
  return $data;
}
