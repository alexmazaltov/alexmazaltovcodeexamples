<?php

/**
 * @file
 * Views field handler for node delete link.
 */

/**
 * Field handler to present a node archive link.
 *
 * @ingroup views_field_handlers
 */
class sonycmp_common_views_handler_field_node_delete_link extends views_handler_field_node {

  function render($values) {
    return array(
      '#theme' => 'link',
      '#text' => t('Delete'),
      '#path' => 'js/node/' . $values->nid . '/delete',
      '#options' => array(
        'attributes' => array('class' => array('icon-delete ctools-modal-confirmation-modal-dialog-style use-ajax ctools-use-modal ctools-use-modal-processed cmp-blurer-processed')),
        'html' => FALSE,
      ),
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }
}
