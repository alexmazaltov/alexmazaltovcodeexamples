<?php

class sonycmp_common_views_handler_field_first_and_last_name extends views_handler_field {
  /** @var  views_plugin_query_default */
  var $query;
  function query() {
    $table = $this->ensure_my_table();

    $field_name = $table .'_first_and_last_name';
    if (!isset($this->query->fields[$field_name])) {
      $first_name_join_table_alias = $table . '_first_name';
      $first_name_join_table_relationship = $table . '_first_name_relationship';
      if (isset($this->query->relationships[$first_name_join_table_relationship])) {
        $first_name_join_table_alias = $this->query->table_queue[$first_name_join_table_relationship]['alias'];
      }
      else {
        $first_name_join = new views_join();
        $first_name_join->definition = array(
          'left_table' => $table,
          'left_field' => 'uid',
          'table' => 'field_data_field_first_name',
          'field' => 'entity_id',
          'extra' => array(
            array('field' => 'entity_type', 'value' => 'user'),
            array('field' => 'bundle', 'value' => 'user'),
            array('field' => 'deleted', 'formula' => ' 0 ')
          )
        );
        $first_name_join->construct();
        $first_name_join_table_alias = $this->query->add_relationship($first_name_join_table_relationship, $first_name_join, $table);
      }

      $last_name_join_table_alias = $table . '_last_name';
      $last_name_join_table_relationship = $table . '_last_name_relationship';
      if (isset($this->query->relationships[$last_name_join_table_relationship])) {
        $last_name_join_table_alias = $this->query->table_queue[$last_name_join_table_relationship]['alias'];
      }
      else {
        $last_name_join = new views_join();
        $last_name_join->definition = array(
          'left_table' => $table,
          'left_field' => 'uid',
          'table' => 'field_data_field_last_name',
          'field' => 'entity_id',
          'extra' => array(
            array('field' => 'entity_type', 'value' => 'user'),
            array('field' => 'bundle', 'value' => 'user'),
            array('field' => 'deleted', 'formula' => ' 0 ')
          )
        );
        $last_name_join->construct();
        $last_name_join_table_alias = $this->query->add_relationship($last_name_join_table_relationship, $last_name_join, $table);
      }
      $field_alias = $this->query->add_field(NULL,     "concat_ws(' ', "
        . $first_name_join_table_alias . '.field_first_name_value, '
        . $last_name_join_table_alias . '.field_last_name_value)',
        $field_name);
    } else {
      $field_alias = $this->query->fields[$field_name]['alias'];
    }
    $this->field_alias = $field_alias;

  }

}