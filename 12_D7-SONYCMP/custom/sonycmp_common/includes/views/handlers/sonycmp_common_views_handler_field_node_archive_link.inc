<?php

/**
 * @file
 * Views field handler for node archive link.
 */

/**
 * Field handler to present a node archive link.
 *
 * @ingroup views_field_handlers
 */
class sonycmp_common_views_handler_field_node_archive_link extends views_handler_field_node {

  function render($values) {
    $classes = array(
      'ctools-modal-confirmation-modal-dialog-style',
      'use-ajax',
      'ctools-use-modal',
      'ctools-use-modal-processed',
      'cmp-blurer-processed',
    );
    $icon_class = 'icon-archive';
    $status = isset($values->node_status) ? (boolean)$values->node_status : TRUE;
    if (!$status) {
      $icon_class = 'icon-unarchive';
    }
    $classes[] = $icon_class;

    return array(
      '#theme' => 'link',
      '#text' => t('Archive'),
      '#path' => 'js/node/' . $values->nid . '/archive/nojs',
      '#options' => array(
        'attributes' => array(
          'class' => $classes,
        ),
        'html' => FALSE,
      ),
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }
}
