<?php

/**
 * This file contain different hooks called when content/users/terms modified
 */

/**
 * Implements hook_views_invalidate_cache().
 */
function sonycmp_common_views_invalidate_cache() {
  _sonycmp_clear_invalidate_internal_cache();
}


/**
 * Implements hook_entity_insert().
 */
function sonycmp_common_entity_insert($entity, $type) {
  _sonycmp_invalidate_cache_on_entity_changes($entity, $type);
}

/**
 * Implements hook_entity_update().
 */
function sonycmp_common_entity_update($entity, $type) {
  _sonycmp_invalidate_cache_on_entity_changes($entity, $type);
}

/**
 * Implements hook_entity_delete().
 */
function sonycmp_common_entity_delete($entity, $type) {
  _sonycmp_invalidate_cache_on_entity_changes($entity, $type);
}

function _sonycmp_invalidate_cache_on_entity_changes($entity, $type) {
  _sonycmp_clear_invalidate_internal_cache();
  if ($type == 'user') {
    sonycmp_collage_get_random_users_avatars(100, TRUE);
  }
}

/**
 * Clear views data cache
 */
function _sonycmp_clear_invalidate_internal_cache() {
  cache_clear_all('*', 'cache_views_data', TRUE);
  cache_clear_all('*', SONY_CMP_CACHE_BIN, TRUE);
}

