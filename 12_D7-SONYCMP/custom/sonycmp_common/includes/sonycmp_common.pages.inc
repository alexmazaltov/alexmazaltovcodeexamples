<?php

/**
 * @file
 * Page callbacks
 */

/**
 * Form builder for AJAX to archive node.
 */
function sonycmp_common_archive_node($form, &$form_state, $node) {
  $form_state['storage']['node'] = $node;
  $action = !$node->status ? t('Unarchive') : t('Archive');
  $confirm_form = confirm_form($form,
    t('Are you sure you want to !action?', array('!action' => $action)),
    'dashboard',
    theme('cmp_confirm_description', array(
      'action' => $action,
      'title' => $node->title,
    )),
    t('Yes'),
    t('Cancel')
  );
  $confirm_form['actions']['submit']['#ajax'] = array(
    'callback' => 'sonycmp_common_archive_node_ajax_submit',
  );

  return $confirm_form;
}

/**
 * Submit callback for 'sonycmp_common_archive_node' form.
 */
function sonycmp_common_archive_node_ajax_submit($form, &$form_state) {
  $node = $form_state['storage']['node'];
  $action = !$node->status ? t('Unarchived') : t('Archived');
  $node->status = !$node->status;
  node_save($node);
  ctools_include('modal');
  ctools_include('ajax');

  $type = _sonycmp_common_get_node_type_human_name($node);

  drupal_set_message(t('@type !task_title was successfully !action', array(
    '@type' => $type,
    '!task_title' => $node->title,
    '!action' => $action,
  )));

  $commands[] = ctools_modal_command_dismiss();
  $commands[] = ctools_ajax_command_reload();
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Form builder for AJAX delete node.
 */
function sonycmp_common_delete_node($form, &$form_state, $node) {
  $form_state['storage']['node'] = $node;
  $confirm_form = confirm_form($form,
    t('Are you sure you want to delete?'),
    'tasks',
    theme('cmp_confirm_description', array(
      'action' => 'delete',
      'title' => $node->title,
    )),
    t('Yes'),
    t('Cancel')
  );
  $confirm_form['actions']['submit']['#ajax'] = array(
    'callback' => 'sonycmp_common_delete_node_ajax_submit',
  );

  return $confirm_form;
}

/**
 * Submit callback for 'sonycmp_common_delete_node' form.
 */
function sonycmp_common_delete_node_ajax_submit($form, &$form_state) {
  $node = $form_state['storage']['node'];
  ctools_include('modal');
  ctools_include('ajax');

  switch ($node->type) {
    case SONY_CMP_CT_BI_WEEKLY_TASK:
    case SONY_CMP_CT_MISC_TASK:
    case SONY_CMP_CT_TOUR_TASK:
      if (cmp_tasks_task_can_be_deleted($node->nid)) {
        node_delete($node->nid);
        drupal_set_message(t('Task !title was Deleted', array('!title' => $node->title)));
      }
      else {
        $message = t("Task !title can't be Deleted because it is the basis of the Final Report", array('!title' => $node->title));
        drupal_set_message($message);
      }
      break;

    case SONY_CMP_CT_TUTORIAL:
      node_delete($node->nid);
      $tutorial_types = array(t('Example'), t('Tutorial'));
      $options = array(
        '!tutorial_type' => $tutorial_types[$node->field_tutorial_type[LANGUAGE_NONE][0]['value']],
        '!tutorial_title' => $node->title,
      );
      drupal_set_message(t('!tutorial_type !tutorial_title was Deleted', $options));
      break;

    default:
      $type = _sonycmp_common_get_node_type_human_name($node);
      node_delete($node->nid);
      drupal_set_message(t('@type @title was Deleted', array(
        '@title' => $node->title,
        '@type' => $type,
      )));
      break;
  }

  $commands[] = ctools_modal_command_dismiss();
  $commands[] = ctools_ajax_command_reload();
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Get human name for node type.
 *
 * @param $node
 *   Node object
 * @return string
 *   Human name
 */
function _sonycmp_common_get_node_type_human_name($node) {
  $map = array(
    SONY_CMP_CT_TOUR_REPORT => t('Report'),
    SONY_CMP_CT_MISC_REPORT => t('Report'),
    SONY_CMP_CT_BI_WEEKLY_REPORT => t('Report'),
    SONY_CMP_CT_TOURS => t('Tour'),
    SONY_CMP_CT_GROUP => t('Group'),
  );

  return isset($map[$node->type]) ? $map[$node->type] : '';
}

function sonycmp_common_rebuild_statuses() {
  $batch = array(
    'operations' => array(
      array('_sonycmp_common_rebuild_statuses_prepare', array()),
      array('_sonycmp_common_rebuild_statuses_process', array()),
      array('_sonycmp_common_rebuild_statuses_finish', array()),
    ),
    'finished' => 'sonycmp_common_rebuild_statuses_finish',
    'title' => t('Rebuild ann Reports and Tasks statuses'),
    'file' => drupal_get_path('module', 'sonycmp_common') . '/includes/sonycmp_common.pages.inc',
  );
  batch_set($batch);
  batch_process('admin');
}

function _sonycmp_common_rebuild_statuses_prepare(&$context) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', sonycmp_tasks_content_types(), 'IN');
  $result = $query->execute();
  $context['results']['nids'] = array();
  if (isset($result['node'])) {
    $context['results']['nids'] = array_keys($result['node']);
  }
  $context['results']['task status changed'] = array();
  $context['results']['report status changed'] = array();
}

function _sonycmp_common_rebuild_statuses_process(&$context) {
  if (empty($context['sandbox']['processed'])) {
    $context['sandbox']['processed'] = 0;
  }
  $nids = array_slice($context['results']['nids'], $context['sandbox']['processed'], 10);
  if ($nids) {
    cmp_messages_enabled(FALSE);
    $nodes = node_load_multiple($nids);
    foreach ($nodes as $node) {
      node_save($node);
    }
    cmp_messages_enabled(TRUE);
  }
  foreach (array('task status changed', 'report status changed') as $event) {
    $context['results'][$event] += sonycmp_common_register_event(NULL, $event);
  }
  $context['sandbox']['processed'] += count($nids);
  $context['finished'] = $context['sandbox']['processed'] / count($context['results']['nids']);
  $context['message'] = t('Processed %processed nodes of %total', array(
    '%processed' => $context['sandbox']['processed'],
    '%total' => count($context['results']['nids'])
  ));
}

function _sonycmp_common_rebuild_statuses_finish($success) {
  if ($success) {
    $message = t('Rebuilded %count tasks and all related reports. <br/>Statuses changed: %tasks tasks, %reports reports', array(
      '%count' => count($success['results']['nids']),
      '%reports' => count($success['results']['report status changed']),
      '%tasks' => count($success['results']['task status changed'])
    ));
    drupal_set_message($message, 'error');
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    drupal_set_message('An error occured', 'error');
  }
}

