<?php

/**
 * @file
 * Theme implementations
 */

/**
 * Default implementation of theme 'cmp_confirm_description.
 */
function theme_cmp_confirm_description($vars) {
  $output = '';
  $output .= t('Are you sure you want to !action', array(
    '!action' => $vars['action'],
  ));
  $output .= '<br />';
  $output .= '<strong>' . check_plain($vars['title']) . ' ?</strong>';
  return $output;
}
