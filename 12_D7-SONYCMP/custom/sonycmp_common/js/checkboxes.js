(function ($) {
    //dependent checkboxes
    Drupal.behaviors.sonycmp_checkboxes = {
        attach: function (context, settings) {
            $('input:checkbox[data-checkboxes-children]', context).once('sonycmp-checkboxes').each(function () {
                var $parent = $(this);
                var groups = $parent.data('checkboxes-children');
                var group;
                var onChangeGroups = [];

                for (group in groups) {
                    var behavior = groups[group];
                    var $children = $('input:checkbox[data-checkboxes-parents]').filter(function () {
                        var groups = $(this).data('checkboxes-parents');
                        return groups != undefined && groups.indexOf(group) != -1
                    });
                    $parent.on('change', function () {
                        if (onChangeGroups.indexOf(group) != -1) {
                            return;
                        }
                        onChangeGroups.push(group);
                        $children.prop('checked', $parent.prop('checked')).trigger('change');
                        onChangeGroups.splice(onChangeGroups.indexOf(group), 1);
                    })

                    $children.on('change', function () {
                        if (onChangeGroups.indexOf(group) != -1) {
                            return;
                        }
                        onChangeGroups.push(group);
                        var $checkbox = $(this);
                        if (behavior.indexOf('autocheck-parent') != -1) {
                            if ($checkbox.prop('checked') && $children.length == $children.filter(':checked').length) {
                                $parent.prop('checked', true).trigger('change');
                            }
                        }
                        if (behavior.indexOf('autouncheck-parent') != -1) {
                            if (!$checkbox.prop('checked')) {
                                $parent.prop('checked', false).trigger('change');
                            }
                        }
                        onChangeGroups.splice(onChangeGroups.indexOf(group), 1);
                    })
                }
            })
        }
    }
})(jQuery);