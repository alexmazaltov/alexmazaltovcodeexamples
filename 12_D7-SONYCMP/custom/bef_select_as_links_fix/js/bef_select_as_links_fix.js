(function ($, Drupal) {

  Drupal.behaviors.befSelectAsLinksFix = {

    attach: function (context, settings) {
      if (settings && settings.views && settings.views.ajaxViews) {
        $.each(settings.views.ajaxViews, function (i, ajax_settings) {
          var ajaxViewNameReplaced = ajax_settings.view_name.replace(/_/g, '-');
          //Fix active class after ajax request based on data selected attr from option
          $('.view-id-' + ajax_settings.view_name + ' option[selected="selected"]').each(function (index) {
            select = $(this).parent('select');
            $('a[data-name^="' + select.attr('name') + '"]').removeClass('active');
            $('a[data-name^="' + select.attr('name') + '"][data-value="' + $(this).attr('value') + '"]').addClass('active');

          });

          if ($('.view-id-' + ajax_settings.view_name + ' a[data-name]').length == 0) {
            return true;
          }

          $('.view-id-' + ajax_settings.view_name + ' a[data-name]').off('click').on('click', function (e) {

            e.preventDefault();
            e.stopPropagation();
            var el = $(this);
            //Make flow to work like client asked about filtering custom depending.
            //option checked for right hilighting of active class for link after ajax request
            $('.bef-select-as-links option').attr('selected', false);
            $('.bef-select-as-links option[value="All"]').attr('selected', 'selected');
            $('.bef-select-as-links option[value=""]').attr('selected', 'selected');

            //hidden value change to let ajax filtering works on submit
            $('.bef-select-as-links input[type="hidden"][class="bef-new-value"]').val('All');

            //Set value extra value for this input in all hidden inputs for get working better exposed blocks with suggestion of all filters
            $('input[name^="' + el.data('name') + '"]').val(el.data('value'));
            $('select[name^="' + el.data('name') + '"]').each(function (index) {
              var select = $(this);
              select.find('option').each(function (index) {
                if ($(this).attr('value') == el.data('value')) {
                  $(this).attr('selected', 'selected');
                } else {
                  $(this).attr('selected', false);
                }
              });
            });
            $('form[id^="views-exposed-form-' + ajaxViewNameReplaced + '"]').find('.views-submit-button *[type=submit]').click();

          });
        });
      }

    }
  };

})(jQuery, Drupal);
