<?php

/**
 * @file
 */

$plugin = array(
  'form'     => 'feeds_tamper_cat_non_mapped_form',
  'callback' => 'feeds_tamper_cat_non_mapped_callback',
  'name'     => 'Cat with non mapped colmn',
  'multi'    => 'direct',
  'category' => 'NXTE',
);

/**
 * Implements hook_form().
 * @param unknown $importer
 * @param unknown $element_key
 * @param unknown $settings
 * @return multitype:multitype:string Ambigous <string, unknown> The Ambigous <The, string, A, Optional>
 */
function feeds_tamper_cat_non_mapped_form($importer, $element_key, $settings) {
  $form = array();

  $form['clmn_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of non mapped colmn'),
    '#default_value' => isset($settings['clmn_name']) ? $settings['clmn_name'] : '',
    '#description' => t('This colmn form csv file will be added to current field<br>Be shure that this name of colmn exist in csv file'),
  );
  $form['separator'] = array(
    '#type' => 'textfield',
    '#title' => t('Separator'),
    '#default_value' => isset($settings['separator']) ? $settings['separator'] : '',
    '#description' => t('This text will be added beetwen current filed and concatinated colmn'),
  );
  return $form;
}

/**
 * @param unknown $source
 * @param unknown $item_key
 * @param unknown $element_key
 * @param unknown $field
 * @param unknown $settings
 */
function feeds_tamper_cat_non_mapped_callback($source, $item_key, $element_key, &$field, $settings) {
  $field = feeds_tamper_cat_non_mapped_value($settings['clmn_name'], $settings['separator'], $field, $source->items[$item_key]);
}

/**
 * @param unknown $code
 * @param unknown $field
 * @param unknown $item
 * @return unknown
 */
function feeds_tamper_cat_non_mapped_value($clmn_name, $separator, $field, $item) {
  if(isste($item[$clmn_name])){
    $field .= $separator . $item[$clmn_name];
  }
  elseif(isset($item[strtolower($clmn_name)])){
    $field .= $separator . $item[strtolower($clmn_name)];
  }
  else{
    $field .= $separator . ' _' . $clmn_name. '_ ' ;
  }
  return $field;
}
