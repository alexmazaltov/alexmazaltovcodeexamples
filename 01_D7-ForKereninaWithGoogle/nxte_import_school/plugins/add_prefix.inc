<?php

/**
 * @file
 * Remove text or whitespace from the beginning, middle, or both sides of text.
 */

$plugin = array(
  'form' => 'feeds_tamper_add_prefix_custom_form',
  'callback' => 'feeds_tamper_add_prefix_custom_callback',
  'name' => 'Add a prefix with checking',
  'multi' => 'loop',
  'category' => 'NXTE',
);

function feeds_tamper_add_prefix_custom_form($importer, $element_key, $settings) {
  $form = array();
  $form['prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Text to prefix'),
    '#default_value' => isset($settings['prefix']) ? $settings['prefix'] : '',
    '#description' => t('Text (e.g. path) to prefix the value'),
  );

  $form['cheking'] = array(
    '#type' => 'checkbox',
    '#title' => t('Chek if already item contained prefix'),
    '#default_value' =>  isset($settings['cheking']) ? $settings['cheking'] : FALSE,
    '#description' => t('Check if added prefix already is in item'),
  );
  $form['cheking_i'] = array(
    '#type' => 'checkbox',
    '#title' => t('Not case sensitive'),
    '#default_value' =>  isset($settings['cheking_i']) ? $settings['cheking_i'] : FALSE,
    '#description' => t('Check this if you need doing cheking of a case-insensitive mode'),
  );
  return $form;
}

function feeds_tamper_add_prefix_custom_callback($source, $item_key, $element_key, &$field, $settings) {

  if ($settings['cheking']){
    if($settings['cheking_i']){
      if(stripos($field, $settings['prefix']) == 0){
        $field = $field;
      }
      else{
        $field = $settings['prefix'] . $field;
      }
    }
    else{
      if(strpos($field, $settings['prefix']) == 0){
       $field = $field;
      }
      else{
        $field = $settings['prefix'] . $field;
      }
    }
  }
  else {
    $field = $settings['prefix'] . $field;
  }

}
