var map;
var myLatlng;
var markers = {};
var infowindows = {};
var emails = {};
var activeMarkerIndex;

(function($){

  $(document).ready(function() {
    //Clear searching input onpage load
    $('#edit-nxte-gmaps-search-input').val('');
    //Get starting zoom into new value of setted zoom
    Drupal.settings.nxte.gmap_setted_zoom = Drupal.settings.nxte.gmap_starting_zoom;
    //Hide webform afterdocument ready
    $('#block-webform-client-block-214').hide();
    //Unset values of hiden fileds in webform
    $('input[name="submitted[coordinator_email]"]').val('');
    $('input[name="submitted[school_nid]"]').val('');

    if ($(window).width() > 1200) {  $('a.show-webform').addClass('hide-text'); }


    if(typeof(Drupal.settings.nxte) == 'undefined') {
      return;
    }

    function getContentStringBySchoolNid(index, schoolNid){

      var ContetnStringBySchoolNid = '';
      //get markers to display via AJAX call
      $.ajax({
        url:   Drupal.settings.basePath + 'getcontentstring/'+schoolNid,
        success:  function(data){
    		  infowindows[index].content = data.contentstring;
    		  infowindows[index].open(map, markers[index]);
        },
      });
    }

    //Initialization google map
    google.maps.event.addDomListener(window, 'load', initialize);

    //initialize map on DOM Ready
    function initialize() {

      myLatlng = new google.maps.LatLng(Drupal.settings.nxte.gmap_center_flexible.lattitude, Drupal.settings.nxte.gmap_center_flexible.longitude);
      var mapOptions = {
        zoom: Drupal.settings.nxte.gmap_starting_zoom,
        center: myLatlng,
        scrollwheel: false,
        disableDoubleClickZoom: true,
        streetViewControl: true,
        panControl: true,
        zoomControl: true,
      };
      map = new google.maps.Map(document.getElementById('main-gmap'), mapOptions);

      //get markers to display via AJAX call
      $.ajax({
        url:   Drupal.settings.basePath + 'map/markers',
        success:  placeMarkers,
      });
    }

    //___________________________function placeMarkers()___________________________________
    /**
     * Place all markers on map with events
     */
    function placeMarkers (data) {

      // Loop for each school
      $.each(data, function(index, val) {
        //create marker for each school
        markers[index] = new google.maps.Marker({
          position: {lat:val.latitude*1, lng:val.longitude*1},
          map: map,
          icon: {url:Drupal.settings.pointIcon},
          title:val.school_name,
          school_nid: val.school_nid,
        });

        emails[index] = val.coordinator_email;
        //create infowindow for each school

        infowindows[index] =  new google.maps.InfoWindow({
          content: ' ',
        });

        //Create onclick handler for each marker and
        google.maps.event.addListener(markers[index], 'click', function(args) {
          activeMarkerIndex = index;
          Drupal.settings.nxte.gmap_setted_zoom = map.getZoom();
          //close all infowindows onclick marker
          $.each(infowindows, function(indexInfoWindow, el) {
             infowindows[indexInfoWindow].close();
          });
          //change all icon to small for all markers
          $.each(markers, function(indexMarker, marker) {
            markers[indexMarker].setIcon({url:Drupal.settings.pointIcon});
          });

          if(infowindows[index].content == ' '){
            getContentStringBySchoolNid(index, markers[index].school_nid);
          }
    		  else {
    			  infowindows[index].open(map, markers[index]);
    		  }

          //show new infowindow for clicked marker
          map.panTo(markers[index].getPosition());
          map.setZoom(16);
          map.panBy(1000 , 0);
          $('input[name="submitted[coordinator_email]"]').val(val.coordinator_email);
          $('input[name="submitted[school_nid]"]').val(val.school_nid);
          $('#block-webform-client-block-214').show();
          //change icon for active marker to large
          this.setIcon({url:Drupal.settings.pointActiveIcon});
          if ($(window).width() > 1200) {  $('a.show-webform').hide(); } else { $('a.show-webform').show(); }

          //change icon to small for active marker 'oncloseclick' infowindow for this marker and hide webform
          google.maps.event.addListener(infowindows[index], 'closeclick', function() {
            markers[index].setIcon({url:Drupal.settings.pointIcon});
            $('input[name="submitted[coordinator_email]"]').val('');
            $('input[name="submitted[school_nid]"]').val('');
            $('#block-webform-client-block-214').hide();
            map.setZoom(Drupal.settings.nxte.gmap_setted_zoom);
            $('#edit-nxte-gmaps-search-input').val('');
          });
        });

        //Close infowindow and change icon when onclick on map and change zoom to start_zoom
        google.maps.event.addListener(map, 'click', function() {
            infowindows[index].close();
            markers[index].setIcon({url:Drupal.settings.pointIcon});
            $('input[name="submitted[coordinator_email]"]').val('');
            $('input[name="submitted[school_nid]"]').val('');
            $('#block-webform-client-block-214').hide();
            map.setZoom(Drupal.settings.nxte.gmap_starting_zoom);
            $('#edit-nxte-gmaps-search-input').val('');
        });

      });

    }
    //___________________________End function placeMarkers()___________________________________


    $('#nxte-gmaps-form').bind('submit', function(event) {
      event.preventDefault();
        //Get some effect if searched school name was not founded second time zoomout to get see other markers on map
        if(map.getZoom() == 14){
          Drupal.settings.nxte.gmap_setted_zoom = 12;
        }
        else{
          Drupal.settings.nxte.gmap_setted_zoom = map.getZoom();
        }

        //Close all opened infoWindows
        $.each(infowindows, function(indexInfoWindow, el) {
          infowindows[indexInfoWindow].close();
        });
        //change all icon to small for all markers
        $.each(markers, function(indexMarker, marker) {
          markers[indexMarker].setIcon({url:Drupal.settings.pointIcon});
        });

        //get markers to display via AJAX call
        $.ajax({
          url:   Drupal.settings.basePath + 'getschoolbyname/'+$('#edit-nxte-gmaps-search-input').val(),
          success:  function(data){
            if( data.length != 0 ){
              $.each(data, function(index, val) {
                //set current map zoom as setted zoom for returning back in this zoom mode after close infoWindow
                // !but! not do it if some InfoWindow opened now and zoom() == 16
                if($('#block-webform-client-block-214').is(':hidden')){
                    Drupal.settings.nxte.gmap_setted_zoom = map.getZoom();
                }
                /* iterate through array or object */
                map.panTo({lat:val.latitude*1, lng:val.longitude*1});
                map.setZoom(16);

                //infowindows[index].open(map, markers[index]);
                if(infowindows[index].content == ' '){
                  getContentStringBySchoolNid(index, markers[index].school_nid);
                }
                else {
                  infowindows[index].open(map, markers[index]);
                }

                map.panBy(1000 , 0);
                activeMarkerIndex = index;

                $('input[name="submitted[coordinator_email]"]').val(val.coordinator_email);
                $('input[name="submitted[school_nid]"]').val(val.school_nid);
                $('#block-webform-client-block-214').show();
                //change icon for active marker to large
                markers[index].setIcon({url:Drupal.settings.pointActiveIcon});
                if ($(window).width() > 1200) {  $('a.show-webform').hide(); } else { $('a.show-webform').show(); }

                //change icon to small for active marker 'oncloseclick' infowindow for this marker and hide webform
                google.maps.event.addListener(infowindows[index], 'closeclick', function() {
                  markers[index].setIcon({url:Drupal.settings.pointIcon});
                  $('input[name="submitted[coordinator_email]"]').val('');
                  $('input[name="submitted[school_nid]"]').val('');
                  $('#block-webform-client-block-214').hide();
                  //set zoom to previouse setted zoom level
                  map.setZoom(Drupal.settings.nxte.gmap_setted_zoom);
                  //to clear search input value
                  $('#edit-nxte-gmaps-search-input').val('');
                });

                return false; //stop loop after open widow for one school that was opened from search box
              });
            }
            //If in search input was inputted not corrected value close info window and clear all data for email and hide contact form
            else{
              //and doing zoom out to 2 steeps to get it be more visula efeects
              if($('#block-webform-client-block-214').is(':visible')){
                map.setZoom(14);
              }
              else{
                map.setZoom(Drupal.settings.nxte.gmap_setted_zoom);
              }
              $('input[name="submitted[coordinator_email]"]').val('');
              $('input[name="submitted[school_nid]"]').val('');
              $('#block-webform-client-block-214').hide();
              //set zoom to previouse setted zoom level
              //to clear search input value
              $('#edit-nxte-gmaps-search-input').val('');
            }
          },
        });

    });

  });

})(jQuery);
z

(function ($) {
  Drupal.behaviors.nxte_gmaps = {
    attach: function (context, settings) {
      if(typeof(activeMarkerIndex) == 'undefined'){
        return;
      }
      if(typeof(emails[activeMarkerIndex]) == 'undefined'){
        return;
      }
      $('input[name="submitted[coordinator_email]"]').val(emails[activeMarkerIndex]);
    }
  };
})(jQuery);
