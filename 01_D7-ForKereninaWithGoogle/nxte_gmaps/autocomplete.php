<?php

/**
 * This is callback function for search engine in site
 * @param  string $string - string that was inputed in search textfield
 * @return json array with schools title
 */
function _nxte_gmaps_autocomplete($string = '') {
  $matches = array();

  if ($string) {

    $q = db_select('node', 'n');
    $q->innerJoin('field_data_field_location', 'fl', 'n.nid = fl.entity_id');
    $q->innerJoin('location', 'l', 'fl.field_location_lid = l.lid');
    $q->fields('n', array('nid', 'type', 'title'));
    $q->condition('n.type', 'schools');
    $q->condition('n.status', NODE_PUBLISHED);
    $q->condition(db_or()
      ->condition('l.postal_code', '%' . db_like($string) . '%', 'LIKE')
      ->condition('l.city', '%' . db_like($string) . '%', 'LIKE')
      ->condition('n.title', '%' . db_like($string) . '%', 'LIKE')
    );
      $q_or = db_or();
        $q_or->condition('l.latitude', 0,'>');
        $q_or->condition('l.longitude', 0,'>');
    $q->condition($q_or);

    $results = $q->execute();

    foreach ($results as $school) {
      $matches[$school->title] = check_plain($school->title);
    }
  }

  return $matches;
}
