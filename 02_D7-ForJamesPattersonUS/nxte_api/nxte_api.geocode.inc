<?php

/**
 * Helper function to call the Google API for determinig the location of an address
 *
 * @param $url
 *  the URL to call
 *
 * @return
 *  Google's response or '-1' if something went wrong
 */
function get_google_response($url) {
  $data = -1;
  $request = drupal_http_request($url);
  //catch error and log it
  if(isset($request->error) && $request->error ) {
    watchdog('google', $request->error, WATCHDOG_ERROR);
  }
  else {
    $data = $request->data;
  }
  return $data;
}

/**
 * Helper function to returns coordinates of city
 */
function get_city_location($city = null){
  if($city !== null && variable_get('nxte_api_try_google', 0)){
    $req = "http://maps.googleapis.com/maps/api/geocode/xml?address=" . $city ."&sensor=false";
    //do Google API request
    $google_response = get_google_response($req);
    if(!empty($google_response) && $google_response != -1){
      $xml = new SimpleXMLElement($google_response);
      if( $xml->status == 'OK' && !empty($xml->result->geometry->location->lat) ){
        $lat_lng = array(
                  'lat' => (string)$xml->result->geometry->location->lat,
                  'lng' => (string)$xml->result->geometry->location->lng,
                );
        return $lat_lng;
      }
    }
  }

  return array(
                  'lat' => '0',
                  'lng' => '0',
                );

}
