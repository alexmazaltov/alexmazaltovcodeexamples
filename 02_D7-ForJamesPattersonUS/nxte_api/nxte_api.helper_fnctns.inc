<?php

/**
 * Helper function doing search for existing uid depend on value in field fbid
 * @param  $fbid Facebook id in field fbid in user entity
 * @return $uid of existing user
 */
function _nxte_api_get_uid($fbid = null){

	$result = &drupal_static(__FUNCTION__ . '_' . $fbid);

	if(!isset($result)) {
		$query = db_query('SELECT u.uid  FROM {users} u WHERE u.name = :fbid', array(':fbid' => (String)$fbid));
		$uid = $query->fetchField();
		$result = empty($uid) ? false : $uid;
	}

	return $result;

}

function _nxte_api_get_current_page($uid){
	
	$result = &drupal_static(__FUNCTION__ . '_' . $uid);

	if(!isset($result)) {
		$query = db_query('SELECT p.page  FROM {nxte_api_pages} p WHERE p.userid = :uid ORDER BY p.page DESC LIMIT 1', array(':uid' => (int) $uid));
		$page = $query->fetchField();
		$result = empty($page) ? 0 : $page;
	}
	
	return $result;
	
}

/**
 * [_nxte_api_get_uid_by_code description]
 * @param  $code                String, Code
 * @param  $timestamp_from_app Timestamp from book APP
 * @return array or empty
 *           uid real user uid on site
 *           isActivated  1 or 0  to know if need return stored data in DB or reinitialize creating new book
 */
function _nxte_api_get_uid_by_code($code, $timestamp_from_app){
  //$timestamp_from_app = REQUEST_TIMESTAMP

  $q = db_select('node', 'n');
  //$q->fields('n', array('nid', 'title'));
  $q->condition('n.title', $code);
  $q->join('field_data_field_reader', 'fr', 'n.nid = fr.entity_id');
  $q->join('field_data_field_climed_expired_time', 'fcet', 'n.nid = fcet.entity_id');
  $q->join('field_data_field_isactivated', 'fisA', 'fr.field_reader_target_id = fisA.entity_id');
  $q->join('field_data_field_status', 'fst', 'fr.field_reader_target_id = fst.entity_id');

  $q->addField('fr', 'field_reader_target_id', 'uid');
  $q->addField('fisA', 'field_isactivated_value', 'isActivated');
  // $q->condition('fst.field_status_value', 1); // only return uids that has status: 1 - wait activation
  $q->condition('fst.field_status_value', array(1, 2, 3, 4));
  $q->condition('fcet.field_climed_expired_time_value', $timestamp_from_app ,'>=');

  $results1 = $q->execute()->fetchAll();

  if(empty($results1)){
    return ;
  }

  return $results1[0];
}

/**
 * Helper function to calculate value of time Left from DB for current user
 * @param  [type] $uid user uid for whom need to calculate time left
 * @return [type] int
 */
function _nxte_api_get_timeLeft($uid){

	// $result = &drupal_static(__FUNCTION__ . '_' . $uid);

	// if(!isset($result)) {

		$result = false;

	  $q = db_select('field_data_field_start_reading', 'fsr');
	  $q->condition('fsr.entity_id', $uid);
	  $q->addField('fsr', 'field_start_reading_value', 'timeStamp');
	  $timeStart = $q->execute()->fetchField();

	  if(!empty($timeStart)){
		  $time_reading = REQUEST_TIME - $timeStart;
		  $time_for_reading = variable_get('nxte_api_max_reading_time', DEFAULT_MAX_READING_TIME);
		  $time_remaining = $time_for_reading - $time_reading;
		  if($time_remaining < 1){
			$time_remaining = 0;
		  }
		  $result = $time_remaining;
	  }

  // }

  return $result;
}

/**
 * Helper function to return yime object
 * @param  $thief_fbid  webhub user's facebook id from api call or empty if user was not loged in
 * @param  $victim_fbid reader's facebook id
 * @return array timeObject with difference statuses=>
 *                          0 => Can steal from this reader.
 *                          1 => Reader hase less than 4 hours remaining.
 *                          2 => Too much time stolen from this reader today.
 *                          3 => You can only steal time once every 24 hours
 *                          4 => Already stolen time from this person. (after doing takeTime)
 *                          5 => User is not logged in
 *                          6 => Reader has finished book
 *                          7 => Reader is exploded
 *                          8 => You can't steal time from yourself
 */
function _nxte_api_get_timeObj($thief_fbid=null, $victim_fbid){
  if(!empty($thief_fbid)){
    $thief_uid = _nxte_api_get_uid($thief_fbid);
    $victim_uid = _nxte_api_get_uid($victim_fbid);
    if($thief_uid == $victim_uid){
      $timeObj = array(
          'canStealTime' => false,
          'status'=>8,
          'message'=>'Can not steal time from yourself',
        );
    }
    else{
      $thief_in_rule = _nxte_api_theft_in_rules($thief_uid, $victim_uid);
      $victin_in_rule = _nxte_api_victim_in_rules($victim_uid);
      if(empty($thief_in_rule)){
        if(empty($victin_in_rule)){
          $timeObj = array(
            'canStealTime' => true,
            'status'=>0,
            'message'=>'Can steal from this reader',
          );
        }
        elseif($victin_in_rule == 1){
          $timeObj = array(
            'canStealTime' => false,
            'status'=>1,
            'message'=>'Reader has less than 4 hours remaining',
          );
        }
        elseif($victin_in_rule == 2){
          $timeObj = array(
            'canStealTime' => false,
            'status'=>2,
            'message'=>'Too much time stolen from this reader already',
          );
        }
        elseif($victin_in_rule == 3){
          $timeObj = array(
            'canStealTime' => false,
            'status'=>6,
            'message'=>'Reader has finished book',
          );
        }
        elseif($victin_in_rule == 4){
          $timeObj = array(
            'canStealTime' => false,
            'status'=>7,
            'message'=>'Reader has already died',
          );
        }
      }
      else{
        if($thief_in_rule > 0 && $thief_in_rule < 1){
          $timeObj = array(
            'canStealTime' => false,
            'status'=>4,
            'message'=>'Already stolen time from this reader',
          );
        }
        elseif($thief_in_rule >= 1){
          $time_interval_theft = variable_get('nxte_api_time_interval_theft', DEFAULT_TIME_INTERVAL_THIEF);
		  $twelve_times = variable_get('nxte_api_twelve_times_rule', DEFAULT_TWELVE_TIMES_RULE);
          $timeObj = array(
            'canStealTime' => false,
            'status'=>3,
            'message'=>'Limit for stealing time exceeded',
          );
        }
      }
    }


  }
  else{
    $timeObj = array(
      'canStealTime' => false,
      'status'=>5,
      'message'=>'User is not logged in',
    );
  }

  return $timeObj;
}

/**
 *  Helper function to get the array that
 *  returns max book number value
 */
function _nxte_api_get_max_book_number(){
  $q = db_select('field_data_field_booknumber', 'fbn');
  $q->addExpression('MAX(fbn.field_booknumber_value)', 'maxBookNumber');
  $maxBookNumber = $q->execute()->fetchField();
  if(empty($maxBookNumber)){
    $maxBookNumber = 0;
  }
  return $maxBookNumber;
}

/**
 * Helper Function to get user stolen time
 * @param  [int] $uid real user id whos time stolen will search
 * @return [int] $time_stolen - int value of count stolen seconds
 */
function _nxte_api_get_stolen_time($uid){
  $q = db_select('field_data_field_time_stolen', 'fts');
  $q->condition('fts.entity_id', $uid);
  $q->addField('fts', 'field_time_stolen_value', 'time_stolen');
  $time_stolen = $q->execute()->fetchField();

  if(empty($time_stolen)){
    $time_stolen = 0;
  }
  return $time_stolen;
}

/*
 * Helper function that doin saving to tha DB new readed Page
 */
function _nxte_api_store_page_in_db($uid, $page, $timestamp){
  db_insert('nxte_api_pages')->fields(array( 'userid' => $uid, 'page' => $page, 'timestamp' => REQUEST_TIME )) ->execute();
}
/**
 * Helper function to get the array that
 * maps JSON methods to local functions
 */
function _nxte_api_get_methods() {
  return array(
//Book
    'checkUserCode' => 'nxte_api_checkUserCode',
    //'saveCurrentPage' => 'nxte_api_saveCurrentPage',
    // if answer data in this call for book app will be different as for webhub we need to use differet functions
    'getGlobalStats' => 'nxte_api_globalStats', //'nxte_api_getGlobalStats',
    'getUpdate' => 'nxte_api_getUpdate',
    'storeUserEmotions' => 'nxte_api_storeUserEmotions',
//Webhub
    'getActivatedReaders' => 'nxte_api_getActivatedReaders',
    'getDestructedReaders' => 'nxte_api_getDestructedReaders',
    'globalStats' => 'nxte_api_globalStats',
    'getUpdates' => 'nxte_api_getUpdates',
    'getReaderDetails' => 'nxte_api_getReaderDetails',
    'takeTime' => 'nxte_api_takeTime',
    'loginRegisterUser' => 'nxte_api_loginRegisterUser',
//Codes
    'getBatch' => 'nxte_api_getBatch',
    'getCode' => 'nxte_api_getCode',
  );
}
/**
 * Helper function returned aray with not needed keys in unswer for userObject
 * need feedback from client about it because for now in user Object too nuch not needed keys
 * @return [type] [description]
 */
function _nxte_api_get_methods_keys_for_unset() {
  return array(
  /////////
  //Book //
  /////////
    'checkUserCode' => array(
                //'unset' => 'unset',
    ),
    'getGlobalStats' => array(
                //'unset' => 'unset',
    ),
    'getUpdate' => array(
                //'unset' => 'unset',
    ),
    'storeUserEmotions' => array(
                //'unset' => 'unset',
    ),
  ///////////
  //Webhub //
  ///////////
    'getActivatedReaders' => array(
                'unset' => 'unset',
    ),
    'getDestructedReaders' => array(
                'unset' => 'unset',
    ),
    'globalStats' => array(
                //'unset' => 'unset',
    ),
    'getUpdates' => array(
                'unset' => 'unset',
    ),
    'getReaderDetails' => array(
                'unset' => 'unset',
    ),
    'takeTime' => array(
                //'unset' => 'unset',
    ),
   'loginRegisterUser' => array(
              'unset' => 'unset',
  ),
  //////////
  //Codes //
  //////////
    'getBatch' => array(
                //'unset' => 'unset',
    ),
    'getCode' => array(
                //'unset' => 'unset',
    ),
  );
}

/**
 * Helper function to get aray that
 * maps core filed names to key in api JSON answer object structure
 * @param  $set for returning different arrays
 *            - user_object_keys              - maping for real dbname of additioanl field
 *            - user_object_system_keys       - maping for real dbname of system fields in user entity
 *            - user_object_order_new_keys    - order queue for keys in answer object
 *            - user_object_new_keys_group_by - grouping for keys in answer
 */
function _nxte_api_get_keys($set){

  switch ($set) {
    case 'user_object_keys':
      $user_object_keys = array(
        'field_username' => 'name',
        'field_picture' => 'picture',
        'field_emotion' => 'mood',
        'field_description' => 'moodMessage',
        'field_start_reading' => 'timeStampApi',
        // 'field_start_reading_in_app' => 'timeStartApp',
        'field_start_reading_in_app' => 'timeStamp',
        'field_time_left'   => 'timeLeft',
        'field_code_inuser' => 'code',
        'field_isactivated' => 'isActivated',
        'field_booknumber'  => 'bookNumber',
        //'field_group_location'=> 'location',
        'field_city' => 'city',
        'field_state' => 'state',
        'field_lat' => 'latitude',
        'field_lng' => 'longitude',
        //'field_group_stats'       => 'stats',
        'field_page_completed' => 'completed',
        'field_time_lost' => 'timeLost',
        'field_time_stolen' => 'timeStolen',
        'field_wordsperminute' => 'wordsPerMinute',
        'field_current_page'  => 'currentPage',
        // 'field_was_exploded'  => 'explodedTimestamp',
        // 'field_was_finished' => 'finishedTimestamp',
        // 'field_on_page_died' => 'onPagedied',
        // 'field_status'      => 'status',

      );
      return $user_object_keys;
      break;
    case 'user_object_system_keys':
      $user_object_system_keys = array(
        'name'  =>  'id',
        // 'name'  =>  'userId',
        // 'uid'   =>  'id',
      );
      return $user_object_system_keys;
      break;
    case 'user_object_order_new_keys':
      $user_object_calc_keys = array(
        'id',
        // 'userId',
        'name',
        'picture',
        'timeLeft',
        'timeStampApi',
        'timeStamp',
        // 'timeStartApp',
        'mood',
        'moodMessage',
        'code',
        'isActivated',
        'bookNumber',
        //...group location...//
        'city',
        'state',
        'latitude',
        'longitude',
        //...group stats...//
        'completed',
        'timeLost',
        'timeStolen',
        'wordsPerMinute',
        'currentPage',
        //Helper group for unset
        // 'explodedTimestamp',
        // 'finishedTimestamp',
        // 'onPagedied',
        // 'status',
      );
      return $user_object_calc_keys;
      break;
    case 'user_object_new_keys_group_by':
        $user_object_calc_keys = array(
        //....tension group...//
          //'value'   => 'tension',
          //'message' => 'tension',
        //...group location...//
          'city'      => 'location',
          'state'     => 'location',
          'latitude' => 'location',
          'longitude' => 'location',
        //...group stats...//
          'completed'       => 'stats',
          'timeLost'        => 'stats',
          'timeStolen'      => 'stats',
          'wordsPerMinute'  => 'stats',
          'currentPage'     => 'stats',
        //...group helper...//
          // 'explodedTimestamp' => 'unset',
          // 'finishedTimestamp' => 'unset',
          // 'onPagedied'        => 'unset',
          // 'status'            => 'unset',
        );
        return $user_object_calc_keys;
        break;
    case 'keys_for_unseted_cache_get_activated_readers':
        $user_object_calc_keys = array(
          'field_emotion',
          'field_description',
          'field_wordsperminute',
          'field_current_page',
          'field_on_page_died',
          'field_was_exploded',
          'field_status',
          'field_isactivated',
          'field_was_finished',
          'field_page_completed',
          'field_time_lost',
          'field_time_stolen',
        );
        return $user_object_calc_keys;
        break;
  }

}
