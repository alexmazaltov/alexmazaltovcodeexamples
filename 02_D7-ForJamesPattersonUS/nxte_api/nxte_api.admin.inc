<?php


/**
 * Configuration form for API default variables
 *   nxte_api_period_book_initial_true - initial period to found updates for last 5min|15min|30min|1h  (for book application)
 *   nxte_api_period_book_initial_false - period to found updates at api call for current user 10sec|30sec|1min|5min (for book application)
 *   nxte_api_period_webhub_initial_true - initial period to found updates for last 5min|15min|30min|1h  (for webhub)
 *   nxte_api_period_webhub_initial_false - period to found updates at api call for current user 10sec|30sec|1min|5min (for webhub)
 */
function nxte_api_settings_form() {
  $form = array(
      'config_updates' => array(
        '#type' => 'fieldset',
        '#collapsible' => true,
        '#collapsed' => true,
        '#title' => t('Updates configuration'),
        'nxte_api_polling_time' =>array(
          '#type' => 'textfield',
          '#title' => t('Polling time'),
          '#size' => 6,
          '#description' => t('Time between two consecutive getUpdate calls in milliseconds'),
          '#default_value' => variable_get('nxte_api_polling_time', DEFAULT_POLLING_TIME),
          '#element_validate' => array('element_validate_integer_positive'),
        ),
        'nxte_api_news_request_num' =>array(
          '#type' => 'textfield',
          '#title' => t('News amount'),
          '#size' => 6,
          '#maxlength' => 2,
          '#description' => t('Max number of news items to request every getUpdate call'),
          '#default_value' => variable_get('nxte_api_news_request_num', DEFAULT_NEWS_REQUEST_NUM),
          '#element_validate' => array('element_validate_integer_positive'),
        ),
      ),

    'book_config' => array(
      '#type' => 'fieldset',
      '#collapsible' => true,
      '#collapsed' => true,
      '#title' => t('Book App Configuration'),
      'nxte_api_max_reading_time' => array(
          '#type' => 'textfield',
          '#title' => t('Reading time'),
          '#size' => 6,
          '#description' => t('Max time, in seconds, for reading the book'),
          '#default_value' => variable_get('nxte_api_max_reading_time', DEFAULT_MAX_READING_TIME),
		      '#element_validate' => array('element_validate_integer_positive'),
      ),
      'nxte_api_max_count_page' => array(
          '#type' => 'textfield',
          '#title' => t('Number of pages'),
          '#size' => 6,
		      '#maxlength' => 4,
          '#default_value' => variable_get('nxte_api_max_count_page', DEFAULT_MAX_COUNT_PAGE),
		      '#element_validate' => array('element_validate_integer_positive'),
      ),
      'nxte_api_words_per_pages' => array(
          '#type' => 'textfield',
          '#title' => t('Words per page'),
          '#size' => 6,
		      '#maxlength' => 4,
          '#description' => t('The average number of words per page. Used to calculate the words a reader reads per minute.'),
          '#default_value' => variable_get('nxte_api_words_per_pages', DEFAULT_WORDS_PER_PAGE),
		      '#element_validate' => array('element_validate_integer_positive'),
      ),
      'nxte_api_start_last_chapter' => array(
          '#type' => 'textfield',
          '#title' => t('Last page before last chapter'),
          '#size' => 6,
		      '#maxlength' => 4,
          '#description' => t('Enter the page where the time left for a reader should drop to preconfigured value'),
          '#default_value' => variable_get('nxte_api_start_last_chapter', DEFAULT_START_LAST_CHAPTER),
		      '#element_validate' => array('element_validate_integer_positive'),
      ),
      'nxte_api_start_last_chapter_drop_time_to' => array(
          '#type' => 'textfield',
          '#title' => t('Drop time to this value'),
          '#size' => 6,
          '#maxlength' => 4,
          '#description' => t('Enter the count of seconds. This value specifyed the time left for a reader should drop to'),
          '#default_value' => variable_get('nxte_api_start_last_chapter_drop_time_to', DEFAULT_START_LAST_CHAPTER_DROP_TIME_TO),
          '#element_validate' => array('element_validate_integer_positive'),
      ),


    ),
    'batch_and_codes_config' => array(
      '#type' => 'fieldset',
      '#collapsible' => true,
      '#collapsed' => true,
      '#title' => t('Baches & codes Configuration'),
      'nxte_api_climes_time_expired' => array(
          '#type' => 'textfield',
          '#title' => t('Claim expire time'),
		      '#size' => 6,
          '#description' => t('Enter the time, in seconds, when a claimed Code expires and can no longer be used to activate a book.'),
          '#default_value' => variable_get('nxte_api_climes_time_expired', DEFAULT_CLIME_EXPIRED_TIME),
          '#element_validate' => array('element_validate_integer_positive'),
      ),
      'nxte_api_max_time_beetwen_two_batch' => array(
          '#type' => 'textfield',
          '#title' => t('Max time for next batch'),
		  '#size' => 6,
          '#description' => t('A batch will only be displayed if it is not more than this amount in seconds in the future'),
          '#default_value' => variable_get('nxte_api_max_time_beetwen_two_batch', DEAFULT_MAX_TIME_BEETWEN_TWO_BATCH),
          '#element_validate' => array('element_validate_integer_positive'),
      ),
    ),

    'nxte_api_theft_rules' => array(
      '#type' => 'fieldset',
      '#collapsible' => true,
      '#collapsed' => true,
      '#title' => t('Theft rules'),
      'nxte_api_time_interval_theft'=>array(
        '#type' => 'textfield',
        '#title' => t('Time interval for stealing'),
		'#size' => 6,
        '#description' => t('Enter the amount of seconds a user can not steal again after he stole time'),
        '#default_value' => variable_get('nxte_api_time_interval_theft', DEFAULT_TIME_INTERVAL_THIEF),
        '#element_validate' => array('element_validate_integer_positive'),
      ),
      'nxte_api_twelve_times_rule'=>array(
        '#type' => 'textfield',
        '#title' => t('Amount of thefts per interval'),
    '#size' => 6,
        '#description' => t('Enter the amount of thefts that each user can do per time interval that is set above'),
        '#default_value' => variable_get('nxte_api_twelve_times_rule', DEFAULT_TWELVE_TIMES_RULE),
        '#element_validate' => array('element_validate_integer_positive'),
      ),
      'nxte_api_time_max_loose_victim'=>array(
        '#type' => 'textfield',
        '#title' => t('Max time to lose'),
		'#size' => 6,
        '#description' => t('Enter the maximum amount of seconds a reader can lose by getting robbed'),
        '#default_value' => variable_get('nxte_api_time_max_loose_victim', DEFAULT_MAX_TIME_LOOSE),
        '#element_validate' => array('element_validate_integer_positive'),
      ),
      'nxte_api_time_min_left_victim'=>array(
        '#type' => 'textfield',
        '#title' => t('Minimum time'),
		'#size' => 6,
        '#description' => t('When a reader has less seconds remaining than this, he can not be robbed'),
        '#default_value' => variable_get('nxte_api_time_min_left_victim', DEFAULT_MIN_LEFT_VICTIM),
        '#element_validate' => array('element_validate_integer_positive'),
      ),
    ),

    // 'nxte_api_try_google'=>array(
        // '#type' => 'select',
        // '#title' => t('Get coordinates from Google on user register'),
        // '#options' => array(
          // 0 => t('Do not try'),
          // 1 => t('Try to get'),
        // ),
        // '#default_value' => variable_get('nxte_api_try_google', 0)
      // ),

    'updates' => array(
      '#type' => 'fieldset',
      '#collapsible' => true,
      '#collapsed' => false,
      '#title' => t('Configurations caching times'),

      'book' => array(
        '#type' => 'fieldset',
        '#collapsible' => true,
        '#collapsed' => true,
        '#title' => t('Book update configurations'),

        'nxte_api_period_book_caching' =>array(
          '#type' => 'textfield',
		  '#size' => 6,
          '#title' => t('Number of seconds to cache the response for certain API calls'),
          // '#options' => array(
            // 10 => t('10 seconds'),
            // 30 => t('30 seconds'),
            // 60 => t('1 minute'),
            // 120 => t('2 minuts'),
            // 300 => t('5 minute'),
          // ),
          '#default_value' => variable_get('nxte_api_period_book_caching', DEFAULT_PERIOD_BOOK_CACHING),
		  '#element_validate' => array('element_validate_integer_positive')
        ),
      ),
      'webhub' => array(
        '#type' => 'fieldset',
        '#collapsible' => true,
        '#collapsed' => true,
        '#title' => t('Webhub update configurations'),

        'nxte_api_period_webhub_caching' =>array(
          '#type' => 'textfield',
		  '#size' => 6,
          '#title' => t('Number of seconds to cache the response for certain API calls'),
          // '#options' => array(
            // 10 => t('10 seconds'),
            // 30 => t('30 seconds'),
            // 60 => t('1 minute'),
            // 120 => t('2 minuts'),
            // 300 => t('5 minute'),
          // ),
          '#default_value' => variable_get('nxte_api_period_webhub_caching', DEFAULT_PERIOD_WEBHUB_CACHING),
		  '#element_validate' => array('element_validate_integer_positive')
        ),
      ),
    ),

  );
  return system_settings_form($form);
}
