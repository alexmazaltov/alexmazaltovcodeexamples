<?php


/**
 * Helper function that doing search in db for available codes that was not getted before under other readers
 * @param
 *   $request_time timestamp of starting work on this function
 * @return [int] count of still availables codes
 */
function _nxte_api_get_current_batch($request_time = null){

    //////////////////////////////////////////////////
    //Get Current batch num                         //
    //    depent on batch num and                   //
    //    timestamp value for last availabled batch //
    //////////////////////////////////////////////////
    // $current_batch_num = db_select('field_data_field_start_available', 'fsa_m');
    // $current_batch_num->join('field_data_field_batch_number', 'fbn_', 'fbn_.entity_id = fsa_m.entity_id');
    // $current_batch_num->condition('fsa_m.field_start_available_value', REQUEST_TIME, '<=');
    // $current_batch_num->addExpression('MAX(fbn_.field_batch_number_value)', 'current_batch_num');
    // $current_batch_num = $current_batch_num->execute()->fetchField();

    $current_batch_num = db_select('field_data_field_start_available', 'fsa_m');
    $current_batch_num->join('field_data_field_batch_number', 'fbn_', 'fbn_.entity_id = fsa_m.entity_id');
    $current_batch_num->condition('fsa_m.field_start_available_value', REQUEST_TIME, '<=');
    $current_batch_num->addField('fbn_', 'field_batch_number_value', 'batch_num');
    $current_batch_num->addField('fsa_m', 'field_start_available_value', 'available_time');
    $current_batch_num->orderBy('available_time', 'DESC');
    $current_batch_num = $current_batch_num->execute()->fetchField();

  $q = db_select('node', 'n');
  $q->condition('n.type', 'code');
  $q->condition('n.status', NODE_PUBLISHED);
  $q->join('field_data_field_start_available', 'fsa', 'n.nid = fsa.entity_id');
  //Get only certain codes that already availables for current batch
  $q->join('field_data_field_batch_number', 'fbn', 'n.nid = fbn.entity_id');
  $q->condition('fbn.field_batch_number_value', $current_batch_num);
  $q->condition('fsa.field_start_available_value', REQUEST_TIME, '<=');

      //////////////////////////////////////////////////////////////////////////
      //Get all  nids that was climed before to exclude them from results     //
      //////////////////////////////////////////////////////////////////////////
      $activated_nids = db_select('field_data_field_reader', 'fr');
      $activated_nids->fields('fr', array('entity_id'));
  $q->condition('n.nid', $activated_nids, 'NOT IN');

  $q->addField('fsa', 'field_start_available_value', 'start_av');
  $q->addField('fbn', 'field_batch_number_value', 'batch_number');
  $q->addExpression('COUNT(n.nid)', 'count_codes_left');
  $current_batch_state = $q->execute()->fetchAll();

  if($current_batch_state[0]->count_codes_left > 0){
    //try to use cache
    $till_codes = array(
        'isAvailable' => true,
        'batchNumber' => $current_batch_state[0]->batch_number,
        //'batchSize' => $current_batch_state[0]->count_codes_left,
        'batchSize' => _nxte_codes_total_codes_in_batch($current_batch_state[0]->batch_number), // -> cached value
    );
    return $till_codes;
  }

  return 0;
}

/**
 * Helper function that doing search in db for available codes that was not getted before under other readers
 * @param
 *   $request_time timestamp of starting work on this function
 * @return [int] count of still availables codes or
 */
function _nxte_api_get_prev_batch($request_time = REQUEST_TIME){

  $till_codes = 0;

   $current_batch_num = db_select('field_data_field_start_available', 'fsa_m');
    $current_batch_num->join('field_data_field_batch_number', 'fbn_', 'fbn_.entity_id = fsa_m.entity_id');
    $current_batch_num->condition('fsa_m.field_start_available_value', REQUEST_TIME, '<=');
    $current_batch_num->addField('fbn_', 'field_batch_number_value', 'batch_num');
    $current_batch_num->addField('fsa_m', 'field_start_available_value', 'available_time');
    $current_batch_num->orderBy('available_time', 'DESC');
    $current_batch_num = $current_batch_num->execute()->fetchField();


  $q = db_select('node', 'n');
  $q->condition('n.type', 'code');
  $q->condition('n.status', NODE_PUBLISHED);
  $q->join('field_data_field_start_available', 'fsa', 'n.nid = fsa.entity_id');
  //Get only certain codes that already availables for current batch
  $q->join('field_data_field_batch_number', 'fbn', 'n.nid = fbn.entity_id');
  $q->condition('fsa.field_start_available_value', REQUEST_TIME, '<=');

      //////////////////////////////////////////////////////////////////////////
      //Get all  nids that was climed before to exclude them from results     //
      //////////////////////////////////////////////////////////////////////////
      $activated_nids = db_select('field_data_field_reader', 'fr');
      $activated_nids->fields('fr', array('entity_id'));
  $q->condition('n.nid', $activated_nids, 'NOT IN');

  $q->orderBy('fsa.field_start_available_value', 'DESC');
  $q->groupBy('fsa.field_start_available_value');
  $q->addField('fbn', 'field_batch_number_value', 'batch_number');
  $current_batch_state = $q->execute();//->fetchAllAssoc();

  foreach ($current_batch_state as $q_obj) {
    $batch_number = $q_obj->batch_number;
        $q = db_select('node', 'n');
        $q->condition('n.type', 'code');
        $q->condition('n.status', NODE_PUBLISHED);
        $q->join('field_data_field_start_available', 'fsa', 'n.nid = fsa.entity_id');
        //Get only certain codes that already availables for current batch
        $q->join('field_data_field_batch_number', 'fbn', 'n.nid = fbn.entity_id');
        $q->condition('fbn.field_batch_number_value', $batch_number);
        //$q->condition('fsa.field_start_available_value', REQUEST_TIME, '<=');

            //////////////////////////////////////////////////////////////////////////
            //Get all  nids that was climed before to exclude them from results     //
            //////////////////////////////////////////////////////////////////////////
            $activated_nids = db_select('field_data_field_reader', 'fr');
            $activated_nids->fields('fr', array('entity_id'));
        $q->condition('n.nid', $activated_nids, 'NOT IN');

        $q->addField('fsa', 'field_start_available_value', 'start_av');
        $q->addField('fbn', 'field_batch_number_value', 'batch_number');
        $q->addExpression('COUNT(n.nid)', 'count_codes_left');
        $current_batch_state = $q->execute()->fetchAll();
          $till_codes = array(
            'isAvailable' => true,
            'batchNumber' => $current_batch_state[0]->batch_number,
            //'batchSize' => $current_batch_state[0]->count_codes_left,
            'batchSize' => _nxte_codes_total_codes_in_batch($current_batch_state[0]->batch_number), // -> cached value
          );
        break;
  }
  return $till_codes;
}


/**
 * Queries that doing choise of state_1 or state_2 for Api getBatches
 *   //-Main condition to see if there are available codes is field "start_available" in "code" node_type
 *   //-This field empty by default and get changes on special administrative way that create ability to get new batch of codes to be able
 * @param
 *   $request_time timestamp of starting work on this function
 * @return int  - value of seconds till to next batch (if it is 0 it means that state 3 - doing new query to get_till_codes)
 *         false if here is note availables codes
 */
function _nxte_api_get_till_time_to_batch($request_time = null){

  $rule_10_hours = $request_time + variable_get('nxte_api_max_time_beetwen_two_batch', DEAFULT_MAX_TIME_BEETWEN_TWO_BATCH);

    $current_batch_num = db_select('field_data_field_start_available', 'fsa_m');
    $current_batch_num->join('field_data_field_batch_number', 'fbn_', 'fbn_.entity_id = fsa_m.entity_id');
    $current_batch_num->condition('fsa_m.field_start_available_value', REQUEST_TIME, '>=');
    $current_batch_num->condition('fsa_m.field_start_available_value', $rule_10_hours, '<' );
    $current_batch_num->addField('fbn_', 'field_batch_number_value)', 'current_batch_num');

    $current_batch_num->orderBy('fsa_m.field_start_available_value', 'ASC');
    $current_batch_num = $current_batch_num->execute()->fetchField();


  $q = db_select('node', 'n');
  $q->condition('n.type', 'code');
  $q->condition('n.status', NODE_PUBLISHED);
  $q->join('field_data_field_start_available', 'fsa', 'n.nid = fsa.entity_id');
  //Get only certain codes that already availables for current batch
  $q->join('field_data_field_batch_number', 'fbn', 'n.nid = fbn.entity_id');
  $q->condition('fbn.field_batch_number_value', $current_batch_num);


      //////////////////////////////////////////////////////////////////////////
      //Get all  nids that was climed before to exclude them from results     //
      //////////////////////////////////////////////////////////////////////////
      $activated_nids = db_select('field_data_field_reader', 'fr');
      $activated_nids->fields('fr', array('entity_id'));
  $q->condition('n.nid', $activated_nids, 'NOT IN');
  $q->addExpression('COUNT(n.nid)', 'count_nids');
  $q->addExpression('MIN(fbn.field_batch_number_value)', 'batch_number');
  $q->addExpression('MIN(fsa.field_start_available_value)', 'time_av_first_code_in_batch');
  $q->addExpression('MAX(fsa.field_start_available_value)', 'time_av_last_code_in_batch');
  $result = $q->execute()->fetchAll();

  if ( $result[0]->count_nids < 1 ){
    return false; // -> false -> there are not soon availables batches
  }
  else{
    $return = array(
      'isAvailable' => false,
      'totalCodes' => _nxte_codes_total_codes_in_batch($result[0]->batch_number),
      'activationTime' => (($result[0]->time_av_first_code_in_batch)-REQUEST_TIME)*1000,
      'batchNumber' => $result[0]->batch_number,
    );
    return $return;
  }

}

/**
 * Function that gets one available code and does linking it for current user in this code
 * @param
 *   $uid - User id who try to doing retreive for code
 * @return
 *   string -if code was sucessfully binding to this user
 *   false - if it was not sucessfully or of user alredy has enabled code
 */
function _nxte_api_get_available_code_nid($uid){

  $already_activited = db_select('field_data_field_reader', 'fr');
    $already_activited->condition('fr.field_reader_target_id', $uid);
    $already_activited->addExpression('COUNT(fr.field_reader_target_id)', 'count');
    $already_activited = $already_activited->execute()->fetchField();
  //dsm($already_activited);
  //! Important before doing node_save($code) need to shure that curent user did not enabling any code yet
  if($already_activited < 1){
    $expired_for_activation = REQUEST_TIME + variable_get('nxte_api_climes_time_expired', DEFAULT_CLIME_EXPIRED_TIME);

      //////////////////////////////////////////////////
      //Get Current batch num                         //
      //    depent on batch num and                   //
      //    timestamp value for last availabled batch //
      //////////////////////////////////////////////////
      $current_batch_num = db_select('field_data_field_start_available', 'fsa_m');
      $current_batch_num->join('field_data_field_batch_number', 'fbn_', 'fbn_.entity_id = fsa_m.entity_id');
      $current_batch_num->condition('fsa_m.field_start_available_value', REQUEST_TIME, '<=');
      $current_batch_num->addField('fbn_', 'field_batch_number_value', 'batch_num');
      $current_batch_num->addField('fsa_m', 'field_start_available_value', 'available_time');
      $current_batch_num->orderBy('available_time', 'DESC');
      $current_batch_num = $current_batch_num->execute()->fetchField();

      $q = db_select('node', 'n');
      $q->condition('n.type', 'code');
      $q->condition('n.status', NODE_PUBLISHED);
      $q->join('field_data_field_start_available', 'fsa', 'n.nid = fsa.entity_id');
      //Get only certain codes that already availables for current batch
      $q->join('field_data_field_batch_number', 'fbn', 'n.nid = fbn.entity_id');
      $q->condition('fbn.field_batch_number_value', $current_batch_num);
      $q->condition('fsa.field_start_available_value', REQUEST_TIME, '<=');

          //////////////////////////////////////////////////////////////////////////
          //Get all  nids that was climed before to exclude them from results     //
          //////////////////////////////////////////////////////////////////////////
          $activated_nids = db_select('field_data_field_reader', 'fr');
          $activated_nids->fields('fr', array('entity_id'));
      $q->condition('n.nid', $activated_nids, 'NOT IN');
      $q->addField('n', 'nid', 'nid');
      $q->addField('n', 'title', 'code');
      $code_nids = $q->execute()->fetchAll();

      $count_av_codes = count($code_nids);

      if($count_av_codes > 0){
        $rand = rand(0,($count_av_codes - 1));
        $nid = $code_nids[$rand]->nid;


        // $node_code = new stdClass();
        // $node_code->nid = $nid;
        // $node_code->type = 'code';
        // $node_code->field_reader[LANGUAGE_NONE][0]['target_id'] = $uid;
        // $node_code->field_climed_expired_time[LANGUAGE_NONE][0]['value'] = $expired_for_activation;

        // field_attach_presave('node', $node_code);
        // field_attach_update('node', $node_code);
        $wrap_code = entity_metadata_wrapper('node', node_load($nid));
          $wrap_code->field_reader->set($uid);
          $wrap_code->field_climed_expired_time->set($expired_for_activation);
        $wrap_code->save();

        return ($code_nids[$rand]->code);
      }
      //IF in current batch are not available not climed codes then try to get uids from previouse batches
      else{
        $prev_batch = _nxte_api_get_prev_batch(REQUEST_TIME);
        if(!empty($prev_batch)){
          $current_batch_num = $prev_batch['batchNumber'];
          $q = db_select('node', 'n');
          $q->condition('n.type', 'code');
          $q->condition('n.status', NODE_PUBLISHED);
          $q->join('field_data_field_start_available', 'fsa', 'n.nid = fsa.entity_id');
          //Get only certain codes that already availables for current batch
          $q->join('field_data_field_batch_number', 'fbn', 'n.nid = fbn.entity_id');
          $q->condition('fbn.field_batch_number_value', $current_batch_num);
          $q->condition('fsa.field_start_available_value', REQUEST_TIME, '<=');

              //////////////////////////////////////////////////////////////////////////
              //Get all  nids that was climed before to exclude them from results     //
              //////////////////////////////////////////////////////////////////////////
              $activated_nids = db_select('field_data_field_reader', 'fr');
              $activated_nids->fields('fr', array('entity_id'));
          $q->condition('n.nid', $activated_nids, 'NOT IN');
          $q->addField('n', 'nid', 'nid');
          $q->addField('n', 'title', 'code');
          $code_nids = $q->execute()->fetchAll();

          $count_av_codes = count($code_nids);

          if($count_av_codes > 0){
            $rand = rand(0,($count_av_codes - 1));
            $nid = $code_nids[$rand]->nid;
            $wrap_code = entity_metadata_wrapper('node', node_load($nid));
              $wrap_code->field_reader->set($uid);
              $wrap_code->field_climed_expired_time->set($expired_for_activation);
            $wrap_code->save();

            return ($code_nids[$rand]->code);
        }
      }
    }
  }
  return false;

}
