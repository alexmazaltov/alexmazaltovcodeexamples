<?php

/**
 * Helper function for get max number of bach number through all codes
 */
function _nxte_codes_get_last_batch_number(){
    $prev_batch_number = db_select('field_data_field_batch_number', 'fbn');
    $prev_batch_number->addExpression('MAX(fbn.field_batch_number_value)', 'max_batch_number');
    $prev_batch_number = $prev_batch_number->execute()->fetchField();
    if(is_numeric($prev_batch_number)){
      return $prev_batch_number;
    }
    return 0;
}

/*
 * Helper function that returns all nids taht was expired for statistics in batxh managmant section
 */
function _nxte_codes_get_expired_nids(){
    $nids_expired = db_select('field_data_field_climed_expired_time', 'fcet');
    $nids_expired->addField('fcet', 'entity_id', 'nid');
    $nids_expired->condition('fcet.field_climed_expired_time_value', REQUEST_TIME, '<=');
    $nids_expired = $nids_expired->execute()->fetchAll();

    return $nids_expired;
}

/*
 * Helper function for returnin from DB all uids of user who was activated
 */
function _nxte_codes_get_activated_nids(){
    //If in user entity in field code in user was code then this user was activated
    $uids_is_active = db_select('field_data_field_code_inuser', 'fcinu');
    $uids_is_active->addField('fcinu', 'entity_id', 'uid');
    $uids_is_active = $uids_is_active->execute()->fetchAll();

    return $uids_is_active;
}

/**
 * Helper function thatreturn cachad value of count codes in batch
 * @param  [int] $batch_num  Number of batch for returning count of codes in
 * @return [int] count of codes in batch
 */
function _nxte_codes_total_codes_in_batch($batch_num){
    if(empty($batch_num)){
        return 'err: need to specify bach_number';
    }
    $cache = cache_get('batch:'.$batch_num);
    if(!$cache){
        $code_nids = db_select('field_data_field_batch_number', 'fbn');
        $code_nids->addField('fbn', 'entity_id', 'nid');
        $code_nids->condition('fbn.field_batch_number_value', $batch_num);
        $code_nids->addExpression('COUNT(fbn.field_batch_number_value)', 'count');
        $code_nids = $code_nids->execute()->fetchAll();
        $count =  $code_nids[0]->count;
        cache_set('batch:'.$batch_num, $count, 'cache');
    }
    else{
        $count = $cache->data;
    }

    return $count;
}
