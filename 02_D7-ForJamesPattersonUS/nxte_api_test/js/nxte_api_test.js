var user;

(function($) {
  function do_json_call(data_obj) {

    $('#edit-response .fieldset-wrapper').html('');

    $.ajax({
      data: JSON.stringify(data_obj),
      //url: Drupal.settings.basePath + '?q=api',
      url: Drupal.settings.basePath + 'api',
      // url: 'http://ec2-54-173-3-253.compute-1.amazonaws.com/api/?q=api',
      type: 'POST',
      contentType: 'application/json',
      dataType: 'json',
      success: function(result, status, jqXHR) {
        $.each(result, function(index, value) {
          var padding = 0;
          if(typeof(value) != 'object'){
            $('#edit-response .fieldset-wrapper').append(index + ': ' + value + '<br />');
          }
          else{
            $('#edit-response .fieldset-wrapper').append('<div style="font-weight: 600; padding-left:'+padding+'px;">'+index + ': </div>');
            var padding1 = 10;
            $.each(value, function(dataIndex, dataValue) {
              if(typeof(dataValue) != 'object'){
                $('#edit-response .fieldset-wrapper').append('<div style="padding-left:'+padding1+'px;">'+dataIndex + ': ' + dataValue + '</div>');
              }
              else{
                $('#edit-response .fieldset-wrapper').append('<div style="font-weight: 600; padding-left:'+padding1+'px;">'+dataIndex + ': </div>');
                var padding2 = 20;
                $.each(dataValue, function(DVindex, DVval) {
                  if(typeof(DVval)!='object'){
                    $('#edit-response .fieldset-wrapper').append('<div style="padding-left:'+padding2+'px;">'+DVindex + ': ' + DVval + '</div>');
                  }
                  else{
                    $('#edit-response .fieldset-wrapper').append('<div style="font-weight: 600; padding-left:'+padding2+'px;">'+DVindex + ': </div>');
                    var padding3 = 30;
                    $.each(DVval, function(DVVindex, DVVval) {
                      if(typeof(DVVval)!='object'){
                        $('#edit-response .fieldset-wrapper').append('<div style="padding-left:'+padding3+'px;">'+DVVindex + ': ' + DVVval + '</div>');
                      }
                      else{
                        $('#edit-response .fieldset-wrapper').append('<div style="font-weight: 600; padding-left:'+padding3+'px;">'+DVVindex + ': </div>');
                        var padding4 = 40;
                        $.each(DVVval, function(DVVVindex, DVVVval) {
                          // if(typeof(DVVVval)!='object'){
                            $('#edit-response .fieldset-wrapper').append('<div style="padding-left:'+padding4+'px;">'+DVVVindex + ': ' + DVVVval + '</div>');
                          // }
                          // else{
                          //   $('#edit-response .fieldset-wrapper').append('<div style="padding-left:'+padding4+'px;">'+DVVVindex + ': </div>');
                          //   var padding5 = 50;
                          //   $.each(DVVVval, function(DVVVVindex, DVVVVval) {
                          //     $('#edit-response .fieldset-wrapper').append('<div style="padding-left:'+padding5+'px;">'+DVVVVindex + ': ' + DVVVVval + '</div>');
                          //   });
                          // }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });

  }

function do_json_call_(string, selector){
  $.ajax({
      url: Drupal.settings.basePath + '?q=strtotime/'+ string,
      success: function(result, status, jqXHR) {
        $(selector).val(result);
      }
    });
}

function do_json_call_get_user_obj(fb_id){
  $.ajax({
      data: JSON.stringify( { method: 'loginRegisterUser', params: { fbId: fb_id } }),
      //url: Drupal.settings.basePath + '?q=api',
      url: Drupal.settings.basePath + 'api',
      // url: 'http://ec2-54-173-3-253.compute-1.amazonaws.com/api/?q=api',
      type: 'POST',
      contentType: 'application/json',
      dataType: 'json',
      async: false,
      success: function(result, status, jqXHR) {
        user = result.data.user;
      }
    });
}

function do_json_call_get_timestamp(userId, selector){
  $.ajax({
      //url: Drupal.settings.basePath + '?q=getstartreadingtimestamp/'+ userId,
      url: Drupal.settings.basePath + 'getstartreadingtimestamp/'+ userId,
      success: function(result, status, jqXHR) {
        $(selector).val(result);
      }
    });
}


  $(document).ready(function() {
    /*__________BOOK___________*/

    $('#edit-call-checkusercode').click(function() {
      do_json_call(
        // {
        //   method: 'checkUserCode',
        //   params: {
        //     code: $('#edit-checkusercode-code').val(),
        //   }
        // }
        {
          method: "checkUserCode",
          params: {
                  user: {
                      id: -1,
                      name: "",
                      location: {
                          // latitude: 39.7392,
                          latitude: $('#edit-checkusercode-lat').val(),
                          // longitude: 74.9847,
                          longitude: $('#edit-checkusercode-lng').val(),
              // city: 'Amsterdam',
              city: $('#edit-checkusercode-city').val(),
              // state: 'NH'
              state: $('#edit-checkusercode-state').val(),
                      },
                      picture: "",
                      timeStamp: $.now()/1000,
                      currentTimeStamp : $.now()/1000,
                      timeLeft: -1,
                      mood: 1,
                      moodMessage: "",
                      stats: {
                          completed: 0,
                          timeLost: 0,
                          timeStolen: 0,
                          wordsPerMinute: 0,
                          currentPage: 1
                      },
                      code: $('#edit-checkusercode-code').val(),
                  }
          }
        }
      );
      return false;
    });


    $('#edit-call-getglobalstats').click(function() {
      do_json_call(
        {
          method: 'getGlobalStats',
          params: {}
        }
      );
      return false;
    });

                //Helper function for getting timeStamp for reader (starting read on app from BD)
                $('#edit-get-start-reading-timestamp').click(function() {
                  do_json_call_get_timestamp( $('#edit-getupdate-userid').val(), '#edit-string-for-str-to-time' );
                  return false;
                });
                //Helper function to convert timestamp throught php ajax strtotime()
                $('#edit-call-str-to-time').click(function() {
                  do_json_call_( $('#edit-string-for-str-to-time').val(), '#edit-string-for-str-to-time' );
                  return false;
                });



    $('#edit-call-getupdate').click(function() {
      do_json_call_get_user_obj($('#edit-getupdate-userid').val());
      user.stats.currentPage = $('#edit-getupdate-current-page').val();
      user.currentTimeStamp = $.now()/1000;
      do_json_call(
        {
          method: 'getUpdate',
          params: {
                    user: user,
                    news: {
                      request: $('#edit-getupdate-news-request').val()
                    }
                  }
        }
      );
      return false;
    });

    $('#edit-call-storeuseremotions').click(function() {
      do_json_call_get_user_obj($('#edit-storeuseremotions-userid').val());
      user.mood = $('#edit-storeuseremotions-emotion').val();
      user.moodMessage = $('#edit-storeuseremotions-description').val();
      do_json_call(
        {
          method: 'storeUserEmotions',
          params:
                  // {
                  //   userId: $('#edit-storeuseremotions-userid').val(),
                  //   emotion: $('#edit-storeuseremotions-emotion').val(),
                  //   description: $('#edit-storeuseremotions-description').val(),
                  // }
                  {
                    user: user,
                    news: {
                      request: 24
                    }
                  }
        }
      );
      return false;
    });

    /*__________WEBHUB___________*/

    $('#edit-call-loginregisteruser').click(function() {
      do_json_call(
        {
          method: 'loginRegisterUser',
          params: {
            fbId: $('#edit-fbid').val(),
            userName: $('#edit-username').val(),
            picture: $('#edit-picture').val(),
            // city: $('#edit-city').val(),
            city: '',
          }
        }
      );
      return false;
    });

    $('#edit-call-taketime').click(function() {
      do_json_call(
        {
          method: 'takeTime',
          params: {
            readerId: $('#edit-taketime-readerid').val(),
            userId:$('#edit-taketime-userid').val(),
          }
        }
      );
      return false;
    });

    $('#edit-call-getreaderdetails').click(function() {
      do_json_call(
        {
          method: 'getReaderDetails',
          params: {
            readerId: $('#edit-getreaderdetails-readerid').val(),
            userId:$('#edit-getreaderdetails-userid').val(),
          }
        }
      );
      return false;
    });

    $('#edit-call-getupdates').click(function() {
      //alert("do_json_call({method: 'getUpdates', params: {}});");
      do_json_call(
        {
          method: 'getUpdates',
          params: {
                  news: {
                      request: $('#edit-getupdates-news-request').val()
                  }
          }
        }
      );
      return false;
    });

    $('#edit-call-globalstats').click(function() {
      //alert("do_json_call({method: 'getUpdates', params: {}});");
      do_json_call( { method: 'globalStats', params: {} } );
      return false;
    });

    $('#edit-call-getdestructedreaders').click(function() {
      //alert("do_json_call({method: 'getUpdates', params: {}});");
      do_json_call( { method: 'getDestructedReaders', params: {} } );
      return false;
    });

    $('#edit-call-getactivatedreaders').click(function() {
      //alert("do_json_call({method: 'getUpdates', params: {}});");
      do_json_call(
        {
          method: 'getActivatedReaders',
          params: {
            userId:$('#edit-getactivatedreaders-userid').val(),
          }
       }
      );
      return false;
    });


    /*__________CODES___________*/
    $('#edit-call-getbatch').click(function() {
      //alert("do_json_call({method: 'getUpdates', params: {}});");
      do_json_call( { method: 'getBatch', params: {} } );
      return false;
    });

    $('#edit-call-getcode').click(function() {
      //alert("do_json_call({method: 'getUpdates', params: {}});");
      do_json_call(
        {
          method: 'getCode',
          params: {
            userId: $('#edit-getcode-userid').val(),
          }
        }
      );
      return false;
    });

  });

})(jQuery);
