(function($) {

  function do_json_call(data_obj) {

    $('#edit-response .fieldset-wrapper').html('');

    $.ajax({
      data: JSON.stringify(data_obj),
      url: Drupal.settings.basePath + 'api',
      type: 'POST',
      contentType: 'application/json',
      dataType: 'json',
      success: function(result, status, jqXHR) {
        $.each(result, function(index, value) {
          $('#edit-response .fieldset-wrapper').append(index + ': ' + value + '<br />')
        });
      }
    });

  }

  $(document).ready(function() {

    $('#edit-call-init').click(function() {
      do_json_call({method: 'init', params: {}});
      return false;
    });

    $('#edit-call-signin').click(function() {
      do_json_call(
        {
          method: 'signIn',
          params: {
            emailAddress: $('#edit-emailaddress').val(),
            countryCode: $('#edit-countrycode').val(),
            language: $('#edit-language').val(),
            wishToReceiveNewsLetter: $('#edit-wishtoreceivenewsletter').val()
          }
        }
      );
      return false;
    });

    $('#edit-call-confirm').click(function() {
      do_json_call(
        {
          method: 'confirmUserDetails',
          params: {
            emailAddress: $('#edit-emailaddress2').val(),
            language: $('#edit-language2').val(),
            gender: $('#edit-gender').val(),
            firstName: $('#edit-firstname').val(),
            lastName: $('#edit-lastname').val(),
            birthday: $('#edit-birthday').val(),
            telephone: $('#edit-telephone').val()
          }
        }
      );
      return false;
    });

    $('#edit-call-wonanswer').click(function() {
      do_json_call(
        {
          method: 'wonAnswer',
          params: {
            emailAddress: $('#edit-emailaddress3').val(),
            securityCode: $('#edit-securitycode').val(),
            securityCode1: $('#edit-securitycode1').val(),
            securityCode2: $('#edit-securitycode2').val(),
            securityCode3: $('#edit-securitycode3').val(),
            answer1: $('#edit-answer1').val(),
            answer2: $('#edit-answer2').val(),
            answer3: $('#edit-answer3').val()
          }
        }
      );
      return false;
    });

  });

})(jQuery);
