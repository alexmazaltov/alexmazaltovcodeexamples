<?php

function nxte_api_test_form(){
  $form = array();

  $form['#attached'] = array(
    'library' => array(
      array('nxte_api_test', 'nxte_api_test_library'),
    ),
  );
//--------------- RESPONSE --------------------
  $form['response'] = array(
    '#type' => 'fieldset',
    '#title' => t('Response')
  );
//--------------- BOOK --------------------
  $form['book'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    // '#collapsed' => TRUE,
    '#title' => 'book',
  );
  // $form['book']['response_book'] = array(
  //   '#type' => 'fieldset',
  //   '#collapsible' => TRUE,
  //   '#collapsed' => FALSE,
  //   '#title' => t('Book Response after checkUserCode')
  // );
  //+   // checkusercode       code
  //+   // savecurrentpage     currentPage, userId
  //+   // getglobalstats
  //+   // getupdate           userId


  $form['book']['checkusercode'] =array(
    '#type' => 'fieldset',
    //'#attributes' => array('class' => array('container-inline')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'checkusercode',
      'checkusercode_code' => array(
        '#type' => 'textfield',
        '#title' => t('code string'),
        '#default_value' => 'J4ckcar7'
      ),
      'checkusercode_city' => array(
        '#type' => 'textfield',
        '#title' => t('City'),
        '#default_value' => 'New York'
      ),
      'checkusercode_state' => array(
        '#type' => 'textfield',
        '#title' => t('State'),
        '#default_value' => 'NY'
      ),
      'checkusercode_lat' => array(
        '#type' => 'textfield',
        '#title' => t('Latitude'),
        '#default_value' => '40.75'
      ),
      'checkusercode_lng' => array(
        '#type' => 'textfield',
        '#title' => t('Longitude'),
        '#default_value' => '-74.00'
      ),
      'call_checkusercode' => array(
        '#type' => 'submit',
        '#value' => t('Call checkUserCode'),
      )
  );
/*
  $form['book']['getglobalstats'] =array(
    '#type' => 'fieldset',
    //'#attributes' => array('class' => array('container-inline')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'getglobalstats',
      'call_getglobalstats' => array(
        '#type' => 'submit',
        '#value' => t('Call getGlobalStats'),
      )
  );*/

  $form['book']['getupdate'] =array(
    '#type' => 'fieldset',
    //'#attributes' => array('class' => array('container-inline')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'getupdate',
      'getupdate_userId' => array(
        '#type' => 'textfield',
        '#title' => t('fbId of User'),
        '#default_value' => '0001'
      ),
      // 'get_start_reading_timeStamp' => array(
        // '#type' => 'submit',
        // '#value' => t('Call get_start_reading_timeStamp test api'),
        // '#submit' => array('get_start_reading_timeStamp_submit'),
      // ),
      // 'string_for_str_to_time' => array(
        // '#type' => 'textfield',
        // '#title' => t('strtotime()'),
        // '#description' => 'strtotime(\'-24 hour\') =>'.strtotime('-24 hour').'<br>date("Y/m/d H:i:s", strtotime(\'-25 hour\')) =>'. date("Y/m/d H:i:s", strtotime('-25 hour')),
        // '#default_value' => '-24hours',
      // ),
      // 'call_str_to_time' => array(
        // '#type' => 'submit',
        // '#value' => t('Call strtotime'),
        // '#submit' => array('call_str_to_time_submit'),
      // ),
      'getupdate_current_page' => array(
        '#type' => 'textfield',
        '#title' => t('Curent Page'),
        '#default_value' => rand(1,1000),
      ),
      'getupdate_news_request' => array(
        '#type' => 'textfield',
        '#title' => t('News->Request'),
        '#default_value' => '24',
      ),
      'call_getupdate' => array(
        '#type' => 'submit',
        '#value' => t('Call getUpdate'),
      )
  );

  $form['book']['storeuseremotions'] =array(
    '#type' => 'fieldset',
    //'#attributes' => array('class' => array('container-inline')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'storeuseremotions',
      'storeuseremotions_userId' => array(
        '#type' => 'textfield',
        '#title' => t('fbId of User'),
        '#default_value' => '0001'
      ),
      'storeuseremotions_emotion' => array(
        '#type' => 'select',
        '#title' => t('Emotion id'),
        '#options' => array(
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
        '#default_value' => 1
      ),
      'storeuseremotions_description' => array(
        '#type' => 'textfield',
        '#title' => t('Description 140 chars'),
        '#default_value' => 'My imagination from last last time'
      ),
      'call_storeuseremotions' => array(
        '#type' => 'submit',
        '#value' => t('Call storeUserEmotions'),
      )
  );


//--------------- WEBHUB --------------------
  $form['webhub'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    // '#collapsed' => TRUE,
    '#title' => 'webhub',
  );
  //+   // getactivatedreaders
  //+   // getdestructedreaders
  //+   // globalstats
  //+   // getupdates       initial
  //+   // getreaderdetails    readerId, userId
  //+   // taketime            readerId, userId
  //+   // loginregisteruser   fbId, userdata(can be empty for already registered users)

  $form['webhub']['getactivatedreaders'] =array(
    '#type' => 'fieldset',
    //'#attributes' => array('class' => array('container-inline')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'getactivatedreaders',
      'getactivatedreaders_userId' => array(
        '#type' => 'textfield',
        '#title' => t('fbId of User'),
        '#default_value' => ''
      ),
        'call_getactivatedreaders' => array(
        '#type' => 'submit',
        '#value' => t('Call getActivatedReaders'),
      )
  );
/*
  $form['webhub']['getdestructedreaders'] =array(
    '#type' => 'fieldset',
    //'#attributes' => array('class' => array('container-inline')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'getdestructedreaders',
        'call_getdestructedreaders' => array(
        '#type' => 'submit',
        '#value' => t('Call getDestructuredReaders'),
      )
  );*/

  $form['webhub']['globalstats'] =array(
    '#type' => 'fieldset',
    //'#attributes' => array('class' => array('container-inline')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'globalstats',
        'call_globalstats' => array(
        '#type' => 'submit',
        '#value' => t('Call globalStats'),
      )
  );

  $form['webhub']['getupdates'] =array(
    '#type' => 'fieldset',
    //'#attributes' => array('class' => array('container-inline')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'getupdates',
      'getupdates_news_request' => array(
        '#type' => 'textfield',
        '#title' => t('News->Request'),
        '#default_value' => '24',
      ),
      'call_getupdates' => array(
        '#type' => 'submit',
        '#value' => t('Call getUpdates'),
      )
  );
/*
  $form['webhub']['getreaderdetails'] =array(
    '#type' => 'fieldset',
    //'#attributes' => array('class' => array('container-inline')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'getreaderdetails',
      'getreaderdetails_readerId' => array(
        '#type' => 'textfield',
        '#title' => t('fbId of Reader'),
        '#default_value' => '0001'
      ),
      'getreaderdetails_userId' => array(
        '#type' => 'textfield',
        '#title' => t('fbId of User'),
        '#default_value' => '0001'
      ),
      'call_getreaderdetails' => array(
        '#type' => 'submit',
        '#value' => t('Call getReaderDetails'),
      )
  );
*/
  $form['webhub']['taketime'] =array(
    '#type' => 'fieldset',
    //'#attributes' => array('class' => array('container-inline')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'taketime',
      'taketime_readerId' => array(
        '#type' => 'textfield',
        '#title' => t('fbId of Reader'),
        '#default_value' => '0001'
      ),
      'taketime_userId' => array(
        '#type' => 'textfield',
        '#title' => t('fbId of User'),
        '#default_value' => '0001'
      ),
      'call_taketime' => array(
        '#type' => 'submit',
        '#value' => t('Call takeTime'),
      )
  );

  $form['webhub']['loginregisteruser'] = array(
    '#type' => 'fieldset',
    //'#attributes' => array('class' => array('container-inline')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'loginregisteruser',
      'fbId' => array(
        '#type' => 'textfield',
        '#title' => t('fbId for user'),
        // '#default_value' => '000'.rand(1,9)
        '#default_value' => ''
      ),
      'userData' => array(
      '#type' => 'fieldset',
      '#collapsed' => FALSE,
      '#title' => 'userData',
        'userName' => array(
          '#type' => 'textfield',
          '#title' => t('user name'),
          // '#default_value' => 'Test User name - '._nxte_api_random_string(3)
          '#default_value' => ucfirst(strtolower(_nxte_api_random_string(6, false))) . ' ' . ucfirst(strtolower(_nxte_api_random_string(10, false))) . ' (test)'
        ),
        'picture' => array(
          '#type' => 'textfield',
          '#title' => t('Picture'),
          '#default_value' => 'http://www.american.edu/uploads/profiles/large/chris_palmer_profile_11.jpg'
        ),
        // 'city' => array(
          // '#type' => 'textfield',
          // '#title' => t('Location city'),
          // '#default_value' => 'New-York'
        // ),
      ),
      'call_loginregisteruser' => array(
        '#type' => 'submit',
        '#value' => t('Call loginRegisterUser'),
      )
  );

//--------------- CODES --------------------
  $form['codes'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    // '#collapsed' => TRUE,
    '#title' => 'codes',
  );

  //+   // getbatch
  //+   // getcode     userId

  $form['codes']['getbatch'] =array(
    '#type' => 'fieldset',
    //'#attributes' => array('class' => array('container-inline')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'getbatch',
        'call_getbatch' => array(
        '#type' => 'submit',
        '#value' => t('Call getBatch'),
      )
  );

  $form['codes']['getcode'] =array(
    '#type' => 'fieldset',
    //'#attributes' => array('class' => array('container-inline')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'getcode',
      'getcode_userId' => array(
        '#type' => 'textfield',
        '#title' => t('fbId of User'),
        '#default_value' => '0001'
      ),
      'call_getcode' => array(
        '#type' => 'submit',
        '#value' => t('Call getCode'),
      )
  );



  return $form;
}


function call_str_to_time_submit(){
  dsm("call_str_to_time_submit()");
}

function get_start_reading_timeStamp_submit(){
  dsm("get_start_reading_timeStamp_submit()");
}
