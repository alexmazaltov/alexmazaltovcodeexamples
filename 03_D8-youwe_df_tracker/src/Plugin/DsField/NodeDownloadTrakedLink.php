<?php

/**
 * @file
 * Contains \Drupal\youwe_df_tracker\Plugin\DsField\NodeDownloadTrakedLink.
 */

namespace Drupal\youwe_df_tracker\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\node\NodeInterface;
//use Drupal\user\UserInterface;
//use Drupal\node\Entity\Node;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;


/**
 * Plugin that renders the link to referenced node in using custom menu path with extra functionality
 *
 * @DsField(
 *   id = "node_track_clicks_reference",
 *   title = @Translation("Download Tracked Link"),
 *   entity_type = "node",
 *   provider = "node",
 *   ui_limit = {"download|*"}
 * )
 */
class NodeDownloadTrakedLink extends DsFieldBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var $node NodeInterface */
    $node = $this->entity();

    $node_from = \Drupal::routeMatch()->getParameter('node');

    if ($node_from && $node->hasField('field_is_private')) {
      $field_is_private = $node->get('field_is_private');
      if ($field_is_private->count() > 0) {
        foreach($field_is_private->getValue() as $is_private_download) {
          if($is_private_download['value'] == 'private'){
            $link_url = Url::fromUserInput('/download/' . $node->id() . '/tracked_in/'.$node_from->id());
            $link_url->setOptions(array(
              'attributes' => array(
                'class' => array('use-ajax'),
                'data-dialog-type' => 'modal',
                'data-dialog-options' => Json::encode(array(
                  'width' => 700,
                )),
              ))
            );

            return array(
              '#markup' => \Drupal::l( $node->getTitle(), $link_url),
              '#attached' => array(
                'library' => array(
                  'core/system',
                  'core/drupal.ajax',
                ),
              ),
            );
          }
        }
      }
    }

    // Otherwise return an renderable array as link to Node download
    return array(
      '#type' => 'markup',
      '#markup' => \Drupal::l( $node->getTitle(), Url::fromUserInput('/node/' . $node->id())),
    );
  }

}