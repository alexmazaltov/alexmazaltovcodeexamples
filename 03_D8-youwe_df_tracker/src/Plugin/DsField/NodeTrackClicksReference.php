<?php

/**
 * @file
 * Contains \Drupal\youwe_df_tracker\Plugin\DsField\NodeTrackClicksReference.
 */

namespace Drupal\youwe_df_tracker\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Drupal\node\Entity\Node;
//use Drupal\Core\Link;
use Drupal\Core\Url;


/**
 * Plugin that renders the link to referenced node in using custom menu path with extra functionality
 *
 * @DsField(
 *   id = "node_track_clicks_reference",
 *   title = @Translation("Design field click tracker"),
 *   entity_type = "node",
 *   provider = "node",
 *   ui_limit = {"unit|*"}
 * )
 */
class NodeTrackClicksReference extends DsFieldBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var $node NodeInterface */
    $node = $this->entity();

    if ($node->hasField('field_design')) {
      $design_refs = $node->get('field_design');
      // if our field has data
      if ($design_refs->count() > 0) {
        $output = array();
        foreach($design_refs->getValue() as $design_ref){
          $download_title = Node::load($design_ref['target_id'])->getTitle();

          $output[] = array(
            '#markup' => t('<a href=":download_link">@title</a>',
              array(
                '@title' => $download_title,
                ':download_link' => Url::fromUserInput('/download_tracked/' . $design_ref['target_id'] . '/from/'.$node->id())->toString()
              )
            ),
            '#cache' => array(
              'tags' => $node->getCacheTags()
            ),
          );

        }
        return $output;
      }
    }

    // Otherwise return an empty array
    return array();
  }

}