<?php

/**
 * @file
 * Contains \Drupal\youwe_df_tracker\Controller\TrackerRouteController.
 */

namespace Drupal\youwe_df_tracker\Controller;

use Drupal\node\Entity\Node;
use Drupal\Core\Ajax;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller routines for page example routes.
 */
class TrackerRouteController extends ControllerBase {

  /**
   * A more complex _controller callback that takes arguments.
   *
   * This callback is mapped to the path
   * 'download_track/{download_id}/from/{from_id}'.
   *
   * The arguments in brackets are passed to this callback from_id the page URL.
   * The placeholder names "download_id" and "from_id" can have any value but should
   * match the callback method variable names; i.e. $download_id and $from_id.
   *
   * This function also demonstrates a more complex render array in the returned
   * values. Instead of rendering the HTML with theme('item_list'), content is
   * left un-rendered, and the theme function name is set using #theme. This
   * content will now be rendered as late as possible, giving more parts of the
   * system a chance to change it if necessary.
   *
   * Consult @link http://drupal.org/node/930760 Render Arrays documentation
   * @endlink for details.
   *
   * @param string $download_id
   *   A string to use, should be a number.
   * @param string $from_id
   *   Another string to use, should be a number.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   If the parameters are invalid.
   */
  public function arguments(Node $download, Node $from) {
    // Make sure you don't trust the URL to be safe!
    if ($download->getType() !== 'download') {
      // We will just show a standard "access denied" page in this case.
      throw new AccessDeniedHttpException();
    }
    $tempstore = \Drupal::service('user.private_tempstore')->get('youwe_df_tracker');
    $last_private_download_node = $tempstore->get('last_private_download_node');

    if($last_private_download_node){
      //TODO: create node and make redirection if in session already stored form submission
      $values = [
        'nid' => NULL,
        'type' => 'private_download',
        'title' => "Download [{$download->getTitle()}] from [{$from->getTitle()}]",
        'uid' => 0,
        'status' => TRUE,
        'field_download' => ['target_id'=>$download->id()],
        'field_from' => ['target_id'=>$from->id()],
        'field_user_name' => ['value'=>$last_private_download_node['name']],
        'field_last_name' => ['value'=>$last_private_download_node['last_name']],
        'field_user_email' => ['value'=>$last_private_download_node['email']],
        'field_company' => ['value'=>$last_private_download_node['company']],
      ];
      $node = \Drupal::entityManager()->getStorage('node')->create($values);
      $node->save();

      $ajax_response = new AjaxResponse();
      $ajax_response->addCommand(new RedirectCommand("/node/{$download->id()}"));

      return $ajax_response;
    }
    $render_array['header'] = array(
      '#markup' => '<p>Before download we would like to ask you to leave your details behind.</p>
                    <p>We will not ask you again until your next visit.</p>'
    );

    $form = \Drupal::formBuilder()->getForm('Drupal\youwe_df_tracker\Form\PFDUserDetailsForm', $download, $from);

    $render_array['form'] = $form;


    //return $form;
    return $render_array;
  }

}
