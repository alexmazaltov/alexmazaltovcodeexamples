<?php

/**
 * @file
 * Contains \Drupal\youwe_df_tracker\Form\PFDUserDetailsForm.
 */

namespace Drupal\youwe_df_tracker\Form;

use Drupal\Core\Ajax;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;

use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Class PFDUserDetailsForm.
 *
 * @package Drupal\youwe_df_tracker\Form
 */
class PFDUserDetailsForm extends FormBase {

  protected $downloadNode;
  protected $fromNode;

  public function storeWhatDownloadFromWhere($download_node, $from_node){
    $this->downloadNode = $download_node;
    $this->fromNode = $from_node;
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'p_f_d_user_details_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, \Drupal\node\Entity\Node $download_node = NULL, \Drupal\node\Entity\Node $from_node = NULL) {
    //Store to $form_state $download_node and $from_node
//    $form_state->setValue('download_node', $download_node);
//    $form_state->setValue('from_node', $from_node);
    $this->storeWhatDownloadFromWhere($download_node, $from_node);

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#required' => TRUE,
      '#attributes' => ['placeholder'=>$this->t('Your name')],
      '#ajax' => array(
        'callback' => array($this, 'validateNameAjax'),
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => t('Verifying name...'),
        ),
      ),
      '#suffix' => '<span class="name-valid-message"></span>'
    );
    $form['last_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Last name'),
      '#required' => TRUE,
      '#attributes' => ['placeholder'=>$this->t('Your last name')],
      '#ajax' => array(
        'callback' => array($this, 'validateLastNameAjax'),
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => t('Verifying last name...'),
        ),
      ),
      '#suffix' => '<span class="last-name-valid-message"></span>'
    );
    $form['email'] = array(
      '#type' => 'email',
      '#title' => $this->t('E-mail'),
      '#required' => TRUE,
      '#attributes' => ['placeholder'=>$this->t('Your email address')],
      '#ajax' => array(
        'callback' => array($this, 'validateEmailAjax'),
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => t('Verifying email...'),
        ),
      ),
      '#suffix' => '<span class="email-valid-message"></span>'
    );
    $form['company'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Company'),
      '#required' => TRUE,
      '#attributes' => ['placeholder'=>$this->t('Your company')],
      '#ajax' => array(
        'callback' => array($this, 'validateCompanyAjax'),
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => t('Verifying company...'),
        ),
      ),
      '#suffix' => '<span class="company-valid-message"></span>'
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Download'),
      '#ajax' => array(
        'callback' => array($this, 'ajaxSubmitFormCloseModalRedirect'),
        'event' => 'click',
        'progress' => array(
          'type' => 'throbber',
          'message' => 'Processing...',
        ),
      ),
//      '#states' => array(
//        'visible' => array(
//          ':input[name="name"]' => array('filled' => TRUE),
//          ':input[name="last_name"]' => array('filled' => TRUE),
//          ':input[name="email"]' => array('filled' => TRUE),
//          ':input[name="company"]' => array('filled' => TRUE),
//        ),
//      ),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /*
     * Have to be commented this form validation since all validation logic implemented in ajax behaviours
     *
     ///** //Validate name
    if (!$this->validateName($form, $form_state)) {
      $form_state->setErrorByName('name', $this->t('There is not a Name.'));
    }
    // Validate email.
    if (!$this->validateEmail($form, $form_state)) {
      $form_state->setErrorByName('email', $this->t('There is not valid email address.'));
    }//**/
  }

  /**
   * Ajax callback to validate the name field.
   */
  public function validateNameAjax(array &$form, FormStateInterface $form_state) {
    $valid = $this->validateName($form, $form_state);
    $response = new AjaxResponse();
    if ($valid) {
      $css = ['border' => '1px solid green'];
      $message = $this->t('Name ok.');
    }
    else {
      $css = ['border' => '1px solid red'];
      $message = $this->t('Name not valid.');
    }
    $response->addCommand(new CssCommand('[id^="edit-name"]', $css));
    $response->addCommand(new HtmlCommand('.name-valid-message', $message));
    return $response;
  }

  /**
   * Ajax callback to validate the last_name field.
   */
  public function validateLastNameAjax(array &$form, FormStateInterface $form_state) {
    $valid = $this->validateLastName($form, $form_state);
    $response = new AjaxResponse();
    if ($valid) {
      $css = ['border' => '1px solid green'];
      $message = $this->t('Last Name ok.');
    }
    else {
      $css = ['border' => '1px solid red'];
      $message = $this->t('Last Name not valid.');
    }
    $response->addCommand(new CssCommand('[id^="edit-last-name"]', $css));
    $response->addCommand(new HtmlCommand('.last-name-valid-message', $message));
    return $response;
  }
  /**
   * Ajax callback to validate the last_name field.
   */
  public function validateCompanyAjax(array &$form, FormStateInterface $form_state) {
    $valid = $this->validateCompany($form, $form_state);
    $response = new AjaxResponse();
    if ($valid) {
      $css = ['border' => '1px solid green'];
      $message = $this->t('Company ok.');
    }
    else {
      $css = ['border' => '1px solid red'];
      $message = $this->t('Company not valid.');
    }
    $response->addCommand(new CssCommand('[id^="edit-company"]', $css));
    $response->addCommand(new HtmlCommand('.company-valid-message', $message));
    return $response;
  }

  protected function validateName(array &$form, FormStateInterface $form_state) {
    return !empty(trim($form_state->getValue('name')));
  }
  protected function validateLastName(array &$form, FormStateInterface $form_state) {
    return !empty(trim($form_state->getValue('last_name')));
  }
  protected function validateCompany(array &$form, FormStateInterface $form_state) {
    return !empty(trim($form_state->getValue('company')));
  }

  /**
   * Ajax callback to validate the email field.
   */
  public function validateEmailAjax(array &$form, FormStateInterface $form_state) {
    $valid = $this->validateEmail($form, $form_state);
    $response = new AjaxResponse();
    if ($valid) {
      $css = ['border' => '1px solid green'];
      $message = $this->t('Email ok.');
    }
    else {
      $css = ['border' => '1px solid red'];
      $message = $this->t('Email not valid.');
    }
    $response->addCommand(new CssCommand('[id^="edit-email"]', $css));
    $response->addCommand(new HtmlCommand('.email-valid-message', $message));
    return $response;
  }

  /**
   * Validates that the email field is correct.
   */
  protected function validateEmail(array &$form, FormStateInterface $form_state) {
    return $this->isValidEmail($form_state->getValue('email'));
  }

  /**
   * Validates that the string $email is correct email.
   */
  protected function isValidEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false
            /*&& preg_match('/@.+\./', $email)*/;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  //    drupal_set_message('Nothing Submitted. Just an Example.');
  //    $form_state->setValue('submitted', TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxSubmitFormCloseModalRedirect(array &$form, FormStateInterface $form_state) {
//    $submitted = $form_state->getValue('submitted');
    $errors = $form_state->getErrors();
    if($errors){
      unset($_SESSION['messages']['error']);
      foreach($errors as $errorField => $confing){
        switch($errorField){
          case 'name':
            return $this->validateNameAjax($form, $form_state);
            break;
          case 'last_name':
            return $this->validateLastNameAjax($form, $form_state);
            break;
          case 'email':
            return $this->validateEmailAjax($form, $form_state);
            break;
          case 'company':
            return $this->validateCompanyAjax($form, $form_state);
            break;
        }
      }
      /*$valid_name = $this->validateName($form, $form_state);
      if (!$valid_name) {
        return $this->validateNameAjax($form, $form_state);
      }
      $valid_last_name = $this->validateLastName($form, $form_state);
      if (!$valid_last_name) {
        return $this->validateLastNameAjax($form, $form_state);
      }
      $valid_email = $this->validateEmail($form, $form_state);
      if (!$valid_email) {
        return $this->validateEmailAjax($form, $form_state);
      }
      $valid_company = $this->validateCompany($form, $form_state);
      if (!$valid_company) {
        return $this->validateCompanyAjax($form, $form_state);
      }*/
    }
    else{
      $values = array(
        'nid' => NULL,
        'type' => 'private_download',
        'title' => "Download [{$this->downloadNode->getTitle()}] from [{$this->fromNode->getTitle()}]",
        'uid' => 0,
        'status' => TRUE,
        'field_download' => ['target_id'=>$this->downloadNode->id()],
        'field_from' => ['target_id'=>$this->fromNode->id()],
        'field_user_name' => ['value'=>$form_state->getValue('name')],
        'field_last_name' => ['value'=>$form_state->getValue('last_name')],
        'field_user_email' => ['value'=>$form_state->getValue('email')],
        'field_company' => ['value'=>$form_state->getValue('company')],
      );
      $node = \Drupal::entityManager()->getStorage('node')->create($values);
      $node->save();

      $last_private_download_node = [
        'name' => $form_state->getValue('name'),
        'last_name' => $form_state->getValue('last_name'),
        'email' => $form_state->getValue('email'),
        'company' => $form_state->getValue('company'),
      ];
      $tempstore = \Drupal::service('user.private_tempstore')->get('youwe_df_tracker');
      $tempstore->set('last_private_download_node', $last_private_download_node);

      $ajax_response = new AjaxResponse();
      $ajax_response->addCommand(new CloseModalDialogCommand());
      $ajax_response->addCommand(new RedirectCommand("/node/{$this->downloadNode->id()}"));

      return $ajax_response;
    }

  }

  private function ajaxSubmitFormClearAllErrors(){
    $response = new AjaxResponse();
      $css = ['border' => '1px solid #ccc'];
      $message = '';
    $response->addCommand(new CssCommand('[id^="edit-name"]', $css));
    $response->addCommand(new CssCommand('[id^="edit-last-name"]', $css));
    $response->addCommand(new CssCommand('[id^="edit-email"]', $css));
    $response->addCommand(new CssCommand('[id^="edit-company"]', $css));
    $response->addCommand(new HtmlCommand('.name-valid-message', $message));
    return $response;
  }

}
