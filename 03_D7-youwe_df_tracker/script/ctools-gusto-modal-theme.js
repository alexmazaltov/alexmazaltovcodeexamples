/**
 * Provide the HTML to create the modal dialog.
 */
Drupal.theme.prototype.CToolsGustoModal = function () {
  var html_default = '';
  html_default += '  <div id="ctools-modal">';
  html_default += '    <div class="ctools-modal-content">'; // panels-modal-content
  html_default += '      <div class="modal-header">';
  html_default += '        <a class="close" href="#">';
  html_default +=            Drupal.CTools.Modal.currentSettings.closeText + Drupal.CTools.Modal.currentSettings.closeImage;
  html_default += '        </a>';
  html_default += '        <span id="modal-title" class="modal-title">&nbsp;</span>';
  html_default += '      </div>';
  html_default += '      <div id="modal-content" class="modal-content">';
  html_default += '      </div>';
  html_default += '    </div>';
  html_default += '  </div>';

  return html_default;

};