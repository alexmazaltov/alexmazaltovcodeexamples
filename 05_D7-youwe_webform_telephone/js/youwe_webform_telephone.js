(function ($) {
    Drupal.behaviors.youweWebformTelephone = {
        attach: function (context, settings) {
            if($('#edit-submitted-email-phone-phone', context).length){
                $('#edit-submitted-email-phone-phone', context).intlTelInput({
                    defaultCountry: Drupal.settings.telephone_lang,
                    allowExtensions:"true",
                    autoHideDialCode:true,
                    nationalMode:false,
                    preferredCountries:["nl", "gb", "be", "fr"],
                    utilsScript: Drupal.settings.youwe_webform_telephone.path_js+"utils.js",
                });
                $('#edit-submitted-email-phone-phone').superLabels({
                    labelLeft:"200px",
                });
            }
        }
    };
})(jQuery);
