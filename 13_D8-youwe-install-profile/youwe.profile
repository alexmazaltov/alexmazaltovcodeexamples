<?php

/**
 * @file
 * Enables modules and site configuration for a Youwe installation.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\youwe\Form\ExtensionSelectForm;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function youwe_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  // Default site email noreply@vardot.com .
  $form['site_information']['site_mail']['#default_value'] = 'developer@youwe.nl';

  // Default user 1 username should be 'root'.
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['admin_account']['account']['mail']['#default_value'] = 'developer@youwe.com';

  // Country/language settings.
  $form['regional_settings']['site_default_country']['#default_value'] = 'NL';
  $form['regional_settings']['date_default_timezone']['#default_value'] = 'Europe/Amsterdam';
}

/**
 * Implements hook_install_tasks().
 */
function youwe_install_tasks(&$install_state) {
  return array(
    'youwe_select_extensions' => array(
      'display_name' => t('Choose extensions'),
      'display' => TRUE,
      'type' => 'form',
      'function' => ExtensionSelectForm::class,
    ),
    'youwe_install_extensions' => array(
      'display_name' => t('Install extensions'),
      'display' => TRUE,
      'type' => 'batch',
    ),
  );
}

/**
 * Install task callback; prepares a batch job to install Youwe extensions.
 *
 * @param array $install_state
 *   The current install state.
 *
 * @return array
 *   The batch job definition.
 */
function youwe_install_extensions(array &$install_state) {
  $batch = array();
  foreach ($install_state['youwe']['extensions'] as $module) {
    $batch['operations'][] = ['youwe_install_module', (array) $module];
  }
  return $batch;
}

/**
 * Batch API callback. Installs a module.
 *
 * @param string|array $module
 *   The name(s) of the module(s) to install.
 */
function youwe_install_module($module) {
  \Drupal::service('module_installer')->install((array) $module);
}