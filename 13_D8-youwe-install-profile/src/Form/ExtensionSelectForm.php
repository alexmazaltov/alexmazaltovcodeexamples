<?php

namespace Drupal\youwe\Form;

use Drupal\Core\Extension\ExtensionDiscovery;
use Drupal\Core\Extension\InfoParserInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\youwe\Form\ExtensionFormHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Yaml;

/**
 * Defines a form for selecting which Youwe extensions to install.
 */
class ExtensionSelectForm extends FormBase {

  /**
   * The Drupal application root.
   *
   * @var string
   */
  protected $root;

  /**
   * The info parser service.
   *
   * @var \Drupal\Core\Extension\InfoParserInterface
   */
  protected $infoParser;

  /**
   * The form helper.
   *
   * @var \Drupal\youwe\Form\FormHelper
   */
  protected $formHelper;

  /**
   * ExtensionSelectForm constructor.
   *
   * @param string $root
   *   The Drupal application root.
   * @param \Drupal\Core\Extension\InfoParserInterface $info_parser
   *   The info parser service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translator
   *   The string translation service.
   * @param \Drupal\youwe\ExtensionFormHelper $form_helper
   *   The form helper.
   */
  public function __construct($root, InfoParserInterface $info_parser, TranslationInterface $translator, ExtensionFormHelper $form_helper) {
    $this->root = $root;
    $this->infoParser = $info_parser;
    $this->stringTranslation = $translator;
    $this->formHelper = $form_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('app.root'),
      $container->get('info_parser'),
      $container->get('string_translation'),
      $container->get('youwe.extension_form_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'youwe_select_extensions';
  }

  /**
   * Get info for each of Youwe Extension.
   */
  protected function getExtensionsInfo() {
    // Init.
    $extensions_path = "{$this->root}/profiles/youwe/youwe.extensions.yml";

    // Extensions yml file does not exists.
    if (!file_exists($extensions_path)) {
      return array();
    }

    // Get extensions form the yml file.
    $info = file_get_contents($extensions_path);
    $extensions_yml = Yaml::decode($info);
    // Init.
    $extensions = [];
    // Exchange keys with values in the array.
    if (isset($extensions_yml['youwe_extensions'])) {
      $extensions = array_flip($extensions_yml['youwe_extensions']);
    }

    // Init.
    $extension_discovery = new ExtensionDiscovery($this->root);
    // Get all modules.
    $all_modules = $extension_discovery->scan('module');
    // Filter out the youwe extensions.
    $extra_components = array_intersect_key($all_modules, $extensions);
    // Init.
    $extensions_info = [];

    // Iterate trough extensions and build the extensions info array.
    foreach ($extra_components as $key => $extra_component) {
      $extensions_info[$key] = $this->infoParser->parse($extra_component->getPathname());
    }

    return $extensions_info;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, array &$install_state = NULL) {
    $form['#title'] = $this->t('Choose Extensions');
    $form['extensions_introduction'] = [
      '#weight' => -1,
      '#prefix' => '<p>',
      '#markup' => $this->t("Select extensions, so that they will be installed."),
      '#suffix' => '</p>',
    ];
    $form['extensions'] = [
      '#title' => $this->t('Youwe extensions'),
      '#type' => 'fieldset'
    ];
    $form['actions'] = [
      'continue' => [
        '#type' => 'submit',
        '#value' => $this->t('Install'),
      ],
      '#type' => 'actions',
      '#weight' => 5,
    ];

    if ($extensions = $this->getExtensionsInfo()) {
      foreach ($extensions as $key => $info) {
        $form['extensions'][$key] = [
          '#type' => 'checkbox',
          '#title' => "{$info['name']} ({$info['version']})",
          '#description' => $info['description'],
        ];
      }
    }
    else {
      $form['extensions']['#description'] = $this->t('No extensions found.');
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($extensions = $this->getExtensionsInfo()) {
      foreach ($extensions as $key => $info) {
        $extensions[$key] = $form_state->getValue($key);
      }
      $extensions = array_filter($extensions);
    }
    $GLOBALS['install_state']['youwe']['extensions'] = array_keys($extensions);
  }

}
