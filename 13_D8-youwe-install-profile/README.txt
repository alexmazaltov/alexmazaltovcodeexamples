-- INSTALLATION --

Install Youwe distribution with:

commandline:
drush si youwe --db-url=mysql://root:pass@localhost:port/dbname --account-pass=youwe

browser:
url/install.php

-- ADDING EXTENSIONS --

Add feature (optional profile extension) in the following dir:
profiles/youwe/modules/features

Add machine name of the feature in the following file to make the extension
available as option in the install process:
profiles/youwe/youwe.extensions.yml