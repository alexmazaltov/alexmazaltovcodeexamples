<?php

/**
 * Hook action info
 * Declare new buk operation to send flow step to conversations
 * @return array
 */
function bot_builder_action_info() {
    return array(
    'bot_builder_bb_send_flow_step' => array(
      'type' => 'node',
      'label' => t('Send flow step to conversations'),
      'behavior' => array('bb_send_flow_step'),
      'configurable' => TRUE,
      'vbo_configurable' => TRUE,
      'triggers' => array('any'),
    ),
  );
}

/**
 * Form to select which step flow node should be sent to conversations
 * selected on previous step in views bulk operation
 * @param $settings
 * @param $form_state
 * @return array
 */
function bot_builder_bb_send_flow_step_form($settings, &$form_state) {
    $form = array();
    global $user;

    // Get all flow step nodes created by current user
    $q = db_select('node', 'n');
    $q->fields('n', ['nid', 'title']);
    $q->condition('n.type', 'bb_flow_step_node');
    // if user is administrator do not apply filter by owner
    if(!in_array('administrator', $user->roles)){
      $q->condition('n.uid', $user->uid);
    }
    $q->condition('n.status', true);
    $flow_steps_options = $q->execute()->fetchAllKeyed();

    $form['flow_step'] = array(
        '#type' => 'select',
        '#title' => t('Choose Step-flow that should be send to conversations'),
        '#options' => $flow_steps_options,
        '#required' => TRUE,
        '#default_value' => isset($settings['settings']['flow_step']) ? $settings['settings']['flow_step'] : '',
    );
    return $form;
}

/**
 * Submit function in conditional bulk operation step
 * should return array with selected data
 * @param $form
 * @param $form_state
 * @return array
 */
function bot_builder_bb_send_flow_step_submit($form, $form_state) {
  $return = array();
  $return['flow_step'] = $form_state['values']['flow_step'];
  return $return; //Note, return value here must be an array.
}

/**
 * Worker callback for new bulk operation
 * @param $node
 * @param $context
 */
function bot_builder_bb_send_flow_step(&$node, $context){
  $conv_node_w = entity_metadata_wrapper('node', $node);
  $user_fbid = $conv_node_w->language(LANGUAGE_NONE)->field_bb_fbuid->value();
  $m['lng'] = LANGUAGE_NONE;
  $message['sender']['id'] = $user_fbid;
  $node_endpoint = $conv_node_w->language(LANGUAGE_NONE)->field_bb_webhook->value();
  $bot = new FbBotApp($node_endpoint->field_bb_access_token[LANGUAGE_NONE][0]['value']);
  $fs_node = node_load($context['flow_step']);
  $fs_node_w = entity_metadata_wrapper('node', $fs_node);
  if ($fs_node_w->language(LANGUAGE_NONE)->field_bb_step->value()){
    // TODO: In future implement conditional multiple steps
    //foreach ($fs_node_w->language(LANGUAGE_NONE)->field_bb_step->value()){
    // TODO: send conditional custom step with custom extra flow e.g. collect value
    //}
    $step_pe_id = $fs_node_w->language(LANGUAGE_NONE)->field_bb_step->value()->item_id;
    $response = bb_postback_playload($step_pe_id, $node, $message, $bot, $m);
  }
  if (!isset($response)) {
    // Nothing were sent because flow step node is not properly created
    //TODO: validation for node save flow_step nodes
  }
}