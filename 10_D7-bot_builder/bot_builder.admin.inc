<?php

/*** SETTINGS ***/
function bot_builder_settings_form() {
	$form = array(
		'truncate' => array(
			'#type' => 'fieldset',
			'#title' => t('Truncate database tables'),
			'messages_sent' => array(
				'#type' => 'submit',
				'#value' => t('Empty Mesages Sent table'),
				'#submit' => array('bot_builder_settings_form_submit_messages_sent')
			),
		),
		'bot_builder_campaign_status' => array(
			'#type' => 'select',
			'#title' => t('Campaign status'),
			'#options' => array(
				0 => t('Development'),
				1 => t('Production')
			),
			'#default_value' => variable_get('bot_builder_campaign_status', 0)
		),
		'bot_builder_message_not_rec' => array(
			'#type' => 'textfield',
			'#required' => TRUE,
			'#title' => t('Message for not recognized text'),
			'#default_value' => variable_get('bot_builder_message_not_rec', t('This command is not recognized'))
		),
		'bot_builder_faq_text' => array(
			'#type' => 'textfield',
			'#required' => TRUE,
			'#title' => t('Text Message for FAQ'),
			'#default_value' => variable_get('bot_builder_faq_text', t('You can get answers for Frequently Asked Questions by clicking on the button below...'))
		),
      'bot_builder_live_chat_text' => array(
			'#type' => 'textfield',
			'#required' => TRUE,
			'#title' => t('Text Message for Live Chat request'),
			'#default_value' => variable_get('bot_builder_live_chat_text', t("Manager will contact you as soon as possible.\nYou can continue interactive dialog."))
		),
		'actions' => array(
			'#type' => 'actions',
			'submit' => array(
				'#type' => 'submit',
				'#value' => t('Submit'),
				'#submit' => array('bot_builder_settings_form_submit_default')
			)
		)
	);	
	
	return $form;	
}

function bot_builder_settings_form_submit_default($form, &$form_state) {
	$v = &$form_state['values'];
	
	if (user_access('administer site configuration')) {
		variable_set('bot_builder_campaign_status', $v['bot_builder_campaign_status']);
		variable_set('bot_builder_message_not_rec', $v['bot_builder_message_not_rec']);
		variable_set('bot_builder_faq_text', $v['bot_builder_faq_text']);
		variable_set('bot_builder_live_chat_text', $v['bot_builder_live_chat_text']);
		drupal_set_message(t('Settings updated'));
	}
	else {
		drupal_set_message(t('You do not have the rights to perform this operation'), 'error');	
	}

}

function bot_builder_settings_form_submit_messages_sent($form, &$form_state) {
	$form_state['redirect'] = 'admin/config/system/bot_builder/delete/messages_sent';
}

function bot_builder_empty_messages_sent_table($form, &$form_state) {
	$form['#submit'][] = 'bot_builder_empty_messages_sent_table_submit';
	return confirm_form($form, t('Are you sure you want to truncate the Messages Sent table?'), 'admin/config/system/bot_builder');
}

function bot_builder_empty_messages_sent_table_submit($form, &$form_state) {
	db_truncate('bot_builder_messages_sent')->execute();
	drupal_set_message(t('The Messages Sent table was truncated'));
	$form_state['redirect'] = 'admin/config/system/bot_builder';
}