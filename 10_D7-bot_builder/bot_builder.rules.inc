<?php

/**
 * @file
 * Default rule configurations.
 */

/**
 * Implement hook_rules_action_info()

 * Declare any meta data about actions for Rules
 */

function bot_builder_rules_action_info() {
  $actions = array(
    'bot_builder_action_send_flowstep' => array(
      'group' => t('Drupal Bot Builder'),
      'label' => t('Send Flow-step Playload to conversation'),
      'parameter' => array(
        'bb_flow_step_node' => array(
          'type' => 'node',
          'label' => t('Referenced flow step'),
        ),
        'bb_conv_node' => array(
          'type' => 'node',
          'label' => t('Conversation node'),
        ),
      ),
    ),
  );

  return $actions;
}

function bot_builder_action_send_flowstep($bb_flow_step_node, $bb_conv_node){
  drupal_set_message(t('Flow step  @flowstep_node, sent to @conv_node', array('@flowstep_node' => $bb_flow_step_node->title, '@conv_node' => $bb_conv_node->title)));
}