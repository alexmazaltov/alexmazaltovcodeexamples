<?php
/**
 * @file
 * dbb.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function dbb_user_default_roles() {
  $roles = array();

  // Exported role: bot owner.
  $roles['bot owner'] = array(
    'name' => 'bot owner',
    'weight' => 3,
  );

  return $roles;
}
