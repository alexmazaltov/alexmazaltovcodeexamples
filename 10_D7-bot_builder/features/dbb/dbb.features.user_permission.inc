<?php
/**
 * @file
 * dbb.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dbb_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer themes'.
  $permissions['administer themes'] = array(
    'name' => 'administer themes',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'system',
  );

  // Exported permission: 'create bb_bulk_messaging content'.
  $permissions['create bb_bulk_messaging content'] = array(
    'name' => 'create bb_bulk_messaging content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create bb_convers content'.
  $permissions['create bb_convers content'] = array(
    'name' => 'create bb_convers content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create bb_custom_text content'.
  $permissions['create bb_custom_text content'] = array(
    'name' => 'create bb_custom_text content',
    'roles' => array(
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create bb_endpoint content'.
  $permissions['create bb_endpoint content'] = array(
    'name' => 'create bb_endpoint content',
    'roles' => array(
      'administrator' => 'administrator',
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create bb_flow_step_node content'.
  $permissions['create bb_flow_step_node content'] = array(
    'name' => 'create bb_flow_step_node content',
    'roles' => array(
      'administrator' => 'administrator',
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create bb_logging content'.
  $permissions['create bb_logging content'] = array(
    'name' => 'create bb_logging content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create bb_texts_group content'.
  $permissions['create bb_texts_group content'] = array(
    'name' => 'create bb_texts_group content',
    'roles' => array(
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bb_bulk_messaging content'.
  $permissions['delete any bb_bulk_messaging content'] = array(
    'name' => 'delete any bb_bulk_messaging content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bb_convers content'.
  $permissions['delete any bb_convers content'] = array(
    'name' => 'delete any bb_convers content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bb_custom_text content'.
  $permissions['delete any bb_custom_text content'] = array(
    'name' => 'delete any bb_custom_text content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any bb_endpoint content'.
  $permissions['delete any bb_endpoint content'] = array(
    'name' => 'delete any bb_endpoint content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bb_flow_step_node content'.
  $permissions['delete any bb_flow_step_node content'] = array(
    'name' => 'delete any bb_flow_step_node content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bb_logging content'.
  $permissions['delete any bb_logging content'] = array(
    'name' => 'delete any bb_logging content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bb_texts_group content'.
  $permissions['delete any bb_texts_group content'] = array(
    'name' => 'delete any bb_texts_group content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own bb_bulk_messaging content'.
  $permissions['delete own bb_bulk_messaging content'] = array(
    'name' => 'delete own bb_bulk_messaging content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bb_convers content'.
  $permissions['delete own bb_convers content'] = array(
    'name' => 'delete own bb_convers content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bb_custom_text content'.
  $permissions['delete own bb_custom_text content'] = array(
    'name' => 'delete own bb_custom_text content',
    'roles' => array(
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bb_endpoint content'.
  $permissions['delete own bb_endpoint content'] = array(
    'name' => 'delete own bb_endpoint content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bb_flow_step_node content'.
  $permissions['delete own bb_flow_step_node content'] = array(
    'name' => 'delete own bb_flow_step_node content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bb_logging content'.
  $permissions['delete own bb_logging content'] = array(
    'name' => 'delete own bb_logging content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own bb_texts_group content'.
  $permissions['delete own bb_texts_group content'] = array(
    'name' => 'delete own bb_texts_group content',
    'roles' => array(
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bb_bulk_messaging content'.
  $permissions['edit any bb_bulk_messaging content'] = array(
    'name' => 'edit any bb_bulk_messaging content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bb_convers content'.
  $permissions['edit any bb_convers content'] = array(
    'name' => 'edit any bb_convers content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bb_custom_text content'.
  $permissions['edit any bb_custom_text content'] = array(
    'name' => 'edit any bb_custom_text content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any bb_endpoint content'.
  $permissions['edit any bb_endpoint content'] = array(
    'name' => 'edit any bb_endpoint content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bb_flow_step_node content'.
  $permissions['edit any bb_flow_step_node content'] = array(
    'name' => 'edit any bb_flow_step_node content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bb_logging content'.
  $permissions['edit any bb_logging content'] = array(
    'name' => 'edit any bb_logging content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bb_texts_group content'.
  $permissions['edit any bb_texts_group content'] = array(
    'name' => 'edit any bb_texts_group content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own bb_bulk_messaging content'.
  $permissions['edit own bb_bulk_messaging content'] = array(
    'name' => 'edit own bb_bulk_messaging content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bb_convers content'.
  $permissions['edit own bb_convers content'] = array(
    'name' => 'edit own bb_convers content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bb_custom_text content'.
  $permissions['edit own bb_custom_text content'] = array(
    'name' => 'edit own bb_custom_text content',
    'roles' => array(
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bb_endpoint content'.
  $permissions['edit own bb_endpoint content'] = array(
    'name' => 'edit own bb_endpoint content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bb_flow_step_node content'.
  $permissions['edit own bb_flow_step_node content'] = array(
    'name' => 'edit own bb_flow_step_node content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bb_logging content'.
  $permissions['edit own bb_logging content'] = array(
    'name' => 'edit own bb_logging content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own bb_texts_group content'.
  $permissions['edit own bb_texts_group content'] = array(
    'name' => 'edit own bb_texts_group content',
    'roles' => array(
      'bot owner' => 'bot owner',
    ),
    'module' => 'node',
  );

  return $permissions;
}
