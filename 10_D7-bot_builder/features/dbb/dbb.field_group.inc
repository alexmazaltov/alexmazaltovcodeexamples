<?php
/**
 * @file
 * dbb.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function dbb_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_bb_artin_v|node|bb_endpoint|default';
  $field_group->group_name = 'group_bb_artin_v';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bb_endpoint';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_bb_artin';
  $field_group->data = array(
    'label' => 'Sub sections',
    'weight' => '31',
    'children' => array(
      0 => 'group_bb_unrecognized',
      1 => 'group_bb_predefined',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-bb-artin-v field-group-tabs',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_bb_artin_v|node|bb_endpoint|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_bb_artin|node|bb_endpoint|default';
  $field_group->group_name = 'group_bb_artin';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bb_endpoint';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_settings';
  $field_group->data = array(
    'label' => 'Custom text flow',
    'weight' => '3',
    'children' => array(
      0 => 'group_bb_artin_v',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Custom text flow',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-bb-artin field-group-tab',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_bb_artin|node|bb_endpoint|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_bb_artin|node|bb_endpoint|form';
  $field_group->group_name = 'group_bb_artin';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bb_endpoint';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_settings';
  $field_group->data = array(
    'label' => 'Custom text flow',
    'weight' => '15',
    'children' => array(
      0 => 'field_bb_artin',
      1 => 'field_bb_texts_group',
      2 => 'field_bb_custtxt_ref',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-bb-artin field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_bb_artin|node|bb_endpoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_bb_predefined|node|bb_endpoint|default';
  $field_group->group_name = 'group_bb_predefined';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bb_endpoint';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_bb_artin_v';
  $field_group->data = array(
    'label' => 'Predefined texts',
    'weight' => '32',
    'children' => array(
      0 => 'field_bb_texts_group',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-bb-predefined field-group-tab',
      ),
    ),
  );
  $field_groups['group_bb_predefined|node|bb_endpoint|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_bb_unrecognized|node|bb_endpoint|default';
  $field_group->group_name = 'group_bb_unrecognized';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bb_endpoint';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_bb_artin_v';
  $field_group->data = array(
    'label' => 'Unrecognized texts',
    'weight' => '33',
    'children' => array(
      0 => 'field_bb_artin',
      1 => 'field_bb_custtxt_ref',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-bb-unrecognized field-group-tab',
      ),
    ),
  );
  $field_groups['group_bb_unrecognized|node|bb_endpoint|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_campaign_status|node|bb_endpoint|form';
  $field_group->group_name = 'group_campaign_status';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bb_endpoint';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_settings';
  $field_group->data = array(
    'label' => 'Campaign',
    'weight' => '12',
    'children' => array(
      0 => 'field_bb_debug_mode',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-campaign-status field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_campaign_status|node|bb_endpoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dialog|node|bb_endpoint|default';
  $field_group->group_name = 'group_dialog';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bb_endpoint';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_settings';
  $field_group->data = array(
    'label' => 'Interactive dialog',
    'weight' => '2',
    'children' => array(
      0 => 'field_bb_flow_step',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Interactive dialog',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-dialog field-group-tab',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_dialog|node|bb_endpoint|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dialog|node|bb_endpoint|form';
  $field_group->group_name = 'group_dialog';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bb_endpoint';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_settings';
  $field_group->data = array(
    'label' => 'Interactive dialog',
    'weight' => '14',
    'children' => array(
      0 => 'field_bb_flow_step',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-dialog field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_dialog|node|bb_endpoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_fb_access|node|bb_endpoint|form';
  $field_group->group_name = 'group_fb_access';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bb_endpoint';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_settings';
  $field_group->data = array(
    'label' => 'Facebook access',
    'weight' => '13',
    'children' => array(
      0 => 'field_bb_access_token',
      1 => 'field_bb_verify_token',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-fb-access field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_fb_access|node|bb_endpoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_mass_messaging|node|bb_endpoint|default';
  $field_group->group_name = 'group_mass_messaging';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bb_endpoint';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_settings';
  $field_group->data = array(
    'label' => 'Bulk Messaging',
    'weight' => '4',
    'children' => array(
      0 => 'field_bb_bulk_messaging',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Bulk Messaging',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-bulk-messaging field-group-tab',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_mass_messaging|node|bb_endpoint|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_mass_messaging|node|bb_endpoint|form';
  $field_group->group_name = 'group_mass_messaging';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bb_endpoint';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_settings';
  $field_group->data = array(
    'label' => 'Bulk Messaging',
    'weight' => '17',
    'children' => array(
      0 => 'field_bb_bulk_messaging',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-bulk-messaging field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_mass_messaging|node|bb_endpoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_settings|node|bb_endpoint|default';
  $field_group->group_name = 'group_settings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bb_endpoint';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Settings',
    'weight' => '1',
    'children' => array(
      0 => 'group_bb_artin',
      1 => 'group_dialog',
      2 => 'group_mass_messaging',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-settings field-group-htabs',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_settings|node|bb_endpoint|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_settings|node|bb_endpoint|form';
  $field_group->group_name = 'group_settings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bb_endpoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Settings',
    'weight' => '2',
    'children' => array(
      0 => 'group_bb_artin',
      1 => 'group_campaign_status',
      2 => 'group_dialog',
      3 => 'group_fb_access',
      4 => 'group_mass_messaging',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'label' => 'Settings',
      'instance_settings' => array(
        'id' => 'settings',
        'classes' => 'group-settings field-group-tabs',
      ),
    ),
  );
  $field_groups['group_settings|node|bb_endpoint|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Bulk Messaging');
  t('Campaign');
  t('Custom text flow');
  t('Facebook access');
  t('Interactive dialog');
  t('Predefined texts');
  t('Settings');
  t('Sub sections');
  t('Unrecognized texts');

  return $field_groups;
}
