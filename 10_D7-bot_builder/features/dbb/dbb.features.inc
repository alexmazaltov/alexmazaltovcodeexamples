<?php
/**
 * @file
 * dbb.features.inc
 */

/**
 * Implements hook_default_cer().
 */
function dbb_default_cer() {
  $items = array();
  $items['node:bb_endpoint:field_bb_texts_group*node:bb_texts_group:field_bb_webhook'] = entity_import('cer', '{
    "cer_bidirectional" : { "und" : [ { "value" : "1" } ] },
    "cer_enabled" : { "und" : [ { "value" : "1" } ] },
    "cer_left" : { "und" : [ { "path" : "node:bb_endpoint:field_bb_texts_group" } ] },
    "cer_right" : { "und" : [ { "path" : "node:bb_texts_group:field_bb_webhook" } ] },
    "cer_weight" : { "und" : [ { "value" : "0" } ] },
    "identifier" : "node:bb_endpoint:field_bb_texts_group*node:bb_texts_group:field_bb_webhook",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function dbb_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function dbb_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function dbb_node_info() {
  $items = array(
    'bb_bulk_messaging' => array(
      'name' => t('[Bulk messaging]'),
      'base' => 'node_content',
      'description' => t('Referenced node with additional condition (date, fb-user filters e.t.c.)'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'bb_convers' => array(
      'name' => t('[Conversation]'),
      'base' => 'node_content',
      'description' => t('Conversation node - to store all data about interactive dialog state with specific fb user'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'bb_custom_text' => array(
      'name' => t('[Custom text]'),
      'base' => 'node_content',
      'description' => t('Custom text messages received from users'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'bb_endpoint' => array(
      'name' => t('[Endpoint]'),
      'base' => 'node_content',
      'description' => t('Webhook bb_endpoint page'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'bb_flow_step_node' => array(
      'name' => t('[Flow step node]'),
      'base' => 'node_content',
      'description' => t('Content type to wrap flow of field_bb_step in node bb_endpoint'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'bb_logging' => array(
      'name' => t('[Logging]'),
      'base' => 'node_content',
      'description' => t('Technical data content type that should be deleted with cron daily or weekly.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'bb_texts_group' => array(
      'name' => t('[Texts group]'),
      'base' => 'node_content',
      'description' => t('Predefined texts for answering with special flow'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function dbb_paragraphs_info() {
  $items = array(
    'bb_btn_lnk' => array(
      'name' => '[Button Link]',
      'bundle' => 'bb_btn_lnk',
      'locked' => '1',
    ),
    'bb_btn_postback' => array(
      'name' => '[Button PostBack]',
      'bundle' => 'bb_btn_postback',
      'locked' => '1',
    ),
    'bb_gen_msg_elem' => array(
      'name' => '[Generic Message Element]',
      'bundle' => 'bb_gen_msg_elem',
      'locked' => '1',
    ),
    'bb_image_msg' => array(
      'name' => '[Image]',
      'bundle' => 'bb_image_msg',
      'locked' => '1',
    ),
    'bb_msg_btn' => array(
      'name' => '[Message-Button]',
      'bundle' => 'bb_msg_btn',
      'locked' => '1',
    ),
    'bb_msg_gen' => array(
      'name' => '[Message-Generic]',
      'bundle' => 'bb_msg_gen',
      'locked' => '1',
    ),
    'bb_msg_receipt' => array(
      'name' => '[Message-Receipt]',
      'bundle' => 'bb_msg_receipt',
      'locked' => '1',
    ),
    'bb_node_flow_step' => array(
      'name' => '[Node Flow Step]',
      'bundle' => 'bb_node_flow_step',
      'locked' => '1',
    ),
    'bb_structured_msg' => array(
      'name' => '[Structured Message]',
      'bundle' => 'bb_structured_msg',
      'locked' => '1',
    ),
    'bb_video_msg' => array(
      'name' => '[Video]',
      'bundle' => 'bb_video_msg',
      'locked' => '1',
    ),
    'message' => array(
      'name' => '[Naked Message]',
      'bundle' => 'message',
      'locked' => '1',
    ),
    'step' => array(
      'name' => '[Step]',
      'bundle' => 'step',
      'locked' => '1',
    ),
  );
  return $items;
}
