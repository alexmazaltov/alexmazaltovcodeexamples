<?php
/**
 * @file
 * dbb.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function dbb_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|bb_custom_text|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'bb_custom_text';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_bb_message_txt',
      ),
    ),
    'fields' => array(
      'field_bb_message_txt' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|bb_custom_text|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|bb_endpoint|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'bb_endpoint';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_bb_debug_mode',
        1 => 'group_settings',
        2 => 'group_dialog',
        3 => 'field_bb_flow_step',
        4 => 'group_bb_artin',
        5 => 'group_mass_messaging',
        6 => 'field_bb_bulk_messaging',
        7 => 'field_bb_artin',
        8 => 'field_bb_custtxt_ref',
        9 => 'field_bb_texts_group',
        10 => 'group_bb_artin_v',
        11 => 'group_bb_predefined',
        12 => 'group_bb_unrecognized',
      ),
    ),
    'fields' => array(
      'field_bb_debug_mode' => 'ds_content',
      'group_settings' => 'ds_content',
      'group_dialog' => 'ds_content',
      'field_bb_flow_step' => 'ds_content',
      'group_bb_artin' => 'ds_content',
      'group_mass_messaging' => 'ds_content',
      'field_bb_bulk_messaging' => 'ds_content',
      'field_bb_artin' => 'ds_content',
      'field_bb_custtxt_ref' => 'ds_content',
      'field_bb_texts_group' => 'ds_content',
      'group_bb_artin_v' => 'ds_content',
      'group_bb_predefined' => 'ds_content',
      'group_bb_unrecognized' => 'ds_content',
    ),
    'limit' => array(
      'field_bb_flow_step' => '1',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|bb_endpoint|default'] = $ds_layout;

  return $export;
}
