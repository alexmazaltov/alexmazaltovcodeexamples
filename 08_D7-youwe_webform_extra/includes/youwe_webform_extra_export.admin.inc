<?php

/**
 * @file
 * Created by AlexM
 * Date: 23.02.16
 */

/**
 * Menu callback for admin/config/content/youwe-webform-extra.
 */
function youwe_webform_extra_export_admin_settings() {
  $form = array();
  // Define submit handler function
  $form['#submit'][] = 'youwe_webform_extra_export_admin_settings_submit';
  $form['#validate'][] = 'youwe_webform_extra_export_admin_settings_validate';

  $form['youwe_webform_extra_export_to_email_default']  = array(
    '#type' => 'textfield',
    '#title' => t('Default Email for Exporting'),
    '#description' => t('Custom default email to send submissions for specified period.'),
    '#default_value' => variable_get('youwe_webform_extra_export_to_email_default', variable_get('site_mail', ini_get('sendmail_from'))),
    '#element_validate' => array('youwe_webform_extra_email_validate'),
    '#required' => TRUE,
  );

  $form['youwe_webform_extra_export_period_default']  = array(
    '#type' => 'radios',
    '#title' => t('Default interval period'),
    '#options' => array(
      'day' => t('Last day'),
      'week' => t('Last week'),
      'month' => t('Last month'),
    ),
    '#default_value' => variable_get('youwe_webform_extra_export_period_default', 'week'),
    '#description' => t('Default interval period when should trigger extra exporting.'),
  );

  $nodes = db_select('node', 'n')
    ->fields('n', array('nid', 'title'))
    ->condition('type', 'webform', '=')
    ->orderBy('nid', 'DESC')
    ->execute()
    ->fetchAllKeyed();
  $settings = variable_get('youwe_webform_extra_allowed_webforms_extra_export', array());

  $form['existing_webforms'] = array(
    '#type' => 'fieldset',
    '#title' => t('All Existing webforms'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  foreach($nodes as $nid => $title){
    $form['existing_webforms'][$nid]['settings'] = array(
      '#type' => 'fieldset',
      '#title' => $nid . ' - ' . $title,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    if(isset($settings[$nid]) && $settings[$nid]['enabled']){
      $form['existing_webforms'][$nid]['settings']['#attributes']['style'][] = 'background-color: #002200; color: yellow;';
    }
    $form['existing_webforms'][$nid]['settings']['exporting_enabled-'.$nid]= array(
      '#type' => 'checkbox',
      '#title' => t('Enable extra exporting'),
      '#default_value' => isset($settings[$nid])?$settings[$nid]['enabled']:FALSE,
    );
    $form['existing_webforms'][$nid]['settings']['export_to_email-'.$nid] = array(
      '#type' => 'textfield',
      '#title' => t('Email for Exporting'),
      '#description' => t('Custom email to send export of submission for specified interval period.'),
      '#default_value' => isset($settings[$nid]['email'])?$settings[$nid]['email']:variable_get('youwe_webform_extra_export_to_email_default', variable_get('site_mail', ini_get('sendmail_from'))),
      '#element_validate' => array('youwe_webform_extra_email_validate')
    );
    $form['existing_webforms'][$nid]['settings']['export_period-'.$nid] = array(
      '#type' => 'radios',
      '#title' => t('Export interval period'),
      '#options' => array(
        'day' => t('Last day'),
        'week' => t('Last week'),
        'month' => t('Last month'),
      ),
      '#default_value' => isset($settings[$nid]['export_period'])?$settings[$nid]['export_period']:variable_get('youwe_webform_extra_export_period_default', 'week'),
      '#description' => t('Export interval period when should trigger extra exporting for this webform.'),
    );
  }
  return system_settings_form($form);
}

/**
 * Validate callback
 */
function youwe_webform_extra_export_admin_settings_validate($form, &$form_state) {
  $values = $form_state['values'];
  foreach($form_state['complete form']['existing_webforms'] as $nid => $webform){
    if($nid[0] !== '#'){
      if($values['exporting_enabled-'.$nid]){
        if(empty($values['export_to_email-'.$nid]) || empty($values['export_period-'.$nid])){
          form_error($form, t('Fill all highlighted fields'));
          if(empty($values['export_to_email-'.$nid])){
            form_set_error('export_to_email-'.$nid);
          }
          if(empty($values['export_period-'.$nid])){
            form_set_error('export_period-'.$nid);
          }
        }

      }
    }
  }
}

/**
 * Submit callback
 */
function youwe_webform_extra_export_admin_settings_submit($form, &$form_state) {
  $values = $form_state['values'];
  $settings = variable_get('youwe_webform_extra_allowed_webforms_extra_export', array());
  foreach($form_state['complete form']['existing_webforms'] as $nid => $webform) {
    if ($nid[0] !== '#') {
      if($values['exporting_enabled-' . $nid] || ( isset($settings[$nid]) && $settings[$nid]['enabled'] !== $values['exporting_enabled-' . $nid]) ){
        $settings[$nid] = array (
          'enabled' => $values['exporting_enabled-' . $nid],
          'export_period' => $values['export_period-'.$nid],
          'email' => $values['export_to_email-'.$nid],
        );
      }

    }
  }
  variable_set('youwe_webform_extra_allowed_webforms_extra_export', $settings);

}