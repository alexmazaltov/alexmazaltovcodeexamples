<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 24.02.16
 * Time: 11:29
 */

function _youwe_webform_extra_submissions_download($node, $period){
  $export_info = youwe_webform_extra_results_export($node);
  youwe_webform_extra_results_download($node, $export_info);
}


/**
 * FormAPI element validate function for the range fieldset.
 */
function youwe_webform_extra_results_download_range_validate($element, $form_state) {
  switch ($element['period']['#value']) {
    case 'interval':
      // Download Start-End range of submissions' date.
      // Start date.
      if ($element['start_date']['#value']['date'] == '') {
        form_error($element['start_date'], t('Start date is required.'));
      }
      // End submission's date.
      if ($element['end_date']['#value']['date'] != '') {
          if (strtotime($element['end_date']['#value']['date']) < strtotime($element['start_date']['#value']['date'])) {
            form_error($element['end_date'], t('End submission date may not be less than Start submission date.'));
          }
      }
      break;
  }
}

/**
 * FormAPI after build function for the download range fieldset.
 */
function youwe_webform_extra_results_download_range_after_build($element, &$form_state) {
  return $element;
}

/**
 * Theme the output of the export range fieldset.
 */
function theme_youwe_webform_extra_results_download_range($variables) {
  drupal_add_library('webform', 'admin');
  $element = $variables['element'];

  // Render Start-End submissions option.
  $element['start_date']['date']['#attributes']['class'][] = 'webform-set-active';
  $element['end_date']['date']['#attributes']['class'][] = 'webform-set-active';
  $element['period']['interval']['#theme_wrappers'] = array('webform_inline_radio');
  $element['start_date']['date']['#title'] = $element['start_date']['date']['#description'] = NULL;
  $element['end_date']['date']['#title'] = $element['end_date']['date']['#description'] = NULL;
  $element['period']['interval']['#inline_element'] = t('All submissions starting: !start and optionally to: !end', array('!start' => drupal_render($element['start_date']['date']), '!end' => drupal_render($element['end_date']['date'])));
  $element['period']['interval']['#title'] = NULL;

  return drupal_render_children($element);
}
/**
 * Form to configure the download of CSV files.
 */
function youwe_webform_extra_results_download_form($form, &$form_state, $node) {

  $settings = variable_get('youwe_webform_extra_allowed_webforms_extra_export', array());
  $form['node'] = array(
    '#type' => 'value',
    '#value' => $node,
  );

  $form['range'] = array(
    '#type' => 'fieldset',
    '#title' => t('Download submissions for сustom period'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
    '#theme' => 'youwe_webform_extra_results_download_range',
    '#element_validate' => array('youwe_webform_extra_results_download_range_validate'),
    '#after_build' => array('youwe_webform_extra_results_download_range_after_build'),
  );

  $form['range']['period'] = array(
    '#type' => 'radios',
    '#title' => t('Get submissions for period:'),
    '#options' => array(
      'day' => t('Last day'),
      'week' => t('Last week'),
      'month' => t('Last month'),
      'interval' => t('Interval'),
    ),
    '#default_value' => isset($settings[$node->nid]['export_period']) ? $settings[$node->nid]['export_period'] : 'week',
  );

  $form['range']['start_date'] = array(
    '#type' => 'date_popup',
    '#date_format' => 'Y-m-d',
    '#default_value' => date('Y-m-d'),
  );
  $form['range']['end_date'] = array(
    '#type' => 'date_popup',
    '#date_format' => 'Y-m-d',
    '#default_value' => date('Y-m-d'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Download'),
  );

  $form_state['#storage']['setting'] = isset($settings[$node->nid])?$settings[$node->nid]:array();
  return $form;
}

function youwe_webform_extra_results_download_form_submit($form, &$form_state) {
  $node = $form_state['values']['node'];
  $settings_range = _youwe_webform_extra_results_get_settings_range_from_period_settings($form_state['values']['range'], $node->nid);
  if($settings_range){
    module_load_include('inc', 'webform', 'includes/webform.report');
    $form_state_ = array();
    $form_state_['values']['node'] = $node;
    $form_state_['values']['format'] = 'delimited';
    $form_state_['values']['range'] = array (
      'range_type' => 'range',
      'latest' => '',
      'start' => $settings_range['start'],
      'end' => $settings_range['end'],
    );
    $form_state_['values']['download'] = FALSE;
    drupal_form_submit('webform_results_download_form', $form_state_, $node);
    $export_info = $form_state_['export_info'];
    youwe_webform_extra_results_download($node, $export_info);
  }
  else{
    drupal_set_message(t('There are not submission for choosed period'));
  }
  $form_state['rebuild'] = TRUE;
}

/**
 *
 * @param $period_setings => array (
 *     'period' => 'week',
 *     'start_date' => '2016-02-24',
 *     'end_date' => '2016-02-24',
 *   )
 * @return array
 */
function _youwe_webform_extra_results_get_settings_range_from_period_settings($period_settings, $nid){

  $query = db_select('webform_submissions', 'ws')
    ->fields('ws', array('sid'))
    ->condition('nid', $nid)
    ->orderBy('sid', 'ASC');
  switch($period_settings['period']){
    case 'interval':
      $query->condition('submitted', strtotime($period_settings['start_date']) , '>=');
      if($period_settings['end_date'] != ''){
        $query->condition('submitted', strtotime($period_settings['end_date']) , '<=');
      }
      break;
    case 'day':
      $query->condition('submitted', strtotime('-1 day') , '>=');
      break;
    case 'week':
      $query->condition('submitted', strtotime('-1 week') , '>=');
      break;
    case 'month':
      $query->condition('submitted', strtotime('-1 month') , '>=');
      break;
  }
  $sids = $query->execute()->fetchCol();
  $settings_range = array();
  if(!empty($sids)){
    $settings_range['start'] = reset($sids);
    $settings_range['end'] = end($sids);
  }
  return !empty($settings_range)?$settings_range:FALSE;
}