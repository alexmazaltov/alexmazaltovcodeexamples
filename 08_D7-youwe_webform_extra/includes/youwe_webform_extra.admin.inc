<?php

/**
 * @file
 * Created by AlexM
 * Date: 23.02.16
 */

/**
 * Menu callback for admin/config/content/extra-notification.
 */
function youwe_webform_extra_admin_settings() {
  $form = array();
  // Define submit handler function
  $form['#submit'][] = 'youwe_webform_extra_admin_settings_submit';
  $form['#validate'][] = 'youwe_webform_extra_admin_settings_validate';

  $form['youwe_webform_extra_notify_to_email_default']  = array(
    '#type' => 'textfield',
    '#title' => t('Default Email for notification'),
    '#description' => t('Custom default email to send notification if maximum of submission have been reached.'),
    '#default_value' => variable_get('youwe_webform_extra_notify_to_email_default', variable_get('site_mail', ini_get('sendmail_from'))),
    '#element_validate' => array('youwe_webform_extra_email_validate'),
    '#required' => TRUE,
  );

  $form['youwe_webform_extra_submissions_count_notify_default']  = array(
    '#type' => 'textfield',
    '#title' => t('Default max submission notification'),
    '#description' => t('Default max submission\'s count when should trigger extra notification.'),
    '#default_value' => variable_get('youwe_webform_extra_submissions_count_notify_default', SUBMISSINS_COUNT_NOTIFY_DEFAULT),
    '#element_validate' => array('element_validate_integer_positive'),
    '#required' => TRUE,
  );

  $nodes = db_select('node', 'n')
    ->fields('n', array('nid', 'title'))
    ->condition('type', 'webform', '=')
    ->orderBy('nid', 'DESC')
    ->execute()
    ->fetchAllKeyed();
  $settings = variable_get('youwe_webform_extra_allowed_webforms', array());

  $form['existing_webforms'] = array(
    '#type' => 'fieldset',
    '#title' => t('All Existing webforms'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  module_load_include('inc', 'webform', 'includes/webform.submissions');
  foreach($nodes as $nid => $title){
    $form['existing_webforms'][$nid]['settings'] = array(
      '#type' => 'fieldset',
      '#title' => $nid . ' - ' . $title,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    if(isset($settings[$nid]) && $settings[$nid]['enabled']){
      $form['existing_webforms'][$nid]['settings']['#attributes']['style'][] = 'background-color: #002200; color: yellow;';
    }
    $form['existing_webforms'][$nid]['settings']['notification_enabled-'.$nid]= array(
      '#type' => 'checkbox',
      '#title' => t('Enable extra notification'),
      '#default_value' => isset($settings[$nid])?$settings[$nid]['enabled']:FALSE,
    );
    $form['existing_webforms'][$nid]['settings']['notify_to_email-'.$nid] = array(
      '#type' => 'textfield',
      '#title' => t('Email for notification'),
      '#description' => t('Custom email to send notification if maximum of submission have been reached.'),
      '#default_value' => isset($settings[$nid]['email'])?$settings[$nid]['email']:variable_get('youwe_webform_extra_notify_to_email_default', variable_get('site_mail', ini_get('sendmail_from'))),
      '#element_validate' => array('youwe_webform_extra_email_validate')
    );
    $submission_count = webform_get_submission_count($nid);
    $form['existing_webforms'][$nid]['settings']['submission_max_count-'.$nid] = array(
      '#type' => 'textfield',
      '#title' => t('Next submission\'s count notification'),
      '#description' => t('Max submission\'s count when should trigger next extra notification.').'<br>'.t('Current submissions count is: ') .'<strong>'. $submission_count.'</strong>',
      '#default_value' => isset($settings[$nid]['submission_max_count'])?$settings[$nid]['submission_max_count']:variable_get('youwe_webform_extra_submissions_count_notify_default', SUBMISSINS_COUNT_NOTIFY_DEFAULT),
      '#element_validate' => array('element_validate_integer_positive'),
    );
  }
  return system_settings_form($form);
}

/**
 * Validate callback
 */
function youwe_webform_extra_admin_settings_validate($form, &$form_state) {
  $values = $form_state['values'];
  foreach($form_state['complete form']['existing_webforms'] as $nid => $webform){
    if($nid[0] !== '#'){
      if($values['notification_enabled-'.$nid]){
        if(empty($values['notify_to_email-'.$nid]) || empty($values['submission_max_count-'.$nid])){
          form_error($form, t('Fill all highlighted fields'));
          if(empty($values['notify_to_email-'.$nid])){
            form_set_error('notify_to_email-'.$nid);
          }
          if(empty($values['submission_max_count-'.$nid])){
            form_set_error('submission_max_count-'.$nid);
          }
        }

      }
    }
  }
}

/**
 * Submit callback
 */
function youwe_webform_extra_admin_settings_submit($form, &$form_state) {
  $values = $form_state['values'];
  $settings = variable_get('youwe_webform_extra_allowed_webforms', array());
  module_load_include('inc', 'webform', 'includes/webform.submissions');
  foreach($form_state['complete form']['existing_webforms'] as $nid => $webform) {
    if ($nid[0] !== '#') {
      if($values['notification_enabled-' . $nid] || ( isset($settings[$nid]) && $settings[$nid]['enabled'] !== $values['notification_enabled-' . $nid]) ){
        $submission_count = webform_get_submission_count($nid);
        $settings[$nid] = array (
          'enabled' => $values['notification_enabled-' . $nid],
          'submission_max_count' => $values['submission_max_count-'.$nid],
          'email' => $values['notify_to_email-'.$nid],
          'notified' => ($submission_count >= $values['submission_max_count-'.$nid]),
        );
      }
    }
  }
  variable_set('youwe_webform_extra_allowed_webforms', $settings);

}