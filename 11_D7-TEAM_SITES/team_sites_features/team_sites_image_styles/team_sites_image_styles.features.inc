<?php
/**
 * @file
 * team_sites_image_styles.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function team_sites_image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: blog_post_detailed_view.
  $styles['blog_post_detailed_view'] = array(
    'name' => 'blog_post_detailed_view',
    'label' => 'Blog Post Detailed View',
    'effects' => array(
      2 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 640,
          'height' => 320,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: profile_photo.
  $styles['profile_photo'] = array(
    'name' => 'profile_photo',
    'label' => 'Profile photo (208x208)',
    'effects' => array(
      3 => array(
        'label' => 'Centre on face and scale',
        'help' => 'Face centred & scaled with pixel-edge buffer surroundings.',
        'effect callback' => 'image_face_detect_centre_scale_face_effect',
        'form callback' => 'image_face_detect_centre_scale_face_form',
        'module' => 'image_face_detect',
        'name' => 'image_face_detect_centre_scale_face',
        'data' => array(
          'width' => 400,
          'height' => 400,
          'bufferzone-y' => 45,
          'bufferzone-x' => 45,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: profile_photo_small.
  $styles['profile_photo_small'] = array(
    'name' => 'profile_photo_small',
    'label' => 'Profile photo small (51x51)',
    'effects' => array(
      4 => array(
        'label' => 'Centre on face and scale',
        'help' => 'Face centred & scaled with pixel-edge buffer surroundings.',
        'effect callback' => 'image_face_detect_centre_scale_face_effect',
        'form callback' => 'image_face_detect_centre_scale_face_form',
        'module' => 'image_face_detect',
        'name' => 'image_face_detect_centre_scale_face',
        'data' => array(
          'width' => 51,
          'height' => 51,
          'bufferzone-y' => 25,
          'bufferzone-x' => 35,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: slider_image.
  $styles['slider_image'] = array(
    'name' => 'slider_image',
    'label' => 'Slider Image',
    'effects' => array(
      1 => array(
        'label' => 'Crop',
        'help' => 'Cropping will remove portions of an image to make it the specified dimensions.',
        'effect callback' => 'image_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_crop_form',
        'summary theme' => 'image_crop_summary',
        'module' => 'image',
        'name' => 'image_crop',
        'data' => array(
          'width' => 558,
          'height' => 258,
          'anchor' => 'center-top',
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: slider_manual_crop.
  $styles['slider_manual_crop'] = array(
    'name' => 'slider_manual_crop',
    'label' => 'slider manual crop',
    'effects' => array(
      5 => array(
        'label' => 'Javascript crop',
        'help' => 'Create a crop with a javascript toolbox.',
        'effect callback' => 'imagecrop_effect',
        'form callback' => 'imagecrop_effect_form',
        'summary theme' => 'imagecrop_effect_summary',
        'module' => 'imagecrop',
        'name' => 'imagecrop_javascript',
        'data' => array(
          'width' => 558,
          'height' => 258,
          'xoffset' => 20,
          'yoffset' => 20,
          'resizable' => 0,
          'downscaling' => 0,
          'aspect_ratio' => '',
          'disable_if_no_data' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
