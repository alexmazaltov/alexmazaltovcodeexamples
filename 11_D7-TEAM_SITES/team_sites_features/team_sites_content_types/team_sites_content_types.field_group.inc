<?php
/**
 * @file
 * team_sites_content_types.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function team_sites_content_types_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_front_page|node|team_site_cms|form';
  $field_group->group_name = 'group_front_page';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'team_site_cms';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Front Page Tab Set',
    'weight' => '1',
    'children' => array(
      0 => 'group_header_image',
      1 => 'group_referenced_pages',
      2 => 'group_text_box',
      3 => 'group_settings',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-front-page field-group-tabs',
      ),
    ),
  );
  $export['group_front_page|node|team_site_cms|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_header_image|node|team_site_cms|form';
  $field_group->group_name = 'group_header_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'team_site_cms';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_front_page';
  $field_group->data = array(
    'label' => 'Header Image',
    'weight' => '6',
    'children' => array(
      0 => 'field_front_header_image',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-header-image field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_header_image|node|team_site_cms|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_referenced_pages|node|team_site_cms|form';
  $field_group->group_name = 'group_referenced_pages';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'team_site_cms';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_front_page';
  $field_group->data = array(
    'label' => 'Referenced pages',
    'weight' => '7',
    'children' => array(
      0 => 'field_columnar_links',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-referenced-pages field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_referenced_pages|node|team_site_cms|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_settings|node|team_site_cms|form';
  $field_group->group_name = 'group_settings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'team_site_cms';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_front_page';
  $field_group->data = array(
    'label' => 'Settings',
    'weight' => '9',
    'children' => array(
      0 => 'field_blog',
      1 => 'field_fields_order',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-settings field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_settings|node|team_site_cms|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_box|node|team_site_cms|form';
  $field_group->group_name = 'group_text_box';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'team_site_cms';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_front_page';
  $field_group->data = array(
    'label' => 'Text Box',
    'weight' => '8',
    'children' => array(
      0 => 'body',
      1 => 'field_text_box_title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-text-box field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_text_box|node|team_site_cms|form'] = $field_group;

  return $export;
}
