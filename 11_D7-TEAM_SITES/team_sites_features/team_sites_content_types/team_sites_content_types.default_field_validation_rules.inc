<?php
/**
 * @file
 * team_sites_content_types.default_field_validation_rules.inc
 */

/**
 * Implements hook_default_field_validation_rule().
 */
function team_sites_content_types_default_field_validation_rule() {
  $export = array();

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Two member with equal email not allows';
  $rule->name = 'two_member_with_equal_email_not_';
  $rule->field_name = 'field_email';
  $rule->col = 'email';
  $rule->entity_type = 'node';
  $rule->bundle = 'member';
  $rule->validator = 'field_validation_unique_validator';
  $rule->settings = array(
    'data' => 'bundle',
    'per_user' => 0,
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      8 => 0,
      6 => 0,
      3 => 0,
      7 => 0,
      5 => 0,
      4 => 0,
      9 => 0,
    ),
    'errors' => 0,
  );
  $rule->error_message = 'Two member with equal email not allows in member content type';
  $export['two_member_with_equal_email_not_'] = $rule;

  return $export;
}
