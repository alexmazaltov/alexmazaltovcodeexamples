<?php
/**
 * @file
 * team_sites_roles_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function team_sites_roles_permissions_user_default_roles() {
  $roles = array();

  // Exported role: Campus editor.
  $roles['Campus editor'] = array(
    'name' => 'Campus editor',
    'weight' => 6,
  );

  // Exported role: Site admin.
  $roles['Site admin'] = array(
    'name' => 'Site admin',
    'weight' => 4,
  );

  // Exported role: Site editor.
  $roles['Site editor'] = array(
    'name' => 'Site editor',
    'weight' => 5,
  );

  // Exported role: Spouse.
  $roles['Spouse'] = array(
    'name' => 'Spouse',
    'weight' => 7,
  );

  // Exported role: Super admin.
  $roles['Super admin'] = array(
    'name' => 'Super admin',
    'weight' => 3,
  );

  // Exported role: root.
  $roles['root'] = array(
    'name' => 'root',
    'weight' => 2,
  );

  return $roles;
}
