<?php
/**
 * @file
 * team_sites_search.features.inc
 */

/**
 * Implements hook_default_search_api_index().
 */
function team_sites_search_default_search_api_index() {
  $items = array();
  $items['ts1_comments_elastica_index'] = entity_import('search_api_index', '{
    "name" : "ts1_comments_elastica_index",
    "machine_name" : "ts1_comments_elastica_index",
    "description" : null,
    "server" : "elastic_search_production",
    "item_type" : "comment",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "40",
      "number_of_shards" : "1",
      "number_of_replicas" : "0",
      "fields" : {
        "cid" : { "type" : "integer" },
        "comment_body:value" : { "type" : "text" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "integer" },
        "subject" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_comment_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "subject" : true, "comment_body:value" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "subject" : true, "comment_body:value" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "subject" : true, "comment_body:value" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "subject" : true, "comment_body:value" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "0",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  $items['ts1_nodes_elastica_index'] = entity_import('search_api_index', '{
    "name" : "ts1_nodes_elastica_index",
    "machine_name" : "ts1_nodes_elastica_index",
    "description" : null,
    "server" : "elastic_search_production",
    "item_type" : "node",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "50",
      "number_of_shards" : "1",
      "number_of_replicas" : "0",
      "fields" : {
        "author" : { "type" : "integer", "entity_type" : "user" },
        "body:value" : { "type" : "text" },
        "comments" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "comment" },
        "field_cathegorie:description" : { "type" : "text" },
        "field_cathegorie:name" : { "type" : "text" },
        "field_sub_title" : { "type" : "text" },
        "field_tags:description" : { "type" : "list\\u003Ctext\\u003E" },
        "field_tags:name" : { "type" : "list\\u003Ctext\\u003E" },
        "nid" : { "type" : "integer" },
        "search_api_access_node" : { "type" : "list\\u003Cstring\\u003E" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "boolean" },
        "title" : { "type" : "text" },
        "type" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : {
            "default" : "1",
            "bundles" : {
              "member" : "member",
              "projects" : "projects",
              "simplenews" : "simplenews",
              "team_site_cms" : "team_site_cms",
              "webform" : "webform"
            }
          }
        },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "field_sub_title" : true,
              "body:value" : true,
              "field_tags:name" : true,
              "field_tags:description" : true,
              "field_cathegorie:name" : true,
              "field_cathegorie:description" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true, "field_sub_title" : true, "body:value" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_sub_title" : true,
              "body:value" : true,
              "field_tags:name" : true,
              "field_tags:description" : true,
              "field_cathegorie:name" : true,
              "field_cathegorie:description" : true
            },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_sub_title" : true,
              "body:value" : true,
              "field_tags:name" : true,
              "field_tags:description" : true,
              "field_cathegorie:name" : true,
              "field_cathegorie:description" : true
            },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function team_sites_search_default_search_api_server() {
  $items = array();
  $items['elastic_search_production'] = entity_import('search_api_server', '{
    "name" : "Elastic Search production",
    "machine_name" : "elastic_search_production",
    "description" : "",
    "class" : "search_api_elasticsearch_elastica_service",
    "options" : {
      "0" : {
        "host" : "apps-search-prd.lifechurch.tv",
        "port" : "443",
        "headers" : { "http_user" : "admin", "Authentication" : "Basic YWRtaW46dGZhc3Jv" },
        "path" : "",
        "url" : "",
        "transport" : "Https",
        "persistent" : 1,
        "timeout" : "300",
        "log" : 0,
        "retryOnConflict" : "0"
      },
      "facet_limit" : ""
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  $items['local_host_server'] = entity_import('search_api_server', '{
    "name" : "Local host server",
    "machine_name" : "local_host_server",
    "description" : "http:\\/\\/localhost:9200\\/_plugin\\/head\\/",
    "class" : "search_api_elasticsearch_elastica_service",
    "options" : {
      "0" : {
        "host" : "127.0.0.1",
        "port" : "9200",
        "headers" : { "http_user" : "" },
        "path" : "",
        "url" : "",
        "transport" : "Http",
        "persistent" : 1,
        "timeout" : "300",
        "log" : 0,
        "retryOnConflict" : "0"
      },
      "facet_limit" : ""
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}
