<?php
/**
 * @file
 * team_sites_ldap.ldap_query.inc
 */

/**
 * Implements hook_default_ldap_query().
 */
function team_sites_ldap_default_ldap_query() {
  $export = array();

  $qid = new stdClass();
  $qid->disabled = FALSE; /* Edit this to true to make a default qid disabled initially */
  $qid->api_version = 1;
  $qid->qid = 'get_person';
  $qid->name = 'Get Person';
  $qid->sid = 'lifechurch_ldap';
  $qid->status = TRUE;
  $qid->base_dn_str = 'DC=unity,DC=com';
  $qid->filter = '(&(objectClass=user)(samaccounttype=805306368)(samaccountname=@samaccountname)(objectCategory=person)(!(lockouttime>=1))(!(useraccountcontrol=514))(!(useraccountcontrol=66050))(memberof=CN=allsubscribers5a8c7c15,CN=Users,DC=unity,DC=com)(!(msexchhidefromaddresslists=TRUE)))';
  $qid->attributes_str = 'givenname,sn,cn,mail,title,mobile,telephonenumber,manager,displayname,extensionattribute1,extensionattribute2,extensionattribute3, extensionattribute5, physicalDeliveryOfficeName,sAMAccountName,thumbnailphoto,ipPhone';
  $qid->sizelimit = 0;
  $qid->timelimit = 0;
  $qid->deref = FALSE;
  $qid->scope = TRUE;
  $export['get_person'] = $qid;

  $qid = new stdClass();
  $qid->disabled = FALSE; /* Edit this to true to make a default qid disabled initially */
  $qid->api_version = 1;
  $qid->qid = 'get_person_dev';
  $qid->name = 'Get Person Dev';
  $qid->sid = 'lifechurch_ldap';
  $qid->status = TRUE;
  $qid->base_dn_str = 'DC=unity,DC=com';
  $qid->filter = '(&(objectClass=user)(samaccounttype=805306368)(samaccountname=@samaccountname)(objectCategory=person)(!(lockouttime>=1))(!(useraccountcontrol=514))(!(useraccountcontrol=66050))(!(msexchhidefromaddresslists=TRUE)))';
  $qid->attributes_str = 'givenname,sn,cn,mail,title,mobile,telephonenumber,manager,displayname,extensionattribute1,extensionattribute2,extensionattribute3, extensionattribute5, physicalDeliveryOfficeName,sAMAccountName,thumbnailphoto,ipPhone';
  $qid->sizelimit = 0;
  $qid->timelimit = 0;
  $qid->deref = FALSE;
  $qid->scope = TRUE;
  $export['get_person_dev'] = $qid;

  $qid = new stdClass();
  $qid->disabled = FALSE; /* Edit this to true to make a default qid disabled initially */
  $qid->api_version = 1;
  $qid->qid = 'person-type-dynamic';
  $qid->name = 'Get number of users by personality type';
  $qid->sid = 'lifechurch_ldap';
  $qid->status = TRUE;
  $qid->base_dn_str = 'DC=unity,DC=com';
  $qid->filter = '(&(objectClass=user)(samaccounttype=805306368)(objectCategory=person)(!(lockouttime>=1))(!(useraccountcontrol=514))(!(useraccountcontrol=66050))(memberof=CN=allsubscribers5a8c7c15,CN=Users,DC=unity,DC=com)(!(msexchhidefromaddresslists=TRUE))(extensionAttribute1=@personality_type))';
  $qid->attributes_str = 'cn';
  $qid->sizelimit = 0;
  $qid->timelimit = 0;
  $qid->deref = FALSE;
  $qid->scope = TRUE;
  $export['person-type-dynamic'] = $qid;

  $qid = new stdClass();
  $qid->disabled = FALSE; /* Edit this to true to make a default qid disabled initially */
  $qid->api_version = 1;
  $qid->qid = 'user-dynamic';
  $qid->name = 'Dynamic Query';
  $qid->sid = 'lifechurch_ldap';
  $qid->status = TRUE;
  $qid->base_dn_str = 'DC=unity,DC=com';
  $qid->filter = '(&(objectClass=user)(samaccounttype=805306368)(objectCategory=person)(!(lockouttime>=1))(!(useraccountcontrol=514))(!(useraccountcontrol=66050))(memberof=CN=allsubscribers5a8c7c15,CN=Users,DC=unity,DC=com)(!(msexchhidefromaddresslists=TRUE)))';
  $qid->attributes_str = 'givenname,sn,cn,mail,title,mobile,telephonenumber,manager,displayname,extensionattribute1,extensionattribute2,extensionattribute3, extensionattribute5,physicalDeliveryOfficeName,sAMAccountName,thumbnailphoto,ipPhone';
  $qid->sizelimit = 0;
  $qid->timelimit = 0;
  $qid->deref = FALSE;
  $qid->scope = TRUE;
  $export['user-dynamic'] = $qid;

  return $export;
}
