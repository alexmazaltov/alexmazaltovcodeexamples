<?php
/**
 * @file
 * team_sites_blocks.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function team_sites_blocks_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['moc_ts_blog-categories_filters'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'categories_filters',
    'module' => 'moc_ts_blog',
    'node_types' => array(),
    'pages' => 'blog*',
    'roles' => array(),
    'themes' => array(
      'team_sites' => array(
        'region' => 'sidebar',
        'status' => 1,
        'theme' => 'team_sites',
        'weight' => -19,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['moc_ts_blog-tags_filters'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'tags_filters',
    'module' => 'moc_ts_blog',
    'node_types' => array(),
    'pages' => 'blog*',
    'roles' => array(),
    'themes' => array(
      'team_sites' => array(
        'region' => 'sidebar',
        'status' => 1,
        'theme' => 'team_sites',
        'weight' => -18,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['moc_ts_links_page-link_parent_cathegorie_filters'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'link_parent_cathegorie_filters',
    'module' => 'moc_ts_links_page',
    'node_types' => array(
      0 => 'links_page',
    ),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'team_sites' => array(
        'region' => 'sidebar',
        'status' => 1,
        'theme' => 'team_sites',
        'weight' => -17,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
