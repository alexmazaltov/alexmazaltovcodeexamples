<?php

/**
 * @file
 * Twitter API functions
 */

module_load_include('php', 'oauth_common', 'lib/OAuth');

/**
 * Connect to the Twitter API.
 *
 * @param null $account
 *   An authenticated twitter_account object to be used to authenticate against
 *   Twitter.
 *
 * @return bool|Twitter
 *   a Twitter object ready to be used to query the Twitter API or FALSE.
 */
function team_sites_twitter_connect($account = NULL) {
  if (!$account) {
    // Load the first authenticated account.
    $query = db_select('staff_twitter_account', 'sta');
    $query->addField('sta', 'twitter_uid');
    $query->condition('sta.oauth_token', '', '<>');
    $query->condition('sta.oauth_token_secret', '', '<>');
    $twitter_uid = $query->execute()->fetchField();

    $account = team_sites_twitter_account_load($twitter_uid);
  }
  if ($account) {
    $auth = $account->get_auth();
    if (isset($auth['oauth_token']) && isset($auth['oauth_token_secret'])) {
      return new Twitter(variable_get('twitter_consumer_key', ''), variable_get('twitter_consumer_secret', ''), $auth['oauth_token'], $auth['oauth_token_secret']);
    }
  }

  return FALSE;
}

/**
 * Saves a TwitterUser object to {staff_twitter_account}
 */
function team_sites_twitter_account_save($twitter_user, $save_auth = FALSE) {
  $values = (array) $twitter_user;
  $values['twitter_uid'] = $values['id'];
  foreach (array('protected', 'verified', 'profile_background_tile') as $k) {
    if (isset($values[$k])) {
      $values[$k] = (int) $values[$k];
    }
  }

  if ($save_auth) {
    $values += $twitter_user->get_auth();
  }
  $schema = drupal_get_schema('staff_twitter_account');
  foreach ($values as $k => $v) {
    if (!isset($schema['fields'][$k])) {
      unset($values[$k]);
    }
  }

  db_merge('staff_twitter_account')->key(array('twitter_uid' => $values['twitter_uid']))->fields($values)->execute();
}

/**
 * Load a Twitter account from {staff_twitter_account}.
 *
 * @param $id
 *   int Twitter User id or string Twitter user screen name.
 *
 * @return null|TwitterUser
 *   TwitterUser object or NULL.
 */
function team_sites_twitter_account_load($id) {
  $query = db_select('staff_twitter_account', 'sta');
  $query->fields('sta');
  $query->condition(db_or()->condition('sta.twitter_uid', $id)->condition('sta.screen_name', $id));
  $values = $query->execute()->fetchAssoc();

  if (!empty($values)) {
    $values['id'] = $values['twitter_uid'];
    $account = new TwitterUser($values);
    $account->set_auth($values);
    $account->import = $values['import'];
    $account->mentions = $values['mentions'];
    $account->is_global = $values['is_global'];

    return $account;
  }

  return NULL;
}

/**
 * Loads all Twitter accounts added by a user.
 *
 * @return array
 *   array of TwitterUser objects.
 */
function team_sites_twitter_account_load_all() {
  $accounts = array();
  $query = db_select('staff_twitter_account', 'sta');
  $query->addField('sta', 'twitter_uid');
  $query->condition('sta.uid', 0, '<>');
  $result = $query->execute()->fetchAll();

  foreach ($result as $account) {
    $accounts[] = team_sites_twitter_account_load($account->twitter_uid);
  }

  return $accounts;
}

/**
 *  Returns a list of authenticated Twitter accounts.
 *
 * @return array
 *   array of TwitterUser objects.
 */

function team_sites_twitter_load_authenticated_accounts() {
  $accounts = team_sites_twitter_account_load_all();
  $auth_accounts = array();
  foreach ($accounts as $account) {
    if ($account->is_auth()) {
      $auth_accounts[] = $account;
    }
  }

  return $auth_accounts;
}

/**
 * Delete a twitter account and its statuses.
 *
 * @param $twitter_uid
 *   An integer with the Twitter UID.
 */
function team_sites_twitter_account_delete($twitter_uid) {
  // Delete from {staff_twitter_account}.
  $query = db_delete('staff_twitter_account');
  $query->condition('twitter_uid', $twitter_uid);
  $query->execute();

  // Delete from {authmap}.
  $query = db_delete('authmap');
  $query->condition('authname', $twitter_uid);
  $query->condition('module', 'twitter');
  $query->execute();
}
