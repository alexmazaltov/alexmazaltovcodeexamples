<?php

/**
 * @file
 *  Team Sites API resources
 */

/**
 * Login a user using the specified credentials.
 *
 * @param $email
 *   User email to be logged in.
 * @param $password
 *   Password, must be plain text and not hashed.
 * @param $device
 *   Unique device id.
 *
 * @return object with user unique token.
 */
function team_sites_api_user_app_login($email, $password, $device) {
  global $user;

  try {
    $form_state = array(
      'values' => array(
        'name' => $email,
        'pass' => $password,
        'form_id' => 'user_login',
        'op' => t('Log in')
      )
    );

    drupal_form_submit('user_login', $form_state);
    // Error if needed.
    if ($errors = form_get_errors()) {
      return services_error(implode(" ", $errors), 102, array('errors' => $errors));
    }
    if (empty($form_state['uid']) && user_is_anonymous()) {
      throw new Exception(t('The user with email %mail has not been activated or is blocked.', array('%mail' => $email)), TEAM_SITES_API_ERROR_USER_IS_BLOCKED);
    }
    if ($user->uid) {
      $response = new stdClass();
      $response->token = services_user_token_auth_create_user_token($user->uid, $device);
      $response->user = team_sites_api_get_app_user_object($user);

      return $response;
    }
    throw new Exception(t('Wrong email or password.'), TEAM_SITES_API_ERROR_USER_INVALID_DATA);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Logout the current user.
 *
 * @param string $token
 *   Unique user token
 * @param string $endpoint
 *   Amazon SNS endpoint
 *
 * @return array
 *  Results
 */
function team_sites_api_user_resource_logout($token, $endpoint) {
  global $user;

  try {
    if (!$user->uid) {
      // User is not logged in.
      throw new Exception(t('User is not logged in.'), TEAM_SITES_API_ERROR_USER_IS_NOT_LOGGED_IN);
    }

    watchdog('user', 'Session closed for %name.', array('%name' => $user->name));

    if ($endpoint) {
      $config = team_sites_common_get_sns_config();
      $sns = team_sites_common_get_sns_class($config);
      if ($sns) {
        $q = db_select('users_sns_endpoints', 'usnse');
        $q->addField('usnse', 'id');
        $q->addField('usnse', 'subscriptionarn');
        $q->condition('usnse.uid', $user->uid);
        $q->condition('usnse.endpoint', $endpoint);
        $use = $q->execute()->fetchAssoc();
        if (!empty($use['subscriptionarn'])) {
          db_delete('users_sns_endpoints')->condition('id', $use['id'])->execute();
          $sns->unsubscribe($use['subscriptionarn']);
        }
      }
    }

    // Destroy the current session.
    module_invoke_all('user_logout', $user);
    drupal_session_destroy_uid($user->uid);
    services_user_token_auth_delete_token($token);

    // Load the anonymous user.
    $user = drupal_anonymous_user();

    return array('status' => TRUE);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Create new question node.
 *
 * @param string $token
 *   Unique user token
 * @param array $question
 *   Question data array
 *
 * @return mixed
 */
function team_sites_api_create_question($token, $question) {
  try {
    global $user;

    if (isset($question['title'])) {
      $question['title'] = trim($question['title']);
      if (empty($question['title'])) {
        throw new Exception(t('Invalid question data'), TEAM_SITES_API_ERROR_INVALID_QUESTION_DATA);
      }

      $node = team_sites_common_get_blank_node_object($question['title'], TEAM_SITES_CT_QUESTION, $user->uid);
      if (!empty($question['question'])) {
        $node->body[LANGUAGE_NONE][0]['value'] = $question['question'];
        $node->body[LANGUAGE_NONE][0]['format'] = 'filtered_html';
      }
      $node->comment = COMMENT_NODE_OPEN;
      $node->name = $user->name;
      $node = node_submit($node);
      node_save($node);

      if (isset($node->nid) && !empty($node->nid)) {
        $question = team_sites_api_get_question_object($node);

        return $question;
      }
      else {
        throw new Exception(t('Fatal error'), TEAM_SITES_API_ERROR_FATAL_EXCEPTION);
      }

    }
    else {
      throw new Exception(t('Invalid question data'), TEAM_SITES_API_ERROR_INVALID_QUESTION_DATA);
    }
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Update question node.
 *
 * @param string $token
 *   Unique user token
 * @param array $question
 *   Question data array
 *
 * @return mixed
 */
function team_sites_api_update_question($token, $question) {
  global $user;

  try {
    if (!empty($question['id']) && !empty($question['title'])) {
      $question['title'] = trim($question['title']);
      if (empty($question['title'])) {
        throw new Exception(t('Invalid question data'), TEAM_SITES_API_ERROR_INVALID_QUESTION_DATA);
      }

      $node = node_load($question['id']);
      if ($node && $node->type == TEAM_SITES_CT_QUESTION) {
        // Can edit only own question
        if ($node->uid != $user->uid) {
          throw new Exception(t('Access Denied'), TEAM_SITES_API_ERROR_ACCESS_DENIED);
        }
        $node->title = $question['title'];
        $node->body[LANGUAGE_NONE][0]['value'] = $question['question'];
        node_save($node);

        $question = team_sites_api_get_question_object($node);

        return $question;
      }
      throw new Exception(t('Question not found'), TEAM_SITES_API_ERROR_QUESTION_NOT_FOUND);
    }
    else {
      throw new Exception(t('Invalid question data'), TEAM_SITES_API_ERROR_INVALID_QUESTION_DATA);
    }
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Get questions list or search questions.
 *
 * Optional list can be sorted by type:
 *   'popular', 'newest' 'no_answers'
 *
 * @param string $token
 *   Unique user token
 * @param int $limit
 *   Questions limit
 * @param int $offset
 *   Question offset
 * @param string $sort_type
 *   Questions sort type
 * @param string $search_query
 *   Search query string
 *
 * @return array
 *   Array of question objects without answers
 */
function team_sites_api_get_questions_list($token, $limit, $offset, $sort_type, $search_query) {
  if ($search_query) {
    $questions = team_sites_api_question_search($search_query, $limit, $offset, $sort_type);
  }
  else {
    $questions = team_sites_api_get_questions($limit, $offset, $sort_type);
  }

  foreach ($questions as &$question) {
    $question->id = (int) $question->id;
    $question->votes = (int) $question->votes;
    $question->created = (int) $question->created;
    $question->voted = (bool) $question->voted;
    $question->total_answers = (int) $question->total_answers;
    // Check empty, because after search questions comes already with user object.
    if (empty($question->user)) {
      $question->user = team_sites_api_get_app_user_object($question->user_id);
    }

    unset($question->user_name);
    unset($question->user_id);
  }

  return $questions;
}

/**
 * Vote up or vote down question
 *
 * @param string $token
 *   Unique user token
 * @param int $nid
 *   Question node nid
 *
 * @return array|mixed
 */
function team_sites_api_vote_question($token, $nid) {
  try {
    $node = node_load($nid);
    if ($node && $node->type == TEAM_SITES_CT_QUESTION) {
      team_sites_common_entity_vote($node->nid, 'node');
      // Reload node with reset cache for get new votes count
      $node = node_load($nid, NULL, TRUE);
      $question = team_sites_api_get_question_object($node, TRUE);
      unset($question->answers);

      return $question;
    }
    throw new Exception(t('Question not found'), TEAM_SITES_API_ERROR_QUESTION_NOT_FOUND);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Get question.
 *
 * @param string $token
 *   Unique user token
 * @param int $nid
 *   Question node nid.
 *
 * @return mixed|stdClass
 */
function team_sites_api_get_question($token, $nid) {
  try {
    $node = node_load($nid);
    if ($node && $node->type == TEAM_SITES_CT_QUESTION) {
      return team_sites_api_get_question_object($node, TRUE);
    }
    throw new Exception(t('Question not found'), TEAM_SITES_API_ERROR_QUESTION_NOT_FOUND);

  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Get list of answers for question.
 * Deprecated for new app version. Saved for old app compatibility.
 *
 * @param string $token
 *   Unique user token
 * @param int $nid
 *   Question node nid
 * @param int $limit
 *   Answers limit
 * @param int $offset
 *   Answers offset
 *
 * @return array
 * @throws Exception
 * @deprecated
 */
function team_sites_api_get_answers_list($token, $nid, $limit, $offset) {
  try {
    $node = node_load($nid);
    if ($node && $node->type == TEAM_SITES_CT_QUESTION) {
      $comments = _team_sites_common_load_answers($nid, $offset, $limit);

      return _team_sites_api_preprocess_answers($comments);
    }
    throw new Exception(t('Question not found'), TEAM_SITES_API_ERROR_QUESTION_NOT_FOUND);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Return questions answers. Use pagination by answer id instead default pagination.
 *
 * @param string $token
 *  Unique user token
 * @param int $nid
 *  Question nid.
 * @param int $limit
 *   Answers limit
 * @param int $answer_id
 *   Aanswer id
 * @param string $direction
 *   paginate direction. It can be 'next' or 'prev'
 *
 * @return mixed
 *   Array of answers
 */
function team_sites_api_get_question_answers($token, $nid, $limit, $answer_id, $direction) {
  try {
    $node = node_load($nid);
    if ($node && $node->type == TEAM_SITES_CT_QUESTION) {
      switch ($direction) {
        case 'next':
          return array('next_answers' => team_sites_common_get_node_comments_paginate($nid, $answer_id, $limit, $direction));

        case 'prev':
          return array('prev_answers' => team_sites_common_get_node_comments_paginate($nid, $answer_id, $limit, $direction));

        case 'both':
          $prev = array();
          $next = array();
          $comments = team_sites_common_get_node_comments_paginate($nid, $answer_id, $limit, $direction);
          if ($comments) {
            foreach ($comments as $key => $comment) {
              if ($comment->id != $answer_id) {
                $prev[] = $comment;
                unset($comments[$key]);
              }
              else {
                break;
              }
            }

            array_shift($comments);
            $next = array_values($comments);
          }

          return array('prev_answers' => $prev, 'next_answers' => $next);
      }
      return array();
    }
    throw new Exception(t('Question not found'), TEAM_SITES_API_ERROR_QUESTION_NOT_FOUND);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Create answer for question.
 *
 * @param string $token
 *   Unique user token
 * @param array $answer
 *   Answer array.
 *
 * @return array
 * @throws Exception
 */
function team_sites_api_create_answer($token, $answer) {
  global $user;

  try {
    if (isset($answer['question_id']) && !empty($answer['answer'])) {
      if (team_sites_api_node_is_question($answer['question_id'])) {
        $answer['answer'] = trim($answer['answer']);
        if (empty($answer['answer'])) {
          throw new Exception(t('Invalid answer data'), TEAM_SITES_API_ERROR_INVALID_ANSWER_DATA);
        }

        $comment = new stdClass();
        $comment->cid = NULL;
        $comment->nid = $answer['question_id'];
        $comment->pid = 0;
        $comment->uid = $user->uid;
        $comment->mail = $user->mail;
        $comment->name = $user->name;
        $comment->created = REQUEST_TIME;
        $comment->is_anonymous = 0;
        $comment->homepage = '';
        $comment->status = COMMENT_PUBLISHED;
        $comment->language = LANGUAGE_NONE;
        $comment->subject = '';
        $comment->comment_body[$comment->language][0]['value'] = $answer['answer'];
        $comment->comment_body[$comment->language][0]['format'] = 'filtered_html';
        comment_submit($comment);
        comment_save($comment);

        if (isset($comment->cid) && !empty($comment->cid)) {
          return team_sites_api_get_answer_object($comment);
        }
        else {
          throw new Exception(t('Fatal error'), TEAM_SITES_API_ERROR_FATAL_EXCEPTION);
        }

      }
      throw new Exception(t('Question not found'), TEAM_SITES_API_ERROR_QUESTION_NOT_FOUND);
    }
    throw new Exception(t('Invalid answer data'), TEAM_SITES_API_ERROR_INVALID_ANSWER_DATA);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Update answer.
 *
 * @param string $token
 *   Unique user token
 * @param array $answer
 *   Answer array.
 *
 * @return array
 * @throws Exception
 */
function team_sites_api_update_answer($token, $answer) {
  global $user;

  try {
    if (isset($answer['id']) && !empty($answer['answer'])) {
      $answer['answer'] = trim($answer['answer']);
      if (empty($answer['answer'])) {
        throw new Exception(t('Invalid answer data'), TEAM_SITES_API_ERROR_INVALID_ANSWER_DATA);
      }

      $comment = comment_load($answer['id']);
      if ($comment && $comment->uid == $user->uid) {
        $comment->comment_body[$comment->language][0]['value'] = $answer['answer'];
        $comment->comment_body[$comment->language][0]['format'] = 'filtered_html';
        comment_save($comment);

        return team_sites_api_get_answer_object($comment);
      }
      throw new Exception(t('Access denied'), TEAM_SITES_API_ERROR_ACCESS_DENIED);
    }
    throw new Exception(t('Invalid answer data'), TEAM_SITES_API_ERROR_INVALID_ANSWER_DATA);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Vote up or vote down answer.
 *
 * @param string $token
 *   Unique user token
 * @param int $cid
 *   Comment cid
 *
 * @return array|mixed
 */
function team_sites_api_answer_vote($token, $cid) {
  try {
    $comment = comment_load($cid);
    if ($comment && $comment->node_type == TEAM_SITES_COMMENT_TYPE_QUESTION) {

      team_sites_common_entity_vote($cid, 'comment');
      // Update comment for get new votes value
      $comment = comment_load($cid, TRUE);

      return team_sites_api_get_answer_object($comment);
    }
    throw new Exception(t('Answer not found'), TEAM_SITES_API_ERROR_ANSWER_NOT_FOUND);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Get list of events.
 *
 * @param string $token
 *   Unique user token
 *
 * @return array
 *   Array of objects with events.
 */
function team_sites_api_get_events($token) {
  try {
    $response = team_sites_api_get_views_content(TEAM_SITES_API_VIEW_NAME, TEAM_SITES_API_EVENTS_LIST_DISPLAY);
    $events = team_sites_api_group_events_by_day($response);

    return $events;
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Get list of flash update posts.
 *
 * @param string $token
 *   Unique user token
 *
 * @return array
 *   Array of flash updates posts.
 */
function team_sites_api_get_flash_update($token) {
  global $user;
  try {
    $response = array();
    if (empty($user->field_user_campus[LANGUAGE_NONE][0]['tid'])) {
      return $response;
    }

    $nodes = team_sites_common_get_user_flash_update_nodes();
    if ($nodes) {
      $response = team_sites_api_preprocess_flash_update($nodes, $user);
    }

    return $response;
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Get list of social posts.
 *
 * @param string $token
 *   Unique user token
 * @param int $limit
 *   Posts limit
 * @param int $offset
 *   Posts offset
 *
 * @return array|mixed
 */
function team_sites_api_get_social_list($token, $limit, $offset) {
  try {
    $result = team_sites_api_get_views_content(TEAM_SITES_API_VIEW_NAME, TEAM_SITES_API_SOCIAL_LIST_DISPLAY, array(), $limit, $offset);

    return team_sites_api_preprocess_social_list($result);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Get general app information.
 *
 * @param string $token
 *
 * @return array|mixed
 */
function team_sites_api_get_general_info($token) {
  try {
    global $user;

    // User for the needs of mobile app.
    $app_user = team_sites_api_get_app_user_object($user);

    $cache = team_sites_common_get_cache(TEAM_SITES_API_APP_GENERAL_CID);
    if ($cache) {
      $cache['user'] = $app_user;

      return $cache;
    }

    $result = team_sites_api_get_app_general_content();
    team_sites_common_set_cache(TEAM_SITES_API_APP_GENERAL_CID, $result);
    $result['user'] = $app_user;

    return $result;
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Get whats new feed data with events.
 * This method for old apps.
 *
 * @param string $token
 *   Unique user token
 * @param $filter
 *   Array of content type names
 * @param int $offset
 *   Items offset
 *
 * @return array|mixed
 * @deprecated
 * @todo Remove this method after apps update.
 */
function team_sites_api_get_whats_new_list($token, $filter, $offset) {
  try {
    $args = NULL;
    $response = array();
    if ($offset == "-1") {
      $response = new stdClass();
      $response->items = array();
      $response->next_offset = -1;

      return $response;
    }

    // get slide items if this is request without offset
    if (!$offset) {
      $response['slide_items'] = team_sites_api_get_whats_new_slide_items();
    }

    // get upcoming events
    if (!$offset && (empty($filter) || ($filter == TEAM_SITES_CT_EVENTS))) {
      $response['upcoming_events'] = _team_sites_api_get_upcoming_events_list();
    }

    $items = array();
    $first_post = variable_get('team_sites_common_date_first_post');
    while (count($items) < TEAM_SITES_API_DEFAULT_LIMIT) {
      $next_offset = $offset + TEAM_SITES_API_WHATS_NEW_STEP_DAYS;
      // start date
      $start = 'now';
      $end = '-' . TEAM_SITES_API_WHATS_NEW_STEP_DAYS . ' days';
      if ($next_offset > 1) {
        $start = '-' . $offset . ' days';
        $end = '-' . $next_offset . ' days';
      }
      $start = new DateTime($start);
      $start->setTime(23, 59, 59); // set time for end of day

      // end date
      $end = new DateTime($end);
      $end->setTime(23, 59, 59); // set time for start of day

      if ($first_post > $end->getTimestamp()) {
        break;
      }
      if (!empty($filter)) {
        switch ($filter) {
          case TEAM_SITES_CT_EVENTS:
            // get only events
            $events = team_sites_api_get_whats_new_events($start, $end);
            $items = array_merge($items, $events);
            break;

          case TEAM_SITES_CT_POST:
            // If filter is post, return blog posts and posts items.
            $filter = array($filter, TEAM_SITES_CT_BLOG_POST);
            $filter = implode(',', $filter);
            $content = team_sites_api_get_whats_new_content($start, $end, array($filter));
            $items = array_merge($items, $content);
            break;

          default:
            // get everything except events using filter
            $content = team_sites_api_get_whats_new_content($start, $end, array($filter));
            $items = array_merge($items, $content);
            break;
        }
      }
      else {
        // get everything except events using filter
        $content = team_sites_api_get_whats_new_content($start, $end);
        $items = array_merge($items, $content);
      }
      $offset += TEAM_SITES_API_WHATS_NEW_STEP_DAYS;
    }
    // sort items and return
    $result_array = array();
    // Save item count for fix bug with same creation date sort
    $items_count = count($items);
    foreach ($items as $item) {
      $result_array[$item->date] = $item;
    }
    ksort($result_array, SORT_NUMERIC);
    $response['items'] = array_reverse($result_array);
    $response['next_offset'] = $next_offset;
    if ($items_count < TEAM_SITES_API_DEFAULT_LIMIT) {
      $response['next_offset'] = "-1";
    }

    return $response;
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Get What's New items list without events.
 *
 * @param string $token
 * @param string $filter
 * @param int $limit
 * @param int $offset
 *
 * @return array|mixed
 */
function team_sites_api_get_whats_new($token, $filter, $limit, $offset) {
  try {
    $response = array();
    if (empty($filter) && !$offset) {
      // Get upcoming events.
      $response['upcoming_events'] = _team_sites_api_get_upcoming_events_list();
    }

    if (!$offset) {
      $response['slide_items'] = team_sites_api_get_whats_new_slide_items();
    }

    if ($filter == 'post') {
      $filter = array(TEAM_SITES_CT_BLOG_POST, TEAM_SITES_CT_POST);
      $filter = array(implode(',', $filter));
    }
    else {
      $filter = !empty($filter) ? array($filter) : array();
    }

    $items = team_sites_api_get_views_content(TEAM_SITES_API_VIEW_NAME, TEAM_SITES_API_WHATS_NEW_LIST_DISPLAY, $filter, $limit, $offset);
    team_sites_api_preprocess_whats_new($items);
    $response['items'] = $items;

    return $response;
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Search users in LDAP
 *
 * @param string $token
 *   Unique user token
 *
 * @param array $filters
 *  List of filters:
 *  - @string keyword
 *  - @string personality_type
 *  - @string strengths
 *  - @string campus
 *
 * @param int $limit
 *
 * @return array|mixed
 */
function team_sites_api_tsstaff_directory_search($token, $filters, $limit) {
  try {
    if (!isset($filters['keyword']) && !isset($filters['personality_type']) && !isset($filters['strengths']) && !isset($filters['campus'])) {
      throw new Exception(t('At least one filter is required.'), TEAM_SITES_API_ERROR_ANSWER_NOT_FOUND);
    }

    if (isset($filters['keyword'])) {
      $filters['keyword'] = team_sites_common_prevalidate_search_query($filters['keyword']);
    }

    $result = team_sites_common_ldap_tsstaff_directory_search($filters);
    if (!empty($result)) {
      return team_sites_api_tsstaff_directory_prepare_profiles($result);
    }

    return array();
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Global app search.
 *
 * Search in questions, events, pages, blog posts and LDAP profiles.
 *
 * @param string $token
 *   User unique token
 * @param string $search_query
 *   Search query
 * @param int $limit
 *   Result limit
 * @param int $offset
 *   Result offset
 *
 * @deprecated
 * @todo remove this API method after apps update.
 *
 * @return array|mixed
 *   Array of found items.
 */
function team_sites_api_global_search($token, $search_query, $limit, $offset) {
  try {
    if (is_string($search_query) && !empty($search_query)) {
      $response = array();

      $found = _team_sites_api_content_search($search_query, $limit, $offset);
      if (!empty($found)) {
        $response = $found;
      }

      $found_comments = _team_sites_api_comment_search($search_query, $limit, $offset);
      if (!empty($found_comments)) {
        $response = array_merge($response, $found_comments);
      }

      $profiles = team_sites_common_ldap_tsstaff_directory_search(array('keyword' => $search_query), $limit);
      $profiles = team_sites_api_tsstaff_directory_prepare_profiles($profiles);
      foreach ($profiles as $profile) {
        $item = new stdClass();
        $item->type = 'profile';
        $item->profile_item = $profile;
        $response[] = $item;
      }

      shuffle($response);

      return $response;
    }
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * New version of method for general search
 *
 * @param string $token
 * @param string $search_query
 * @param int $limit
 * @param int $offset
 *
 * @return array|mixed
 */
function team_sites_api_app_global_search($token, $search_query, $limit, $offset) {
  try {
    if (is_string($search_query) && !empty($search_query)) {
      $found_content = array();
      $found_profiles = array();

      $search_query = team_sites_common_prevalidate_search_query($search_query);

      $found = _team_sites_api_content_search($search_query, $limit, $offset);
      if (!empty($found)) {
        $found_content = $found;
      }

      $found_comments = _team_sites_api_comment_search($search_query, $limit, $offset);
      if (!empty($found_comments)) {
        $found_content = array_merge($found_content, $found_comments);
      }

      $profiles = team_sites_common_ldap_tsstaff_directory_search(array('keyword' => $search_query), $limit);
      $profiles = team_sites_api_tsstaff_directory_prepare_profiles($profiles);
      foreach ($profiles as $profile) {
        $item = new stdClass();
        $item->type = 'profile';
        $item->profile_item = $profile;
        $found_profiles[] = $item;
      }

      usort($found_content, 'team_sites_common_sort_by_search_score');

      $response = new stdClass();
      $response->content = $found_content;
      $response->profiles = $profiles;

      return $response;
    }
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Register spouse for user
 *
 * @param string $token
 *   Unique user token
 * @param array $spouse
 *   Object of spouse data
 *
 * @return mixed|stdClass
 *   Return object of response data
 */
function team_sites_api_spouse_register($token, $spouse) {
  global $user;

  try {
    if (!team_sites_common_is_user_spouse($user)) {
      // Check required fields
      if (empty($spouse['name']) || empty($spouse['mail']) || empty($spouse['pass'])) {
        throw new Exception(t('Invalid Spouse Data'), TEAM_SITES_API_ERROR_SPOUSE_DATA_INVALID);
      }
      if (!empty($spouse['name']) && !preg_match(TEAM_SITES_SPOUSE_NAME_VALIDATE_REGEXP, $spouse['name'])) {
        throw new Exception(t('Spouse name can not contain special characters'), TEAM_SITES_API_ERROR_SPOUSE_DATA_INVALID);
      }
      // Check pass length
      if (strlen($spouse['pass']) < TEAM_SITES_COMMON_MIN_SPOUSE_PASS_LENGTH) {
        throw new Exception(t("Password must be greater than @min_length characters", array('@min_length' => TEAM_SITES_COMMON_MIN_SPOUSE_PASS_LENGTH)), TEAM_SITES_API_ERROR_INVALID_SPOUSE_PASS_LENGTH);
      }
      // Check email format
      if (!valid_email_address($spouse['mail'])) {
        throw new Exception(t('The e-mail address @email is not valid.', array('@email' => $spouse['mail'])), TEAM_SITES_API_ERROR_INVALID_SPOUSE_EMAIL);
      }
      // Check that email is unique
      if (team_sites_common_check_user_email($spouse['mail'])) {
        throw new Exception(t("Email already exists"), TEAM_SITES_API_ERROR_SPOUSE_EMAIL_ALREADY_EXIST);
      }
      // If user already have spouse.
      if (!empty($user->field_spouse[LANGUAGE_NONE][0]['uid'])) {
        throw new Exception(t('Spouse for your account already exists. You can add only one spouse'), TEAM_SITES_API_ERROR_SPOUSE_ALREADY_CREATED);
      }

      $spouse = (object) $spouse;
      $spouse_user = team_sites_common_create_spouse($spouse);

      $response = new stdClass();
      $response->spouse = team_sites_api_get_spouse_object($spouse_user);

      return $response;
    }
    throw new Exception(t("Can't register spouse for this user"), TEAM_SITES_API_ERROR_SPOUSE_IMPOSSIBLE_SPOUSE_REGISTER);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Update spouse profile
 *
 * @param string $token
 *   Unique user token
 * @param array $spouse
 *   Object of spouse data
 *
 * @return bool|mixed|stdClass
 *   Return updated object of spouse data
 */
function team_sites_api_spouse_profile_update($token, $spouse) {
  try {
    global $user;

    $spouse = (object) $spouse;
    if (empty($spouse->id) || empty($spouse->name) || empty($spouse->mail)) {
      throw new Exception(t('Invalid Spouse Data'), TEAM_SITES_API_ERROR_SPOUSE_DATA_INVALID);
    }

    if (!empty($user->field_spouse[LANGUAGE_NONE][0]['uid']) && $user->field_spouse[LANGUAGE_NONE][0]['uid'] == $spouse->id) {
      $spouse_user = user_load($spouse->id);
      if ($spouse_user) {
        $edit['mail'] = $spouse->mail;
        if (!empty($spouse->pass)) {
          $edit['pass'] = $spouse->pass;
        }
        $edit['field_display_name'][LANGUAGE_NONE][0]['value'] = $spouse->name;

        user_save($spouse_user, $edit);
        $response = new stdClass();
        $response->spouse = team_sites_api_get_spouse_object($spouse_user);

        return $response;
      }
    }
    throw new Exception(t('Invalid Spouse id'), TEAM_SITES_API_ERROR_SPOUSE_NOT_EXISTS);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Remove spouse account
 *
 * @param string $token
 *   Unique user token
 *
 * @return array
 *  Array with operation status
 *
 * @throws Exception
 */
function team_sites_api_spouse_delete($token) {
  try {
    global $user;

    if (!empty($user->field_spouse[LANGUAGE_NONE][0]['uid'])) {
      $uid = $user->field_spouse[LANGUAGE_NONE][0]['uid'];
      user_delete($uid);
    }
    else {
      throw new Exception(t("Spouse account has already been removed."), TEAM_SITES_API_ERROR_SPOUSE_NOT_EXISTS);
    }

    return array('status' => TRUE);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Watchdog log information
 *
 * @param array|object|string $log
 *
 * @return array
 */
function team_sites_api_watchdog_log($log) {
  watchdog(WATCHDOG_INFO, 'Mobile log information: @message', array('@message' => print_r($log, TRUE)));

  return array('status' => TRUE);
}

/**
 * Get user data object
 *
 * @param string $token
 *   Unique user token
 *
 * @return stdClass
 */
function team_sites_api_get_user($token) {
  global $user;

  return team_sites_api_get_app_user_object($user);
}

/**
 * Register user device endpoint in the amazon topic by user campus
 *
 * @param string $token
 *   Unique user token
 * @param string $endpoint
 *  Unique device endpoint from Amazon SNS service
 *
 * @return array|mixed
 */
function team_sites_api_register_endpoint($token, $endpoint) {
  global $user;

  try {
    if (empty($user->field_user_campus[LANGUAGE_NONE][0]['tid'])) {
      throw new Exception(t("User doesn't have a campus."), TEAM_SITES_API_ERROR_USER_NOT_CAMPUS);
    }
    if (empty($endpoint)) {
      throw new Exception(t("Endpoint is required."), TEAM_SITES_API_ERROR_SNS_ENDPOINT_REQUIRED);
    }
    $q = db_select('users_sns_endpoints', 'usnse');
    $q->addField('usnse', 'id');
    $q->condition('usnse.endpoint', $endpoint);
    $q->condition('usnse.uid', $user->uid);
    $use_id = $q->execute()->fetchColumn();
    if (!empty($use_id)) {
      return array(
        'status' => TRUE,
        'message' => t('Endpoint already exist and subscribed.')
      );
    }

    $term = taxonomy_term_load($user->field_user_campus[LANGUAGE_NONE][0]['tid']);
    if (!empty($term->field_topic_arn)) {
      $topic_arn = $term->field_topic_arn[LANGUAGE_NONE][0]['value'];
      $config = team_sites_common_get_sns_config();
      $sns = team_sites_common_get_sns_class($config);
      if ($sns) {
        $result = $sns->subscribe($topic_arn, $endpoint);
        $subscription_arn = $result->get('SubscriptionArn');
        if (!empty($subscription_arn)) {
          // save endpoint to our table.
          $record = new stdClass();
          $record->uid = $user->uid;
          $record->endpoint = $endpoint;
          $record->ts = REQUEST_TIME;
          $record->subscriptionarn = $subscription_arn;
          drupal_write_record('users_sns_endpoints', $record);

          return array('status' => TRUE);
        }
      }
    }

    return array(
      'status' => FALSE,
      'message' => t('Can\'t subscribe user to topic.')
    );
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Get user LDAP profile
 *
 * @param string $token
 *  Unique user token
 * @param int $uid
 *  user id
 *
 * @return stdClass
 */
function team_sites_api_get_ldap_profile($token, $uid) {
  try {
    $query = db_select('field_data_field_samaccountname', 'fdfs');
    $query->condition('fdfs.entity_type', 'user');
    $query->condition('fdfs.bundle', 'user');
    $query->condition('fdfs.entity_id', $uid);
    $query->addField('fdfs', 'field_samaccountname_value');
    $samaccountname = $query->execute()->fetchField();

    if ($samaccountname) {
      $profile = team_sites_common_ldap_profile_load($samaccountname);
      if ($profile) {
        $response = $profile;
        $response->uid = $uid;
        return $response;
      }
    }

    throw new Exception(t('Can\'t get information for this user from LDAP.'), TEAM_SITES_API_ERROR_LOAD_LDAP_DATA);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Return list of upcoming events
 *
 * @param string $token
 *  Unique user token
 *
 * @return array
 * Upcoming events array
 */
function team_sites_api_get_upcoming_events($token) {
  return _team_sites_api_get_upcoming_events_list();
}

/**
 * Subscribe user to event.
 *
 * @param string $token
 *  Unique user token
 * @param $nid
 *   Event node nid.
 * @param $interval
 *   Notification interval. It can be NULL if event not have specific start time.
 *
 * @return mixed
 */
function team_sites_api_event_subscribe($token, $nid, $interval) {
  global $user;

  try {
    $node = node_load($nid);

    if ($node && $node->type == TEAM_SITES_CT_EVENTS) {
      if (!team_sites_common_subscribe_user_to_event($node, $interval, $user)) {
        throw new Exception(t('User already subscribed to this event.'), TEAM_SITES_API_ERROR_USER_EVENT_SUBSCRIBED);
      }

      $node = node_load($nid, NULL, TRUE);

      return team_sites_api_get_event_from_node($node);
    }

    throw new Exception(t('Event not found.'), TEAM_SITES_API_ERROR_EVENT_NOT_FOUND);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Unsubscribe user from event
 *
 * @param string $token
 *  Unique user token
 * @param int $nid
 *  Event id(node id)
 *
 * @return mixed
 */
function team_sites_api_event_unsubscribe($token, $nid) {
  global $user;

  try {
    $node = node_load($nid);
    if ($node && $node->type == TEAM_SITES_CT_EVENTS) {
      team_sites_common_unsubscribe_user_from_event($nid, $user);

      $node = node_load($nid, NULL, TRUE);

      return team_sites_api_get_event_from_node($node);
    }

    throw new Exception(t('Event not found.'), TEAM_SITES_API_ERROR_EVENT_NOT_FOUND);
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}

/**
 * Return link with special single sign in token.
 *
 * @param string $token
 *  Unique user token
 * @param string $link
 *  Link
 *
 * @return array|mixed
 */
function team_sites_api_generate_single_sign_in_link($token, $link) {
  global $user;

  try {
    $response = array();
    $link_token = hash('sha1', $user->uid . 'link' . $link) . '/' . base64_encode($user->uid) . '/' . base64_encode(time());
    $response['url'] = $link . '?key=' . $link_token;

    return $response;
  }
  catch (Exception $e) {
    return team_sites_api_exception_helper($e);
  }
}
