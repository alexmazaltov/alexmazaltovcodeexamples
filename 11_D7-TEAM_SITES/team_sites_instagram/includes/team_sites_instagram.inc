<?php
/**
 * @file
 * File contain functions for work with Intagramm API.
 */

?>

<?php
use Instagram\Instagram;
use Instagram\Auth;

/**
 * Get instagram media by instagram account id.
 *
 * @param int $id
 *   Instagram account id.
 * @param int $count
 *   Photo count limit.
 *
 * @return bool|\Instagram\Collection\StdClass
 *   Return Instagram Collection object or FALSE.
 */
function team_sites_instagram_load_media_by_user_id($id, $count = NULL) {
  try {
    $instagram = team_sites_instagram_get_instagram_class();

    $client_id = variable_get(TEAM_SITES_INSTAGRAM_CLIENTS_ID_VARIABLE);
    if ($client_id) {
      if (!$count) {
        $count = TEAM_SITES_INSTAGRAM_INSTAGRAM_PHOTO_COUNT;
      }

      $instagram->setClientID($client_id);

      $instagram_account = $instagram->getUser($id);
      $media = $instagram_account->getMedia(array('count' => $count));
      if (!empty($media)) {
        return $media->getData();
      }
      else {
        return FALSE;
      }
    }
  }
  catch (Exception $e) {
    $message = t('Error with get instagram photos - @message, code: @code.', array('@message' => $e->getMessage()));
    watchdog('Instagram', $message, array(
      '@code' => $e->getCode(),
    ), WATCHDOG_ALERT);

    return FALSE;
  }
}

/**
 * Load instagram user by instagram name
 *
 * @param $username
 *
 * @return bool|\Instagram\User
 */
function team_sites_instagram_get_user_by_username($username) {
  try {
    $auth_acc = team_sites_instagram_get_auth_account();
    if ($auth_acc && !empty($auth_acc['oauth_token'])) {
      $instagram = team_sites_instagram_get_instagram_class();

      $client_id = variable_get(TEAM_SITES_INSTAGRAM_CLIENTS_ID_VARIABLE);
      if ($client_id) {
        $instagram->setClientID($client_id);
        $account = $instagram->getUserByUsername($username);

        if (!empty($account)) {
          return $account;
        }
      }

      return FALSE;
    }
  }
  catch (Exception $e) {
    watchdog('Instagram', 'Error with get instagram account - @message, code: @code.', array(
      '@message' => $e->getMessage(),
      '@code' => $e->getCode(),
    ), WATCHDOG_ALERT);

    return FALSE;
  }
}

/**
 * OAuth authorization on Instagram
 */
function team_sites_instagram_auth() {
  $library = libraries_load('instagram_api');
  libraries_load_files($library);

  $client_id = variable_get(TEAM_SITES_INSTAGRAM_CLIENTS_ID_VARIABLE);
  $client_secret = variable_get(TEAM_SITES_INSTAGRAM_CLIENTS_SECRET_VARIABLE);

  if (!empty($client_id)) {
    $auth_config = array(
      'client_id' => $client_id,
      'client_secret' => $client_secret,
      'redirect_uri' => _team_sites_instagram_get_auth_url(),
      'scope' => array('likes', 'comments', 'relationships')
    );

    $auth = new Auth($auth_config);
    $auth->authorize();
  }
}

/**
 * Oauth menu callback. Save auth instagram account with oauth token
 */
function team_sites_instagram_save_oauth_token() {
  $library = libraries_load('instagram_api');
  libraries_load_files($library);

  $client_id = variable_get(TEAM_SITES_INSTAGRAM_CLIENTS_ID_VARIABLE);
  $client_secret = variable_get(TEAM_SITES_INSTAGRAM_CLIENTS_SECRET_VARIABLE);

  if (!empty($client_id)) {
    $auth_config = array(
      'client_id' => $client_id,
      'client_secret' => $client_secret,
      'redirect_uri' => _team_sites_instagram_get_auth_url(),
      'scope' => array('likes', 'comments', 'relationships')
    );

    $auth = new Auth($auth_config);
    $oauth_token = $auth->getAccessToken($_GET['code']);
    if ($oauth_token) {
      $instagram = new Instagram($oauth_token);
      $current_user = $instagram->getCurrentUser();
      team_sites_instagram_account_save($current_user, TRUE, $oauth_token);
    }

    drupal_goto(TEAM_SITES_INSTAGRAM_ADMIN_SETTINGS_PAGE);
  }
}

/**
 * Load instagram class
 *
 * @return \Instagram\Instagram
 */
function team_sites_instagram_get_instagram_class() {
  $class = & drupal_static(__FUNCTION__);
  if (!isset($class)) {
    $library = libraries_load('instagram_api');
    libraries_load_files($library);
    $class = new Instagram();
  }

  return $class;
}
