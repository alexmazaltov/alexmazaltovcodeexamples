<?php
/**
 * @file Helpers functions for Team sites Instagram module
 */

/**
 * Helper for get OAuth redirect URL.
 */
function _team_sites_instagram_get_auth_url() {
  return url(TEAM_SITES_INSTAGRAM_OAUTH_URL, array('absolute' => TRUE));
}

/**
 * Save instagram account
 *
 * @param $account
 * @param bool $save_auth
 * @param null $oauth_token
 *
 * @throws \Exception
 * @throws \InvalidMergeQueryException
 */
function team_sites_instagram_account_save($account, $save_auth = FALSE, $oauth_token = NULL) {
  global $user;

  $instagram_uid = $account->getUserId();
  if (!empty($instagram_uid)) {
    $values['name'] = $account->getUserName();
    $values['full_name'] = $account->getFullName();
    $values['instagram_uid'] = $account->getUserId();
    $values['uid'] = $user->uid;

    if ($save_auth && $oauth_token) {
      $values['oauth_token'] = $oauth_token;
    }

    db_merge('staff_instagram_account')
      ->key(array('instagram_uid' => $values['instagram_uid']))
      ->fields($values)
      ->execute();
    drupal_set_message(t('Instagram account added successfully'));
  }
}

/**
 * Helper for get instagram authorized account
 */
function team_sites_instagram_get_auth_account() {
  $query = db_select('staff_instagram_account', 'sia');
  $query->fields('sia');
  $query->isNotNull('sia.oauth_token');

  return $query->execute()->fetchAssoc();
}

/**
 * Load instagram account from DB
 *
 * @param $inst_uid
 *
 * @return mixed
 */
function team_sites_instagram_account_load($inst_uid) {
  $query = db_select('staff_instagram_account', 'sia');
  $query->fields('sia');
  $query->condition('sia.instagram_uid', $inst_uid);

  return $query->execute()->fetchAssoc();
}

/**
 * Helper for get instagram non authorized account
 */
function team_sites_instagram_get_non_auth_accounts() {
  $query = db_select('staff_instagram_account', 'sia');
  $query->fields('sia');
  $query->isNull('sia.oauth_token');

  return $query->execute()->fetchAllAssoc('instagram_uid');
}

/**
 * Load all accounts from DB
 */
function team_sites_instagram_accounts_load() {
  $query = db_select('staff_instagram_account', 'sia');
  $query->fields('sia');

  return $query->execute()->fetchAllAssoc('instagram_uid');
}

/**
 * Delete instagram account form DB
 *
 * @param $inst_uid
 */
function team_sites_instagram_account_delete($inst_uid) {
  db_delete('staff_instagram_account')->condition('instagram_uid', $inst_uid)->execute();
}

/**
 * Create new node with instagram standart resolution image
 *
 * @param $photo
 *  Instagram item object
 * @param null $uid
 *
 * @return bool
 */
function team_sites_instagram_create_photo_node($photo, $uid = NULL) {
  try {
    $image = $photo->getStandardResImage();

    if ($image) {
      // Not create if node with this photo is exists.
      if (team_sites_instagram_check_instagram_item_exists($photo->getId(), TEAM_SITES_CT_BLOG_PHOTO)) {
        return FALSE;
      }

      $photo_id = $photo->getId();
      $url = $image->url;

      $caption = $photo->getCaption();

      $title = _team_sites_instagram_preprare_instagram_item_title($caption);
      if (empty($title)) {
        $title = t('Instagram Photo ' . $photo_id);
      }

      $node = team_sites_common_get_blank_node_object($title, TEAM_SITES_CT_BLOG_PHOTO, $uid);
      // Save photo URL.
      $node->field_instagram_photo_link[LANGUAGE_NONE][0]['url'] = $url;
      // Save instagram item id for check exists node with this item.
      $node->field_instagram_photo_id[LANGUAGE_NONE][0]['value'] = $photo_id;
      if (!empty($caption)) {
        // Save instagram photo description
        $node->field_photo_description[LANGUAGE_NONE][0]['value'] = team_sites_instagram_remove_emoji(check_plain($caption));
      }
      node_save($node);
    }
  }
  catch (Exception $e) {
    $message = t('Error with import instagram photo: @message, code: @code.', array(
      '@message' => $e->getMessage(),
    ));
    watchdog('Team_Sites_Instagram', $message, array(
      '@code' => $e->getCode(),
    ), WATCHDOG_ALERT);

    return FALSE;
  }
}

/**
 * Create What's New Video node with instagram video link.
 *
 * @param $media
 * @param null $uid
 *
 * @return bool
 */
function team_sites_instagram_create_video_node($media, $uid = NULL) {
  try {
    $video = $media->getStandardResVideo();

    if ($video) {
      $video_id = $media->getId();

      if (team_sites_instagram_check_instagram_item_exists($video_id, TEAM_SITES_CT_BLOG_VIDEO)) {
        return FALSE;
      }

      $url = $video->url;
      $image = $media->getStandardResImage();
      $caption = $media->getCaption();
      $caption = team_sites_instagram_remove_emoji($caption);

      $title = _team_sites_instagram_preprare_instagram_item_title($caption);
      if (empty($title)) {
        $title = t('Instagram Video @video_id', array('@video_id' => $video_id));
      }

      $node = team_sites_common_get_blank_node_object($title, TEAM_SITES_CT_BLOG_VIDEO, $uid);
      // Save video URl.
      $node->field_instagram_video_url[LANGUAGE_NONE][0]['url'] = $url;
      // Save photo URL for thumbnail and video poster.
      $node->field_instagram_photo_link[LANGUAGE_NONE][0]['url'] = $image->url;
      // Save instagram item id for check exists node with this item.
      $node->field_instagram_photo_id[LANGUAGE_NONE][0]['value'] = $video_id;
      // Save Instagram description
      if (!empty($caption)) {
        $node->field_photo_description[LANGUAGE_NONE][0]['value'] = check_plain($caption);
      }
      node_save($node);
    }
  }
  catch (Exception $e) {
    $message = t('Error with import instagram video: @message, code: @code.', array(
      '@message' => $e->getMessage(),
      '@code' => $e->getCode(),
    ));
    watchdog('Team_Sites_Instagram', $message, array(
      '@code' => $e->getCode(),
    ), WATCHDOG_ALERT);

    return FALSE;
  }
}

/**
 * Check instagram item with specific id exists in bundle or not.
 *
 * @param $item_id
 *   Instagram item id
 * @param string $bundle
 *   Content type machine name
 *
 * @return mixed
 */
function team_sites_instagram_check_instagram_item_exists($item_id, $bundle = 'photo') {
  $query = db_select('field_data_field_instagram_photo_id', 'fdfipi');
  $query->fields('fdfipi', array('entity_id'));
  $query->condition('fdfipi.field_instagram_photo_id_value', $item_id);
  $query->condition('fdfipi.entity_type', 'node');
  $query->condition('fdfipi.bundle', $bundle);

  return $query->execute()->fetchField();
}

/**
 * Helper for get plain title from instagram item caption.
 *
 * @param $item_caption
 *
 * @return string
 */
function _team_sites_instagram_preprare_instagram_item_title($item_caption) {
  try {
    $title = check_plain($item_caption);
    $title = decode_entities($title);
    $title = team_sites_instagram_remove_emoji($title);

    $title = truncate_utf8($title, TEAM_SITES_INSTAGRAM_MAX_TITLE_LENGTH, TRUE, TRUE);

    return $title;
  }
  catch (Exception $e) {
    $message = t('Error with get photo title: @message, code: @code.', array(
      '@message' => $e->getMessage(),
    ));
    watchdog('Team_Sites_Instagram', $message, array(
      '@code' => $e->getCode(),
    ), WATCHDOG_ALERT);

    return FALSE;
  }
}

/**
 * Remove instagram emoji bytes from string.
 *
 * @param $text
 *
 * @return mixed|string
 */
function team_sites_instagram_remove_emoji($text) {
  // Match Emoticons
  $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
  $clean_text = preg_replace($regexEmoticons, '', $text);

  // Match Miscellaneous Symbols and Pictographs
  $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
  $clean_text = preg_replace($regexSymbols, '', $clean_text);

  // Match Transport And Map Symbols
  $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
  $clean_text = preg_replace($regexTransport, '', $clean_text);

  // Match Miscellaneous Symbols
  $regexMisc = '/[\x{2600}-\x{26FF}]/u';
  $clean_text = preg_replace($regexMisc, '', $clean_text);

  // Match Dingbats
  $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
  $clean_text = preg_replace($regexDingbats, '', $clean_text);

  return $clean_text;
}
