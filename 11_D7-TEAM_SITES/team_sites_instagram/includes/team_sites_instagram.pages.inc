<?php

/**
 * @file Page callback and form builders
 */

/**
 * Confirm form for delete instagram non auth account
 */
function team_sites_instagram_account_delete_confirm($form, $form_state, $inst_uid) {
  $account = team_sites_instagram_account_load($inst_uid);
  $form['instagram_uid'] = array('#type' => 'value', '#value' => $account['instagram_uid']);
  if ($account) {
    return confirm_form($form, t('Are you sure you want to delete %title?', array('%title' => $account['name'])), TEAM_SITES_INSTAGRAM_ADMIN_SETTINGS_PAGE, t('This action cannot be undone.'), t('Delete'), t('Cancel'));
  }
  drupal_not_found();
}

/**
 * Executes account deletion.
 *
 * @see team_sites_instagram_account_delete_confirm()
 */
function team_sites_instagram_account_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    team_sites_instagram_account_delete($form_state['values']['instagram_uid']);
    drupal_set_message(t('Account has been deleted.'));
  }

  $form_state['redirect'] = TEAM_SITES_INSTAGRAM_ADMIN_SETTINGS_PAGE;
}

/**
 * Page callback for instagram admin settings page
 */
function team_sites_instagram_admin_page() {
  $output = array();

  $output['settings'] = drupal_get_form('team_sites_instagram_admin_settings_form');
  $output['auth_account'] = drupal_get_form('team_sites_instagram_auth_account_form');
  $output['accounts'] = drupal_get_form('team_sites_instagram_account_form');

  return $output;
}

/**
 * Form builder for instagram settings page form
 */
function team_sites_instagram_admin_settings_form($form, $form_state) {
  $form = array();

  $form['instagram'] = array(
    '#type' => 'fieldset',
    '#title' => t('Instagram Settings'),
    '#description' => t('The following settings for connect to Instagram APIs.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['instagram']['team_sites_instagram_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Instagram CLIENT ID'),
    '#size' => 60,
    '#description' => t('Instagram CLIENT ID from @url', array('@url' => TEAM_SITES_INSTAGRAM_URL_APP_REGISTER)),
    '#default_value' => variable_get('team_sites_instagram_client_id'),
  );

  $form['instagram']['team_sites_instagram_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Instagram CLIENT SECRET'),
    '#size' => 60,
    '#description' => t('Instagram CLIENT SECRET from @url', array('@url' => TEAM_SITES_INSTAGRAM_URL_APP_REGISTER)),
    '#default_value' => variable_get('team_sites_instagram_client_secret'),
  );

  return system_settings_form($form);
}

/**
 * Form to add an Instagram account.
 */
function team_sites_instagram_account_form($form, $form_state) {
  $form['add_account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add Instagram accounts'),
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $accounts = team_sites_instagram_get_non_auth_accounts();
  if ($accounts) {
    $header = array('name', 'full_name', 'actions');

    $rows = array();
    foreach ($accounts as $account) {
      $del_link = l(t('Delete'), 'admin/team-sites/instagram/' . $account->instagram_uid . '/delete');
      $rows[] = array($account->name, $account->full_name, $del_link);
    }

    $form['add_account']['accounts'] = array(
      '#type' => 'markup',
      '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
    );
  }

  $auth_acc = team_sites_instagram_get_auth_account();

  if ($auth_acc) {
    $form['add_account']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Instagram account name'),
      '#size' => 60,
      '#description' => t('Instagram account name'),
      '#default_value' => '',
    );
    $form['add_account']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add'),
    );
  }
  else {
    $form['add_account']['non_auth_text'] = array(
      '#type' => 'markup',
      '#markup' => t('One authenticated account is needed for add non authenticated accounts.'),
    );
  }

  return $form;
}

/**
 * Form to add an authenticated Instagram account.
 */
function team_sites_instagram_auth_account_form($form, $form_state) {
  $form['auth'] = array(
    '#type' => 'fieldset',
    '#title' => t('Auth Instagram account'),
    '#description' => 'Authenticated account is needed for work with Instagram API.',
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $auth = team_sites_instagram_get_auth_account();
  if (!$auth) {
    $form['auth']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Go to Instagram to add an authenticated account'),
      '#submit' => array('team_sites_instagram_auth_account_form_submit'),
    );
  }
  else {
    $header = array('name', 'full_name');
    $rows[] = array($auth['name'], $auth['full_name']);
    $form['auth']['auth_account'] = array(
      '#type' => 'markup',
      '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
    );
    $form['auth']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete auth account'),
      '#submit' => array('team_sites_instagram_remove_auth_account_submit'),
    );
  }

  return $form;
}

/**
 * Form validation for adding a new Instagram account.
 */
function team_sites_instagram_auth_account_form_validate($form, &$form_state) {
  $client_id = variable_get(TEAM_SITES_INSTAGRAM_CLIENTS_ID_VARIABLE);
  $client_secret = variable_get(TEAM_SITES_INSTAGRAM_CLIENTS_SECRET_VARIABLE);

  if ($client_id == '') {
    form_set_error('', t('Please configure your client id key'));
  }

  if ($client_secret == '') {
    form_set_error('', t('Please configure your client secret key'));
  }
}

/**
 * Submit callback for 'team_sites_instagram_account_form'.
 * Save instagram account to db.
 */
function team_sites_instagram_account_form_submit($form, $form_state) {
  $account = team_sites_instagram_get_user_by_username($form_state['values']['name']);
  if ($account) {
    team_sites_instagram_account_save($account);
  }
}

/**
 * Form submit handler for adding a Instagram account.
 */
function team_sites_instagram_auth_account_form_submit($form, &$form_state) {
  team_sites_instagram_auth();
}

/**
 * Submit callback for delete auth instagram account
 */
function team_sites_instagram_remove_auth_account_submit($form, $form_state) {
  db_delete('staff_instagram_account')->isNotNull('oauth_token')->execute();
  drupal_set_message(t('Instagram account deleted successfully'));
}

/**
 * Validate callback for 'team_sites_instagram_account_form' form
 */
function team_sites_instagram_account_form_validate($form, $form_state) {
  if (!empty($form_state['values']['name'])) {
    $account = team_sites_instagram_get_user_by_username($form_state['values']['name']);
    if (!$account) {
      form_set_error('name', t('Not found account with this instagram name'));
    }
  }
}
