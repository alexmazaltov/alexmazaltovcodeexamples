<?php
/**
 * @file Template for render instagram embeded video
 * Available variables:
 * - $inst_short_url: Instagram short url.
 */
?>

<blockquote class="instagram-media" data-instgrm-version="3">
  <a href="<?php print $inst_short_url; ?>" target="_top"></a>
</blockquote>
<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>