<?php
/**
 * @file
 * File contains all functions which works with cron.
 */

/**
 * Implements hook_cronapi().
 */
function team_sites_common_cronapi($op, $job = NULL) {
  $items['team_sites_common_check_campuses_cron'] = array(
    'description' => 'Check that campuses list is actual and display message if need to synchronize',
    // Execute job every day at 4:00
    'rule' => '0 4 * * *',
    'callback' => 'team_sites_common_check_lifechurch_campuses',
  );

//  $items['team_sites_common_cron_update_personality_type_count_users'] = array(
//    'description' => 'Update number of users for each personality type',
//    // Execute job every day at 3:00
//    'rule' => '0 3 * * *',
//    'callback' => 'team_sites_common_cron_update_personality_type_count_users',
//  );

//  $items['team_sites_common_clear_upcoming_events_cache'] = array(
//    'description' => 'Clear upcoming events API cache',
//    // Execute job every day at 00:00
//    'rule' => '0 0 * * *',
//    'callback' => 'team_sites_common_clear_upcoming_events_api_cache',
//  );

//  $items['team_sites_common_cron_push_queue_send'] = array(
//    'description' => 'Send push messages from queue',
//    // Execute job every 1 minute
//    'rule' => '*/1 * * * *',
//    'callback' => 'team_sites_common_cron_send_push_from_queue',
//  );

//  $items['team_sites_common_remove_sent_push_from_queue'] = array(
//    'description' => 'Remove already sent push messages from queue table',
//    // Execute job every day at 01:00
//    'rule' => '0 1 * * *',
//    'callback' => 'team_sites_common_remove_sent_push_from_queue',
//  );

  return $items;
}

/**
 * Check and compare lifechurch campuses.
 *
 * @param null $term
 *   Deleted term
 */
function team_sites_common_check_lifechurch_campuses($term = NULL) {
  $campuses = team_sites_common_get_lifechurch_api_campus_list();
  if ($campuses) {
    $voc = taxonomy_vocabulary_machine_name_load(TEAM_SITES_TAXONOMY_VOC_NAME_CAMPUSES);
    $terms = taxonomy_term_load_multiple(array(), array('vid' => $voc->vid));

    // Unset deleted term
    if (is_object($term)) {
      unset($terms[$term->tid]);
    }

    $new = array();
    foreach ($campuses as $campus) {
      if (!_team_sites_common_campus_term_is_exists($campus, $terms)) {
        $new[] = $campus['name'];
      }
    }

    $deleted = array();
    foreach ($terms as $term) {
      if (!_team_sites_common_campus_is_exists($term, $campuses)) {
        $deleted[] = $term->name;
      }
    }

    team_sites_common_save_campus_message($new, $deleted);
  }
}

/**
 * Helper for check exist taxonomy term for current campus or not
 *
 * @param $campus
 *   Campus data array
 * @param $terms
 *   Taxonomy terms array
 *
 * @return bool
 *   True if term exists or FALSE if terms is not exists.
 */
function _team_sites_common_campus_term_is_exists($campus, $terms) {
  foreach ($terms as $term) {
    if (!empty($campus['name']) && $term->name == $campus['name']) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Check that exists campus in lifechurch API for this campus or not
 *
 * @param $term
 *   Campus taxonomy term
 * @param $campuses
 *   Array of campuses
 *
 * @return bool
 *   Return TRUE if exists and FALSE if not exists
 */
function _team_sites_common_campus_is_exists($term, $campuses) {
  foreach ($campuses as $campus) {
    if (!empty($term->name) && $term->name == $campus['name']) {
      return TRUE;
    }
  }

  return FALSE;
}


/**
 * Update number of users associated with personality type.
 */
function team_sites_common_cron_update_personality_type_count_users() {
  $voc = taxonomy_vocabulary_machine_name_load(TEAM_SITES_TAXONOMY_VOC_NAME_PERSONALITY_TYPES);
  $terms = taxonomy_term_load_multiple(array(), array('vid' => $voc->vid));
  $ldap_query = ldap_query_get_queries(TEAM_SITES_LDAP_QUERY_NAME_PERSONALITY_TYPE_DYNAMIC, 'enabled', TRUE);
  $ldap_query->original_filter = $ldap_query->filter;
  foreach ($terms as $term) {
    $ldap_query->filter = $ldap_query->original_filter;
    $replacements = array('@personality_type' => $term->name);
    $result = team_sites_common_ldap_query_execute($ldap_query, $replacements);
    $count = 0;
    if (is_array($result)) {
      $count = count($result);
    }
    $term->field_number_of_people[LANGUAGE_NONE][0]['value'] = $count;
    taxonomy_term_save($term);
  }
}

/**
 * Clear upcoming events data cache.
 */
function team_sites_common_clear_upcoming_events_api_cache() {
  team_sites_common_clear_cache(TEAM_SITES_API_UPCOMING_EVENTS_CID);
}

/**
 * Send push by queue.
 */
function team_sites_common_cron_send_push_from_queue() {
  $id_array = array();
  $disbaled_endpoints = array();
  $sns_config = team_sites_common_get_sns_config();
  $sns = team_sites_common_get_sns_class($sns_config);

  // Send events pushes.
  $query = db_select('staff_push_notifications_queue', 'spnq');
  $query->condition('spnq.status', 0);
  $query->condition('spnq.send_time', date(TEAM_SITES_COMMON_PUSH_QUEUE_DATE_FORMAT, REQUEST_TIME));
  $query->condition('spnq.type', TEAM_SITES_CT_EVENTS);
  $query->fields('spnq', array('id', 'nid', 'params'));
  $query->leftJoin('node', 'n', 'spnq.nid = n.nid');
  $query->addField('n', 'title');
  $query->condition('n.status', NODE_PUBLISHED);
  $query->leftJoin('users_sns_endpoints', 'usev', 'usev.uid = spnq.uid');
  $query->addField('usev', 'endpoint');
  $event_messages = $query->execute()->fetchAll();

  if ($event_messages) {
    foreach ($event_messages as $event_push) {
      try {
        if (!empty($event_push->params)) {
          $params = unserialize($event_push->params);
          if (isset($params['event_date'])) {
            $date = new DateTime($params['event_date']);
            $date = $date->format(TEAM_SITES_COMMON_APP_EVENT_DATE_FORMAT);

            $message = team_sites_common_build_sns_message($event_push->title, $event_push->nid, TEAM_SITES_CT_EVENTS, array('event_date' => $date));

            if ($sns && $sns->sendPersonalPush($message, $event_push->endpoint)) {
              $id_array[] = $message->id;
            }
          }
        }
      }
      catch (Exception $e) {
        if ($e->getCode() == 0) {
          $disbaled_endpoints[] = $message->endpoint;
        }
        continue;
      }
    }
  }

  // Send Scheduled Flash Updates
  $query = db_select('staff_push_notifications_queue', 'spnq');
  $query->condition('spnq.status', 0);
  $query->condition('spnq.send_time', date(TEAM_SITES_COMMON_PUSH_QUEUE_DATE_FORMAT, REQUEST_TIME));
  $query->condition('spnq.type', TEAM_SITES_CT_FLASH_UPDATE);
  $query->fields('spnq', array('id', 'nid'));
  $flash_messages = $query->execute()->fetchAll();

  foreach ($flash_messages as $message) {
    // Publish node if not published yet
    $node = node_load($message->nid);
    if (!$node->status) {
      node_publish_action($node);
      node_save($node);
    }
    // Send push via SNS
    _team_sites_common_send_flash_update_push($node);
    $id_array[] = $message->id;
  }

  // Set sent status
  if (!empty($id_array)) {
    db_update('staff_push_notifications_queue')->fields(array('status' => 1))->condition('id', $id_array, 'in')
      ->execute();
  }

  // remove disabled endpoints
  if (!empty($disbaled_endpoints)) {
    db_delete('users_sns_endpoints')->condition('endpoint', $disbaled_endpoints, 'in')->execute();
  }

}

/**
 * Remove already sent push messages from queue table
 */
function team_sites_common_remove_sent_push_from_queue() {
  db_delete('staff_push_notifications_queue')
    ->condition('status', 1)
    ->execute();
}
