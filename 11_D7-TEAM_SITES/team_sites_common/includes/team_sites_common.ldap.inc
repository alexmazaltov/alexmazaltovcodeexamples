<?php
/**
 * @file
 * File contains all functions which works with ldap.
 */

define('TEAM_SITES_LDAP_QUERY_NAME_USER_DYNAMIC', 'user-dynamic');
define('TEAM_SITES_LDAP_QUERY_NAME_PERSONALITY_TYPE_DYNAMIC', 'person-type-dynamic');
define('TEAM_SITES_LDAP_QUERY_NAME_GET_PERSON', 'get_person');
define('TEAM_SITES_LDAP_QUERY_NAME_GET_PERSON_DEV', 'get_person_dev');


/**
 * Execute ldap query and made some modifications
 *
 * @param LdapQuery $query
 *  Ldap query
 * @param array $replace
 *  List of replacements
 *
 * @return array|bool
 *  List of results or FALSE
 */
function team_sites_common_ldap_query_execute(LdapQuery $query, array $replace = array()) {

  if (!empty($replace)) {
    $query->filter = format_string($query->filter, $replace);
    // For accounts that contain single quotes. Like kyle.o'neal
    $query->filter = decode_entities($query->filter);
  }
  $results = $query->query();
  if (!empty($query->attributes)) {
    $rendered_result = array();
    foreach ($results as $val) {
      if (is_array($val)) {
        $item = new stdClass();
        foreach ($query->attributes as $attr) {
          $attr = strtolower($attr);
          if ($val[$attr]['count'] = 1) {
            $item->$attr = isset($val[$attr][0]) ? $val[$attr][0] : NULL;
          }
          else {
            $item->$attr = NULL; // we don't support many values in attribute
            watchdog('common', 'LDAP search returned object with multiple values inside attribute: !object', array('!object' => print_r($attr, TRUE)));
          }
        }
        $rendered_result[] = $item;
      }
    }

    return $rendered_result;
  }

  return $results;
}

/**
 * Get LDAP query and replacements for staff directory search.
 *
 * @param $filters
 *   Array of filters
 * @param int $limit
 *   Query limit
 * @return array
 *   Array with query and replacements
 */
function team_sites_common_get_tsstaff_directory_ldap_query($filters, $limit = 0) {
  $query = ldap_query_get_queries(TEAM_SITES_LDAP_QUERY_NAME_USER_DYNAMIC, 'enabled', TRUE);

  $replacements = array();
  $dynamic_filter = '(&(objectClass=user)(samaccounttype=805306368)(objectCategory=person)(!(lockouttime>=1))(!(useraccountcontrol=514))(!(useraccountcontrol=66050))(memberof=CN=allsubscribers5a8c7c15,CN=Users,DC=unity,DC=com)(!(msexchhidefromaddresslists=TRUE))';
  if (isset($filters['keyword']) && !empty($filters['keyword'])) {
    $dynamic_filter .= '(|(displayname=*@keyword*)(mobile=*@keyword*)(telephoneNumber=*@keyword*)(title=*@keyword*))';
    $replacements['@keyword'] = $filters['keyword'];
  }

  if (isset($filters['personality_type']) && !empty($filters['personality_type'])) {
    if (is_array($filters['personality_type'])) {
      $subfilter = '(|';
      foreach ($filters['personality_type'] as $i => $type) {
        $subfilter .= '(extensionAttribute1=*@personality' . $i . '*)';
        $replacements['@personality' . $i] = $type;
      }
      $dynamic_filter .= $subfilter . ')';
    }
    else {
      $dynamic_filter .= '(extensionAttribute1=@personality)';
      $replacements['@personality'] = $filters['personality_type'];
    }
  }

  if (isset($filters['strengths']) && !empty($filters['strengths'])) {
    if (is_array($filters['strengths'])) {
      $subfilter = '(|';
      foreach ($filters['strengths'] as $i => $type) {
        $subfilter .= '(extensionattribute2=*@strengths' . $i . '*)';
        $replacements['@strengths' . $i] = $type;
      }
      $dynamic_filter .= $subfilter . ')';
    }
    else {
      $dynamic_filter .= '(extensionattribute2=*@strengths*)';
      $replacements['@strengths'] = $filters['strengths'];
    }
  }

  if (isset($filters['campus']) && !empty($filters['campus'])) {
    if (is_array($filters['campus'])) {
      $subfilter = '(|';
      foreach ($filters['campus'] as $i => $type) {
        $subfilter .= '(physicaldeliveryofficename=@campus' . $i . ')';
        $replacements['@campus' . $i] = $type;
      }
      $dynamic_filter .= $subfilter . ')';
    }
    else {
      $dynamic_filter .= '(physicaldeliveryofficename=@campus)';
      $replacements['@campus'] = $filters['campus'];
    }
  }


  $dynamic_filter .= ')';
  $query->filter = $dynamic_filter;
  $query->sizelimit = $limit;

  return array('query' => $query, 'replacements' => $replacements);
}

/**
 * Build LDAP query for staff directory search
 *
 * @param array $filters
 *  List of filters:
 *  - @string keyword
 *  - @string personality_type
 *  - @string strengths
 *  - @string campus
 * @param int $limit
 *  Limit of results
 *
 * @return array|bool
 */
function team_sites_common_ldap_tsstaff_directory_search($filters, $limit = 0) {
  $query_array = team_sites_common_get_tsstaff_directory_ldap_query($filters, $limit = 0);
  return team_sites_common_ldap_query_execute($query_array['query'], $query_array['replacements']);
}

/**
 * Load LDAP profile by user samaccountname
 *
 * @param string $samaccountname
 * @return array|bool
 */
function team_sites_common_ldap_profile_load($samaccountname) {
  if ($samaccountname) {
    $query = ldap_query_get_queries(TEAM_SITES_LDAP_QUERY_NAME_GET_PERSON, 'enabled', TRUE);
    if ($query) {
      $profile = team_sites_common_ldap_query_execute($query, array('@samaccountname' => $samaccountname));
      if (isset($profile[0])) {
        $profile = team_sites_api_tsstaff_directory_prepare_profiles($profile);
        return array_shift($profile);
      }
    }
  }

  return FALSE;
}
