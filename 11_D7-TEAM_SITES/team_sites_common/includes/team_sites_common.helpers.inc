<?php
/**
 * @file
 *  Helper functions
 */


/**
 * Truncate a UTF-8-encoded string safely to a number of characters.
 *
 * @param string $string
 *   The string to truncate.
 * @param int $len
 *   An upper limit on the returned string length.
 * @param bool $word_safe
 *   Flag to truncate at last space within the upper limit. Defaults to FALSE.
 * @param bool $dots
 *   Flag to add trailing dots. Defaults to FALSE.
 *
 * @return string
 *   The truncated string.
 */
function team_sites_common_truncate_utf8($string, $len, $word_safe = FALSE, $dots = FALSE) {
  if (drupal_strlen($string) <= $len) {
    return $string;
  }

  if ($word_safe) {
    $string = drupal_substr($string, 0, $len + 1); // leave one more character
    $last_space = strrpos($string, ' ');
    if ($last_space) { // space exists AND is not on position 0
      $string = substr($string, 0, $last_space);
    }
    else {
      $string = drupal_substr($string, 0, $len);
    }
  }
  else {
    $string = drupal_substr($string, 0, $len);
  }

  $string = trim($string);
  if ($dots) {
    $string .= '...';
  }

  return $string;
}

/**
 * Entity vote helper
 *
 * @param $entity_id
 * @param $entity_type
 *   'node' of 'comment'
 */
function team_sites_common_entity_vote($entity_id, $entity_type) {
  if (module_exists('rate')) {
    $widget = team_sites_common_get_rate_question_widget();
    rate_save_vote($widget, $entity_type, $entity_id, TEAM_SITES_RATE_QUESTION_VOTE_VALUE, TRUE);
  }
}

/**
 * Load rate module widget for questions votes.
 *
 * @return object
 */
function team_sites_common_get_rate_question_widget() {
  $widgets = variable_get(RATE_VAR_WIDGETS, array());
  if (isset($widgets[TEAM_SITES_RATE_QUESTION_WIDGET_ID])) {
    $widget = $widgets[TEAM_SITES_RATE_QUESTION_WIDGET_ID];

    return $widget;
  }
}

/**
 * Get vote count for entity.
 *
 * @param $entity_id
 *   Entity id
 * @param $entity_type
 *   Entity type. node of comment for example
 *
 * @return mixed
 */
function team_sites_common_get_entity_vote_count($entity_id, $entity_type) {
  $query = db_select('votingapi_cache', 'vc');
  $query->fields('vc', array('value'));
  $query->condition('vc.entity_type', $entity_type);
  $query->condition('vc.entity_id', $entity_id);
  $query->condition('vc.value_type', 'points');
  $query->condition('vc.tag', 'question_vote');
  $query->condition('vc.function', 'count');

  return $query->execute()->fetchField();
}

/**
 * Custom helper to get comment thread.
 *
 * Added offset and order by votes.
 *
 * @param $node
 * @param int $limit
 * @param int $offset
 *
 * @return mixed
 */
function team_sites_common_comment_get_thread($node, $limit = TEAM_SITES_API_DEFAULT_LIMIT, $offset = TEAM_SITES_API_DEFAULT_OFFSET) {
  $query = db_select('comment', 'c');
  $query->addField('c', 'cid');
  $query->condition('c.nid', $node->nid)->addTag('node_access')->addTag('comment_filter')->addMetaData('node', $node);

  $query->leftJoin('votingapi_cache', 'vc', "vc.entity_id = c.cid AND vc.function = 'count' AND vc.entity_type = 'comment'");
  $query->range($offset, $limit);

  if (!user_access('administer comments')) {
    $query->condition('c.status', COMMENT_PUBLISHED);
  }

  $query->orderBy('vc.value', 'DESC');
  $query->orderBy('c.created', 'ASC');
  $cids = $query->execute()->fetchCol();

  return $cids;
}

/**
 * Return answers objects for questions node with paginate by answer id
 *
 * @param $nid
 *   Question node nid
 * @param $start_cid
 *   Answer if with which to begin paginate
 * @param $limit
 *   Answers limit
 * @param string $direction
 *   Paginate direction.
 *
 * @return mixed
 */
function team_sites_common_get_node_comments_paginate($nid, $start_cid, $limit, $direction = 'next') {
  if ($start_cid == 0) {
    $comments = _team_sites_common_load_answers($nid, $start_cid, $limit);

    return _team_sites_api_preprocess_answers($comments);
  }

  // Load answers data by limit
  $start_count = team_sites_common_get_answer_position_number($start_cid, $nid);
  $offset = 0;
  switch ($direction) {
    case 'next':
      $offset = $start_count;
      break;

    case 'prev':
      if ($start_count < $limit) {
        $offset = 0;
        $limit = $start_count - 1;
      }
      else {
        $offset = $start_count - $limit;
      }
      break;

    case 'both':
      if ($start_count < $limit) {
        $offset = 0;
        $limit += $start_count;
      }
      else {
        $offset = $start_count - $limit;
        $offset--;
        $limit = $limit * 2;
        $limit++;
      }
      break;
  }

  $comments = _team_sites_common_load_answers($nid, $offset, $limit);
  $answers = _team_sites_api_preprocess_answers($comments);

  return $answers;
}

/**
 * Helper for load answers for question.
 *
 * @param $nid
 *   Question node nid
 * @param $offset
 *   Start offset
 * @param $limit
 *   Limit
 *
 * @return mixed
 */
function _team_sites_common_load_answers($nid, $offset = TEAM_SITES_API_DEFAULT_OFFSET, $limit = TEAM_SITES_API_DEFAULT_LIMIT) {
  global $user;

  $query = db_select('comment', 'c');
  $query->distinct();
  $query->addField('c', 'cid');
  $query->addField('c', 'nid');
  $query->addField('c', 'uid');
  $query->addField('c', 'created');
  $query->addField('vc', 'value');
  $query->addField('fdcb', 'comment_body_value', 'answer');
  $query->addExpression('vv.uid = :uid', 'voted', array(':uid' => $user->uid));
  $query->condition('c.nid', $nid);
  $query->leftJoin('votingapi_cache', 'vc', "vc.entity_id = c.cid AND vc.function = 'count' AND vc.entity_type = 'comment'");
  $query->leftJoin('votingapi_vote', 'vv', "vv.entity_id = c.cid AND vc.entity_type = 'comment' AND vv.uid = :uid", array(':uid' => $user->uid));
  $query->leftJoin('field_data_comment_body', 'fdcb', "fdcb.entity_id = c.cid");
  $query->range($offset, $limit);
  $query->condition('c.status', COMMENT_PUBLISHED);
  $query->orderBy('vc.value', 'DESC');
  $query->orderBy('c.created', 'ASC');
  $query->orderBy('c.cid');

  return $query->execute()->fetchAll();
}

/**
 * Get comment position in thread.
 *
 * @param $cid
 * @param $nid
 *
 * @return mixed
 */
function team_sites_common_get_answer_position_number($cid, $nid) {
  $vote_cache_join = "vc.entity_id = c.cid AND vc.function = 'count' AND vc.entity_type = 'comment'";
  // Get data of start answer.
  $query = db_select('comment', 'c');
  $query->addField('c', 'created');
  $query->addField('vc', 'value');
  $query->leftJoin('votingapi_cache', 'vc', $vote_cache_join);
  $query->condition('c.cid', $cid);
  $query->condition('c.status', COMMENT_PUBLISHED);
  $result = $query->execute()->fetchAssoc();

  $args = array(
    ':created' => $result['created'],
    ':cid' => $cid,
    ':value' => $result['value']
  );

  // Get previous answer position in questions answers.
  $query = db_select('comment', 'c');
  $query->addExpression('count(*)');
  $query->leftJoin('votingapi_cache', 'vc', $vote_cache_join);
  $query->condition('c.status', COMMENT_PUBLISHED);
  $query->condition('c.nid', $nid);

  if (!is_null($result['value'])) {
    $query->where("(
      (vc.value > :value)
        OR ((vc.value = :value ) AND (c.created < :created))
        OR ((vc.value = :value ) AND (c.created = :created) AND (c.cid < :cid))
      )", $args);
  }
  else {
    $query->where("(
      (vc.value IS NOT NULL)
        OR ((vc.value IS NULL) AND (c.created < :created))
        OR ((vc.value IS NULL) AND (c.created = :created) AND (c.cid < :cid))
      )", $args);
  }
  $start_count = $query->execute()->fetchField();
  $start_count++;

  return $start_count;
}

/**
 * Prepare comments returned by _team_sites_common_load_answers to answers.
 *
 * @deprecated
 * @see _team_sites_preprocess_answers().
 *
 * @param $comments
 *
 * @return array
 */
function _team_sites_api_preprocess_answers($comments) {
  return _team_sites_common_preprocess_answers($comments);
}

/**
 * Prepare comments returned by _team_sites_common_load_answers to answers.
 *
 * @param $comments
 *
 * @return array
 */
function _team_sites_common_preprocess_answers($comments) {
  $answers = array();
  foreach ($comments as $comment) {
    $answer = new stdClass();
    $answer->id = (int) $comment->cid;
    $answer->answer = $comment->answer;
    $answer->timestamp = (int) $comment->created;
    $answer->voted = empty($comment->voted) ? FALSE : (bool) $comment->voted;
    $answer->votes = empty($comment->value) ? 0 : (int) $comment->value;
    $answer->question_id = (int) $comment->nid;

    $answer->user = team_sites_api_get_app_user_object($comment->uid);
    $answers[] = $answer;
  }

  return $answers;
}

/**
 * Set flash update post as viewed by user in 'users_flash_updates' table.
 *
 * @param $nid
 *   Flash update post node nid
 * @param null $user
 *   User that viewed post.
 */
function team_sites_common_set_flash_update_read($nid, $user = NULL) {
  if (!$user) {
    global $user;
  }

  $keys = array();
  $q = db_select('users_flash_updates', 'ufu');
  $q->addField('ufu', 'id');
  $q->condition('ufu.nid', $nid);
  $q->condition('ufu.uid', $user->uid);
  $fp_id = $q->execute()->fetchColumn();
  if (!empty($fp_id)) {
    $keys = array('nid', 'uid');
  }
  $record = new stdClass();
  $record->nid = $nid;
  $record->uid = $user->uid;
  $record->view_date = REQUEST_TIME;
  drupal_write_record('users_flash_updates', $record, $keys);
}

/**
 * Get campus-list from lifechurch API
 *
 * @return mixed
 */
function team_sites_common_get_lifechurch_api_campus_list() {
  $url = variable_get('team_sites_common_campus_api_url', '');
  if (!empty($url)) {
    $request = drupal_http_request($url);

    if ($request->status_message == 'OK' && $request->code == 200) {
      return drupal_json_decode($request->data);
    }
  }

  return FALSE;
}

/**
 * Save message for notice if campuse list does not match.
 *
 * @param array $new_campuses
 *   Array of added campuses.
 * @param array $removed_campuses
 *   Array of removed campuses.
 */
function team_sites_common_save_campus_message($new_campuses, $removed_campuses) {
  if (empty($new_campuses) && empty($removed_campuses)) {
    variable_set(TEAM_SITES_CAMPUS_MESSAGE_VAR, '');

    return;
  }

  $message = '';
  $text = t("Current server don't has available campuses:");
  if (!empty($new_campuses)) {
    $message .= theme_item_list(array(
      'items' => $new_campuses,
      'type' => 'ul',
      'title' => $text,
      'attributes' => array()
    ));
  }

  $text = t("Current server has extra campuses:");
  if (!empty($removed_campuses)) {
    $message .= theme_item_list(array(
      'items' => $removed_campuses,
      'type' => 'ul',
      'title' => $text,
      'attributes' => array()
    ));
  }

  variable_set(TEAM_SITES_CAMPUS_MESSAGE_VAR, $message);
}

/**
 * Save or update picture for profile of user from LDAP.
 *
 * @param string $image_str
 *  Binary image
 * @param null|string $user_id
 *  Unique user id
 *
 * @return bool|stdClass
 */
function team_sites_common_tsstaff_directory_get_profile_picture($image_str, $user_id = NULL) {
  $uri = file_build_uri('staff-profiles');
  // try to create a directory if it no exist
  if (!file_prepare_directory($uri, FILE_CREATE_DIRECTORY)) {
    watchdog('common', "Can't create directory", array(), WATCHDOG_WARNING);

    return FALSE;
  }
  if ($user_id) {
    // build real path to file
    $path = file_destination(drupal_realpath($uri . '/' . $user_id . '.png'), FILE_EXISTS_REPLACE);
    if (file_exists($path)) {
      $stat = stat($path);
      // get exist image
      if (($stat['ctime'] + 86400) > time()) { // todo tmp solution, save image 1 day
        return team_sites_common_build_get_image_data_by_path($path);
      }
    }
  }
  else {
    $path = file_destination(drupal_realpath($uri . '/no_id.png'), FILE_EXISTS_RENAME);
  }
  // create image file
  $im = imagecreatefromstring($image_str);
  if ($im !== FALSE) {
    // save image to png
    if (imagepng($im, $path)) {
      if (file_exists($path)) {
        imagedestroy($im);

        return team_sites_common_build_get_image_data_by_path($path);
      }
    }
  }

  return FALSE;
}

/**
 * Get image info about image stored in the drupal files directory.
 *
 * @param string $path
 *  Full system path
 *
 * @return stdClass
 */
function team_sites_common_build_get_image_data_by_path($path) {
  $image_info = getimagesize($path);
  $stat = stat($path);
  $part_url = str_replace(DRUPAL_ROOT . '/', '', $path);
  // return image data
  $image = new stdClass();
  $image->url = url($part_url, array('absolute' => TRUE));
    //Get replaced path for each child team sites => 'sites/default/files/'
    $replace_path = variable_get('file_public_path', conf_path() . '/files') . '/';
  $image->uri = file_build_uri(str_replace($replace_path, '', $part_url));
  $image->width = $image_info[0];
  $image->height = $image_info[1];
  $image->created = $stat['ctime'];
  $image->update = $stat['mtime'];

  return $image;
}

/**
 * Create new drupal user with spouse role.
 *
 * @param $spouse
 *   Object of spouse datas
 *
 * @return bool|stdClass
 *   Drupal user object
 */
function team_sites_common_create_spouse($spouse) {
  global $language;
  global $user;

  $account = array(
    'name' => 'email_registration_' . user_password(),
    'mail' => $spouse->mail,
    'pass' => $spouse->pass,
    'language' => $language->language,
    'status' => TRUE,
  );
  $account['roles'] = array(
    DRUPAL_AUTHENTICATED_RID => 'authenticated user',
    TEAM_SITES_SPOUSE_RID => 'spouse',
  );
  $account['field_display_name'][LANGUAGE_NONE][0]['value'] = check_plain($spouse->name);
  $account['field_spouse'][LANGUAGE_NONE][0]['uid'] = $user->uid;
  $account['field_user_campus'][LANGUAGE_NONE][0]['tid'] = $user->field_user_campus[LANGUAGE_NONE][0]['tid'];
  $spouse_user = user_save('', $account);

  // Save user reference for parent user.
  $edit['field_spouse'][LANGUAGE_NONE][0]['uid'] = $spouse_user->uid;
  user_save($user, $edit);
  // Send notify mail.
  _user_mail_notify('register_no_approval_required', $spouse_user);

  return $spouse_user;
}

/**
 * Search user profiles and return themed results
 *
 * @param $filters
 *   Array of filters
 * @return null|string
 */
function team_sites_common_tsstaff_directory_search_profiles($filters) {
  $profiles = team_sites_common_search_profiles($filters);
  $links = array(
    'personality_types_link' => variable_get('team_sites_personality_types_info_link', ''),
    'strengths_link' => variable_get('team_sites_strengths_info_link', ''),
    'spiritual_gifts_link' => variable_get('team_sites_spiritual_gifts_info_link', ''),
  );

  $items = array();
  foreach ($profiles as $profile) {
    $items[] = theme('team_sites_common_team_sites_profile', array(
      'profile' => $profile,
      'links' => $links,
      'with_filters' => TRUE,
      'contextual_links' => _team_sites_common_get_profile_image_contextual_links($profile->id),
    ));
  }

  if ($items) {
    return theme('item_list', array('items' => $items, 'attributes' => array('class' => 'profiles-list')));
  }
  return FALSE;
}

/**
 * Helper for get contextual links array for staff profile photo.
 *
 * @param $samaccountname
 * @return array
 */
function _team_sites_common_get_profile_image_contextual_links($samaccountname) {
  return array(
    '#prefix' => '<div class="contextual-links-wrapper">',
    '#suffix' => '</div>',
    '#theme' => 'links__contextual',
    '#links' => array(
      'crop-photo' => array(
        'title' => t('Edit Photo'),
        'href' => url('profile/' . $samaccountname . '/photo_crop', array(
          'absolute' => TRUE,
          'attributes' => array('target' => '_blank'),
        )),
      ),
    ),
    '#attributes' => array('class' => array('contextual-links')),
    '#attached' => array(
      'library' => array(array('contextual', 'contextual-links')),
    ),
    '#access' => user_access('access contextual links'),
  );
}

/**
 * Helper for get sorted profiles from LDAP by filters
 *
 * @param $filters
 *
 * @return array
 */
function team_sites_common_search_profiles($filters) {
  $profiles = array();
  $result = team_sites_common_ldap_tsstaff_directory_search($filters);
  if (!empty($result)) {
    $profiles = team_sites_api_tsstaff_directory_prepare_profiles($result);
    $sort = array();
    foreach ($profiles as $profile) {
      $sort[$profile->name] = $profile;
    }
    ksort($sort);
    $profiles = $sort;
  }

  return $profiles;
}

function team_sites_common_get_manual_cropped_staff_profile_picture($profile_id) {
  $uri = file_build_uri('staff-profiles-cropped');
  if ($profile_id) {
    // build real path to file
    $path = file_destination(drupal_realpath($uri . '/' . $profile_id . '.png'), FILE_EXISTS_REPLACE);
    if (file_exists($path)) {
      return team_sites_common_build_get_image_data_by_path($path);
    }
  }
}

/**
 * Build new or reload masonry layout
 *
 * @param $html
 * @param bool $new
 *
 * @return array
 */
function team_sites_common_ajax_masonry_reload($html, $new = FALSE) {
  $command = array(
    'command' => 'masonry_reload',
    'selector' => '#whats-new',
    'html' => $html,
    'new' => $new
  );

  return $command;
}

/**
 * Return object for SNS push
 *
 * @param $title
 *   Node title
 * @param $nid
 *   Node nid
 * @param $type
 *   Node type
 * @param $additional_vars
 *   Array with additional push variables
 *
 * @return stdClass
 *   Object with message data
 */
function team_sites_common_build_sns_message($title, $nid, $type, $additional_vars = array()) {
  $message = new stdClass();
  // AmazonSNS subject must be less than 100 characters long.
  $message->subject = team_sites_common_truncate_utf8($title, TEAM_SITES_AWS_MESSAGE_SUBJECT_MAXSIZE, TRUE, TRUE);
  $message->body = $title;
  $message->type = 'json';
  $message->vars = array('id' => (int) $nid, 'type' => $type);
  $message->vars = array_merge($message->vars, $additional_vars);

  return $message;
}

/**
 * Helper function for send flash update push message
 */
function _team_sites_common_send_flash_update_push($node) {
  if ($node->status && $node->type == TEAM_SITES_CT_FLASH_UPDATE) {
    $message = team_sites_common_build_sns_message($node->title, $node->nid, $node->type);
    switch ($node->field_recipients[LANGUAGE_NONE][0]['value']) {
      default:
      case 'all':
        team_sites_common_send_push_message($message);
        break;

      case 'campuses':
        $tids = array();
        foreach ($node->field_campus[LANGUAGE_NONE] as $term) {
          $tids[] = $term['tid'];
        }
        team_sites_common_send_push_message_to_campuses($message, $tids);
        break;

      case 'users':
        if (!empty($node->field_flash_update_users)) {
          $uids = array();
          foreach ($node->field_flash_update_users[LANGUAGE_NONE] as $user) {
            $uids[] = $user['uid'];
          }

          team_sites_common_send_push_message_to_selected_users($message, $uids);
        }
        break;

      case 'users_group':
        if (!empty($node->field_user_group)) {
          $nids = array();
          foreach ($node->field_user_group[LANGUAGE_NONE] as $nid) {
            $nids[] = $nid['nid'];
          }
          // We can send push to groups plus some extra users
          $additional_users = array();
          if (!empty($node->field_flash_update_users)) {
            foreach ($node->field_flash_update_users[LANGUAGE_NONE] as $uid) {
              $additional_users[] = $uid['uid'];
            }
          }
          team_sites_common_send_push_to_user_group($message, $nids, $additional_users);
        }
        break;
    }
  }
}

/**
 * Helper for getting path from view object for selected display
 *
 * @param object $view
 *  View object
 * @param string $display
 *  Display name
 *
 * @return string
 */
function team_sites_common_get_view_path($view, $display) {
  global $language;
  if ($view->display[$display]->display_plugin = 'i18n_page' && isset($view->display[$display]->display_options['path_' . $language->language])) {
    $news_path = $view->display[$display]->display_options['path_' . $language->language];
  }
  else {
    $news_path = $view->display[$display]->display_options['path'];
  }

  return $news_path;
}

/**
 * Set value to session
 *
 * @param $name Name
 * @param $val Value, if value is null remove name from session
 */
function team_sites_common_set_session($name, $val = NULL) {
  if (!is_null($val)) {
    $_SESSION[$name] = $val;
  }
  else {
    unset($_SESSION[$name]);
  }
}

/**
 * Get session value by name
 *
 * @param $name Name
 * @param $default_value Return this value if name is not exist
 *
 * @return session value
 */
function team_sites_common_get_session($name, $default_value = NULL) {
  if (isset($_SESSION[$name])) {
    return $_SESSION[$name];
  }

  return $default_value;
}

/**
 * Load all terms for specific vocabulary
 *
 * @param $voc
 * Taxonomy vocabulary object or vocabulary name.
 *  * @param $max_depth
 *   The number of levels of the tree to return. Leave NULL to return all levels.
 * @param $load_entities
 *   If TRUE, a full entity load will occur on the term objects. Otherwise they
 *   are partial objects queried directly from the {taxonomy_term_data} table to
 *   save execution time and memory consumption when listing large numbers of
 *   terms. Defaults to FALSE.
 *
 * @return array of terms
 */
function team_sites_common_get_vocabulary_terms_tree($voc, $parent = 0, $max_depth = NULL, $load_entities = FALSE) {
  if (is_string($voc)) {
    $voc = taxonomy_vocabulary_machine_name_load($voc);
  }
  if (!$voc || !is_object($voc)) {
    return FALSE;
  }

  return taxonomy_get_tree($voc->vid, $parent, $max_depth, $load_entities);
}

/**
 * Helper for get taxonomy term by campus slug
 *
 * @param $slug
 *
 * @return bool
 */
function team_sites_common_get_campus_tid_by_slug($slug) {
  $query = db_select('field_data_field_slug', 'fdfs');
  $query->addField('fdfs', 'entity_id ', 'tid');
  $query->condition('fdfs.field_slug_value', $slug);
  $tid = $query->execute()->fetchField();
  if ($tid) {
    $term = taxonomy_term_load($tid);

    return $term;
  }

  return FALSE;
}

/**
 * Returns a file object which can be passed to file_save().
 *
 * @param string $uri
 *   A string containing the URI, path, or filename.
 * @param bool $use_existing
 *   (Optional) If TRUE and there's an existing file in the {file_managed}
 *   table with the passed in URI, then that file object is returned.
 *   Otherwise, a new file object is returned. Default is TRUE.
 *
 * @return object|bool
 *   A file object, or FALSE on error.
 */
function team_sites_common_file_uri_to_object($uri, $use_existing = TRUE) {
  $file = FALSE;
  $uri = file_stream_wrapper_uri_normalize($uri);

  if ($use_existing) {
    $files = entity_load('file', FALSE, array('uri' => $uri));
    $file = !empty($files) ? reset($files) : FALSE;
  }

  if (empty($file)) {
    $file = new stdClass();
    $file->uid = $GLOBALS['user']->uid;
    $file->filename = drupal_basename($uri);
    $file->uri = $uri;
    $file->filemime = file_get_mimetype($uri);
    $file->filesize = filesize($uri);
    $file->timestamp = REQUEST_TIME;
    $file->status = FILE_STATUS_PERMANENT;
  }

  return $file;
}

/**
 * Sorts a structured array by the 'score' element.
 *
 * Callback for usort() used in various functions.
 *
 * @param $a
 *   First item for comparison. The compared items should be associative arrays
 *   that optionally include a 'score' element. For items without a 'score'
 *   element, a default value of 0 will be used.
 * @param $b
 *   Second item for comparison.
 *
 * @return sort
 */
function team_sites_common_sort_by_search_score($a, $b) {
  $a_weight = isset($a->score) ? $a->score : 0;
  $b_weight = isset($b->score) ? $b->score : 0;
  if ($a_weight == $b_weight) {
    return 0;
  }

  return ($a_weight > $b_weight) ? -1 : 1;
}

/**
 * Custom uasort() callback for sort search views results by relevance.
 *
 * @param $a
 * @param $b
 *
 * @return int
 */
function team_sites_common_sort_search_api_relevance($a, $b) {
  $a_weight = (is_object($a) && !empty($a->_entity_properties['search_api_relevance'])) ? $a->_entity_properties['search_api_relevance'] : 0;
  $b_weight = (is_object($b) && isset($b->_entity_properties['search_api_relevance'])) ? $b->_entity_properties['search_api_relevance'] : 0;
  if ($a_weight == $b_weight) {
    return 0;
  }

  return ($a_weight < $b_weight) ? -1 : 1;
}

/**
 * Create thumbnail for social mp4 video.
 *
 * @param $nid
 *   Social node nid
 * @param bool $rewrite
 *   Rewrite old thumbnail if exists.
 *
 * @return bool|string
 */
function team_sites_common_create_social_mp4_thumbnail($nid, $video_uri, $rewrite = FALSE) {
  $uri = file_build_uri('video-thumbnails');
  $path = file_destination(drupal_realpath($uri . '/social_video_thumb_' . $nid . '.png'), FILE_EXISTS_REPLACE);

  $uri = file_build_uri('video-thumbnails/social_video_thumb_' . $nid . '.png');

  if (!file_exists($path) && $rewrite) {
    // Create MP4 video thumbnail.
    try {
      $ffmpeg = php_ffmpeg();
      $file = drupal_realpath($video_uri);
      // try to create a directory if it no exist
      if (file_prepare_directory($uri, FILE_CREATE_DIRECTORY)) {
        $video_file = $ffmpeg->open($file);
        $video_file->frame(FFMpeg\Coordinate\TimeCode::fromSeconds(TEAM_SITES_API_VIDEO_THUMB_START_SECONDS))
                   ->save($path);
      }
    }
    catch (Exception $e) {
      $uri = file_build_uri('') . 'thumbnail.png';
    }
  }

  return file_create_url($uri);
}

/**
 * Return length of employment formatted string.
 *
 * @param $hire_date
 *   String of date
 *
 * @return null|string
 *   Formatted string
 */
function team_sites_common_get_employment_length_formatted($hire_date) {
  $start_date = new DateTime($hire_date);
  $since_start = $start_date->diff(new DateTime('now'));

  // "If you are in-between 1 year and 2 years then if you're in the first 6 months after your first year then it would show 1 year.
  // After 6 months it would show 1.5 years and then once you hit the second year it would show 2 years and so on."
  // So if less than half of year return New.
  if ($since_start->y < 1 && $since_start->m < 6) {
    return t('New');
  }
  $year = ($since_start->m < 6) ? $since_start->y : $since_start->y + 0.5;

  return format_plural($year, '1 year', '@count years');
}

/**
 * Implements callback_filter_process().
 *
 * Provides filtering of input into accepted HTML.
 * Custom implementation of _filter_html() process callback with.
 *
 * Not use filter_xss() because need save inline styles for font colors.
 *
 * @see _filter_html()
 * @see filter_xss()
 */
function _team_sites_common_filter_html($text, $filter) {
  $allowed_tags = preg_split('/\s+|<|>/', $filter->settings['allowed_html'], -1, PREG_SPLIT_NO_EMPTY);

  if (!drupal_validate_utf8($text)) {
    return '';
  }

  _filter_xss_split($allowed_tags, TRUE);
  // Remove NULL characters (ignored by some browsers).
  $text = str_replace(chr(0), '', $text);
  // Remove Netscape 4 JS entities.
  $text = preg_replace('%&\s*\{[^}]*(\}\s*;?|$)%', '', $text);

  // Defuse all HTML entities.
  $text = str_replace('&', '&amp;', $text);
  // Change back only well-formed entities in our whitelist:
  // Decimal numeric entities.
  $text = preg_replace('/&amp;#([0-9]+;)/', '&#\1', $text);
  // Hexadecimal numeric entities.
  $text = preg_replace('/&amp;#[Xx]0*((?:[0-9A-Fa-f]{2})+;)/', '&#x\1', $text);
  // Named entities.
  $text = preg_replace('/&amp;([A-Za-z][A-Za-z0-9]*;)/', '&\1', $text);

  if ($filter->settings['filter_html_nofollow']) {
    $html_dom = filter_dom_load($text);
    $links = $html_dom->getElementsByTagName('a');
    foreach ($links as $link) {
      $link->setAttribute('rel', 'nofollow');
    }
    $text = filter_dom_serialize($html_dom);
  }

  return trim($text);
}

/**
 * Get count of search result sorted by type.
 * It need for display counts in search tabs.
 *
 * @param $keyword
 *   Search keyword
 *
 * @return array
 *   Array results counts
 */
function team_sites_common_get_search_result_counts_by_type($keyword) {
  $types = array();
  $index = search_api_index_load(TEAM_SITES_COMMON_DATABASE_SEARCH_INDEX);
  if ($index) {
    // Content types that ,must display in 'Other' search tab.
    $types_for_pages_tab = array(
      TEAM_SITES_CT_BLOG_POST,
      //TEAM_SITES_CT_QUOTE,
      //TEAM_SITES_CT_TUTORIALS,
      TEAM_SITES_CT_BASIC_PAGE,
      //TEAM_SITES_CT_LINK,
    );

    $query = new SearchApiQuery($index, array('parse mode' => 'terms', 'conjunction' => 'OR'));
    $query->keys($keyword);

    $filter = $query->createFilter('AND');
    $filter->condition('status', NODE_PUBLISHED);

    $sub_filter = $query->createFilter('OR');
    //$sub_filter->condition('type', TEAM_SITES_CT_QUESTION);
    //
    //Do search elastica for articles if blog is not disabled
    if( variable_get('moc_ts_blog_blog_enabled', TRUE) ){
      $sub_filter->condition('type', TEAM_SITES_CT_BLOG_POST);
    }
    $sub_filter->condition('type', TEAM_SITES_CT_BASIC_PAGE);
    //$sub_filter->condition('type', TEAM_SITES_CT_TEAM_VIDEO);
    //$sub_filter->condition('type', TEAM_SITES_CT_QUOTE);
    //$sub_filter->condition('type', TEAM_SITES_CT_TUTORIALS);
    //$sub_filter->condition('type', TEAM_SITES_CT_LINK);
    // Because we can search only future events
      // $events_filter = $query->createFilter('AND');
      // $events_filter->condition('type', TEAM_SITES_CT_EVENTS);
      // $events_filter->condition('field_event_date', time(), '>=');
      // $sub_filter->filter($events_filter);

    $filter->filter($sub_filter);
    $query->filter($filter);
    $query->range(0, TEAM_SITES_COMMON_SEARCH_API_QUERY_MAX_RANGE);
    $data = $query->execute();
    $results = $data['results'];

    foreach ($results as $result) {
      if (!empty($result['fields']['type'])) {
        if (in_array($result['fields']['type'], $types_for_pages_tab)) {
          $key = 'other';
        }
        else {
          $key = $result['fields']['type'];
        }

        if (isset($types[$key])) {
          $count = $types[$key];
          $count++;
          $types[$key] = $count;
        }
        else {
          $types[$key] = 1;
        }
      }
    }

    $data = _team_sites_common_search_comment($keyword);
    if ($data) {
      if (isset($types[TEAM_SITES_CT_QUESTION])) {
        $types[TEAM_SITES_CT_QUESTION] = $types[TEAM_SITES_CT_QUESTION] + $data['result count'];
      }
      else {
        $types[TEAM_SITES_CT_QUESTION] = $data['result count'];
      }
    }
  }

  return $types;
}

/**
 * Helper fot custom search api query for comments
 *
 * @param $keywords
 * @return array
 */
function _team_sites_common_search_comment($keywords) {
  $comment_index = search_api_index_load(TEAM_SITES_COMMON_COMMENTS_SEARCH_INDEX);
  if ($comment_index) {
    $query = new SearchApiQuery($comment_index);
    $query->keys($keywords);
    $query->range(0, TEAM_SITES_COMMON_SEARCH_API_QUERY_MAX_RANGE);
    return $query->execute();
  }
  NULL;
}

/**
 * Load flash update nodes for specific user.
 *
 * @param null $user
 *   Drupal user object
 *
 * @return bool
 *   Return array of nodes or FALSE
 */
function team_sites_common_get_user_flash_update_nodes($user = NULL) {
  if (!$user) {
    global $user;
    $user = user_load($user->uid);
  }

  if (empty($user->field_user_campus[LANGUAGE_NONE][0]['tid'])) {
    return FALSE;
  }

  $campus_tid = $user->field_user_campus[LANGUAGE_NONE][0]['tid'];
  $nids = _team_sites_common_get_user_flash_update_nids($campus_tid, $user->uid);

  if ($nids) {
    return node_load_multiple($nids);
  }

  return FALSE;
}

/**
 * Get flash update nids for specific user
 *
 * @param $campus_tid
 *   Campus taxonomy term tid
 * @param $uid
 *   Drupal user uid
 *
 * @return mixed
 *   Array of nids
 */
function _team_sites_common_get_user_flash_update_nids($campus_tid, $uid) {
  /*$q = db_select('node', 'n');
  $q->addField('n', 'nid', 'id');
  $q->leftJoin('field_data_field_recipients', 'fdfr', "n.nid = fdfr.entity_id AND (fdfr.entity_type = 'node')");
  $q->leftJoin('field_data_field_flash_update_users', 'fdffuu', "n.nid = fdffuu.entity_id");
  $q->leftJoin('field_data_field_user_group', 'fdfug', "n.nid = fdfug.entity_id");
  $q->leftJoin('field_data_field_users', 'fdfu', "fdfu.entity_id = fdfug.field_user_group_nid");
  $q->leftJoin('field_data_field_campus', 'fdfc', "n.nid = fdfc.entity_id AND (fdfc.entity_type = 'node')");
  $q->condition('n.status', NODE_PUBLISHED);
  $q->condition('n.type', TEAM_SITES_CT_FLASH_UPDATE);
  // Notifications for all, user campus, users, users group or user group plus some extra users.
  $q->where("(
      (fdfr.field_recipients_value = 'all') OR
      ( (fdfr.field_recipients_value = 'campuses') AND (fdfc.field_campus_tid = :campus_tid) ) OR
      ( (fdfr.field_recipients_value = 'users') AND fdffuu.field_flash_update_users_uid = :uid ) OR
      ( (fdfr.field_recipients_value = 'users_group') AND fdfu.field_users_uid = :uid) OR
      ( (fdfr.field_recipients_value = 'users_group') AND fdffuu.field_flash_update_users_uid = :uid) )", array(
    ':campus_tid' => $campus_tid,
    ':uid' => $uid
  ));
  $q->orderBy('n.created', "DESC");
  $q->range(0, TEAM_SITES_API_FLASH_UPDATE_LIMIT);

  return $q->execute()->fetchCol();*/
    return NULL;
}

/**
 * Write db record for set that user view notification node.
 *
 * @param $nid
 *   Flash Update node nid
 * @param null $user
 *   Drupal user object
 * @return bool
 */
function team_sites_common_set_view_notification($nid, $user = NULL) {
  if (!$user) {
    global $user;
  }

  $query = db_select('users_flash_updates', 'ufu');
  $query->addField('ufu', 'id');
  $query->condition('ufu.nid', $nid);
  $query->condition('ufu.uid', $user->uid);
  $view_id = $query->execute()->fetchAll();

  if ($view_id) {
    // there is no need to store viewing data, if there is already a record.
    return FALSE;
  }

  $record = new stdClass();
  $record->nid = $nid;
  $record->uid = $user->uid;
  $record->view_date = REQUEST_TIME;
  drupal_write_record('users_flash_updates', $record);
}

/**
 * Get count of tutorials node for specific category term
 *
 * @param $tid
 *   Tutorials category term tid
 *
 * @return mixed
 */
function team_sites_common_tutorial_categories_count_nodes($tid) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')->entityCondition('bundle', TEAM_SITES_CT_TUTORIALS)
    ->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_tutorials_category', 'tid', $tid, '=')
    ->count();

  return $query->execute();
}

/**
 * Gets the rendered or rendered content of a block.
 *
 * @param string $module
 *   The machine name of the module that provides the block.
 * @param mixed $delta
 *   The delta of the desired block.
 *
 * @return mixed
 *   The string representing the rendered block contents.
 */
function team_sites_common_block_embed_block($module, $delta) {
  $block = module_invoke($module, 'block_view', $delta);

  return $block['content'];
}

/**
 * Small helper for get rendered views exposed form
 *
 * @param $view_name
 * @param $display_name
 *
 * @return mixed
 */
function _team_sites_common_get_rendered_views_exposed_form($view_name, $display_name) {
  $view = views_get_view($view_name);
  $view->set_display($display_name);
  $view->init_handlers();
  $exposed_form = $view->display_handler->get_plugin('exposed_form');

  return $exposed_form->render_exposed_form(TRUE);
}

/**
 * Get blank node object for specific type and user
 *
 * @param $title
 *   Node type
 * @param $type
 *   Node type
 * @param $uid
 *   Node author uid
 *
 * @return stdClass
 */
function team_sites_common_get_blank_node_object($title, $type, $uid) {
  global $language;

  $node = new stdClass();
  $node->type = $type;
  node_object_prepare($node);
  $node->title = $title;
  $node->language = $language->language;
  $node->uid = $uid ? $uid : 1;
  $node->status = NODE_PUBLISHED;
  $node->promote = NODE_NOT_PROMOTED;
  $node->sticky = NODE_NOT_STICKY;

  switch ($type) {
    case TEAM_SITES_CT_BLOG_PHOTO:
    case TEAM_SITES_CT_BLOG_VIDEO:
      $node->field_share[LANGUAGE_NONE][0]['value'] = 1;
      break;

    case TEAM_SITES_CT_QUOTE:
      $node->field_tweet[LANGUAGE_NONE][0]['value'] = TRUE;
      break;
  }

  return $node;
}

/**
 * Helper for get count of found profiles. It uses on search pages for build tabs
 *
 * @param $keyword
 *   Search keywords
 * @return int
 */
function team_sites_common_get_search_ldap_profiles_count($keyword) {
  if (!empty($keyword)) {
    $query_array = team_sites_common_get_tsstaff_directory_ldap_query(array('keyword' => $keyword), $limit = 0);
    extract($query_array);
    if (!empty($query) && !empty($replacements)) {
      $query->attributes = array('mail');
      if (!empty($replacements)) {
        $query->filter = format_string($query->filter, $replacements);
        $query->filter = decode_entities($query->filter);
      }
      $results = $query->query();
      return $results['count'];
    }
  }

  return 0;
}

/**
 * Prevalidate global search query string.
 * Remove special characters
 *
 * @param $query
 *  Search keywords string
 * @return string
 */
function team_sites_common_prevalidate_search_query($query) {
  $query = trim(preg_replace('/\s*\([^)]*\)/', '', $query));
  $query = str_replace('"', '', $query);
  return $query;
}

/**
 * Save LDAP photo to user picture field.
 * If user NULL only return file fid without user saving
 *
 * @param $profile
 *   LDAP profile object
 * @param null $user
 *   user you want to add photos
 * @return mixed
 * @throws Exception
 */
function team_sites_common_save_ldap_photo_to_user_picture($profile, $user = NULL) {
  $edit = array();
  if ($profile && !empty($profile->thumbnailphoto)) {
    $image = team_sites_common_tsstaff_directory_get_profile_picture($profile->thumbnailphoto, $profile->id);
    if ($image) {
      $uri = file_build_uri('staff-profiles/' . $profile->id . '.png');

      $file = team_sites_common_file_uri_to_object($uri);

      if (!isset($file->fid)) {
        $file = file_save($file);
      }

      if (!empty($file->fid) && $user) {
        $edit['picture'] = $file;
        user_save($user, $edit);
      }

      return $file->fid;
    }
  }

  return FALSE;
}

/**
 * Check that absolute url is HTTPS and set HTTPS for HTTP urls
 *
 * @param $url
 * @return string
 */
function team_sites_common_check_url_https($url) {
  if (!preg_match('/https/', $url)) {
    $url_explode = explode(':', $url);
    if (!empty($url_explode[0])) {
      $url_explode[0] .= 's';
      $url = implode(':', $url_explode);
    }
  }

  return $url;
}
