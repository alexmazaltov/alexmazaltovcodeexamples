<?php

/**
 * @file
 *  Provide debug functions.
 */

/***************************************************************************/

/**
 * Debug...
 */
function team_sites_sho() {
  $args = func_get_args();
  print '<pre>';
  foreach ($args as $arg) {
    print_r($arg);
    print '<br/>-----------------------<br/>';
  }
  print '</pre>';
}

function team_sites_shoc() {
  $args = func_get_args();
  print '<div style="width: 750px; height: 650px; overflow: auto; position: relative; background-color: #ffffcc; border: 2px solid #ffcc00;">';
  call_user_func_array('team_sites_sho', $args);
  print '</div>';
}

function team_sites_shodie() {
  $args = func_get_args();
  call_user_func_array('team_sites_sho', $args);
  die();
}

function team_sites_shocdie() {
  $args = func_get_args();
  call_user_func_array('team_sites_shoc', $args);
  die();
}

function team_sites_shokpr() {
  $input = func_get_args();
  if (merits_krumo($input)) {
    return krumo($input);
  }
  else {
    team_sites_sho($input);
  }
}

function team_sites_shokprdie() {
  $input = func_get_args();
  if (merits_krumo($input)) {
    krumo($input);
  }
  else {
    team_sites_sho($input);
  }
  die;
}


/**
 * Devs functions
 */
function team_sites_printr($val, $die = TRUE) {
  print_r($val);
  if ($die) {
    die();
  }
}
