<?php

/**
 * @file
 *  Provide Amazon SNS features.
 */

/**
 * Get Amazon sns config
 *
 * @return array|null
 */
function team_sites_common_get_sns_config() {
  $environment = variable_get('team_sites_common_aws_environment', 'dev');
  $access_key = variable_get('team_sites_common_aws_' . $environment . '_access_key_id', '');
  $secret_key = variable_get('team_sites_common_aws_' . $environment . '_secret_access_key_id', '');
  if (!empty($access_key) && !empty($secret_key)) {
    $topic_arn = variable_get('team_sites_common_aws_sns_' . $environment . '_topic_arn', '');
    $region = variable_get('team_sites_common_aws_sns_' . $environment . '_region', '');

    return array(
      'access_key' => $access_key,
      'secret_key' => $secret_key,
      'topic_arn' => array($topic_arn),
      'region' => $region
    );
  }

  return NULL;
}

/**
 * Load amazon sns class.
 *
 * @param array $config
 *  Config
 *
 * @return class Team_Sites_Sns
 */
function team_sites_common_get_sns_class($config) {
  $class = & drupal_static(__FUNCTION__, NULL);
  if (!isset($class)) {
    try {
      $library = libraries_load('amazon-sns');
      libraries_load_files($library);
      module_load_include('php', 'team_sites_common', 'includes/Team_Sites_Sns');


      if (!empty($config)) {
        $class = new Team_Sites_Sns($config);
      }
    }
    catch (\Aws\CloudFront\Exception\Exception $e) {
      watchdog('Amazon SNS', 'Exception error trying get amazon sns client. @message', array('@message' => $e->getMessage()), WATCHDOG_EMERGENCY);
    }
  }

  return $class;
}

/**
 * Helper function for sending notifications throw Amazon SNS service
 *
 * @param object $message
 *  - string 'subject': Subject of message
 *  - array|string 'body': Body of message
 *  - string 'type': Type pf message: json or string
 *  - array 'vars': Array of additional variables
 *
 * @return bool
 */
function team_sites_common_send_push_message($message) {
  $config = team_sites_common_get_sns_config();
  $sns = team_sites_common_get_sns_class($config);
  if ($sns) {
    return $sns->sendPush($message);
  }

  return FALSE;
}

/**
 * Send push messages to one or more campus users
 *
 * @param object $message
 *  - string 'subject': Subject of message
 *  - array|string 'body': Body of message
 *  - string 'type': Type pf message: json or string
 *  - array 'vars': Array of additional variables
 *
 * @param array $campuses
 *  List of taxonomy term ids
 *
 * @return bool
 */
function team_sites_common_send_push_message_to_campuses($message, $campuses = array()) {
  if (empty($campuses)) {
    return team_sites_common_send_push_message($message);
  }
  $config = team_sites_common_get_sns_config();
  $config['topic_arn'] = array();
  $terms = taxonomy_term_load_multiple($campuses);
  foreach ($terms as $term) {
    if (!empty($term->field_topic_arn[LANGUAGE_NONE][0]['value'])) {
      $config['topic_arn'][] = $term->field_topic_arn[LANGUAGE_NONE][0]['value'];
    }
  }

  $sns = team_sites_common_get_sns_class($config);
  if ($sns) {
    return $sns->sendPush($message);
  }

  return FALSE;
}

/**
 * Send pushes to custom user group
 *
 * @param object $message
 * @param array $uids
 *
 * @return mixed
 */
function team_sites_common_send_push_message_to_selected_users($message, $uids = array()) {
  if (empty($uids)) {
    return team_sites_common_send_push_message($message);
  }

  $endpoints = _team_sites_common_get_endpoints_by_uids($uids);
  if ($endpoints) {
    _team_sites_common_send_push_to_endpoints($message, $endpoints);
  }

  return FALSE;
}

/**
 * Send push to selected user group
 *
 * @param object $message
 *   Message
 * @param array $nids
 *   User Group node nid
 * @param $additional_users
 *   Array of uids for extra users that must have push notification too
 */
function team_sites_common_send_push_to_user_group($message, $nids, $additional_users = array()) {
  $q = db_select('field_data_field_users', 'fdfu');
  $q->addField('fdfu', 'field_users_uid');
  $q->condition('fdfu.entity_id', $nids, 'in');
  $q->condition('fdfu.bundle', TEAM_SITES_CT_USER_GROUP);
  $uids = $q->execute()->fetchCol();
  $uids = array_merge($uids, $additional_users);
  $uids = array_unique($uids);

  $endpoints = _team_sites_common_get_endpoints_by_uids($uids);
  if ($endpoints) {
    _team_sites_common_send_push_to_endpoints($message, $endpoints);
  }
}

/**
 * Get endpoints for selected users
 *
 * @param array $uids
 *  user ids
 *
 * @return mixed
 */
function _team_sites_common_get_endpoints_by_uids($uids) {
  $query = db_select('users_sns_endpoints', 'usend');
  $query->condition('usend.uid', $uids, 'in');
  $query->addField('usend', 'endpoint');
  $query->isNotNull('usend.endpoint');
  $result = $query->execute()->fetchAll();

  return $result;
}

/**
 * Sens push message to endpoints
 *
 * @param object $message
 *   Message
 * @param $endpoints
 *   Array of endpoints
 *
 * @return bool
 */
function _team_sites_common_send_push_to_endpoints($message, $endpoints) {
  if ($endpoints) {
    $sns_config = team_sites_common_get_sns_config();
    $sns = team_sites_common_get_sns_class($sns_config);
    if ($sns) {
      $disbaled_endpoints = array();
      foreach ($endpoints as $push) {
        if (!empty($push->endpoint)) {
          try {
            $sns->sendPersonalPush($message, $push->endpoint);
          }
          catch (Exception $e) {
            if ($e->getCode() == 0) {
              $disbaled_endpoints[] = $push->endpoint;
            }
            continue;
          }
        }
      }

      if (!empty($disbaled_endpoints)) {
        db_delete('users_sns_endpoints')->condition('endpoint', $disbaled_endpoints, 'in')->execute();
      }

      return TRUE;
    }
  }

  return FALSE;
}

