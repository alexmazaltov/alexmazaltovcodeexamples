<?php

use Aws\Sns\SnsClient;
use Aws\Common\Credentials\Credentials;

/**
 * Class Team_Sites_Sns
 */
class Team_Sites_Sns {

  private $client;

  /**
   * List of config settings for Amazon
   *
   * @var array
   *  - string 'access_key': Amazon Access key
   *  - string 'secret_key': Amazon secret key
   *  - string 'region': Amazon sns region
   *  - array 'topic_arn': List of topic arn's
   */
  private $config;

  /**
   * Initialize AWS client.
   */
  public function __construct($config) {
    $this->config = $config;
    $credentials = new Credentials($this->config['access_key'], $this->config['secret_key']);

    $client = SnsClient::factory(array(
      'credentials' => $credentials,
      'region' => $this->config['region']
    ));
    $this->client = $client;
  }

  /**
   * Prepare body of message for json format
   *
   * @param object $message
   *  - string 'subject': Subject of message
   *  - array|string 'body': Body of message
   *  - string 'type': Type pf message: json or string
   *  - array 'vars': Array of additional variables
   *
   * @return string
   *  json encoded string
   */
  public function prepareJsonBodyMessage($message) {
    $environment = variable_get('team_sites_common_aws_environment', 'dev');
    $json_array = array(
      'default' => $message->subject
    );
    switch ($environment) {
      case 'dev':
        $json_array['APNS_SANDBOX'] = array(
          'aps' => array(
            'alert' => $message->subject,
            'sound' => 'default',
            'badge' => 1,
          )
        );
        if (isset($message->vars)) {
          $json_array['APNS_SANDBOX'] = array_merge($json_array['APNS_SANDBOX'], $message->vars);
        }
        $json_array['APNS_SANDBOX'] = drupal_json_encode($json_array['APNS_SANDBOX']);
        break;

      case 'prod':
        $json_array['APNS'] = array(
          'aps' => array(
            'alert' => $message->subject,
            'sound' => "default",
            'badge' => 1,
          )
        );

        if (isset($message->vars)) {
          $json_array['APNS'] = array_merge($json_array['APNS'], $message->vars);
        }
        $json_array['APNS'] = drupal_json_encode($json_array['APNS']);
        break;
    }
    $json_array['GCM'] = array(
      'data' => array(
        "message" => $message->subject
      )
    );
    if (isset($message->vars)) {
      $json_array['GCM']['data'] = array_merge($json_array['GCM']['data'], $message->vars);
    }
    $json_array['GCM']['data']['default'] = $message->subject;
    $json_array['GCM'] = json_encode($json_array['GCM']);

    return drupal_json_encode($json_array);
  }

  /**
   * Send notification
   *
   * @param object $message
   *  - string 'subject': Subject of message
   *  - array|string 'body': Body of message
   *  - string 'type': Type pf message: json or string
   *  - array 'vars': Array of additional variables
   */
  public function sendPush($message) {

    if (empty($message->type)) {
      $message->type = 'string';
    }
    if ($message->type == 'json') {
      $message->body = $this->prepareJsonBodyMessage($message);
    }

    foreach ($this->config['topic_arn'] as $topic_arn) {
      $result = $this->client->publish(array(
        'TopicArn' => $topic_arn,
        'Message' => $message->body,
        'Subject' => $message->subject,
        'MessageStructure' => $message->type,
      ));
      $messageID = $result->get('MessageId');
      if (empty($messageID)) {
        watchdog('Amazon SNS', 'Message @message was not delivered to topic @topic_arn', array(
          '@message' => print_r($message, TRUE),
          '@topic_arn' => $topic_arn
        ), WATCHDOG_WARNING);
      }
    }
  }

  public function sendPersonalPush($message, $endpointArn) {
    if (empty($message->type)) {
      $message->type = 'string';
    }
    if ($message->type == 'json') {
      $message->body = $this->prepareJsonBodyMessage($message);
    }

    $result = $this->client->publish(array(
      'TargetArn' => $endpointArn,
      // Message is required
      'Message' => $message->body,
      'Subject' => $message->subject,
      'MessageStructure' => $message->type,
    ));

    $messageID = $result->get('MessageId');
    if (!empty($messageID)) {
      return TRUE;
    }

    watchdog('Amazon SNS', 'Message @message was not delivered to personal target ARN @target_arn', array(
      '@message' => print_r($message, TRUE),
      '@target_arn' => $endpointArn
    ), WATCHDOG_WARNING);

    return FALSE;
  }

  /**
   * Get topic list from Amazon SNS
   *
   * @return Model
   */
  public function listTopics() {
    $topics = $this->client->listTopics();

    return $topics->get('Topics');
  }

  /**
   * Create new SNS Topic
   *
   * @param $name
   *
   * @return Model
   */
  public function createTopic($name) {
    return $this->client->createTopic(array('Name' => $name));
  }

  /**
   * Executes the subscribe operation
   *
   * @param string $TopicArn
   *  The ARN of the topic you want to subscribe to.
   * @param string $Endpoint
   *  The endpoint that you want to receive notifications.
   *
   * @return Model
   */
  public function subscribe($TopicArn, $Endpoint) {
    return $this->client->subscribe(array(
      'TopicArn' => $TopicArn,
      'Protocol' => 'Application',
      'Endpoint' => $Endpoint,
    ));
  }

  /**
   * Executes the Unsubscribe operation.
   *
   * @param string $subscriptionArn
   *  The ARN of the subscription to be deleted.
   *
   * @return Model
   */
  public function unsubscribe($subscriptionArn) {
    return $this->client->unsubscribe(array(
      'SubscriptionArn' => $subscriptionArn
    ));
  }
}
