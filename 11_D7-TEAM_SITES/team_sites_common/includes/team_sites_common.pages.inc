<?php
/**
 * @file
 * Page callback for team sites common module
 */

/**
 * Page callback for display LDAP profiles in global search.
 */
function team_sites_common_search_profile_page() {
  if (!empty($_GET['keyword'])) {
    $filters = array('keyword' => $_GET['keyword']);
  }
  else {
    return theme('team_sites_common_profiles_search', array('keyword' => ''));
  }

  $items = array();
  $profiles = team_sites_common_search_profiles($filters);

  if (empty($profiles)) {
    // If profiles was empty - render empty results content
    return theme('team_sites_common_profiles_search', array(
      'keyword' => $_GET['keyword'],
      'profiles' => array()
    ));
  }

  if (!empty($profiles)) {
    foreach ($profiles as $profile) {
      if (!empty($profile->id)) {
        $items[] = l($profile->name, 'profile/' . $profile->id);
      }
    }
  }

  return theme('team_sites_common_profiles_search', array(
    'keyword' => $_GET['keyword'],
    'profiles' => theme('item_list', array('items' => $items))
  ));
}

/**
 * Helper for get upcoming events rendered block content.
 *
 * @return string
 */
function _team_sites_common_get_upcoming_events_block_content() {
  $upcoming_block = block_load('views', 'calendar-upcoming_events');
  $render_array = _block_get_renderable_array(_block_render_blocks(array($upcoming_block)));

  return render($render_array);
}

/**
 * Return render array with contextual links for what's new blocks.
 *
 * @param $item
 *
 * @return array
 */
function _team_sites_common_get_whats_new_contextual_links($item) {
  // If node
  if (isset($item->nid)) {
    $item->id = $item->nid;
  }

  $links = array(
    'edit-node' => array(
      'title' => t('Edit content'),
      'href' => url('node/' . $item->id . '/edit', array(
        'absolute' => TRUE,
        'query' => array('destination' => current_path())
      )),
    ),
    'add-new-item' => array(
      'title' => t('Add new item with the same type'),
      'href' => url('node/add/' . $item->type, array(
        'absolute' => TRUE,
        'query' => array('destination' => current_path())
      )),
    ),
    'unpublish-node' => array(
      'title' => t('Unpublish'),
      'href' => url('node/' . $item->id . '/unpublish', array(
        'absolute' => TRUE,
        'query' => array('destination' => current_path())
      )),
    ),
  );

  if (isset($item->sticky) && $item->sticky) {
    $links['remove-from-carousel'] = array(
      'title' => t('Remove content from carousel'),
      'href' => url('node/' . $item->id . '/remove-from-sticky', array(
        'absolute' => TRUE,
        'query' => array('destination' => current_path())
      )),
    );
  }
  else {
    $links['add-to-carousel'] = array(
      'title' => t('Add content into carousel'),
      'href' => url('node/' . $item->id . '/add-to-sticky', array(
        'absolute' => TRUE,
        'query' => array('destination' => current_path())
      )),
    );
  }

  $contextual_links = array(
    '#prefix' => '<div class="contextual-links-wrapper">',
    '#suffix' => '</div>',
    '#theme' => 'links__contextual',
    '#links' => $links,
    '#attributes' => array('class' => array('contextual-links')),
    '#attached' => array(
      'library' => array(array('contextual', 'contextual-links')),
    ),
    '#access' => user_access('access contextual links'),
  );

  return $contextual_links;
}

/**
 * Page callback for AJAX user group creation.
 */
function team_sites_common_create_user_group_page($js = FALSE) {
  global $user;

  $node = (object) array(
    'uid' => $user->uid,
    'name' => (isset($user->name) ? $user->name : ''),
    'type' => TEAM_SITES_CT_USER_GROUP,
    'language' => LANGUAGE_NONE,
  );

  $form_state = array(
    'ajax' => TRUE,
  );
  $form_state['build_info']['args'] = array($node);

  $commands = ctools_modal_form_wrapper(TEAM_SITES_CT_USER_GROUP . '_node_form', $form_state);
  $commands[] = ajax_command_remove('.save-in-group-btn');
  $commands[] = array(
    'command' => 'set_users_to_group_form',
  );
  if (!empty($form_state['executed'])) {
    $commands = array();
    $commands[] = ajax_command_html('#messages-wrapper', theme('status_messages'));
    $commands[] = ctools_modal_command_dismiss();
  }

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Implements page callback for js/%ctools_js/notifications menu item
 * Render modal with user notifications
 */
function team_sites_common_notifications_page() {
  $nodes = team_sites_common_get_user_flash_update_nodes();
  if ($nodes) {
    $content = array();
    foreach ($nodes as $node) {
      $content[] = node_view($node, TEAM_SITES_COMMON_TEASER_VIEW_MODE);
    }
    $output = theme('team_sites_notifications', array('nodes_content' => $content));
  }
  else {
    $output = t('Not notifications available');
  }

  ctools_modal_render(t('Notifications'), $output);
  drupal_exit();
}

/**
 * Implements page callback for team-directory menu item.
 */
function team_sites_common_tsstaff_directory_page() {
  $form = drupal_get_form('team_sites_common_tsstaff_directory_form');

  return theme('tsstaff_directory_page', array('form' => $form));
}

/**
 * Implements page callback for LDAP profile detail page
 */
function team_sites_common_ldap_profile_page($profile) {
  if ($profile) {
    drupal_set_title($profile->name);
    $profile = theme('team_sites_common_team_sites_profile', array(
      'profile' => $profile
    ));
    $profile = theme('item_list', array(
      'items' => array($profile),
      'attributes' => array('class' => 'profiles-list')
    ));
    return theme('team_sites_common_ldap_profile_detail', array('profile' => $profile));
  }
}

/**
 * Page callback for profile photo manual crop page
 *
 * @param $samaccountname
 * @return string
 * @throws Exception
 */
function team_sites_common_ldap_profile_crop_photo_page($profile) {
  if ($profile) {
    // Load library for manual image crop
    libraries_load('jquery.jcrop');
    drupal_add_js(drupal_get_path('module', 'team_sites_common') . '/scripts/team_sites_common_jcrop.js');

    drupal_set_title(decode_entities(t('@name - Edit Photo', array('@name' => $profile->name))));

    // Check photo presence
    $uri = file_build_uri('staff-profiles' . '/' . $profile->id . '.png');
    // Build form for cropping
    $crop_form = drupal_get_form('team_sites_common_profile_jcrop_form');
    $crop_form['image_uri']['#value'] = $uri;
    $crop_form['profile_id']['#value'] = $profile->id;

    $remove_form = array();
    // If profile already have cropped image - Build form for remove cropped image
    if (!empty($profile->crop_photo)) {
      $remove_form = drupal_get_form('team_sites_common_delete_manual_crop_profile_image_form');
      $remove_form['profile_id']['#value'] = $profile->id;
    }

    if ($uri) {
      return theme('team_sites_common_ldap_profile_photo_crop', array(
        'photo_uri' => $uri,
        'profile' => $profile,
        'crop_form' => $crop_form,
        'remove_form' => $remove_form,
      ));
    }
  }
}
