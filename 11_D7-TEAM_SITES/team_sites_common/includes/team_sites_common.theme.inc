<?php

/**
 * @file
 *  Team Sites Common theme functions definition
 */

/**
 * Implements hook_theme().
 */
function team_sites_common_theme() {
  return array(
    'team_sites_common_profiles_search' => array(
      'variables' => array(
        'profiles' => NULL,
        'personality_types_link' => NULL,
        'strengths_link' => NULL,
        'spiritual_gifts_link' => NULL,
        'keyword' => NULL,
      ),
    ),
    'team_sites_common_whats_new_day_events' => array(
      'variables' => array(
        'events' => NULL,
        'day' => NULL,
        'month' => NULL,
      ),
    ),
    'team_sites_common_calendar_day' => array(
      'variables' => array(
        'items' => NULL,
        'date' => NULL,
      ),
    ),
    'team_sites_common_empty_calendar_day' => array(
      'variables' => array(
        'date' => NULL,
        'active' => NULL,
      ),
    ),
    'team_sites_notifications' => array(
      'variables' => array(
        'nodes_content' => NULL,
      ),
    ),
    'team_sites_tutorials_categories_list' => array(
      'variables' => array(
        'categories' => NULL,
      ),
    ),
    'team_sites_tutorials_tags_list' => array(
      'variables' => array(
        'terms' => NULL,
      ),
    ),
    'tsstaff_directory_page' => array(
      'variables' => array(
        'form' => NULL,
      ),
    ),
    'team_sites_common_note_message' => array(
      'variables' => array(
        'note_title' => NULL,
        'note_text' => NULL,
      ),
    ),
    'team_sites_common_team_sites_profile' => array(
      'variables' => array(
        'profile' => NULL,
        'links' => array(),
      )
    ),
    'team_sites_common_tsstaff_directory_result' => array(
      'variables' => array(
        'results' => NULL,
      )
    ),
    'team_sites_common_ldap_profile_detail' => array(
      'variables' => array(
        'profile' => NULL,
      )
    ),
    'team_sites_common_ldap_profile_photo_crop' => array(
      'variables' => array(
        'profile' => NULL,
        'photo_uri' => NULL,
        'crop_form' => NULL,
        'remove_form' => NULL,
      ),
    ),
  );
}

/**
 * Dummy theme function for profiles page on global search.
 */
function theme_team_sites_common_profiles_search() {
  return 'Dummy';
}

/**
 * Dummy theme function for what's new day events block
 */
function theme_team_sites_common_whats_new_day_events() {
  return 'Dummy';
}

/**
 * Dummy function for render what's new photo item
 */
function theme_team_sites_common_whats_new_photo() {
  return 'Dummy';
}

/**
 * Dummy function for what's video block
 */
function theme_team_sites_common_whats_new_video() {
  return 'Dummy';
}

/**
 * Dummy theme function for what's new quote block.
 */
function theme_team_sites_common_whats_new_quote() {
  return 'Dummy';
}

/**
 * Dummy theme function for what's new post block.
 */
function theme_team_sites_common_whats_new_post() {
  return 'Dummy';
}

/**
 * Dummy theme function for what's new blog post block.
 */
function theme_team_sites_common_whats_new_blog_post() {
  return 'Dummy';
}

/**
 * Dummy theme function for what's new page.
 */
function theme_team_sites_common_whats_new_page() {
  return 'Dummy';
}

/**
 * Dummy theme function for render Global Calendar Day with events.
 */
function theme_team_sites_common_calendar_day() {
  return 'Dummy';
}

/**
 * Dummy theme function for render empty Global Calendar Day.
 */
function theme_team_sites_common_empty_calendar_day() {
  return 'Dummy';
}

/**
 * Dummy theme function for render user notifications
 */
function theme_team_sites_notifications() {
  return 'Dummy';
}

/**
 * Dummy theme function for render tutorials category list
 */
function theme_team_sites_tutorials_categories_list() {
  return 'Dummy';
}

/**
 * Dummy theme function for render tutorials tags list
 */
function theme_team_sites_tutorials_tags_list() {
  return 'Dummy';
}

/**
 * Dummy theme function for staff directory page
 */
function theme_tsstaff_directory_page() {
  return 'Dummy';
}

/**
 * Dummy theme function for note block display
 */
function theme_team_sites_note_message() {
  return 'Dummy';
}

/**
 * Dummy theme function for render staff directory profile
 */
function theme_team_sites_common_team_sites_profile() {
  return 'Dummy';
}

/**
 * Dummy theme function for staff directory results
 */
function theme_team_sites_common_tsstaff_directory_result() {
  return 'Dummy';
}

/**
 * Dummy theme function ldap profile detail page
 */
function theme_team_sites_common_ldap_profile_detail() {
  return 'Dummy';
}

/**
 * Dummy theme function for profile photo crop page
 */
function theme_team_sites_common_ldap_profile_photo_crop() {
  return 'Dummy';
}
