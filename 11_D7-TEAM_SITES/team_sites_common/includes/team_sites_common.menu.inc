<?php
/**
 * @file hook_menu() for team_sites_common module
 */

/**
 * Implements hook_menu().
 */
function team_sites_common_menu() {
  $items = array(
    'admin/team-sites' => array(
      'title' => 'Team Sites CMS',
      'description' => 'Administer settings.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('administer team sites configuration'),
      'file' => 'system.admin.inc',
      'file path' => drupal_get_path('module', 'system'),
    ),
    'admin/team-sites/settings' => array(
      'title' => 'General Parameters',
      'description' => 'General Parameters',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('team_sites_common_admin_settings_page_form'),
      'access arguments' => array('administer team sites configuration'),
      'file' => 'includes/team_sites_common.forms.inc',
      'file path' => drupal_get_path('module', 'team_sites_common'),
    ),
    'admin/team-sites/sns-settings' => array(
      'title' => 'Amazon Sns Parameters',
      'description' => 'Amazon Sns Parameters',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('team_sites_common_amazon_sns_page_form'),
      'access arguments' => array('administer team sites configuration'),
      'file' => 'includes/team_sites_common.forms.inc',
      'file path' => drupal_get_path('module', 'team_sites_common'),
    ),
    'user/spouses' => array(
      'title' => 'Add spouse',
      'description' => 'Register new spouse for your account',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('team_sites_common_spouse_profile_form'),
      'access callback' => 'team_sites_common_is_user_auth',
    ),
    'search/profiles' => array(
      'title' => 'Found Profiles',
      'description' => 'Profiles search result',
      'page callback' => 'team_sites_common_search_profile_page',
      'access arguments' => array('access content'),
      'file' => 'includes/team_sites_common.pages.inc',
      'file path' => drupal_get_path('module', 'team_sites_common'),
    ),
    'js/whats-new/load' => array(
      'title' => 'Whats New',
      'description' => 'Whats New Page',
      'page callback' => 'team_sites_common_whats_new_page_ajax_load',
      'access arguments' => array('access content'),
      'delivery callback' => 'ajax_deliver',
      'theme callback' => 'ajax_base_page_theme',
      'file' => 'includes/team_sites_common.pages.inc',
      'file path' => drupal_get_path('module', 'team_sites_common'),
      'type' => MENU_CALLBACK,
    ),
    'team-directory' => array(
      'title' => 'Team Directory',
      'description' => 'Search profiles using LDAP',
      'page callback' => 'team_sites_common_tsstaff_directory_page',
      'access arguments' => array('access staff directory page'),
      'file' => 'includes/team_sites_common.pages.inc',
      'file path' => drupal_get_path('module', 'team_sites_common'),
    ),
    'node/%node/unpublish' => array(
      'title' => 'Unpublish',
      'access arguments' => array('administer nodes'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('team_sites_common_contextual_node_unpublish', 1),
      'file' => 'includes/team_sites_common.pages.inc',
      'file path' => drupal_get_path('module', 'team_sites_common'),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
    ),
    'node/%node/add-to-sticky' => array(
      'title' => 'Sticky at top of list',
      'access arguments' => array('administer nodes'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('team_sites_common_contextual_node_sticky_add', 1),
      'file' => 'includes/team_sites_common.pages.inc',
      'file path' => drupal_get_path('module', 'team_sites_common'),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
    ),
    'node/%node/remove-from-sticky' => array(
      'title' => 'Sticky at top of list',
      'access arguments' => array('administer nodes'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('team_sites_common_contextual_node_sticky_remove', 1),
      'file' => 'includes/team_sites_common.pages.inc',
      'file path' => drupal_get_path('module', 'team_sites_common'),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
    ),
    'js/%ctools_js/add-user-group' => array(
      'title' => 'Create a New User Group',
      'page callback' => 'team_sites_common_create_user_group_page',
      'file' => 'includes/team_sites_common.pages.inc',
      'file path' => drupal_get_path('module', 'team_sites_common'),
      'type' => MENU_CALLBACK,
      'delivery callback' => 'ajax_deliver',
      'theme callback' => 'ajax_base_page_theme',
      'access arguments' => array('administer staff user groups'),
    ),
    'js/%ctools_js/notifications' => array(
      'title' => 'Notifications',
      'page callback' => 'team_sites_common_notifications_page',
      'file' => 'includes/team_sites_common.pages.inc',
      'file path' => drupal_get_path('module', 'team_sites_common'),
      'type' => MENU_CALLBACK,
      'delivery callback' => 'ajax_deliver',
      'theme callback' => 'ajax_base_page_theme',
      'access arguments' => array('access content'),
    ),
    'profile/%team_sites_common_ldap_profile' => array(
      'title' => 'Profile',
      'page callback' => 'team_sites_common_ldap_profile_page',
      'page arguments' => array(1),
      'file' => 'includes/team_sites_common.pages.inc',
      'file path' => drupal_get_path('module', 'team_sites_common'),
      'access arguments' => array('access content'),
    ),
    'profile/%team_sites_common_ldap_profile/photo_crop' => array(
      'title' => 'Crop Profile Photo',
      'page callback' => 'team_sites_common_ldap_profile_crop_photo_page',
      'page arguments' => array(1),
      'file' => 'includes/team_sites_common.pages.inc',
      'file path' => drupal_get_path('module', 'team_sites_common'),
      'access arguments' => array('administer team sites configuration'),
    ),
  );

  return $items;
}

/**
 * Implements hook_menu_alter().
 */
function team_sites_common_menu_alter(&$items) {
  $items['node']['access callback'] = FALSE;
  $items['rss.xml']['access callback'] = FALSE;
  $items['user/%user/edit']['access callback'] = 'user_access';
  $items['user/%user/edit']['access arguments'] = array('administer users');
  // Re-point the /admin/user/user/create path back to the User module's
  // callback, since Views is taking over /admin/user/user,
  //  because we use custom view for People page.
  $items['admin/people/create']['page callback'] = 'user_admin';
  $items['admin/people/create']['file'] = 'user.admin.inc';
}
