<?php

/**
 * @file
 * Custom views field handler for users_events table.
 */
class views_handler_events_uid_field extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['display_type'] = array('default' => 'default');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['display_type'] = array(
      '#type' => 'select',
      '#title' => t('Field display type.'),
      '#description' => t('Type of field display. By default display user uid.'),
      '#default_value' => isset($this->options['display_type']) ? $this->options['display_type'] : 'default',
      '#options' => array(
        'subscribe' => t('Event subscribe'),
        'default' => t('Default'),
      ),
    );
    parent::options_form($form, $form_state);
  }

  function query() {
    parent::query();

    // If option 'subscribe' return subscribed or not subscribed current user to event.
    if ($this->options['display_type'] == 'subscribe') {
      global $user;
      unset($this->query->fields['users_events_uid']);
      $this->query->add_field('', 'users_events.uid = :uid', 'users_events_uid', array('placeholders' => array(':uid' => $user->uid)));
      $this->query->table_queue['users_events']['join']->extra = array(
        array(
          'table' => 'users_events',
          'field' => 'uid',
          'operator' => '=',
          'value' => $user->uid
        )
      );
    }
  }
}
