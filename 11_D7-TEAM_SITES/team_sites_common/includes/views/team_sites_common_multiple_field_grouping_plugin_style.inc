<?php

/**
 * @file
 */

class team_sites_common_multiple_field_grouping_plugin_style extends views_plugin_style {
	/**
	 * Group records as needed for rendering.
	 *
	 * @param $records
	 *   An array of records from the view to group.
	 * @param $groupings
	 *   An array of grouping instructions on which fields to group. If empty, the
	 *   result set will be given a single group with an empty string as a label.
	 * @param $group_rendered
	 *   Boolean value whether to use the rendered or the raw field value for
	 *   grouping. If set to NULL the return is structured as before
	 *   Views 7.x-3.0-rc2. After Views 7.x-3.0 this boolean is only used if
	 *   $groupings is an old-style string or if the rendered option is missing
	 *   for a grouping instruction.
	 * @return
	 *   The grouped record set.
	 *   A nested set structure is generated if multiple grouping fields are used.
	 *
	 *   @code
	 *   array(
	 *     'grouping_field_1:grouping_1' => array(
	 *       'group' => 'grouping_field_1:content_1',
	 *       'rows' => array(
	 *         'grouping_field_2:grouping_a' => array(
	 *           'group' => 'grouping_field_2:content_a',
	 *           'rows' => array(
	 *             $row_index_1 => $row_1,
	 *             $row_index_2 => $row_2,
	 *             // ...
	 *           )
	 *         ),
	 *       ),
	 *     ),
	 *     'grouping_field_1:grouping_2' => array(
	 *       // ...
	 *     ),
	 *   )
	 *   @endcode
	 */
	function render_grouping($records, $groupings = array(), $group_rendered = NULL) {
		// This is for backward compability, when $groupings was a string containing
		// the ID of a single field.
		if (is_string($groupings)) {
			$rendered = $group_rendered === NULL ? TRUE : $group_rendered;
			$groupings = array(array('field' => $groupings, 'rendered' => $rendered));
		}

		// Make sure fields are rendered
		$this->render_fields($this->view->result);
		$sets = array();
		if ($groupings) {
			foreach ($records as $index => $row) {
				// Iterate through configured grouping fields to determine the
				// hierarchically positioned set where the current row belongs to.
				// While iterating, parent groups, that do not exist yet, are added.
				$set = &$sets;
				foreach ($groupings as $info) {
					$multiple = FALSE;
					$field = $info['field'];
					$rendered = isset($info['rendered']) ? $info['rendered'] : $group_rendered;
					$rendered_strip = isset($info['rendered_strip']) ? $info['rendered_strip'] : FALSE;
					$grouping = '';
					$group_content = '';
					// Group on the rendered version of the field, not the raw.  That way,
					// we can control any special formatting of the grouping field through
					// the admin or theme layer or anywhere else we'd like.
					if (isset($this->view->field[$field])) {
						$group_content = $this->get_field($index, $field);
						if ($this->view->field[$field]->options['label']) {
							$group_content = $this->view->field[$field]->options['label'] . ': ' . $group_content;
						}
						$values = $this->get_field_value($index, $field);
						if ($rendered) {
							$grouping = $group_content;
							if ($rendered_strip) {
								$group_content = $grouping = strip_tags(htmlspecialchars_decode($group_content));
							}
						}
						elseif (count($values) > 1) {
							foreach ($row->{'field_' . $field} as $category) {
								$grouping = array(array('tid' => $category['raw']['tid']));
								if (!is_scalar($grouping)) {
									$grouping = md5(serialize($grouping));
								}
								$group_content = drupal_render($category['rendered']);
								if (empty($sets[$grouping])) {
									$sets[$grouping]['group'] = $group_content;
									$sets[$grouping]['rows'][$index] = $row;
								}
								else {
									$sets[$grouping]['rows'][$index] = $row;
								}
								$sets[$grouping]['tid'] = $category['raw']['tid'];
							}
							$multiple = TRUE;
							continue;
						}
						else {
							$grouping = $this->get_field_value($index, $field);
							// Not all field handlers return a scalar value,
							// e.g. views_handler_field_field.
							if (!is_scalar($grouping)) {
								$grouping = md5(serialize($grouping));
							}
						}
					}

					// Create the group if it does not exist yet.
					if (empty($set[$grouping])) {
						$set[$grouping]['group'] = $group_content;
						$set[$grouping]['rows'] = array();
						if (!empty($row->{'field_' . $field}[0]['raw']['tid'])) {
							$set[$grouping]['tid'] = $row->{'field_' . $field}[0]['raw']['tid'];
						};
					}

					// Move the set reference into the row set of the group we just determined.
					$set = &$set[$grouping]['rows'];
				}
				// Add the row to the hierarchically positioned row set we just determined.
				if (!$multiple) {
					$set[$index] = $row;
				}
			}
		}
		else {
			// Create a single group with an empty grouping field.
			$sets[''] = array(
				'group' => '',
				'rows' => $records,
			);
		}

		if ($group_rendered === NULL) {
			$old_style_sets = array();
			foreach ($sets as $group) {
				$old_style_sets[$group['group']] = $group['rows'];
			}
			$sets = $old_style_sets;
		}

		return $sets;
	}

	/**
	 * Render the grouping sets.
	 *
	 * Plugins may override this method if they wish some other way of handling
	 * grouping.
	 *
	 * @param $sets
	 *   Array containing the grouping sets to render.
	 * @param $level
	 *   Integer indicating the hierarchical level of the grouping.
	 *
	 * @return string
	 *   Rendered output of given grouping sets.
	 */
	function render_grouping_sets($sets, $level = 0) {
		$output = '';
		foreach ($sets as $set) {
			$row = reset($set['rows']);
			// Render as a grouping set.
			if (is_array($row) && isset($row['group'])) {
				$output .= theme(views_theme_functions('views_view_grouping', $this->view, $this->display),
					array(
						'view' => $this->view,
						'grouping' => $this->options['grouping'][$level],
						'grouping_level' => $level,
						'rows' => $set['rows'],
						'title' => $set['group'])
				);
			}
			// Render as a record set.
			else {
				if ($this->uses_row_plugin()) {
					foreach ($set['rows'] as $index => $row) {
						$this->view->row_index = $index;
						$set['rows'][$index] = $this->row_plugin->render($row);
					}
				}

				$tid = !empty($set['tid']) ? $set['tid']: NULL;
				$output .= theme($this->theme_functions(),
					array(
						'view' => $this->view,
						'options' => $this->options,
						'grouping_level' => $level,
						'rows' => $set['rows'],
						'title' => $set['group'],
						'tid' => $tid)
				);
			}
		}
		unset($this->view->row_index);
		return $output;
	}

}
