<?php
/**
 * @file
 *  Provide views data and handlers.
 *
 * @ingroup views_module_handlers
 */

/**
 * Implements hook_views_data().
 */
function team_sites_common_views_data() {
  $data = array();
  // users_events table
  $data['users_events']['table']['group'] = t('Team Sites User Events');
  $data['users_events']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Users events table'),
    'help' => t('Store events subscribers'),
    'weight' => -10,
  );
  $data['users_events']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );
  $data['users_events']['nid'] = array(
    'title' => t('Node ID'),
    'help' => t('The node ID of the event node'),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
    ),
  );
  $data['users_events']['uid'] = array(
    'field' => array(
      'handler' => 'views_handler_events_uid_field',
      'click sortable' => TRUE,
    ),
    'title' => t('UID of user that subscribe to event'),
    'help' => t('UID of user that subscribe to some event'),
    'relationship' => array(
      'base' => 'users',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('Users Table for Events'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['users_events']['events_count'] = array(
    'title' => t('Event Repeat count'),
    'help' => t('Event Repeat count'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function team_sites_common_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'team_sites_common') . '/includes/views/handlers',
    ),
    'handlers' => array(
      'views_handler_events_uid_field' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_plugins
 */
function team_sites_common_views_plugins() {
  $module_path = drupal_get_path('module', 'calendar');
  module_load_include('inc', 'calendar', 'theme/theme');

  // Limit these plugins to base tables that represent entities.
  $base = array_keys(date_views_base_tables());
  // Custom calendar views style plugin
  $data = array(
    'module' => 'team_sites_common',

    'style' => array(
      'team_sites_common_calendar_style' => array(
        'title' => t('Team Sites Calendar'),
        'help' => t('Present view results as a Calendar.'),
        'handler' => 'team_sites_common_calendar_plugin_style',
        'path' => drupal_get_path('module', 'team_sites_common') . "/includes/views",
        'theme' => 'calendar_style',
        'theme file' => 'theme.inc',
        'theme path' => "$module_path/theme",
        'additional themes' => array(
          'calendar_mini' => 'style',
          'calendar_day' => 'style',
          'calendar_week' => 'style',
          'calendar_month' => 'style',
          'calendar_year' => 'style',
          'calendar_day_overlap' => 'style',
          'calendar_week_overlap' => 'style',
        ),
        'uses fields' => TRUE,
        'uses grouping' => FALSE,
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'even empty' => TRUE,
        'base' => $base,
      ),
        'team_sites_common_multiple_field_grouping' => array(
            'title' => t('Team Sites Tutorials'),
            'help' => t('This style allows to display a one tutorial in several categories at the same time with grouping results by category tid'),
            'handler' => 'team_sites_common_multiple_field_grouping_plugin_style',
            'path' => drupal_get_path('module', 'team_sites_common') . "/includes/views",
            'theme' => 'views_view_unformatted',
            'uses row plugin' => TRUE,
            'uses row class' => TRUE,
            'uses grouping' => TRUE,
            'uses options' => TRUE,
            'type' => 'normal',
            'help topic' => 'style-unformatted',
        ),
    ),
    'row' => array(
      'calendar_entity' => array(
        'title' => t('Calendar Entities'),
        'help' => t('Displays each selected entity as a Calendar item.'),
        'handler' => 'calendar_plugin_row',
        'theme' => 'views_view_fields',
        'path' => "$module_path/includes",
        'base' => $base,
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
  return $data;
}
