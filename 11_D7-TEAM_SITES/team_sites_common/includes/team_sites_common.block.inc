<?php

/**
 * @file
 * Custom Team Sites blocks
 */

/**
 * Implements hook_block_info().
 */
function team_sites_common_block_info() {
  return array(
    'team_sites_tutorials_category' => array(
      'info' => t('Display tutorials categories list.'),
      'cache' => DRUPAL_NO_CACHE
    ),
    'team_sites_tutorials_tags' => array(
      'info' => t('Display tutorials tags list.'),
      'cache' => DRUPAL_NO_CACHE
    ),
  );
}

/**
 * Implements hook_block_view().
 */
function team_sites_common_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'team_sites_tutorials_category':
      $block['subject'] = t('Categories');
      $block['content'] = team_sites_common_tutorials_categories_block_content();
      break;

    case 'team_sites_tutorials_tags':
      $block['subject'] = t('Tags');
      $block['content'] = team_sites_common_tutorials_tags_block_content();
      break;
  }

  return $block;
}
