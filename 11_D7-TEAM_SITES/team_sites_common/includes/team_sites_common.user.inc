<?php
/**
 * @file
 * File contain functions for work with users.
 */

/**
 * Check is user has manager role
 *
 * @param object $user or NULL
 *
 * @return bool
 *   TRUE if manager or FALSE
 */
function team_sites_common_is_user_manager($user = NULL) {
  if (is_null($user)) {
    global $user;
  }

  return array_key_exists(TEAM_SITES_MANAGER_RID, $user->roles);
}

/**
 * Check that user have campus manager role
 *
 * @param null $user
 *  Drupal user object
 * @return bool
 */
function team_sites_common_is_user_campus_manager($user = NULL) {
  if (is_null($user)) {
    global $user;
  }

  return array_key_exists(TEAM_SITES_CAMPUS_MANAGER_RID, $user->roles);
}

/**
 * Check that user have only auth role
 *
 * @param null $user
 *
 * @return bool
 */
function team_sites_common_is_user_auth($user = NULL) {
  if (is_null($user)) {
    global $user;
  }

  // Allow access for root.
  if (team_sites_common_is_user_root($user)) {
    return TRUE;
  }

  if (count($user->roles) == 1 && array_key_exists(TEAM_SITES_AUTHENTICATED_USER_RID, $user->roles)) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Check is user root.
 *
 * @param null $user
 *
 * @return bool
 *   TRUE if root
 */
function team_sites_common_is_user_root($user = NULL) {
  if (is_null($user)) {
    global $user;
  }

  return array_key_exists(TEAM_SITES_ROOT_RID, $user->roles);
}

/**
 * Check that user have spouse role.
 *
 * @param null $user
 *
 * @return bool
 *   TRUE if user have spouse role or FALSE
 */
function team_sites_common_is_user_spouse($user = NULL) {
  if (is_null($user)) {
    global $user;
  }

  return array_key_exists(TEAM_SITES_SPOUSE_RID, $user->roles);
}

/**
 * Get user name.
 *
 * @param null|object $user
 *   User object
 *
 * @return string
 *   User name
 */
function team_sites_common_get_user_name($user = NULL) {
  if (!$user) {
    global $user;
  }

  if (is_numeric($user)) {
    $user = user_load($user);
  }

  $name = NULL;
  if (!empty($user->field_display_name[LANGUAGE_NONE][0]['value'])) {
    $name = $user->field_display_name[LANGUAGE_NONE][0]['value'];
  }

  if (empty($name)) {
    $name = format_username($user);
  }

  return $name;
}

/**
 * Get user position
 *
 * @param null $user
 *   Drupal user
 *
 * @return string
 *   User position
 */
function team_sites_common_get_user_position($user = NULL) {

  if (is_numeric($user)) {
    $user = user_load($user);
  }
  if (!$user) {
    global $user;
  }

  $position = '';
  if (!empty($user->field_position[LANGUAGE_NONE][0]['value'])) {
    $position = $user->field_position[LANGUAGE_NONE][0]['value'];
  }

  return $position;
}

/**
 * Check exist user with email or not
 *
 * @param string $email
 *   user email
 *
 * @return mixed
 *   User uid if user exist or FALSE
 */
function team_sites_common_check_user_email($email) {
  $query = db_select('users', 'u');
  $query->addField('u', 'uid');
  $query->where("LOWER(u.mail) = LOWER(:mail)", array(':mail' => $email));

  return $query->execute()->fetchCol();
}

/**
 * Save event to users_events table.
 *
 * @param $node
 * @param $notification_interval
 * @param null $user
 *
 * @return bool
 *   Return TRUE if user successfully subscribed and FALSE if not.
 */
function team_sites_common_subscribe_user_to_event($node, $notification_interval, $user = NULL) {
  if (!$user) {
    global $user;
  }

  $query = db_select('users_events', 'ue');
  $query->addField('ue', 'id');
  $query->condition('ue.nid', $node->nid);
  $query->condition('ue.uid', $user->uid);
  $subscribed = $query->execute()->fetchField();

  if ($subscribed) {
    return FALSE;
  }

  $record = new stdClass();
  $record->nid = $node->nid;
  $record->uid = $user->uid;
  $record->ts = REQUEST_TIME;
  drupal_write_record('users_events', $record);

  if (!empty($node->field_event_date)) {
    foreach ($node->field_event_date[LANGUAGE_NONE] as $event_date) {
      if (!empty($event_date['value'])) {
        // If event without time - send push 12 hours before the event
        $push_date = new DateTime($event_date['value']);
        if ($push_date->format('H:i:s') != '00:00:00') {
          $i = new DateInterval('PT' . $notification_interval . 'S');
        }
        else {
          $i = new DateInterval('PT' . TEAM_SITES_EVENT_WITHOUT_TIME_PUSH_INTERVAL . 'H');
        }

        $push_date = $push_date->sub($i);
        $date = $push_date->format(TEAM_SITES_COMMON_MYSQL_DATE_FORMAT);

        $record = new stdClass();
        $record->nid = $node->nid;
        $record->uid = $user->uid;
        $record->status = FALSE;
        $record->type = TEAM_SITES_CT_EVENTS;
        $record->params = array('event_date' => $event_date['value']);
        $record->send_time = $date;
        $record->timestamp = REQUEST_TIME;
        drupal_write_record('staff_push_notifications_queue', $record);
      }
    }
  }

  return TRUE;
}

/**
 * Unsubscribe user from event.
 *
 * @param $nid
 * @param null $user
 *
 * @return DatabaseStatementInterface
 */
function team_sites_common_unsubscribe_user_from_event($nid, $user = NULL) {
  if (!$user) {
    global $user;
  }

  // Remove from records
  db_delete('users_events')->condition('uid', $user->uid)->condition('nid', $nid)->execute();

  // Remove from push queue
  db_delete('staff_push_notifications_queue')->condition('uid', $user->uid)->condition('nid', $nid)->execute();

  return TRUE;
}

/**
 * Login user by token in url.
 *
 * @param $token
 *
 * @return bool
 */
function _team_sites_common_authenticate_user_by_url_token($token) {
  global $user;

  $token_parts = explode('/', $token);
  if (!empty($token_parts[1]) && !empty($token_parts[2])) {
    $uid = base64_decode($token_parts[1]);
    $token_created_time = base64_decode($token_parts[2]);
    $current_time = time();

    $passed_time = $current_time - $token_created_time;
    $ttl = variable_get('team_sites_single_sign_in_link_ttl', TEAM_SITES_SINGLE_SIGN_IN_TTL);
    if ($passed_time < $ttl) {
      $account = user_load($uid);
      if ($account && $account->uid != 0) {
        $path = isset($_GET['q']) ? $_GET['q'] : '<front>';
        $link = url($path, array('absolute' => TRUE));

        $hash = hash('sha1', $account->uid . 'link' . $link);
        if ($hash != $token_parts[0]) {
          return FALSE;
        }

        // Update the user table timestamp noting user has logged in.
        $account->login = REQUEST_TIME;
        db_update('users')->fields(array('login' => $account->login))->condition('uid', $account->uid)->execute();

        $user = $account;
        drupal_session_regenerate();

        $edit = array();
        user_module_invoke('login', $edit, $user);
        drupal_goto($path);
      }
    }
  }

  return FALSE;
}

/**
 * Check that user has unread flash update or not
 *
 * @param null $user
 *   Drupal user object
 *
 * @return bool
 *   TRUE if has or FALSE.
 */
function team_sites_common_is_user_has_unread_flash_update($user = NULL) {
  if (!$user) {
    global $user;
  }
  if (empty($user->field_user_campus[LANGUAGE_NONE][0]['tid'])) {
    return FALSE;
  }

  $campus_tid = $user->field_user_campus[LANGUAGE_NONE][0]['tid'];
  $nids = _team_sites_common_get_user_flash_update_nids($campus_tid, $user->uid);

  if ($nids) {
    $q = db_select('users_flash_updates', 'ufu');
    $q->addExpression("COUNT(ufu.id)");
    $q->condition('ufu.uid', $user->uid);
    $q->condition('ufu.nid', $nids, 'in');
    $result = $q->execute()->fetchField();
    if (!$result || $result < TEAM_SITES_API_FLASH_UPDATE_LIMIT) {
      return TRUE;
    }
  }

  return FALSE;
}
