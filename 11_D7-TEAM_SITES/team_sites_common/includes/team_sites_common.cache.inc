<?php

/**
 * @file
 *  Provide cache functions.
 */

define('TEAM_SITES_CACHE_TABLE', 'cache');

/**
 * Generate cache id for specified type
 *
 * @param string $type
 *  Unique type of cache.
 *
 * @return string
 */
function team_sites_common_generate_cid($type) {
  return 'team_sites_' . md5($type);
}

/**
 * Clear cache for specified type
 *
 * @param string $type
 *  Unique type of cache.
 */
function team_sites_common_clear_cache($type) {
  $cid = team_sites_common_generate_cid($type);
  cache_clear_all($cid, TEAM_SITES_CACHE_TABLE);
}

/**
 * Get cache data for specified type
 *
 * @param string $type
 *  Unique type of cache.
 *
 * @return mixed/bool
 *  Data or False
 */
function team_sites_common_get_cache($type) {
  $cid = team_sites_common_generate_cid($type);
  $cache_data = cache_get($cid, TEAM_SITES_CACHE_TABLE);
  if (!empty($cache_data) && !empty($cache_data->data)) {
    return $cache_data->data;
  }

  return FALSE;
}

/**
 * Set cache
 *
 * @param string $type
 *  Unique type of cache.
 * @param mixed $data
 *  Data, which should be cached
 */
function team_sites_common_set_cache($type, $data) {
  $cid = team_sites_common_generate_cid($type);
  cache_set($cid, $data, TEAM_SITES_CACHE_TABLE);
}
