/**
 * @file
 *
 * Main js file for Team Sites custom modules.
 */

/**
 * Declare some containers
 */
Drupal.teamSitesCommon = Drupal.teamSitesCommon || {};
Drupal.teamSitesCommon.Ajax = Drupal.teamSitesCommon.Ajax || {}; // for ajax callbacks
Drupal.teamSitesCommon.Constants = Drupal.teamSitesCommon.Constants || {}; // for saving global constants
Drupal.teamSitesCommon.Theme = Drupal.teamSitesCommon.Theme || {}; // for theming or changing view of elements
Drupal.teamSitesCommon.Behaviors = Drupal.teamSitesCommon.Behaviors || {}; // for creating dynamic things on the page(animations, etc)

(function ($) {
    /**
     * Check flash update scheduled checkbox
     * @type {{attach: Function}}
     */
    Drupal.behaviors.flashUpdateScheduleTrigger = {
        attach: function (context, settings) {
            $('#edit-field-flash-update-scheduled-und').click(function () {
                if (!$(this).is(':checked')) {
                    $('.date-clear input:checkbox').prop('checked', false);
                }
            });
        }
    };

    Drupal.behaviors.flashUpdateUserGroups = {
        attach: function (context, settings) {
            var $users_select = $('#edit-field-flash-update-users-und');
            var $user_groups_select = $('#edit-field-user-group-und');
            var addExtraUserBtn = $('#add-extra-users-btn');

            // Add button for save users to custom user group
            $users_select.change(function (e) {
                e.preventDefault();
                var $save_group_btn = $('.save-in-group-btn');
                if ($users_select.chosen().val() && $save_group_btn.length == 0) {
                    var url = Drupal.settings.basePath + 'js/nojs/add-user-group';
                    var saveGroupBtn = $('<a/>', {
                        href: url,
                        text: Drupal.t('Save to Group'),
                        class: 'btn-primary btn button use-ajax save-in-group-btn'
                    });
                    $('#edit_field_flash_update_users_und_chosen').after(saveGroupBtn);
                    Drupal.attachBehaviors(context, settings);
                }
                else if (!$users_select.chosen().val()) {
                    $save_group_btn.remove();
                }
            });

            if ($('#edit-field-recipients-und-users-group:checked').val() && (addExtraUserBtn.length == 0)) {
                $users_select.val('').trigger('chosen:updated');
            }

            $user_groups_select.change(function () {
                if ($user_groups_select.chosen().val() && ($('#add-extra-users-btn').length == 0)) {
                    createAddNotificationExtraUserBtn();

                    Drupal.attachBehaviors(context, settings);
                    $('#edit_field_user_group_und_chosen').css('width', '94%');
                }
            });

            $('#add-extra-users-btn').click(function (e) {
                $('#edit-field-flash-update-users').fadeIn(300);
                $('#add-extra-users-btn').hide();
                return false;
            });

        }
    };

    /**
     * Custom ajax command for set users to modal form
     */
    Drupal.ajax.prototype.commands.set_users_to_group_form = function (ajax, response, status) {
        var users = $("#edit-field-flash-update-users-und").chosen().val();
        var $groupUserSelector = $('#edit-field-users-und');
        $groupUserSelector.val(users);
        $groupUserSelector.trigger("chosen:updated");
    };

    /**
     * Scroll to specific element on the page;
     * @param elt
     * @param bottom
     * @param speed
     */
    Drupal.teamSitesCommon.scrollTo = function (elt, bottom, speed) {
        if (!bottom) {
            bottom = false;
        }

        if (!speed) {
            speed = 500;
        }

        var offset = elt.offset();
        if (offset.top - 10 < $(window).scrollTop()) {
            $('html,body').animate({
                scrollTop: offset.top - 40
            }, speed);
        }
        else if (bottom) {
            if (offset.top - 10 > $(window).scrollTop()) {
                $('html,body').animate({
                    scrollTop: offset.top - 40
                }, speed);
            }
        }
    };

    function createAddNotificationExtraUserBtn() {
        var title = Drupal.t('Add extra users', [], []);
        var addExtraUsersBtn = $('<a/>', {
            id: 'add-extra-users-btn',
            href: 'javascript:void(0);',
            title: title,
            text: '+',
            class: 'btn-primary btn button show-users-select'
        });
        $('#edit_field_user_group_und_chosen').after(addExtraUsersBtn);
    }

})(jQuery);
