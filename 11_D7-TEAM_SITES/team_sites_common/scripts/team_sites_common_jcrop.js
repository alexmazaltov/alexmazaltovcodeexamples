/**
 * @file
 *
 * Js file for javascript crop images functionality.
 */

(function ($) {
    Drupal.behaviors.team_sites_common_jcrop = {
        attach: function (context, settings) {
            $('#crop_target').Jcrop({
                aspectRatio: 1,
                onSelect: updateCoords,
                minSize: 200,
                setSelect: [ 50, 50, 200, 200 ]
            });
        }
    };

    function updateCoords(c) {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    }

})(jQuery);
