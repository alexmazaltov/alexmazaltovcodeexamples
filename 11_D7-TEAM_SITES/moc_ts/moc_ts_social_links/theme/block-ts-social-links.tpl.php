<?php
/**
 * @file Template for render block social link in footer
 * Available variables:
 * - $custom_var: Not needed for now.
 */
?>

<ul class="ts-social-links">
  <li class="fb">
    <a href="http://facebook.com">facebook</a>
  </li>
  <li class="tw">
    <a href="http://twitter.com">twitter</a>
  </li>
  <li class="lin">
    <a href="http://linkedin.com">linkedin</a>
  </li>
  <li class="ig">
    <a href="http://instagram.com">Instagram</a>
  </li>
  <li class="rss">
    <a href="http://ts1.lifechurch.loc/">RSS</a>
  </li>
</ul>
