<?php
/**
 * Maintained by Oleksii Bondarenko
 * alex.mazaltov@gmail.com
 * 19/06/2015
 */

/**
 * @file
 * Administration page callbacks for moc_ts_sharing module
 */

/** ---------------------------------- * */
/** ------- CALLBACK FUNCTIONS ------- * */
/** ---------------------------------- * */

/**
 * Form builder. Configure TEAM SITES SHARING settings
 *
 * @see system_settings_form()
 */
function moc_ts_sharing_settings($form, &$form_state) {
    $form = array();
    $form['moc_ts_sharing_api_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Sharing settings'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );

    $form['moc_ts_sharing_api_settings']['moc_ts_sharing_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable pull from Staff portal'),
      '#options' => array(1=>1,0=>0),
      '#default_value' => variable_get('moc_ts_sharing_enabled', 0),
    );
    $form['moc_ts_sharing_api_settings']['moc_ts_sharing_resource_url'] = array(
      '#title' => t('Staffportal API-endpoint url'),
      '#type' => 'textfield',
      '#default_value' => variable_get('moc_ts_sharing_resource_url', 'https://hurricanegold.lifechurch.tv/sharing/api'),
      '#description' => t('prefix "http://"" or "https://"" is require'),
      '#require' => TRUE,
    );

    return system_settings_form($form);
}
