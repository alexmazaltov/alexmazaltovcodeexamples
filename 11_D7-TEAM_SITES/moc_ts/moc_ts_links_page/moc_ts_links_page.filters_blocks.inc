<?php

/**
 * Function for generating cached block with categories flters for blog page on each team sites
 */
function _ts_links_page_block_categories_content(){
  //@TODO:ADD conditionaly functionality to show only tags that is according only to related "links Page"
  $categories = array();
  $voc = taxonomy_vocabulary_machine_name_load(TEAM_SITES_TAXONOMY_VOC_LINK_PARENT_CATEGORIES);
  if(empty($voc)) {
    return;
  }
  $terms = taxonomy_get_tree($voc->vid, 0, 1);

  foreach ($terms as $term) {
    $count = _ts_links_page_categories_count_nodes($term->tid, FALSE);
    //Add category tag in to the block if count of nodes is not 0
    if(($count*1) > 0) {
      $categories[] = array('term' => $term, 'count' => $count);
    }
  }

  $categories_list = _ts_links_page_categories_items(array('categories' => $categories));
  $categories_list_murkup = theme('moc_ts_links_page_categories_list',array('items'=>$categories_list));

  return $categories_list_murkup;
}


/**
 * Get count of Article node (blog article) for specific category term
 * @param $tid   Blog category term tid
 * @return mixed
 */
function _ts_links_page_categories_count_nodes($tid) {
  $target_nid=NULL;
  $target_node = menu_get_object();
  if(!empty($targed_node)){
    $target_nid=$target_node->nid;
  }
  $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node');
    $query->entityCondition('bundle', TEAM_SITES_CT_LINKS_FOR_PAGE);
    $query->propertyCondition('status', NODE_PUBLISHED);
    $query->fieldCondition('field_link_parent_cathegorie', 'tid', $tid, '=');
    //Condition on field according referenced filed link_page_type
    if($target_nid){
      //Count only amount of nodes referencing to the current links page
      $query->fieldCondition('field_page_type', 'target_id', $target_nid, '=');
    }
    $query->count();

  return $query->execute();
}


/**
 * Theme function for render tutorials category list.
 */
function _ts_links_page_categories_items($vars) {
  $query = array();
  $output = '';
  $items = array();
  $request_path = request_path();
  if ($vars['categories']) {
    foreach ($vars['categories'] as $category) {
      $name = format_string('@category <span>(@count)</span>', array(
        '@category' => $category['term']->name,
        '@count' => $category['count']
      ));
      $query['field_link_parent_cathegorie_tid'] = $category['term']->tid;
      if(isset($_GET['title']) && !empty($_GET['title'])){
        $query['title'] = $_GET['title'];
      }
      //Expand existing categories in query according to selected tags on that page
      //$query = _ts_links_page_block_categories_content_build_query($query);
      $is_active = (in_array($category['term']->tid, array(@$_GET['field_link_parent_cathegorie_tid'])))?'active':'';
      $items[] = l($name, "/$request_path", array(
        'html' => TRUE,
        'attributes' => array('data-tid' => $category['term']->tid, 'class' => array('blog-category-link '.$is_active)),
        'query' => $query,
      ));
    }
    //$output = theme('item_list', $items);
  }

  return $items;
}

