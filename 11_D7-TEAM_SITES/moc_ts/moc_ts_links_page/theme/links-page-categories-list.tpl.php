<?php
/**
 * @file Template for render block social link in footer
 * Available variables:
 * - $items
 */
?>
<ul>
  <?php foreach ($items as $item): ?>
    <li><?php print $item; ?></li>
  <?php endforeach; ?>
</ul>
