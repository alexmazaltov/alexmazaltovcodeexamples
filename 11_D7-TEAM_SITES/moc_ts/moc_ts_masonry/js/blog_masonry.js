
(function ($) {

  Drupal.behaviors.blogMasonry = {
    attach: function() {

      //Read more: http://osvaldas.info/responsive-jquery-masonry-or-pinterest-style-layout
      var masonryView = $(".view-id-blog > .view-content");

      masonryView.imagesLoaded(function () {
        masonryView.masonry({
          itemSelector: '.masonry-item'
        });
      });


    }
  };

})(jQuery);
