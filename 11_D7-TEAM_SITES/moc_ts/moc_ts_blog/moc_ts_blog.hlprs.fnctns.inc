<?php

/**
 * Helpers functions for this module
 */

/**
 * Terurn Category tag name according to node in which is it
 * @param  [type] $nid [description]
 * @return [type]      [description]
 */
function _ts_blog_get_cathegory_name_by_tid($nid = NULL){
  if(isset($nid) && is_numeric($nid)){

    $q = db_select('taxonomy_term_data', 'ttd');
    $q->fields('ttd', array('name', 'tid'));
    $q->innerJoin('taxonomy_index', 'ti', 'ti.tid = ttd.tid');
    $q->condition('ttd.vid', 2);
    $q->condition('ti.nid', $nid);
    $result = $q->execute()->fetchAll();
    //Check if in node there is one tag and get result[0]
    if (!empty($result)){
      $category = $result[0];
      $output = l( ((empty($category))?'':$category->name), '/blog', array('query' => array('field_cathegorie_tid' => $category->tid)));
      return $output;
    }
  }
  return '';
}

/**
 * Returns link to user profile on staffportal site
 * @param  int $uid
 *         user id who created node article
 * @return mixed
 *         Custom markup with external link to user profile on staffportal
 */
function _ts_get_custom_link_to_user_profile_by_uid($uid){
  $q = db_select('users', 'u');
  $q->leftJoin('field_data_field_display_name', 'fdn', 'u.uid = fdn.entity_id');
  $q->fields('fdn', array('field_display_name_value'));
  $q->fields('u', array('name'));
  $q->where(" ( (u.uid = :uid) ) ",
              array(
                ':uid' => $uid
              )
  );
  $users = $q->execute()->fetchAll();
  $user = $users[0];

  //user_name should work for LDAP users and for standart drupal users entity
  $user_name = (empty($user->field_display_name_value)) ? $user->name : $user->field_display_name_value;
  $urlAlias = drupal_lookup_path('alias',"user/".$uid);

  $output = l($user_name, '//staffportal.lifechurch.tv/' . $urlAlias, array('attributes'=>array('target'=>'_blank')));
  return $output;
}


/**
 * Helper function to enable blog views and menu item
 */
function _moc_ts_blog_enable_blog() {
  $viewnames = array(
    'blog',
  );
  $views_status = variable_get('views_defaults', array());
  foreach ($viewnames as $key => $viewname) {
    $views_status[$viewname] = TRUE;
  }
  variable_set('views_defaults', $views_status);
  if (function_exists('views_invalidate_cache')) {
    views_invalidate_cache();
  }
}

/**
 * Helper function to disable blog views and menu item
 */
function _moc_ts_blog_disable_blog() {
  $viewnames = array(
    'blog',
  );
  $views_status = variable_get('views_defaults', array());
  foreach ($viewnames as $key => $viewname) {
    $views_status[$viewname] = FALSE;
  }
  variable_set('views_defaults', $views_status);
  if (function_exists('views_invalidate_cache')) {
    views_invalidate_cache();
  }
}
