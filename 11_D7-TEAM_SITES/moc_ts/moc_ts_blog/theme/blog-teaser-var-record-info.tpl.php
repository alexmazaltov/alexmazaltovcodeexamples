<?php
/**
 * @file Template for render block social link in footer
 * Available variables:
 * - $date
 * - $commments
 * - $category
 * - $created_by
 */
?>
<span class="date">  <i><?php print $date; ?></i></span>/
<span class="comments"> <?php print $comments; ?></span>/
<span class="category"><?php print $category; ?></span>
<span class="author"><i>By </i><?php print $created_by; ?>
</span>

