<?php

/**
 * Get count of Article node (blog article) for specific category term
 *
 * @param $tid
 *   Blog category term tid
 *
 * @return mixed
 */
function _ts_blog_categories_count_nodes($tid) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')->entityCondition('bundle', TEAM_SITES_CT_BLOG_POST)
    ->propertyCondition('status', NODE_PUBLISHED)->fieldCondition('field_cathegorie', 'tid', $tid, '=')
    ->count();

  return $query->execute();
}

/**
 * Function for generating cached block with categories flters for blog page on each team sites
 */
function _ts_blog_block_categories_content(){

  $cache = team_sites_common_get_cache(TEAM_SITES_BLOG_CATEGORIES_BLOCK_CID);
  if (FALSE && $cache) {
    return $cache;
  }

  $categories = array();
  $voc = taxonomy_vocabulary_machine_name_load(TEAM_SITES_TAXONOMY_VOC_BLOG_CATEGORIES);
  $terms = taxonomy_get_tree($voc->vid, 0, 1);

  foreach ($terms as $term) {
    $count = _ts_blog_categories_count_nodes($term->tid, FALSE);
    //Add category tag in to the block if count of nodes is not 0
    if(($count*1) > 0) {
      $categories[] = array('term' => $term, 'count' => $count);
    }
  }

  $categories_list = _ts_blog_categories_items(array('categories' => $categories));
  $categories_list_murkup = theme('moc_ts_blog_categories_list',array('items'=>$categories_list));
  team_sites_common_set_cache(TEAM_SITES_BLOG_CATEGORIES_BLOCK_CID, $categories_list_murkup);

  return $categories_list_murkup;
}


/**
 * Function for generating cached block with tags fiters for blog page on each team sites
 */
function _ts_blog_block_tags_content(){

//  $cache = team_sites_common_get_cache(TEAM_SITES_BLOG_TAGS_BLOCK_CID);
//  if (FALSE && $cache) {
//    return $cache;
//  }

  $current_cathegorie_tid = @$_GET['field_cathegorie_tid'];
  if(empty($current_cathegorie_tid)){
    $current_cathegorie_tid = 'All';
  }
  if(isset($current_cathegorie_tid) && !empty($current_cathegorie_tid) && $current_cathegorie_tid == 'All'){
    //Load all tids tags from vocabulary tags
    $voc = taxonomy_vocabulary_machine_name_load(TEAM_SITES_TAXONOMY_VOC_BLOG_TAGS);
    $terms = taxonomy_get_tree($voc->vid, 0, 1);

    $tags = _ts_blog_tags_items(array('terms' => $terms));
    $tags_markup = theme('moc_ts_blog_tags_list', array('items'=>$tags));
  }
  elseif(isset($current_cathegorie_tid) && !empty($current_cathegorie_tid)){
    //Custom logic for showing tags for only related choosed category
    $nids = (taxonomy_select_nodes(array($current_cathegorie_tid)));
    if(!empty($nids)){
      $terms = array();
      $query = db_select('taxonomy_index', 'ti');
      $query->distinct();
      $query->innerJoin('taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $query->fields('ti', array('tid'));
      $query->condition('ti.nid', $nids, 'IN' );
      $query->condition('ttd.vid', '1' );
      $tids = $query->execute()->fetchCol();
        foreach ($tids as $tid) {
          $terms[] = taxonomy_term_load($tid);
        }


      $tags = _ts_blog_tags_items(array('terms' => $terms));
      $tags_markup = theme('moc_ts_blog_tags_list', array('items'=>$tags));
    }
  }

//  team_sites_common_set_cache(TEAM_SITES_BLOG_TAGS_BLOCK_CID, $tags_markup);

  return $tags_markup;
}

/**
 * Build extra get parameters according to $_GET variables
 */
function _ts_blog_block_categories_content_build_query($query){
  // $current_tags_tid = @$_GET['field_tags_tid'];

  // if(isset($current_tags_tid) && !empty($current_tags_tid)){
  //   $query_tmp = array();
  //   $query_tmp['field_tags_tid'] = $current_tags_tid;
  //   $query = array_merge($query, $query_tmp);
  //   return $query;
  // }

  return $query;

}

/**
 * Build extra get parameters according to $_GET variables custom logic for build diff and merge tags
 */
function _ts_blog_block_tags_content_build_query($query){

   $current_tags_tid = @$_GET['field_tags_tid'];
  if(isset($current_tags_tid) && !empty($current_tags_tid)){
   $query_tmp['field_tags_tid'] = array_diff($query['field_tags_tid'], $current_tags_tid);
   if(empty($query_tmp['field_tags_tid'])){
    $query['field_tags_tid'] = array_diff($current_tags_tid, $query['field_tags_tid']);
   }
   else{
    $query['field_tags_tid'] = array_merge($current_tags_tid, $query['field_tags_tid']);
   }
  }

  $current_cathegorie_tid = @$_GET['field_cathegorie_tid'];
  if(isset($current_cathegorie_tid) && !empty($current_cathegorie_tid)){
    $query_tmp = array();
    $query_tmp['field_cathegorie_tid'] = $current_cathegorie_tid;
    $query = array_merge($query, $query_tmp);
  }
  return $query;
}

