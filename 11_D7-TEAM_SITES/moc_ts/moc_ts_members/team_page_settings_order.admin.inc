<?php

/**
 * @file
 * Administration page callbacks for the Team Page configuration module
 */

/**
 * Form builder. Configure Team members order
 *
 */
function moc_ts_members_order_form($form, &$form_state) {


  $form = array();

  $rows = array();
  $row_elements = array();
  // Put it into a fieldset for no reason.
  $form['data_table'] = array(
    '#type' => 'fieldset',
    '#title' => t('Data table'),
  );
  // Collect your data.
  $data = array(
    'some-id-1' => array(
      'enable' => TRUE,
      'default' => TRUE,
      'weight' => 1,
      'name' => 'some text from config',
      'description' => 'some description text',
    ),
    'some-id-2' => array(
      'enable' => TRUE,
      'default' => FALSE,
      'weight' => 3,
      'name' => 'some more text from config',
      'description' => 'more description text',
    ),
    'some-id-3' => array(
      'enable' => FALSE,
      'default' => TRUE,
      'weight' => 2,
      'name' => 'and even more text from config',
      'description' => 'mooore description text',
    ),
  );
  // Sort the rows.
  uasort($data, '_moc_ts_members_order_form_weight_arraysort');
  // Build the rows.
  foreach ($data as $id => $entry) {
    // Build the table rows.
    $rows[$id] = array(
      'data' => array(
        // Cell for the cross drag&drop element.
        array('class' => array('entry-cross')),
        // Weight item for the tabledrag.
        array('data' => array(
          '#type' => 'weight',
          '#title' => t('Weight'),
          '#title_display' => 'invisible',
          '#default_value' => $entry['weight'],
          '#parents' => array('data_table', $id, 'weight'),
          '#attributes' => array(
            'class' => array('entry-order-weight'),
          ),
        )),
        // Enabled checkbox.
        array('data' => array(
          '#type' => 'checkbox',
          '#title' => t('Enable'),
          '#title_display' => 'invisible',
          '#default_value' => $entry['enable'],
          '#parents' => array('data_table', $id, 'enabled'),
        )),
        // Default checkbox.
        array('data' => array(
          '#type' => 'checkbox',
          '#title' => t('Default'),
          '#title_display' => 'invisible',
          '#default_value' => $entry['default'],
          '#parents' => array('data_table', $id, 'default'),
        )),
        // Name textfield.
        array('data' => array(
          '#type' => 'textfield',
          '#size' => 10,
          '#title' => t('Name'),
          '#title_display' => 'invisible',
          '#default_value' => $entry['name'],
          '#parents' => array('data_table', $id, 'name'),
        )),
        // Entry description.
        check_plain($entry['description']),
        // Operations.
        array('data' => array(
          '#theme' => 'link',
          '#text' => t('Edit settings'),
          '#path' => 'tabledrag/' . $id . '/edit',
          '#options' => array('attributes' => array(), 'html' => FALSE),
        )),
        array('data' => array(
          '#theme' => 'link',
          '#text' => t('Delete entry'),
          '#path' => 'tabledrag/' . $id . '/delete',
          '#options' => array('attributes' => array(), 'html' => FALSE),
        )),
      ),
      'class' => array('draggable'),
    );
    // Build rows of the form elements in the table.
    $row_elements[$id] = array(
      'weight' => &$rows[$id]['data'][1]['data'],
      'enabled' => &$rows[$id]['data'][2]['data'],
      'default' => &$rows[$id]['data'][3]['data'],
      'name' => &$rows[$id]['data'][4]['data'],
    );
  }
  // Add the table to the form.
  $form['data_table']['table'] = array(
    '#theme' => 'table',
    // The row form elements need to be processed and build,
    // therefore pass them as element children.
    'elements' => $row_elements,
    '#header' => array(
      // We need two empty columns for the weigth field and the cross.
      array('data' => NULL, 'colspan' => 2),
      t('Enabled'),
      t('Default'),
      t('Name'),
      t('Description'),
      array('data' => t('Operations'), 'colspan' => 2),
    ),
    '#rows' => $rows,
    '#empty' => t('There are no entries available.'),
    '#attributes' => array('id' => 'entry-order'),
  );
  drupal_add_tabledrag('entry-order', 'order', 'sibling', 'entry-order-weight');
  return $form;
}
/**
 * Helper function for sorting entry weights.
 */
function _moc_ts_members_order_form_weight_arraysort($a, $b) {
  if (isset($a['weight']) && isset($b['weight'])) {
    return $a['weight'] < $b['weight'] ? -1 : 1;
  }
  return 0;
}
