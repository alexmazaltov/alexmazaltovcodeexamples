<?php

/**
 * Function for generating cached block with teams flters for blog page on each team sites
 */
function _ts_members_block_teams_content(){

  $teams = array();
  $voc = taxonomy_vocabulary_machine_name_load(TEAM_SITES_TAXONOMY_VOC_MEMBERS_TEAMS);
  $terms = taxonomy_get_tree($voc->vid, 0, 1);

  $count_teams_non_empty = 0;
  foreach ($terms as $term) {
    if( $count = _ts_members_teams_count_members($term->tid, FALSE)){
      $count_teams_non_empty++;
      $teams[] = array('term' => $term, 'count' => $count);
    }
  }

  if($count_teams_non_empty >= 1){
    $teams_list = _ts_members_team_items(array('teams' => $teams));
    $teams_list_murkup = theme('moc_ts_members_teams_list',array('items'=>$teams_list));

    return $teams_list_murkup;
  }
  return '';
}


/**
 * Get count of node (member) for specific team term
 *
 * @param $tid
 *   Member team term tid
 *
 * @return mixed
 */
function _ts_members_teams_count_members($tid = null) {

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node');
    $query->entityCondition('bundle', TEAM_SITES_CT_MEMBER);
    $query->propertyCondition('status', NODE_PUBLISHED);
    if($tid){
      $query->fieldCondition('field_team', 'tid', $tid, '=');
    }
    $query->count();

    return $query->execute();

}

/**
 * Theme function for render members team list according to filter in path.
 */
function _ts_members_team_items($vars) {
  $query = array();
  $output = '';
  $items = array();
  if ($vars['teams']) {
    foreach ($vars['teams'] as $team) {
      $name = format_string('@team <span>(@count)</span>', array(
        '@team' => $team['term']->name,
        '@count' => $team['count']
      ));
      $query['field_team_tid[]'] = $team['term']->tid;

      $is_active = ( !empty(@$_GET['field_team_tid']) && (in_array($team['term']->tid, @$_GET['field_team_tid'])))?'active':'';
      $items[] = array(
        'markup' => l(
            $name, '/team', array(
              'html' => TRUE,
              'attributes' => array('data-tid' => $team['term']->tid, 'class' => array('memebers-team-link '.$is_active)),
              'query' => $query,
            )),
        'is_active' => $is_active,
      );
    }
    //$output = theme('item_list', $items);
  }

  return $items;
}
