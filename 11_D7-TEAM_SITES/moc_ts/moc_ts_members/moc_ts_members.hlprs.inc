<?php

/**
 * Helpers functions
 */


function _ldap_get_user_attr_by_email($user_email = NULL){

  $sid = 'lifechurch_ldap';

  ldap_servers_module_load_include('inc', 'ldap_servers', 'ldap_servers.functions');
  $ldap_server = ldap_servers_get_servers($sid, 'all', TRUE);

  $user_data = $ldap_server->userUserNameToExistingLdapEntry($user_email);

  return $user_data?$user_data:array('');

}

/**
 * Save or update picture for profile of user from LDAP.
 *
 * @param string $image_str
 *  Binary image
 * @param null|string $user_id
 *  Unique user id
 *
 * @return bool|stdClass
 */
function _moc_ts_members_save_member_picture($image_str, $user_id = NULL) {
  $uri = file_build_uri('staff-profiles');
  // try to create a directory if it no exist
  if (!file_prepare_directory($uri, FILE_CREATE_DIRECTORY)) {
    watchdog('moc_ts_members', "Can't create directory", array(), WATCHDOG_WARNING);

    return FALSE;
  }
  if ($user_id) {
    // build real path to file
    $path = file_destination(drupal_realpath($uri . '/' . $user_id . '.png'), FILE_EXISTS_REPLACE);
    if (file_exists($path)) {
      $stat = stat($path);
      // get exist image
      if (($stat['ctime'] + 86400) > time()) { // todo tmp solution, save image 1 day
        return _moc_ts_members_build_get_image_data_by_path($path);
      }
    }
  }
  else {
    $path = file_destination(drupal_realpath($uri . '/no_id.png'), FILE_EXISTS_RENAME);
  }
  // create image file
  $im = imagecreatefromstring($image_str);
  if ($im !== FALSE) {
    // save image to png
    if (imagepng($im, $path)) {
      if (file_exists($path)) {
        imagedestroy($im);

        return _moc_ts_members_build_get_image_data_by_path($path);
      }
    }
  }

  return FALSE;
}

/**
 * Get image info about image stored in the drupal files directory and in file managed.
 *
 * @param string $path
 *  Full system path
 *
 * @return stdClass
 */
function _moc_ts_members_build_get_image_data_by_path($path) {
  $part_url = str_replace(DRUPAL_ROOT . '/', '', $path);
  $parts_path = pathinfo($path);
  $image_uri = file_build_uri('staff-profiles/' . $parts_path['basename']);
  $image_ = file_get_contents($path);
  $file = file_save_data($image_, $image_uri, FILE_EXISTS_REPLACE);
  return $file;

}

/**
 * Helper function
 * Add role "Super Admin" to user by user's email
 */
function _moc_ts_members_add_role_by_email($email){
    //dsm('_________________ADD_______________________');
    $account = user_load_by_mail($email);
    $role = user_role_load_by_name('Super admin');
    user_save($account, array('roles' => $account->roles + array($role->rid => $role->name)));
}

/**
 * Helper function
 * Delete role "Super admin" from user by user's email
 */
function _moc_ts_members_delete_role_by_email($email){
    //dsm('_________________DELET_______________________');
    $account = user_load_by_mail($email);
    $role = user_role_load_by_name('Super admin');
    user_save($account, array('roles' => array_filter($account->roles, function($item) use ($role) {
        return $item != $role->name;
    })));
}
