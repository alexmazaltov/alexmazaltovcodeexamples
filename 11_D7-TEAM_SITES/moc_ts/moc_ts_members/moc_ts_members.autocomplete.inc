<?php

/** ---------------------------------- **/
/** -------- HELPER FUNCTIONS -------- **/
/** ---------------------------------- **/

/**
 * Helper function to generate autocomplete for members to choose which of them will add to current team site
 * $string - string
 */

function _moc_ts_members_autocomplete($string){

  $matches = array();


  $filter = '(&';
  $filter .= '(&(objectClass=user)(objectCategory=person))';
  $filter .= "(mail=$string*)";
  $filter .= ')';

  $results = ___ldap_get_user_attr_by_email($filter);
  if($results['count'] == 0){
    return '';
  }

  foreach ($results as $key => $value) {
    if(is_numeric($key)){
      $matches[$value['mail'][0]] = $value['cn'][0];
    }
  }

  return $matches;
}


/**
 * Custom query to get Ldap objects according to filter string
 */
function ___ldap_get_user_attr_by_email($filter){

  $sid = 'lifechurch_ldap';

  ldap_servers_module_load_include('inc', 'ldap_servers', 'ldap_servers.functions');
  $ldap_server = ldap_servers_get_servers($sid, 'all', TRUE);

  $results = $ldap_server->searchAllBaseDns($filter);
  return $results;

}
