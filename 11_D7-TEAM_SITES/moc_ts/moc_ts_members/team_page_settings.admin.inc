<?php

/**
 * @file
 * Administration page callbacks for the Team Page configuration module
 */

/**
 * Form builder. Configure Team Page settings
 *
 */
function team_page_settings($form, &$form_state) {

  $form_state['storage']['new_members'] = isset($form_state['storage']['new_members']) ? $form_state['storage']['new_members'] : 0;

  $form['new_members'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#prefix' => '<div id="new_members">',
    '#suffix' => '</div>',
  );
  if ($form_state['storage']['new_members']) {
    for ($i = 1; $i <= $form_state['storage']['new_members']; $i++) {
      $form['new_members'][$i] = array(
        '#type' => 'fieldset',
        '#tree' => TRUE,
      );
      $form['new_members'][$i]['mail'] = array(
        '#title' => t('Mail'),
        '#type' => 'textfield',
        '#autocomplete_path' => 'moc_ts_members/autocomplete',
      );
    }
  }
  $form['add_member'] = array(
    '#type' => 'button',
    '#value' => t('Add A Member'),
    '#href' => '',
    '#ajax' => array(
      'callback' => 'custom_registration_ajax_add_member',
      'wrapper' => 'new_members',
    ),
  );

  $form['added_members_views'] = array(
    '#type' => 'markup',
    '#markup' => views_embed_view('members_content', 'page_1'),
  );

  $form_state['storage']['new_members']++;

  $form['submit'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
  );

	return $form;
}

/**
 * Ajax function for getting multiple members storing
 */
function custom_registration_ajax_add_member($form, $form_state) {
	return $form['new_members'];
}


/**
 * Custom validate function for the team_page_settings form
 */
function team_page_settings_validate($form, &$form_state) {
	//TODO: validate logic to be right in separate validation function that is not invoked an ajax
  //$v = &$form_state['values'];
	//dsm($v);
	//form_set_error('this_ts_members', 'Test mode now, is fail error');
  // form validate
  //form_set_error("new_members[0][mail]", t('You must enter a valid e-mail address.'));
  // if (!valid_email_address($form_state['values']['mail'])) {
  // }
}

/**
 * Custom submit function for the team_page_settings form
 */
function team_page_settings_submit($form, &$form_state) {

	$v = &$form_state['values'];
    //If new members was added - to handle its
    if(isset($v['new_members'])){
        foreach ($v['new_members'] as $key => $value) {
            if(isset($value['mail']) && !empty($value['mail'])){
                if(valid_email_address($value['mail'])){
                  //Check if this member was added before
                  if(!_moc_ts_members_member_exist($value['mail'])){
                      //See if inputed email is in Database for new member
                      $string = $value['mail'];
                      $filter = '(&';
                      $filter .= '(&(objectClass=user)(objectCategory=person))';
                      $filter .= "(mail=$string*)";
                      $filter .= ')';
                      module_load_include('inc', 'moc_ts_members', 'moc_ts_members.autocomplete');
                      $results = ___ldap_get_user_attr_by_email($filter);
                      if($results['count'] == 0){
                          //TODO: validate logic to be right in separate validation function that is not invoked an ajax
                          form_set_error("mail", "There isn't user in AD with email=>>>> $string");
                      }
                      else{
                        _team_page_settings_add_memeber(_ldap_get_user_attr_by_email($value['mail']));
                      }
                  }
                  else{
                    form_set_error("mail", "Member already exist =>>>> {$value['mail']}");
                  }
               }
               else {
                 form_set_error("mail", "Wrong email =>>>> {$value['mail']}");
               }
            }
        }
    }

}

/**
 * Function that write to DB table mot_ts_member new member eamil
 */
function _team_page_settings_add_memeber($ldap_user) {

	$node = new stdClass();
	$node->type = 'member';
	node_object_prepare($node);

	$node->title    = $ldap_user['attr']['displayname'][0];
	$node->language = LANGUAGE_NONE;
	$node->field_email[$node->language][0]['email'] = $ldap_user['mail'];
	$node->field_member_position[$node->language][0]['value'] = $ldap_user['attr']['description'][0]?$ldap_user['attr']['description'][0]:'';
	$node->field_member_phone[$node->language][0]['value'] = isset($ldap_user['attr']['mobile'][0])?$ldap_user['attr']['mobile'][0]:'';
	$node->field_office_phone[$node->language][0]['value'] = isset($ldap_user['attr']['telephonenumber'][0])?$ldap_user['attr']['telephonenumber'][0]:'';

	if (isset($ldap_user['attr']['thumbnailphoto']) && !empty($ldap_user['attr']['thumbnailphoto'][0])) {
		$image = _moc_ts_members_save_member_picture($ldap_user['attr']['thumbnailphoto'][0], $ldap_user['attr']['samaccountname'][0]);
		if ($image) {
			$node->field_member_image[$node->language][0] = (array)$image;
		}
	}
	node_save($node);

	$user = _moc_ts_members_get_create_user_by_user_mail($ldap_user);

	$values = array(
		'name_email' => $ldap_user['mail'],
		'nid' => $node->nid,
		'uid' => isset($user->uid)?$user->uid:'',
		'timestamp' => REQUEST_TIME,
	);
	try {
			drupal_write_record('moc_ts_members', $values);
	} catch (Exception $e) {
			watchdog('DB writing error moc_ts_members', t('Write to DB new memeber failed'), array(), WATCHDOG_WARNING);
	};
}


/**
 * Create new drupal user with "content admin" role for this site.
 *
 * @param $ldap_user
 *   User Attrs from AD of new member
 *
 * @return bool|stdClass
 *   Drupal user object
 */
function _moc_ts_members_get_create_user_by_user_mail($ldap_user) {
	global $language;
	$member_email = $ldap_user['mail'];
	$existing_user = user_load_by_name($member_email);
	if($existing_user){
		return $existing_user;
	}

	 $account = array(
	 	'name' => $member_email,
	 	'mail' => $member_email,
	 	'pass' => NULL,
	 	'language' => $language->language,
	 	'status' => TRUE,
	 );
	 $account['field_display_name'][LANGUAGE_NONE][0]['value'] = $ldap_user['attr']['displayname'][0];
	 $account['field_samaccountname'][LANGUAGE_NONE][0]['value'] = check_plain($ldap_user['attr']['samaccountname'][0]);

	 //$role1 = user_role_load_by_name('General Manager');
	 $account['roles'] = array(
	 	DRUPAL_AUTHENTICATED_RID => 'authenticated user',
        //$role1->rid => 'General Manager',
	 );
     $account_obj = (object)$account;
	 $new_user = user_save($account_obj);

	 // Send notify mail.
	 //_user_mail_notify('register_no_approval_required', $new_user);

	return FALSE;
}

/**
 * Check function try to found in custom table already added before a member by his email
 * @param  string $mail Email of user
 * @return bool|stdClass
 *  Fetched Field from DB
 */
function _moc_ts_members_member_exist($mail){
	$result = db_select('moc_ts_members', 'mtm')
		->fields('mtm', array('name_email'))
		->condition('name_email', $mail)
		->execute()->fetchField();
	return $result;
}

/**
 * Returns all members that was added before
 * @return array(Obj(mail, nid, uid))
 */
function _moc_ts_members_added(){

		$q = db_select('moc_ts_members', 'mtm');
		$q->fields('mtm', array('name_email', 'nid', 'uid'));
		$q->orderBy('nid');
		$result = $q->execute()->fetchAllAssoc('nid');

	return $result;

}
