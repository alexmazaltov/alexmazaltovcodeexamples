<?php
/**
 * @file Template for render block teams filter in block for view page team
 * Available variables:
 * - $items
 */
?>
    <ul class="menu">
      <?php //Add Item to list in menu wit all members
       $is_active = (isset($_GET['field_team_tid'])?'':'active');
      ?>
      <li <?php print (($is_active)?'class="active"':''); ?>>
        <a href="/team" class="memebers-team-link <?php print $is_active?>">All members
          <span>(<?php print _ts_members_teams_count_members()?>)</span>
        </a>
      </li>
      <?php foreach ($items as $item): ?>
        <li <?php print ($item['is_active'])?'class="'.$item['is_active'].'"':''; ?>><?php print $item['markup']; ?></li>
      <?php endforeach; ?>
    </ul>
